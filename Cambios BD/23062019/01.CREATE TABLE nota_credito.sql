
/****** Object:  Table [dbo].[nota_credito]    Script Date: 23/06/2019 17:32:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[nota_credito](
	[idnotacredito] [int] IDENTITY(1,1) NOT NULL,
	[idcliente] [int] NULL,
	[idusuario] [int] NULL,
	[idventa] [int] NULL,
	[fecha_registro] [datetime] NULL,
	[tipo_comprobante] [varchar](10) NULL,
	[serie_comprobante] [varchar](50) NULL,
	[numero_comprobante] [varchar](50) NULL,
	[tipo_comprobante_relacionado] [varchar](10) NULL,
	[serie_comprobante_relacionado] [varchar](50) NULL,
	[numero_comprobante_relacionado] [varchar](50) NULL,
	[tipo_nota] [varchar](10) NULL,
	[tipo_nota_descripcion] [varchar](100) NULL,
	[cod_estado_envio_sunat] [int] NULL,
	[mensaje_envio_sunat] [varchar](500) NULL,
	[estado_envio_sunat] [int] NULL,
	[archivo_xml] [varchar](250) NULL,
	[archivo_cdr] [varchar](250) NULL,
	[archivo_pdf] [varchar](250) NULL,
	[fecha_envio_sunat] [datetime] NULL,
 CONSTRAINT [PK_nota_credito] PRIMARY KEY CLUSTERED 
(
	[idnotacredito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


