/****** Object:  Table [dbo].[nota_debito_detalle]    Script Date: 23/06/2019 17:34:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[nota_debito_detalle](
	[iddetalle_nota_debito] [int] IDENTITY(1,1) NOT NULL,
	[idnotadebito] [int] NOT NULL,
	[idproducto] [int] NULL,
	[desproducto] [varchar](150) NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](18, 2) NOT NULL,
	[undm] [varchar](50) NULL,
	[iddetalle_ingreso] [int] NULL,
	[cundm] [int] NULL,
	[sundm] [varchar](50) NULL,
	[precio_compra] [decimal](18, 2) NULL,
 CONSTRAINT [PK_nota_debito_detalle] PRIMARY KEY CLUSTERED 
(
	[iddetalle_nota_debito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


