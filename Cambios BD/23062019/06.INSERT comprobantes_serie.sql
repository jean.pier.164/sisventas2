
SET IDENTITY_INSERT [dbo].[comprobantes_serie] ON 

INSERT [dbo].[comprobantes_serie] ( [cod_sunat], [descripcion], [serie], [numeracion], [nrocaja], [cod_sunat_tipo_cp]) VALUES (N'07', N'NOTA DE CREDITO FACTURA', N'FC', 1, N'01', N'01')
INSERT [dbo].[comprobantes_serie] ( [cod_sunat], [descripcion], [serie], [numeracion], [nrocaja], [cod_sunat_tipo_cp]) VALUES (N'07', N'NOTA DE CREDITO BOLETA', N'BC', 1, N'01', N'03')
INSERT [dbo].[comprobantes_serie] ( [cod_sunat], [descripcion], [serie], [numeracion], [nrocaja], [cod_sunat_tipo_cp]) VALUES (N'08', N'NOTA DE DEBITO FACTURA', N'FD', 1, N'01', N'01')
INSERT [dbo].[comprobantes_serie] ( [cod_sunat], [descripcion], [serie], [numeracion], [nrocaja], [cod_sunat_tipo_cp]) VALUES (N'08', N'NOTA DE DEBITO BOLETA', N'FB', 1, N'01', N'03')
SET IDENTITY_INSERT [dbo].[comprobantes_serie] OFF
