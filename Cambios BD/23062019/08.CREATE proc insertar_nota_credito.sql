CREATE proc [dbo].[insertar_nota_credito]
@idcliente as integer,
@idusuario as integer,
@idventa as integer,
@fecha_venta as datetime,
@tipo_documento as varchar(50),
@serie_documento as varchar(50),
@num_documento as varchar(50),
@tipo_documento_rel as varchar(50),
@serie_documento_rel as varchar(50),
@num_documento_rel as varchar(50),
@tipo_nota as varchar(10),
@tipo_nota_descripcion as varchar(100),
@cod_estado_envio_sunat as integer,
@mensaje_envio_sunat as varchar(500),
@estado_envio_sunat as integer,
@archivo_xml as varchar(250),
@archivo_cdr as varchar(250),
@archivo_pdf as varchar(250)
as 

DECLARE @CORRELATIVO INT

set nocount on

INSERT INTO [dbo].[nota_credito]
           ([idcliente]           ,[idusuario]           ,[idventa]           ,[fecha_registro]
           ,[tipo_comprobante]           ,[serie_comprobante]           ,[numero_comprobante]
           ,[tipo_comprobante_relacionado]           ,[serie_comprobante_relacionado]           ,[numero_comprobante_relacionado]
           ,[tipo_nota]           ,[tipo_nota_descripcion]		   ,[cod_estado_envio_sunat]
           ,[mensaje_envio_sunat]           ,[estado_envio_sunat]           ,[archivo_xml]
           ,[archivo_cdr]           ,[archivo_pdf]           ,[fecha_envio_sunat])
     VALUES
           (@idcliente           ,@idusuario           ,@idventa           ,@fecha_venta
           ,@tipo_documento           ,@serie_documento           ,@num_documento
           ,@tipo_documento_rel           ,@serie_documento_rel           ,@num_documento_rel
           ,@tipo_nota           ,@tipo_nota_descripcion           ,@cod_estado_envio_sunat
           ,@mensaje_envio_sunat           ,@estado_envio_sunat           ,@archivo_xml
           ,@archivo_cdr           ,@archivo_pdf           ,getdate())

		   --select 16
		   SELECT @@IDENTITY
