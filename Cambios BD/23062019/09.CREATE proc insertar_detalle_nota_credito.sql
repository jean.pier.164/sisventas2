CREATE proc [dbo].[insertar_detalle_nota_credito]
@idnotacredito as integer,
@idproducto as integer,
@cantidad as integer,
@precio_unitario as decimal (18,2),
@undm as varchar (50),
@cundm as  decimal (18,1),
@sundm as varchar(50),
@precio_compra as decimal(18,2)

as
insert into nota_credito_detalle(idnotacredito,idproducto,cantidad,precio_unitario,undm,cundm,sundm,precio_compra)
values (@idnotacredito,@idproducto,@cantidad,@precio_unitario,@undm,@cundm,@sundm,@precio_compra)