CREATE procedure [dbo].[generar_comprobantem_notacredito]

@idnotacredito int,
@tiponota as varchar(10)
as

IF @tiponota='NC'
BEGIN
	SELECT        nt.idventa AS venta, dbo.cliente.nombre, dbo.cliente.apellidos, dbo.cliente.dni, nt.fecha_registro, 
	nt.tipo_comprobante,nt.serie_comprobante, nt.numero_comprobante, 
	nt.tipo_comprobante_relacionado,nt.serie_comprobante_relacionado, nt.numero_comprobante_relacionado, 
	nt.tipo_nota,nt.tipo_nota_descripcion,
	dbo.producto.nombre AS Descripcion, 
	dtn.cantidad, dtn.sundm, dtn.undm, dtn.precio_unitario, 
	dtn.cantidad * dtn.precio_unitario AS Total_Parcial, 
	dbo.usuario.login,nt.idnotacredito as idnota, @tiponota as cpNota
	FROM dbo.nota_credito nt 
	left JOIN dbo.nota_credito_detalle dtn ON nt.idnotacredito = dtn.idnotacredito left JOIN
	dbo.producto ON dtn.idproducto = dbo.producto.idproducto INNER JOIN
	dbo.cliente ON nt.idcliente = dbo.cliente.idcliente INNER JOIN
	dbo.usuario ON nt.idusuario = dbo.usuario.idusuario
	where nt.idnotacredito=@idnotacredito
END
IF @tiponota='ND'
BEGIN
	SELECT        nt.idventa AS venta, dbo.cliente.nombre, dbo.cliente.apellidos, dbo.cliente.dni, nt.fecha_registro, 
	nt.tipo_comprobante,nt.serie_comprobante, nt.numero_comprobante, 
	nt.tipo_comprobante_relacionado,nt.serie_comprobante_relacionado, nt.numero_comprobante_relacionado, 
	nt.tipo_nota,nt.tipo_nota_descripcion,
	dtn.desproducto AS Descripcion, 
	dtn.cantidad, dtn.sundm, dtn.undm, dtn.precio_unitario, 
	dtn.cantidad * dtn.precio_unitario AS Total_Parcial, 
	dbo.usuario.login,nt.idnotadebito as idnota, @tiponota as cpNota
	FROM dbo.nota_debito nt 
	left JOIN dbo.nota_debito_detalle dtn ON nt.idnotadebito = dtn.idnotadebito left JOIN
	dbo.producto ON dtn.idproducto = dbo.producto.idproducto INNER JOIN
	dbo.cliente ON nt.idcliente = dbo.cliente.idcliente INNER JOIN
	dbo.usuario ON nt.idusuario = dbo.usuario.idusuario
	where nt.idnotadebito=@idnotacredito
END