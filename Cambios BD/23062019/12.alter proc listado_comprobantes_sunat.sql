alter proc [dbo].[listado_comprobantes_sunat]
@fechaInicio date,
@fechaFin date
as

select a.idventa,c.descripcion, e.login as usuario,a.fecha_venta, a.tipo_comprobante,a.serie_comprobante, a.numero_comprobante, f.descripcion_tipo_doc,d.dni, d.nombre, d.apellidos,
(select sum(b.cantidad*b.precio_unitario) from detalle_venta b where b.idventa = a.idventa) as total,
case when a.flag_enviar_sunat = 0 then 'ENVIO ANULADO' else '' END as descrip_estado_envio,a.motivo_anulacion_envio,
a.cod_estado_envio_sunat,a.mensaje_envio_sunat,a.archivo_pdf,a.archivo_xml,a.archivo_cdr,a.fecha_envio_sunat
from ventas a
inner join comprobantes_serie c on c.cod_sunat = a.tipo_comprobante
inner join cliente d on d.idcliente = a.idcliente
inner join usuario e on e.idusuario = a.idusuario
inner join cliente_tipo_documento f on f.cod_sunat = d.tipo_doc
where cast(a.fecha_venta as date) between @fechaInicio and @fechaFin and 
a.tipo_comprobante is not null

union

select a.idventa,c.descripcion, e.login as usuario,a.fecha_registro as fecha_venta, a.tipo_comprobante,a.serie_comprobante, a.numero_comprobante, f.descripcion_tipo_doc,d.dni, d.nombre, d.apellidos,
(select sum(b.cantidad*b.precio_unitario) from nota_credito_detalle b where b.idnotacredito = a.idnotacredito) as total,
'' as descrip_estado_envio,'' as motivo_anulacion_envio,
a.cod_estado_envio_sunat,a.mensaje_envio_sunat,a.archivo_pdf,a.archivo_xml,a.archivo_cdr,a.fecha_envio_sunat
from nota_credito a
inner join comprobantes_serie c on c.cod_sunat = a.tipo_comprobante and c.cod_sunat_tipo_cp = a.tipo_comprobante_relacionado
inner join cliente d on d.idcliente = a.idcliente
inner join usuario e on e.idusuario = a.idusuario
inner join cliente_tipo_documento f on f.cod_sunat = d.tipo_doc
where cast(a.fecha_registro as date) between @fechaInicio and @fechaFin and 
a.tipo_comprobante is not null

union

select a.idventa,c.descripcion, e.login as usuario,a.fecha_registro as fecha_venta, a.tipo_comprobante,a.serie_comprobante, a.numero_comprobante, f.descripcion_tipo_doc,d.dni, d.nombre, d.apellidos,
(select sum(b.cantidad*b.precio_unitario) from nota_debito_detalle b where b.idnotadebito = a.idnotadebito) as total,
'' as descrip_estado_envio,'' as motivo_anulacion_envio,
a.cod_estado_envio_sunat,a.mensaje_envio_sunat,a.archivo_pdf,a.archivo_xml,a.archivo_cdr,a.fecha_envio_sunat
from nota_debito a
inner join comprobantes_serie c on c.cod_sunat = a.tipo_comprobante and c.cod_sunat_tipo_cp = a.tipo_comprobante_relacionado
inner join cliente d on d.idcliente = a.idcliente
inner join usuario e on e.idusuario = a.idusuario
inner join cliente_tipo_documento f on f.cod_sunat = d.tipo_doc
where cast(a.fecha_registro as date) between @fechaInicio and @fechaFin and 
a.tipo_comprobante is not null


order by fecha_venta desc