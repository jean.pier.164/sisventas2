CREATE proc [dbo].[insertar_detalle_nota_debito]
@idnotadebito as integer,
@idproducto as integer,
@desproducto as varchar(150),
@cantidad as integer,
@precio_unitario as decimal (18,2),
@undm as varchar (50),
@cundm as  decimal (18,1),
@sundm as varchar(50),
@precio_compra as decimal(18,2)

as
insert into nota_debito_detalle(idnotadebito,idproducto,desproducto,cantidad,precio_unitario,undm,cundm,sundm,precio_compra)
values (@idnotadebito,@idproducto,@desproducto,@cantidad,@precio_unitario,@undm,@cundm,@sundm,@precio_compra)