﻿namespace FinalXML
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonNavigator1 = new ComponentFactory.Krypton.Navigator.KryptonNavigator();
            this.kryptonPage1 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel3 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.chkmoneda = new System.Windows.Forms.CheckBox();
            this.kryptonButton2 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonGroupBox1 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.DGPedidos = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.idpedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sigla1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serie1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeracion1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numdocumento1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codcliente1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.femision1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kryptonWrapLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonWrapLabel();
            this.kryptonWrapLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonWrapLabel();
            this.f2 = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.f1 = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.kryptonPage2 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel2 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.cboTipdoc = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel30 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboEmpresaDoc = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.btnSalir = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnGeneraPDF = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnEnvioSunat = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnGeneraXML = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblmensaje = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.dgListadoVentas = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.sigla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeracion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numdocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codcliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.femision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codsunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mensajesunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadosunat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xml = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cdr = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pdf = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Nomxml = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nomcdr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nompdf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFiltrar = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dtpHasta = new System.Windows.Forms.DateTimePicker();
            this.dtpDesde = new System.Windows.Forms.DateTimePicker();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonPage6 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel7 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.cboTipdocAcu = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel31 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel32 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboEmpresaDocAcu = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.btnSalirAcu = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnGeneraPDFAcu = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnEnvioSunatAcu = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnGeneraXMLAcu = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lblmensajeAcu = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.dgListadoVentasAcu = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.siglaA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serieA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeracionA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numdocumentoA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codclienteA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clienteA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.femisionA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codsunatA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mensajesunatA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadosunatA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xmlA = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cdrA = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pdfA = new System.Windows.Forms.DataGridViewButtonColumn();
            this.NomxmlA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomcdrA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NompdfA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFiltrarA = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dtpHastaAcu = new System.Windows.Forms.DateTimePicker();
            this.dtpDesdeAcu = new System.Windows.Forms.DateTimePicker();
            this.kryptonLabel34 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel35 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonPage4 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel5 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonGroupBox3 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnDelBol = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.grvResDetail = new System.Windows.Forms.DataGridView();
            this.numdoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codcli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dircli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecemi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totdoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kryptonGroupBox4 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.dtpFecFin = new System.Windows.Forms.DateTimePicker();
            this.dtpFecIni = new System.Windows.Forms.DateTimePicker();
            this.kryptonLabel10 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboEmpresa = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel8 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btnSendResumen = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnConsultarRes = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonGroupBox2 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.btnConsultarResumen = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonGroupBox5 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.txtNumTicketResumen = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel9 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel7 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtDetailRes = new System.Windows.Forms.TextBox();
            this.kryptonPage7 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel8 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.cboEstadoCD = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel40 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboTipdocCD = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel33 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel36 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboEmpresaDocCD = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonButton8 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton9 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton10 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton11 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonLabel37 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.dgListadoVentasCD = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.siglaD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serieD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeracionD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numdocumentoD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codclienteD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clienteD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccionD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.femisionD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codsunatD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mensajesunatD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadosunatD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xmlD = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cdrD = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pdfD = new System.Windows.Forms.DataGridViewButtonColumn();
            this.NomxmlD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomcdrD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NompdfD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnFiltrarCD = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dtpHastaCD = new System.Windows.Forms.DateTimePicker();
            this.dtpDesdeCD = new System.Windows.Forms.DateTimePicker();
            this.kryptonLabel38 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel39 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonPage3 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel4 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.kryptonButton7 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtNroTicket = new System.Windows.Forms.TextBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonButton5 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dglista2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.motivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GComunicacionBaja = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.cboEmpresaBaj = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel11 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.FechaEmisionDocBaja = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.kryptonButton6 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton3 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton4 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtmotivo = new System.Windows.Forms.TextBox();
            this.txtcorrelativo2 = new System.Windows.Forms.TextBox();
            this.label26 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label27 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label25 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.label31 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.label24 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonPage5 = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.kryptonPanel6 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.btnCancelEditEmi = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnEditarEmisor = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.grvEmisores = new ComponentFactory.Krypton.Toolkit.KryptonDataGridView();
            this.numruc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.razsoc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Agencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estemi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.conemi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codubi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomdep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nompro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomdis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dirfis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sernam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.basdat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabcab = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabdet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estreg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSaveEmisor = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonGroupBox6 = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.lblEstadoEmisor = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboEstadoEmisor = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.txtpasssun = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtusersun = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel29 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel28 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtCodAge = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel27 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtuser = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel26 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboTablaDet = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.cboTablaCab = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.kryptonLabel25 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel24 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtpass = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel23 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.cboBaseDatos = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.btnConectarServer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonLabel22 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtserver = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel21 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtnomdis = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel20 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtnomprv = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel19 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtnomdep = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel18 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel17 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtubigeo = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtdomfis = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.btnConsultarRuc = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonLabel16 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtconemi = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txtestemi = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel15 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel14 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtrazsoc = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel13 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.txtnumruc = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonLabel12 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonNavigator1)).BeginInit();
            this.kryptonNavigator1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage1)).BeginInit();
            this.kryptonPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel3)).BeginInit();
            this.kryptonPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox1.Panel)).BeginInit();
            this.kryptonGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGPedidos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage2)).BeginInit();
            this.kryptonPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).BeginInit();
            this.kryptonPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipdoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListadoVentas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage6)).BeginInit();
            this.kryptonPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel7)).BeginInit();
            this.kryptonPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipdocAcu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaDocAcu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListadoVentasAcu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage4)).BeginInit();
            this.kryptonPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel5)).BeginInit();
            this.kryptonPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox3.Panel)).BeginInit();
            this.kryptonGroupBox3.Panel.SuspendLayout();
            this.kryptonGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvResDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox4.Panel)).BeginInit();
            this.kryptonGroupBox4.Panel.SuspendLayout();
            this.kryptonGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox2.Panel)).BeginInit();
            this.kryptonGroupBox2.Panel.SuspendLayout();
            this.kryptonGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox5.Panel)).BeginInit();
            this.kryptonGroupBox5.Panel.SuspendLayout();
            this.kryptonGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage7)).BeginInit();
            this.kryptonPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel8)).BeginInit();
            this.kryptonPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboEstadoCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipdocCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaDocCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListadoVentasCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage3)).BeginInit();
            this.kryptonPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel4)).BeginInit();
            this.kryptonPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglista2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GComunicacionBaja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GComunicacionBaja.Panel)).BeginInit();
            this.GComunicacionBaja.Panel.SuspendLayout();
            this.GComunicacionBaja.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaBaj)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage5)).BeginInit();
            this.kryptonPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel6)).BeginInit();
            this.kryptonPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvEmisores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox6.Panel)).BeginInit();
            this.kryptonGroupBox6.Panel.SuspendLayout();
            this.kryptonGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboEstadoEmisor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTablaDet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTablaCab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBaseDatos)).BeginInit();
            this.SuspendLayout();
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.kryptonNavigator1);
            this.kryptonPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel1.Location = new System.Drawing.Point(20, 60);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.Size = new System.Drawing.Size(1346, 565);
            this.kryptonPanel1.TabIndex = 2;
            // 
            // kryptonNavigator1
            // 
            this.kryptonNavigator1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonNavigator1.Location = new System.Drawing.Point(0, 0);
            this.kryptonNavigator1.Name = "kryptonNavigator1";
            this.kryptonNavigator1.Pages.AddRange(new ComponentFactory.Krypton.Navigator.KryptonPage[] {
            this.kryptonPage1,
            this.kryptonPage2,
            this.kryptonPage6,
            this.kryptonPage4,
            this.kryptonPage7,
            this.kryptonPage3,
            this.kryptonPage5});
            this.kryptonNavigator1.SelectedIndex = 1;
            this.kryptonNavigator1.Size = new System.Drawing.Size(1346, 565);
            this.kryptonNavigator1.TabIndex = 26;
            this.kryptonNavigator1.Text = "kryptonNavigator1";
            // 
            // kryptonPage1
            // 
            this.kryptonPage1.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage1.Controls.Add(this.kryptonPanel3);
            this.kryptonPage1.Flags = 65534;
            this.kryptonPage1.LastVisibleSet = true;
            this.kryptonPage1.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage1.Name = "kryptonPage1";
            this.kryptonPage1.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage1.Text = "Pedidos";
            this.kryptonPage1.ToolTipTitle = "Page ToolTip";
            this.kryptonPage1.UniqueName = "EF983AE22CE2482FC49E452A2D1F2A98";
            this.kryptonPage1.Visible = false;
            // 
            // kryptonPanel3
            // 
            this.kryptonPanel3.Controls.Add(this.chkmoneda);
            this.kryptonPanel3.Controls.Add(this.kryptonButton2);
            this.kryptonPanel3.Controls.Add(this.kryptonButton1);
            this.kryptonPanel3.Controls.Add(this.kryptonGroupBox1);
            this.kryptonPanel3.Controls.Add(this.DGPedidos);
            this.kryptonPanel3.Controls.Add(this.kryptonWrapLabel2);
            this.kryptonPanel3.Controls.Add(this.kryptonWrapLabel1);
            this.kryptonPanel3.Controls.Add(this.f2);
            this.kryptonPanel3.Controls.Add(this.f1);
            this.kryptonPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel3.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel3.Name = "kryptonPanel3";
            this.kryptonPanel3.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.SeparatorHighInternalProfile;
            this.kryptonPanel3.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPanel3.TabIndex = 0;
            // 
            // chkmoneda
            // 
            this.chkmoneda.AutoSize = true;
            this.chkmoneda.BackColor = System.Drawing.Color.Transparent;
            this.chkmoneda.Location = new System.Drawing.Point(478, 509);
            this.chkmoneda.Name = "chkmoneda";
            this.chkmoneda.Size = new System.Drawing.Size(139, 17);
            this.chkmoneda.TabIndex = 42;
            this.chkmoneda.Text = "Aplicar Moneda Dólares";
            this.chkmoneda.UseVisualStyleBackColor = false;
            // 
            // kryptonButton2
            // 
            this.kryptonButton2.Location = new System.Drawing.Point(680, 500);
            this.kryptonButton2.Name = "kryptonButton2";
            this.kryptonButton2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.kryptonButton2.Size = new System.Drawing.Size(142, 35);
            this.kryptonButton2.TabIndex = 39;
            this.kryptonButton2.Values.Image = global::FinalXML.Properties.Resources.pdfs_20;
            this.kryptonButton2.Values.Text = "Generar Documento";
            this.kryptonButton2.Click += new System.EventHandler(this.kryptonButton2_Click);
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(950, 9);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.kryptonButton1.Size = new System.Drawing.Size(90, 35);
            this.kryptonButton1.TabIndex = 27;
            this.kryptonButton1.Values.Image = global::FinalXML.Properties.Resources.examine_24;
            this.kryptonButton1.Values.Text = "Filtrar";
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // kryptonGroupBox1
            // 
            this.kryptonGroupBox1.Location = new System.Drawing.Point(204, 94);
            this.kryptonGroupBox1.Name = "kryptonGroupBox1";
            this.kryptonGroupBox1.Size = new System.Drawing.Size(8, 8);
            this.kryptonGroupBox1.TabIndex = 12;
            // 
            // DGPedidos
            // 
            this.DGPedidos.AllowUserToAddRows = false;
            this.DGPedidos.AllowUserToDeleteRows = false;
            this.DGPedidos.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.DGPedidos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGPedidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idpedido,
            this.sigla1,
            this.serie1,
            this.numeracion1,
            this.numdocumento1,
            this.codcliente1,
            this.cliente1,
            this.direccion1,
            this.femision1,
            this.total1});
            this.DGPedidos.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.DGPedidos.Location = new System.Drawing.Point(202, 77);
            this.DGPedidos.Name = "DGPedidos";
            this.DGPedidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGPedidos.Size = new System.Drawing.Size(1156, 400);
            this.DGPedidos.TabIndex = 5;
            this.DGPedidos.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.DGPedidos_RowStateChanged);
            // 
            // idpedido
            // 
            this.idpedido.HeaderText = "ID";
            this.idpedido.Name = "idpedido";
            this.idpedido.Visible = false;
            // 
            // sigla1
            // 
            this.sigla1.HeaderText = "SIGLA";
            this.sigla1.Name = "sigla1";
            this.sigla1.Visible = false;
            // 
            // serie1
            // 
            this.serie1.HeaderText = "SERIE";
            this.serie1.Name = "serie1";
            this.serie1.Visible = false;
            // 
            // numeracion1
            // 
            this.numeracion1.HeaderText = "NUMERACIÓN";
            this.numeracion1.Name = "numeracion1";
            this.numeracion1.Visible = false;
            // 
            // numdocumento1
            // 
            this.numdocumento1.HeaderText = "NUM DOCUMENTO";
            this.numdocumento1.Name = "numdocumento1";
            this.numdocumento1.Width = 180;
            // 
            // codcliente1
            // 
            this.codcliente1.HeaderText = "COD CLIENTE";
            this.codcliente1.Name = "codcliente1";
            this.codcliente1.Width = 180;
            // 
            // cliente1
            // 
            this.cliente1.HeaderText = "CLIENTE";
            this.cliente1.Name = "cliente1";
            this.cliente1.Width = 300;
            // 
            // direccion1
            // 
            this.direccion1.HeaderText = "DIRECCIÓN";
            this.direccion1.Name = "direccion1";
            this.direccion1.Width = 250;
            // 
            // femision1
            // 
            this.femision1.HeaderText = "FECHA EMISIÓN";
            this.femision1.Name = "femision1";
            // 
            // total1
            // 
            this.total1.HeaderText = "TOTAL";
            this.total1.Name = "total1";
            // 
            // kryptonWrapLabel2
            // 
            this.kryptonWrapLabel2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.kryptonWrapLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            this.kryptonWrapLabel2.Location = new System.Drawing.Point(704, 29);
            this.kryptonWrapLabel2.Name = "kryptonWrapLabel2";
            this.kryptonWrapLabel2.Size = new System.Drawing.Size(40, 15);
            this.kryptonWrapLabel2.Text = "Hasta:";
            // 
            // kryptonWrapLabel1
            // 
            this.kryptonWrapLabel1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.kryptonWrapLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(57)))), ((int)(((byte)(91)))));
            this.kryptonWrapLabel1.Location = new System.Drawing.Point(513, 29);
            this.kryptonWrapLabel1.Name = "kryptonWrapLabel1";
            this.kryptonWrapLabel1.Size = new System.Drawing.Size(42, 15);
            this.kryptonWrapLabel1.Text = "Desde:";
            // 
            // f2
            // 
            this.f2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.f2.Location = new System.Drawing.Point(768, 23);
            this.f2.Name = "f2";
            this.f2.Size = new System.Drawing.Size(149, 21);
            this.f2.TabIndex = 1;
            // 
            // f1
            // 
            this.f1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.f1.Location = new System.Drawing.Point(561, 23);
            this.f1.Name = "f1";
            this.f1.Size = new System.Drawing.Size(105, 21);
            this.f1.TabIndex = 0;
            // 
            // kryptonPage2
            // 
            this.kryptonPage2.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage2.Controls.Add(this.kryptonPanel2);
            this.kryptonPage2.Flags = 65534;
            this.kryptonPage2.LastVisibleSet = true;
            this.kryptonPage2.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage2.Name = "kryptonPage2";
            this.kryptonPage2.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage2.Text = "Enviar Documentos";
            this.kryptonPage2.ToolTipTitle = "Page ToolTip";
            this.kryptonPage2.UniqueName = "51965B96C39C4F3B098A3BBC39D8C510";
            // 
            // kryptonPanel2
            // 
            this.kryptonPanel2.Controls.Add(this.cboTipdoc);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel30);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel6);
            this.kryptonPanel2.Controls.Add(this.cboEmpresaDoc);
            this.kryptonPanel2.Controls.Add(this.btnSalir);
            this.kryptonPanel2.Controls.Add(this.btnGeneraPDF);
            this.kryptonPanel2.Controls.Add(this.btnEnvioSunat);
            this.kryptonPanel2.Controls.Add(this.btnGeneraXML);
            this.kryptonPanel2.Controls.Add(this.lblmensaje);
            this.kryptonPanel2.Controls.Add(this.dgListadoVentas);
            this.kryptonPanel2.Controls.Add(this.btnFiltrar);
            this.kryptonPanel2.Controls.Add(this.dtpHasta);
            this.kryptonPanel2.Controls.Add(this.dtpDesde);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel1);
            this.kryptonPanel2.Controls.Add(this.kryptonLabel2);
            this.kryptonPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel2.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel2.Name = "kryptonPanel2";
            this.kryptonPanel2.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.SeparatorHighInternalProfile;
            this.kryptonPanel2.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPanel2.TabIndex = 36;
            // 
            // cboTipdoc
            // 
            this.cboTipdoc.DropDownWidth = 121;
            this.cboTipdoc.Items.AddRange(new object[] {
            "-- TODOS --",
            "FACTURA",
            "BOLETA",
            "NOTA DE CRÉDITO",
            "NOTA DE DÉBITO"});
            this.cboTipdoc.Location = new System.Drawing.Point(420, 31);
            this.cboTipdoc.Name = "cboTipdoc";
            this.cboTipdoc.Size = new System.Drawing.Size(121, 21);
            this.cboTipdoc.TabIndex = 48;
            // 
            // kryptonLabel30
            // 
            this.kryptonLabel30.Location = new System.Drawing.Point(318, 33);
            this.kryptonLabel30.Name = "kryptonLabel30";
            this.kryptonLabel30.Size = new System.Drawing.Size(105, 20);
            this.kryptonLabel30.TabIndex = 47;
            this.kryptonLabel30.Values.Text = "Tipo Documento:";
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.Location = new System.Drawing.Point(12, 34);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel6.TabIndex = 46;
            this.kryptonLabel6.Values.Text = "Empresa:";
            // 
            // cboEmpresaDoc
            // 
            this.cboEmpresaDoc.DropDownWidth = 299;
            this.cboEmpresaDoc.Location = new System.Drawing.Point(72, 33);
            this.cboEmpresaDoc.Name = "cboEmpresaDoc";
            this.cboEmpresaDoc.Size = new System.Drawing.Size(240, 21);
            this.cboEmpresaDoc.TabIndex = 45;
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(1026, 478);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(98, 35);
            this.btnSalir.TabIndex = 40;
            this.btnSalir.Values.Image = global::FinalXML.Properties.Resources.cancel;
            this.btnSalir.Values.Text = " Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGeneraPDF
            // 
            this.btnGeneraPDF.Location = new System.Drawing.Point(868, 478);
            this.btnGeneraPDF.Name = "btnGeneraPDF";
            this.btnGeneraPDF.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnGeneraPDF.Size = new System.Drawing.Size(142, 35);
            this.btnGeneraPDF.TabIndex = 38;
            this.btnGeneraPDF.Values.Image = global::FinalXML.Properties.Resources.pdfs_20;
            this.btnGeneraPDF.Values.Text = "Generar Documento";
            this.btnGeneraPDF.Click += new System.EventHandler(this.btnGeneraPDF_Click);
            // 
            // btnEnvioSunat
            // 
            this.btnEnvioSunat.Enabled = false;
            this.btnEnvioSunat.Location = new System.Drawing.Point(721, 478);
            this.btnEnvioSunat.Name = "btnEnvioSunat";
            this.btnEnvioSunat.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnEnvioSunat.Size = new System.Drawing.Size(137, 35);
            this.btnEnvioSunat.TabIndex = 37;
            this.btnEnvioSunat.Values.Image = global::FinalXML.Properties.Resources.UI_11_24;
            this.btnEnvioSunat.Values.Text = "Enviar SUNAT";
            this.btnEnvioSunat.Click += new System.EventHandler(this.btnEnvioSunat_Click);
            // 
            // btnGeneraXML
            // 
            this.btnGeneraXML.Location = new System.Drawing.Point(568, 478);
            this.btnGeneraXML.Name = "btnGeneraXML";
            this.btnGeneraXML.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnGeneraXML.Size = new System.Drawing.Size(139, 35);
            this.btnGeneraXML.TabIndex = 36;
            this.btnGeneraXML.Values.Image = global::FinalXML.Properties.Resources._142_xml_export_20;
            this.btnGeneraXML.Values.Text = "Generar XML";
            this.btnGeneraXML.Click += new System.EventHandler(this.btnGeneraXML_Click);
            // 
            // lblmensaje
            // 
            this.lblmensaje.Location = new System.Drawing.Point(204, 493);
            this.lblmensaje.Name = "lblmensaje";
            this.lblmensaje.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.SparkleBlue;
            this.lblmensaje.Size = new System.Drawing.Size(207, 20);
            this.lblmensaje.TabIndex = 39;
            this.lblmensaje.Values.Image = global::FinalXML.Properties.Resources.ok;
            this.lblmensaje.Values.Text = "Archivo generado correctamente ";
            this.lblmensaje.Visible = false;
            // 
            // dgListadoVentas
            // 
            this.dgListadoVentas.AllowUserToAddRows = false;
            this.dgListadoVentas.AllowUserToDeleteRows = false;
            this.dgListadoVentas.AllowUserToOrderColumns = true;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListadoVentas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgListadoVentas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgListadoVentas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.sigla,
            this.serie,
            this.numeracion,
            this.numdocumento,
            this.codcliente,
            this.cliente,
            this.direccion,
            this.femision,
            this.total,
            this.codsunat,
            this.mensajesunat,
            this.estadosunat,
            this.xml,
            this.cdr,
            this.pdf,
            this.Nomxml,
            this.Nomcdr,
            this.Nompdf});
            this.dgListadoVentas.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.dgListadoVentas.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonCustom1;
            this.dgListadoVentas.GridStyles.StyleColumn = ComponentFactory.Krypton.Toolkit.GridStyle.Sheet;
            this.dgListadoVentas.Location = new System.Drawing.Point(5, 77);
            this.dgListadoVentas.Name = "dgListadoVentas";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListadoVentas.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgListadoVentas.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgListadoVentas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgListadoVentas.Size = new System.Drawing.Size(1326, 381);
            this.dgListadoVentas.TabIndex = 1;
            this.dgListadoVentas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgListadoVentas_CellContentClick);
            this.dgListadoVentas.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgListadoVentas_RowStateChanged);
            // 
            // sigla
            // 
            this.sigla.DataPropertyName = "F5_CTD";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sigla.DefaultCellStyle = dataGridViewCellStyle3;
            this.sigla.HeaderText = "SIGLA";
            this.sigla.Name = "sigla";
            this.sigla.Visible = false;
            // 
            // serie
            // 
            this.serie.DataPropertyName = "F5_CNUMSER";
            this.serie.HeaderText = "SERIE";
            this.serie.Name = "serie";
            this.serie.Visible = false;
            // 
            // numeracion
            // 
            this.numeracion.DataPropertyName = "F5_CNUMDOC";
            this.numeracion.HeaderText = "NUMERACION";
            this.numeracion.Name = "numeracion";
            this.numeracion.Visible = false;
            // 
            // numdocumento
            // 
            this.numdocumento.DataPropertyName = "NUMDOC";
            this.numdocumento.HeaderText = "NUM. DOCUMENTO";
            this.numdocumento.Name = "numdocumento";
            // 
            // codcliente
            // 
            this.codcliente.DataPropertyName = "F5_CCODCLI";
            this.codcliente.HeaderText = "COD. CLIENTE";
            this.codcliente.Name = "codcliente";
            this.codcliente.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.codcliente.Visible = false;
            // 
            // cliente
            // 
            this.cliente.DataPropertyName = "F5_CNOMBRE";
            this.cliente.HeaderText = "CLIENTE";
            this.cliente.Name = "cliente";
            this.cliente.Width = 250;
            // 
            // direccion
            // 
            this.direccion.DataPropertyName = "F5_CDIRECC";
            this.direccion.HeaderText = "DIRECCIÓN";
            this.direccion.Name = "direccion";
            this.direccion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.direccion.Width = 300;
            // 
            // femision
            // 
            this.femision.DataPropertyName = "F5_DFECDOC";
            this.femision.HeaderText = "FECHA EMISIÓN";
            this.femision.Name = "femision";
            // 
            // total
            // 
            this.total.DataPropertyName = "F5_NIMPORT";
            this.total.HeaderText = "TOTAL";
            this.total.Name = "total";
            // 
            // codsunat
            // 
            this.codsunat.DataPropertyName = "COD_ESTADO_SUNAT";
            this.codsunat.HeaderText = "COD. SUNAT";
            this.codsunat.Name = "codsunat";
            this.codsunat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // mensajesunat
            // 
            this.mensajesunat.DataPropertyName = "MENSAJE_SUNAT";
            this.mensajesunat.HeaderText = "MENSAJE";
            this.mensajesunat.Name = "mensajesunat";
            this.mensajesunat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // estadosunat
            // 
            this.estadosunat.DataPropertyName = "ESTADO_ENVIO";
            this.estadosunat.HeaderText = "ESTADO";
            this.estadosunat.Name = "estadosunat";
            this.estadosunat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // xml
            // 
            this.xml.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.xml.HeaderText = "XML";
            this.xml.Name = "xml";
            this.xml.Text = "XML";
            this.xml.ToolTipText = "Descargar XML";
            this.xml.UseColumnTextForButtonValue = true;
            // 
            // cdr
            // 
            this.cdr.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cdr.HeaderText = "CDR";
            this.cdr.Name = "cdr";
            this.cdr.Text = "CDR";
            this.cdr.ToolTipText = "Descargar CDR";
            this.cdr.UseColumnTextForButtonValue = true;
            // 
            // pdf
            // 
            this.pdf.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pdf.HeaderText = "PDF";
            this.pdf.Name = "pdf";
            this.pdf.Text = "PDF";
            this.pdf.ToolTipText = "Descargar PDF";
            this.pdf.UseColumnTextForButtonValue = true;
            // 
            // Nomxml
            // 
            this.Nomxml.HeaderText = "Nomxml";
            this.Nomxml.Name = "Nomxml";
            this.Nomxml.Visible = false;
            // 
            // Nomcdr
            // 
            this.Nomcdr.HeaderText = "Nomcdr";
            this.Nomcdr.Name = "Nomcdr";
            this.Nomcdr.Visible = false;
            // 
            // Nompdf
            // 
            this.Nompdf.HeaderText = "Nompdf";
            this.Nompdf.Name = "Nompdf";
            this.Nompdf.Visible = false;
            // 
            // btnFiltrar
            // 
            this.btnFiltrar.Location = new System.Drawing.Point(890, 23);
            this.btnFiltrar.Name = "btnFiltrar";
            this.btnFiltrar.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnFiltrar.Size = new System.Drawing.Size(90, 35);
            this.btnFiltrar.TabIndex = 26;
            this.btnFiltrar.Values.Image = global::FinalXML.Properties.Resources.examine_24;
            this.btnFiltrar.Values.Text = "Filtrar";
            this.btnFiltrar.Click += new System.EventHandler(this.btnFiltrar_Click);
            // 
            // dtpHasta
            // 
            this.dtpHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHasta.Location = new System.Drawing.Point(771, 29);
            this.dtpHasta.Name = "dtpHasta";
            this.dtpHasta.Size = new System.Drawing.Size(100, 20);
            this.dtpHasta.TabIndex = 27;
            // 
            // dtpDesde
            // 
            this.dtpDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesde.Location = new System.Drawing.Point(603, 31);
            this.dtpDesde.Name = "dtpDesde";
            this.dtpDesde.Size = new System.Drawing.Size(100, 20);
            this.dtpDesde.TabIndex = 28;
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.Location = new System.Drawing.Point(550, 31);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel1.TabIndex = 29;
            this.kryptonLabel1.Values.Text = "Desde:";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.Location = new System.Drawing.Point(721, 31);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel2.TabIndex = 30;
            this.kryptonLabel2.Values.Text = "Hasta:";
            // 
            // kryptonPage6
            // 
            this.kryptonPage6.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage6.Controls.Add(this.kryptonPanel7);
            this.kryptonPage6.Flags = 65534;
            this.kryptonPage6.LastVisibleSet = true;
            this.kryptonPage6.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage6.Name = "kryptonPage6";
            this.kryptonPage6.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage6.Text = "Enviar Doc. Acumulados";
            this.kryptonPage6.ToolTipTitle = "Page ToolTip";
            this.kryptonPage6.UniqueName = "28961F4CCCFD492385B47507EC8A5E79";
            this.kryptonPage6.Visible = false;
            // 
            // kryptonPanel7
            // 
            this.kryptonPanel7.Controls.Add(this.cboTipdocAcu);
            this.kryptonPanel7.Controls.Add(this.kryptonLabel31);
            this.kryptonPanel7.Controls.Add(this.kryptonLabel32);
            this.kryptonPanel7.Controls.Add(this.cboEmpresaDocAcu);
            this.kryptonPanel7.Controls.Add(this.btnSalirAcu);
            this.kryptonPanel7.Controls.Add(this.btnGeneraPDFAcu);
            this.kryptonPanel7.Controls.Add(this.btnEnvioSunatAcu);
            this.kryptonPanel7.Controls.Add(this.btnGeneraXMLAcu);
            this.kryptonPanel7.Controls.Add(this.lblmensajeAcu);
            this.kryptonPanel7.Controls.Add(this.dgListadoVentasAcu);
            this.kryptonPanel7.Controls.Add(this.btnFiltrarA);
            this.kryptonPanel7.Controls.Add(this.dtpHastaAcu);
            this.kryptonPanel7.Controls.Add(this.dtpDesdeAcu);
            this.kryptonPanel7.Controls.Add(this.kryptonLabel34);
            this.kryptonPanel7.Controls.Add(this.kryptonLabel35);
            this.kryptonPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel7.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel7.Name = "kryptonPanel7";
            this.kryptonPanel7.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.SeparatorHighInternalProfile;
            this.kryptonPanel7.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPanel7.TabIndex = 37;
            // 
            // cboTipdocAcu
            // 
            this.cboTipdocAcu.DropDownWidth = 121;
            this.cboTipdocAcu.Items.AddRange(new object[] {
            "-- TODOS --",
            "FACTURA",
            "BOLETA",
            "NOTA DE CRÉDITO",
            "NOTA DE DÉBITO"});
            this.cboTipdocAcu.Location = new System.Drawing.Point(420, 31);
            this.cboTipdocAcu.Name = "cboTipdocAcu";
            this.cboTipdocAcu.Size = new System.Drawing.Size(121, 21);
            this.cboTipdocAcu.TabIndex = 48;
            // 
            // kryptonLabel31
            // 
            this.kryptonLabel31.Location = new System.Drawing.Point(318, 33);
            this.kryptonLabel31.Name = "kryptonLabel31";
            this.kryptonLabel31.Size = new System.Drawing.Size(105, 20);
            this.kryptonLabel31.TabIndex = 47;
            this.kryptonLabel31.Values.Text = "Tipo Documento:";
            // 
            // kryptonLabel32
            // 
            this.kryptonLabel32.Location = new System.Drawing.Point(12, 34);
            this.kryptonLabel32.Name = "kryptonLabel32";
            this.kryptonLabel32.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel32.TabIndex = 46;
            this.kryptonLabel32.Values.Text = "Empresa:";
            // 
            // cboEmpresaDocAcu
            // 
            this.cboEmpresaDocAcu.DropDownWidth = 299;
            this.cboEmpresaDocAcu.Location = new System.Drawing.Point(72, 33);
            this.cboEmpresaDocAcu.Name = "cboEmpresaDocAcu";
            this.cboEmpresaDocAcu.Size = new System.Drawing.Size(240, 21);
            this.cboEmpresaDocAcu.TabIndex = 45;
            // 
            // btnSalirAcu
            // 
            this.btnSalirAcu.Location = new System.Drawing.Point(1026, 478);
            this.btnSalirAcu.Name = "btnSalirAcu";
            this.btnSalirAcu.Size = new System.Drawing.Size(98, 35);
            this.btnSalirAcu.TabIndex = 40;
            this.btnSalirAcu.Values.Image = global::FinalXML.Properties.Resources.cancel;
            this.btnSalirAcu.Values.Text = " Salir";
            this.btnSalirAcu.Click += new System.EventHandler(this.btnSalirAcu_Click);
            // 
            // btnGeneraPDFAcu
            // 
            this.btnGeneraPDFAcu.Location = new System.Drawing.Point(868, 478);
            this.btnGeneraPDFAcu.Name = "btnGeneraPDFAcu";
            this.btnGeneraPDFAcu.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnGeneraPDFAcu.Size = new System.Drawing.Size(142, 35);
            this.btnGeneraPDFAcu.TabIndex = 38;
            this.btnGeneraPDFAcu.Values.Image = global::FinalXML.Properties.Resources.pdfs_20;
            this.btnGeneraPDFAcu.Values.Text = "Generar Documento";
            this.btnGeneraPDFAcu.Click += new System.EventHandler(this.btnGeneraPDFAcu_Click);
            // 
            // btnEnvioSunatAcu
            // 
            this.btnEnvioSunatAcu.Enabled = false;
            this.btnEnvioSunatAcu.Location = new System.Drawing.Point(721, 478);
            this.btnEnvioSunatAcu.Name = "btnEnvioSunatAcu";
            this.btnEnvioSunatAcu.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnEnvioSunatAcu.Size = new System.Drawing.Size(137, 35);
            this.btnEnvioSunatAcu.TabIndex = 37;
            this.btnEnvioSunatAcu.Values.Image = global::FinalXML.Properties.Resources.UI_11_24;
            this.btnEnvioSunatAcu.Values.Text = "Enviar SUNAT";
            this.btnEnvioSunatAcu.Click += new System.EventHandler(this.btnEnvioSunatAcu_Click);
            // 
            // btnGeneraXMLAcu
            // 
            this.btnGeneraXMLAcu.Location = new System.Drawing.Point(568, 478);
            this.btnGeneraXMLAcu.Name = "btnGeneraXMLAcu";
            this.btnGeneraXMLAcu.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnGeneraXMLAcu.Size = new System.Drawing.Size(139, 35);
            this.btnGeneraXMLAcu.TabIndex = 36;
            this.btnGeneraXMLAcu.Values.Image = global::FinalXML.Properties.Resources._142_xml_export_20;
            this.btnGeneraXMLAcu.Values.Text = "Generar XML";
            this.btnGeneraXMLAcu.Click += new System.EventHandler(this.btnGeneraXMLAcu_Click);
            // 
            // lblmensajeAcu
            // 
            this.lblmensajeAcu.Location = new System.Drawing.Point(204, 493);
            this.lblmensajeAcu.Name = "lblmensajeAcu";
            this.lblmensajeAcu.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.SparkleBlue;
            this.lblmensajeAcu.Size = new System.Drawing.Size(207, 20);
            this.lblmensajeAcu.TabIndex = 39;
            this.lblmensajeAcu.Values.Image = global::FinalXML.Properties.Resources.ok;
            this.lblmensajeAcu.Values.Text = "Archivo generado correctamente ";
            this.lblmensajeAcu.Visible = false;
            // 
            // dgListadoVentasAcu
            // 
            this.dgListadoVentasAcu.AllowUserToAddRows = false;
            this.dgListadoVentasAcu.AllowUserToDeleteRows = false;
            this.dgListadoVentasAcu.AllowUserToOrderColumns = true;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListadoVentasAcu.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgListadoVentasAcu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.siglaA,
            this.serieA,
            this.numeracionA,
            this.numdocumentoA,
            this.codclienteA,
            this.clienteA,
            this.direccionA,
            this.femisionA,
            this.totalA,
            this.codsunatA,
            this.mensajesunatA,
            this.estadosunatA,
            this.xmlA,
            this.cdrA,
            this.pdfA,
            this.NomxmlA,
            this.NomcdrA,
            this.NompdfA});
            this.dgListadoVentasAcu.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.dgListadoVentasAcu.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonCustom1;
            this.dgListadoVentasAcu.GridStyles.StyleColumn = ComponentFactory.Krypton.Toolkit.GridStyle.Sheet;
            this.dgListadoVentasAcu.Location = new System.Drawing.Point(5, 67);
            this.dgListadoVentasAcu.Name = "dgListadoVentasAcu";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListadoVentasAcu.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgListadoVentasAcu.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgListadoVentasAcu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgListadoVentasAcu.Size = new System.Drawing.Size(1331, 381);
            this.dgListadoVentasAcu.TabIndex = 1;
            this.dgListadoVentasAcu.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgListadoVentasAcu_CellContentClick);
            this.dgListadoVentasAcu.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgListadoVentasAcu_RowStateChanged);
            // 
            // siglaA
            // 
            this.siglaA.DataPropertyName = "F5_CTD";
            this.siglaA.HeaderText = "SIGLA";
            this.siglaA.Name = "siglaA";
            // 
            // serieA
            // 
            this.serieA.DataPropertyName = "F5_CNUMSER";
            this.serieA.HeaderText = "SERIE";
            this.serieA.Name = "serieA";
            // 
            // numeracionA
            // 
            this.numeracionA.DataPropertyName = "F5_CNUMDOC";
            this.numeracionA.HeaderText = "NUMERACION";
            this.numeracionA.Name = "numeracionA";
            // 
            // numdocumentoA
            // 
            this.numdocumentoA.DataPropertyName = "NUMDOC";
            this.numdocumentoA.HeaderText = "NUM. DOCUMENTO";
            this.numdocumentoA.Name = "numdocumentoA";
            // 
            // codclienteA
            // 
            this.codclienteA.DataPropertyName = "F5_CCODCLI";
            this.codclienteA.HeaderText = "COD. CLIENTE";
            this.codclienteA.Name = "codclienteA";
            // 
            // clienteA
            // 
            this.clienteA.DataPropertyName = "F5_CNOMBRE";
            this.clienteA.HeaderText = "CLIENTE";
            this.clienteA.Name = "clienteA";
            // 
            // direccionA
            // 
            this.direccionA.DataPropertyName = "F5_CDIRECC";
            this.direccionA.HeaderText = "DIRECCIÓN";
            this.direccionA.Name = "direccionA";
            // 
            // femisionA
            // 
            this.femisionA.DataPropertyName = "F5_DFECDOC";
            this.femisionA.HeaderText = "FECHA EMISIÓN";
            this.femisionA.Name = "femisionA";
            // 
            // totalA
            // 
            this.totalA.DataPropertyName = "F5_NIMPORT";
            this.totalA.HeaderText = "TOTAL";
            this.totalA.Name = "totalA";
            // 
            // codsunatA
            // 
            this.codsunatA.DataPropertyName = "COD_ESTADO_SUNAT";
            this.codsunatA.HeaderText = "COD. SUNAT";
            this.codsunatA.Name = "codsunatA";
            // 
            // mensajesunatA
            // 
            this.mensajesunatA.DataPropertyName = "MENSAJE_SUNAT";
            this.mensajesunatA.HeaderText = "MENSAJE";
            this.mensajesunatA.Name = "mensajesunatA";
            // 
            // estadosunatA
            // 
            this.estadosunatA.DataPropertyName = "ESTADO_ENVIO";
            this.estadosunatA.HeaderText = "ESTADO";
            this.estadosunatA.Name = "estadosunatA";
            // 
            // xmlA
            // 
            this.xmlA.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.xmlA.HeaderText = "XML";
            this.xmlA.Name = "xmlA";
            this.xmlA.Text = "XML";
            this.xmlA.ToolTipText = "Descargar XML";
            this.xmlA.UseColumnTextForButtonValue = true;
            // 
            // cdrA
            // 
            this.cdrA.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cdrA.HeaderText = "CDR";
            this.cdrA.Name = "cdrA";
            this.cdrA.Text = "CDR";
            this.cdrA.ToolTipText = "Descargar CDR";
            this.cdrA.UseColumnTextForButtonValue = true;
            // 
            // pdfA
            // 
            this.pdfA.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pdfA.HeaderText = "PDF";
            this.pdfA.Name = "pdfA";
            this.pdfA.Text = "PDF";
            this.pdfA.ToolTipText = "Descargar PDF";
            this.pdfA.UseColumnTextForButtonValue = true;
            // 
            // NomxmlA
            // 
            this.NomxmlA.HeaderText = "Nomxml";
            this.NomxmlA.Name = "NomxmlA";
            this.NomxmlA.Visible = false;
            // 
            // NomcdrA
            // 
            this.NomcdrA.HeaderText = "Nomcdr";
            this.NomcdrA.Name = "NomcdrA";
            this.NomcdrA.Visible = false;
            // 
            // NompdfA
            // 
            this.NompdfA.HeaderText = "Nompdf";
            this.NompdfA.Name = "NompdfA";
            this.NompdfA.Visible = false;
            // 
            // btnFiltrarA
            // 
            this.btnFiltrarA.Location = new System.Drawing.Point(890, 23);
            this.btnFiltrarA.Name = "btnFiltrarA";
            this.btnFiltrarA.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnFiltrarA.Size = new System.Drawing.Size(90, 35);
            this.btnFiltrarA.TabIndex = 26;
            this.btnFiltrarA.Values.Image = global::FinalXML.Properties.Resources.examine_24;
            this.btnFiltrarA.Values.Text = "Filtrar";
            this.btnFiltrarA.Click += new System.EventHandler(this.btnFiltrarA_Click);
            // 
            // dtpHastaAcu
            // 
            this.dtpHastaAcu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHastaAcu.Location = new System.Drawing.Point(771, 29);
            this.dtpHastaAcu.Name = "dtpHastaAcu";
            this.dtpHastaAcu.Size = new System.Drawing.Size(100, 20);
            this.dtpHastaAcu.TabIndex = 27;
            // 
            // dtpDesdeAcu
            // 
            this.dtpDesdeAcu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesdeAcu.Location = new System.Drawing.Point(603, 31);
            this.dtpDesdeAcu.Name = "dtpDesdeAcu";
            this.dtpDesdeAcu.Size = new System.Drawing.Size(100, 20);
            this.dtpDesdeAcu.TabIndex = 28;
            // 
            // kryptonLabel34
            // 
            this.kryptonLabel34.Location = new System.Drawing.Point(550, 31);
            this.kryptonLabel34.Name = "kryptonLabel34";
            this.kryptonLabel34.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel34.TabIndex = 29;
            this.kryptonLabel34.Values.Text = "Desde:";
            // 
            // kryptonLabel35
            // 
            this.kryptonLabel35.Location = new System.Drawing.Point(721, 31);
            this.kryptonLabel35.Name = "kryptonLabel35";
            this.kryptonLabel35.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel35.TabIndex = 30;
            this.kryptonLabel35.Values.Text = "Hasta:";
            // 
            // kryptonPage4
            // 
            this.kryptonPage4.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage4.Controls.Add(this.kryptonPanel5);
            this.kryptonPage4.Flags = 65534;
            this.kryptonPage4.LastVisibleSet = true;
            this.kryptonPage4.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage4.Name = "kryptonPage4";
            this.kryptonPage4.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage4.Text = "Envío Resumen Diario";
            this.kryptonPage4.ToolTipTitle = "Page ToolTip";
            this.kryptonPage4.UniqueName = "189DEAB027544E1933A5D1661B44C3CB";
            // 
            // kryptonPanel5
            // 
            this.kryptonPanel5.Controls.Add(this.kryptonGroupBox3);
            this.kryptonPanel5.Controls.Add(this.kryptonGroupBox2);
            this.kryptonPanel5.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel5.Name = "kryptonPanel5";
            this.kryptonPanel5.Size = new System.Drawing.Size(1345, 539);
            this.kryptonPanel5.TabIndex = 1;
            // 
            // kryptonGroupBox3
            // 
            this.kryptonGroupBox3.Location = new System.Drawing.Point(347, 3);
            this.kryptonGroupBox3.Name = "kryptonGroupBox3";
            // 
            // kryptonGroupBox3.Panel
            // 
            this.kryptonGroupBox3.Panel.Controls.Add(this.btnDelBol);
            this.kryptonGroupBox3.Panel.Controls.Add(this.grvResDetail);
            this.kryptonGroupBox3.Panel.Controls.Add(this.kryptonGroupBox4);
            this.kryptonGroupBox3.Panel.Controls.Add(this.btnSendResumen);
            this.kryptonGroupBox3.Panel.Controls.Add(this.btnConsultarRes);
            this.kryptonGroupBox3.Size = new System.Drawing.Size(994, 532);
            this.kryptonGroupBox3.TabIndex = 10;
            this.kryptonGroupBox3.Values.Heading = "Consultar Boletas";
            // 
            // btnDelBol
            // 
            this.btnDelBol.Location = new System.Drawing.Point(783, 146);
            this.btnDelBol.Name = "btnDelBol";
            this.btnDelBol.Size = new System.Drawing.Size(186, 33);
            this.btnDelBol.TabIndex = 15;
            this.btnDelBol.Values.Text = "Eliminar";
            this.btnDelBol.Click += new System.EventHandler(this.btnDelBol_Click);
            // 
            // grvResDetail
            // 
            this.grvResDetail.AllowUserToAddRows = false;
            this.grvResDetail.AllowUserToOrderColumns = true;
            this.grvResDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvResDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numdoc,
            this.codcli,
            this.client,
            this.dircli,
            this.fecemi,
            this.totdoc});
            this.grvResDetail.Location = new System.Drawing.Point(21, 192);
            this.grvResDetail.Name = "grvResDetail";
            this.grvResDetail.Size = new System.Drawing.Size(948, 255);
            this.grvResDetail.TabIndex = 14;
            // 
            // numdoc
            // 
            this.numdoc.HeaderText = "NUM. DOCUMENTO";
            this.numdoc.Name = "numdoc";
            // 
            // codcli
            // 
            this.codcli.HeaderText = "COD. CLIENTE";
            this.codcli.Name = "codcli";
            // 
            // client
            // 
            this.client.HeaderText = "CLIENTE";
            this.client.Name = "client";
            // 
            // dircli
            // 
            this.dircli.HeaderText = "DIRECCIÓN";
            this.dircli.Name = "dircli";
            // 
            // fecemi
            // 
            this.fecemi.HeaderText = "FECHA EMISIÓN";
            this.fecemi.Name = "fecemi";
            // 
            // totdoc
            // 
            this.totdoc.HeaderText = "TOTAL";
            this.totdoc.Name = "totdoc";
            // 
            // kryptonGroupBox4
            // 
            this.kryptonGroupBox4.Location = new System.Drawing.Point(12, 13);
            this.kryptonGroupBox4.Name = "kryptonGroupBox4";
            // 
            // kryptonGroupBox4.Panel
            // 
            this.kryptonGroupBox4.Panel.Controls.Add(this.dtpFecFin);
            this.kryptonGroupBox4.Panel.Controls.Add(this.dtpFecIni);
            this.kryptonGroupBox4.Panel.Controls.Add(this.kryptonLabel10);
            this.kryptonGroupBox4.Panel.Controls.Add(this.cboEmpresa);
            this.kryptonGroupBox4.Panel.Controls.Add(this.kryptonLabel8);
            this.kryptonGroupBox4.Size = new System.Drawing.Size(957, 113);
            this.kryptonGroupBox4.TabIndex = 12;
            this.kryptonGroupBox4.Values.Heading = "Filtros";
            // 
            // dtpFecFin
            // 
            this.dtpFecFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecFin.Location = new System.Drawing.Point(260, 61);
            this.dtpFecFin.Name = "dtpFecFin";
            this.dtpFecFin.Size = new System.Drawing.Size(100, 20);
            this.dtpFecFin.TabIndex = 29;
            // 
            // dtpFecIni
            // 
            this.dtpFecIni.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecIni.Location = new System.Drawing.Point(127, 62);
            this.dtpFecIni.Name = "dtpFecIni";
            this.dtpFecIni.Size = new System.Drawing.Size(100, 20);
            this.dtpFecIni.TabIndex = 30;
            // 
            // kryptonLabel10
            // 
            this.kryptonLabel10.Location = new System.Drawing.Point(17, 22);
            this.kryptonLabel10.Name = "kryptonLabel10";
            this.kryptonLabel10.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel10.TabIndex = 13;
            this.kryptonLabel10.Values.Text = "Empresa:";
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.DropDownWidth = 191;
            this.cboEmpresa.Location = new System.Drawing.Point(126, 22);
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.Size = new System.Drawing.Size(456, 21);
            this.cboEmpresa.TabIndex = 12;
            // 
            // kryptonLabel8
            // 
            this.kryptonLabel8.Location = new System.Drawing.Point(17, 61);
            this.kryptonLabel8.Name = "kryptonLabel8";
            this.kryptonLabel8.Size = new System.Drawing.Size(103, 20);
            this.kryptonLabel8.TabIndex = 10;
            this.kryptonLabel8.Values.Text = "Rango de fechas:";
            // 
            // btnSendResumen
            // 
            this.btnSendResumen.Location = new System.Drawing.Point(301, 458);
            this.btnSendResumen.Name = "btnSendResumen";
            this.btnSendResumen.Size = new System.Drawing.Size(278, 31);
            this.btnSendResumen.TabIndex = 2;
            this.btnSendResumen.Values.Text = "Generar Resumen Diario";
            this.btnSendResumen.Click += new System.EventHandler(this.btnSendResumen_Click);
            // 
            // btnConsultarRes
            // 
            this.btnConsultarRes.Location = new System.Drawing.Point(21, 145);
            this.btnConsultarRes.Name = "btnConsultarRes";
            this.btnConsultarRes.Size = new System.Drawing.Size(278, 33);
            this.btnConsultarRes.TabIndex = 10;
            this.btnConsultarRes.Values.Text = "Consultar Boletas";
            this.btnConsultarRes.Click += new System.EventHandler(this.btnConsultarRes_Click);
            // 
            // kryptonGroupBox2
            // 
            this.kryptonGroupBox2.Location = new System.Drawing.Point(2, 3);
            this.kryptonGroupBox2.Name = "kryptonGroupBox2";
            // 
            // kryptonGroupBox2.Panel
            // 
            this.kryptonGroupBox2.Panel.Controls.Add(this.btnConsultarResumen);
            this.kryptonGroupBox2.Panel.Controls.Add(this.kryptonGroupBox5);
            this.kryptonGroupBox2.Panel.Controls.Add(this.kryptonLabel7);
            this.kryptonGroupBox2.Panel.Controls.Add(this.txtDetailRes);
            this.kryptonGroupBox2.Size = new System.Drawing.Size(339, 532);
            this.kryptonGroupBox2.TabIndex = 9;
            this.kryptonGroupBox2.Values.Heading = "Datos de Envio";
            // 
            // btnConsultarResumen
            // 
            this.btnConsultarResumen.Location = new System.Drawing.Point(81, 284);
            this.btnConsultarResumen.Name = "btnConsultarResumen";
            this.btnConsultarResumen.Size = new System.Drawing.Size(152, 38);
            this.btnConsultarResumen.TabIndex = 15;
            this.btnConsultarResumen.Values.Text = "Consultar";
            this.btnConsultarResumen.Visible = false;
            this.btnConsultarResumen.Click += new System.EventHandler(this.btnConsultarResumen_Click);
            // 
            // kryptonGroupBox5
            // 
            this.kryptonGroupBox5.Location = new System.Drawing.Point(21, 207);
            this.kryptonGroupBox5.Name = "kryptonGroupBox5";
            // 
            // kryptonGroupBox5.Panel
            // 
            this.kryptonGroupBox5.Panel.Controls.Add(this.txtNumTicketResumen);
            this.kryptonGroupBox5.Panel.Controls.Add(this.kryptonLabel9);
            this.kryptonGroupBox5.Size = new System.Drawing.Size(281, 61);
            this.kryptonGroupBox5.TabIndex = 14;
            this.kryptonGroupBox5.Values.Heading = "Consulta";
            // 
            // txtNumTicketResumen
            // 
            this.txtNumTicketResumen.Location = new System.Drawing.Point(83, 5);
            this.txtNumTicketResumen.Name = "txtNumTicketResumen";
            this.txtNumTicketResumen.Size = new System.Drawing.Size(184, 23);
            this.txtNumTicketResumen.TabIndex = 10;
            // 
            // kryptonLabel9
            // 
            this.kryptonLabel9.Location = new System.Drawing.Point(17, 6);
            this.kryptonLabel9.Name = "kryptonLabel9";
            this.kryptonLabel9.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel9.TabIndex = 10;
            this.kryptonLabel9.Values.Text = "N° Ticket";
            // 
            // kryptonLabel7
            // 
            this.kryptonLabel7.Location = new System.Drawing.Point(21, 13);
            this.kryptonLabel7.Name = "kryptonLabel7";
            this.kryptonLabel7.Size = new System.Drawing.Size(104, 20);
            this.kryptonLabel7.TabIndex = 9;
            this.kryptonLabel7.Values.Text = "Detalle del envio:";
            // 
            // txtDetailRes
            // 
            this.txtDetailRes.Location = new System.Drawing.Point(21, 39);
            this.txtDetailRes.Multiline = true;
            this.txtDetailRes.Name = "txtDetailRes";
            this.txtDetailRes.Size = new System.Drawing.Size(278, 159);
            this.txtDetailRes.TabIndex = 8;
            // 
            // kryptonPage7
            // 
            this.kryptonPage7.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage7.Controls.Add(this.kryptonPanel8);
            this.kryptonPage7.Flags = 65534;
            this.kryptonPage7.LastVisibleSet = true;
            this.kryptonPage7.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage7.Name = "kryptonPage7";
            this.kryptonPage7.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage7.Text = "Consulta Documentos";
            this.kryptonPage7.ToolTipTitle = "Page ToolTip";
            this.kryptonPage7.UniqueName = "58F9D7F34B4D45074BBF50FF08A204F4";
            // 
            // kryptonPanel8
            // 
            this.kryptonPanel8.Controls.Add(this.cboEstadoCD);
            this.kryptonPanel8.Controls.Add(this.kryptonLabel40);
            this.kryptonPanel8.Controls.Add(this.cboTipdocCD);
            this.kryptonPanel8.Controls.Add(this.kryptonLabel33);
            this.kryptonPanel8.Controls.Add(this.kryptonLabel36);
            this.kryptonPanel8.Controls.Add(this.cboEmpresaDocCD);
            this.kryptonPanel8.Controls.Add(this.kryptonButton8);
            this.kryptonPanel8.Controls.Add(this.kryptonButton9);
            this.kryptonPanel8.Controls.Add(this.kryptonButton10);
            this.kryptonPanel8.Controls.Add(this.kryptonButton11);
            this.kryptonPanel8.Controls.Add(this.kryptonLabel37);
            this.kryptonPanel8.Controls.Add(this.dgListadoVentasCD);
            this.kryptonPanel8.Controls.Add(this.btnFiltrarCD);
            this.kryptonPanel8.Controls.Add(this.dtpHastaCD);
            this.kryptonPanel8.Controls.Add(this.dtpDesdeCD);
            this.kryptonPanel8.Controls.Add(this.kryptonLabel38);
            this.kryptonPanel8.Controls.Add(this.kryptonLabel39);
            this.kryptonPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel8.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel8.Name = "kryptonPanel8";
            this.kryptonPanel8.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.SeparatorHighInternalProfile;
            this.kryptonPanel8.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPanel8.TabIndex = 37;
            // 
            // cboEstadoCD
            // 
            this.cboEstadoCD.DropDownWidth = 121;
            this.cboEstadoCD.Items.AddRange(new object[] {
            "-- TODOS --",
            "ACEPTADO",
            "RECHAZADO"});
            this.cboEstadoCD.Location = new System.Drawing.Point(623, 33);
            this.cboEstadoCD.Name = "cboEstadoCD";
            this.cboEstadoCD.Size = new System.Drawing.Size(121, 21);
            this.cboEstadoCD.TabIndex = 50;
            this.cboEstadoCD.SelectedIndexChanged += new System.EventHandler(this.kryptonComboBox1_SelectedIndexChanged);
            // 
            // kryptonLabel40
            // 
            this.kryptonLabel40.Location = new System.Drawing.Point(568, 34);
            this.kryptonLabel40.Name = "kryptonLabel40";
            this.kryptonLabel40.Size = new System.Drawing.Size(50, 20);
            this.kryptonLabel40.TabIndex = 49;
            this.kryptonLabel40.Values.Text = "Estado:";
            this.kryptonLabel40.Paint += new System.Windows.Forms.PaintEventHandler(this.kryptonLabel40_Paint);
            // 
            // cboTipdocCD
            // 
            this.cboTipdocCD.DropDownWidth = 121;
            this.cboTipdocCD.Items.AddRange(new object[] {
            "-- TODOS --",
            "FACTURA",
            "BOLETA",
            "NOTA DE CRÉDITO",
            "NOTA DE DÉBITO"});
            this.cboTipdocCD.Location = new System.Drawing.Point(420, 31);
            this.cboTipdocCD.Name = "cboTipdocCD";
            this.cboTipdocCD.Size = new System.Drawing.Size(121, 21);
            this.cboTipdocCD.TabIndex = 48;
            // 
            // kryptonLabel33
            // 
            this.kryptonLabel33.Location = new System.Drawing.Point(318, 33);
            this.kryptonLabel33.Name = "kryptonLabel33";
            this.kryptonLabel33.Size = new System.Drawing.Size(105, 20);
            this.kryptonLabel33.TabIndex = 47;
            this.kryptonLabel33.Values.Text = "Tipo Documento:";
            // 
            // kryptonLabel36
            // 
            this.kryptonLabel36.Location = new System.Drawing.Point(12, 34);
            this.kryptonLabel36.Name = "kryptonLabel36";
            this.kryptonLabel36.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel36.TabIndex = 46;
            this.kryptonLabel36.Values.Text = "Empresa:";
            // 
            // cboEmpresaDocCD
            // 
            this.cboEmpresaDocCD.DropDownWidth = 299;
            this.cboEmpresaDocCD.Location = new System.Drawing.Point(72, 33);
            this.cboEmpresaDocCD.Name = "cboEmpresaDocCD";
            this.cboEmpresaDocCD.Size = new System.Drawing.Size(240, 21);
            this.cboEmpresaDocCD.TabIndex = 45;
            // 
            // kryptonButton8
            // 
            this.kryptonButton8.Location = new System.Drawing.Point(1026, 478);
            this.kryptonButton8.Name = "kryptonButton8";
            this.kryptonButton8.Size = new System.Drawing.Size(98, 35);
            this.kryptonButton8.TabIndex = 40;
            this.kryptonButton8.Values.Image = global::FinalXML.Properties.Resources.cancel;
            this.kryptonButton8.Values.Text = " Salir";
            this.kryptonButton8.Click += new System.EventHandler(this.kryptonButton8_Click);
            // 
            // kryptonButton9
            // 
            this.kryptonButton9.Location = new System.Drawing.Point(868, 478);
            this.kryptonButton9.Name = "kryptonButton9";
            this.kryptonButton9.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.kryptonButton9.Size = new System.Drawing.Size(142, 35);
            this.kryptonButton9.TabIndex = 38;
            this.kryptonButton9.Values.Image = global::FinalXML.Properties.Resources.pdfs_20;
            this.kryptonButton9.Values.Text = "Generar Documento";
            this.kryptonButton9.Visible = false;
            // 
            // kryptonButton10
            // 
            this.kryptonButton10.Enabled = false;
            this.kryptonButton10.Location = new System.Drawing.Point(721, 478);
            this.kryptonButton10.Name = "kryptonButton10";
            this.kryptonButton10.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.kryptonButton10.Size = new System.Drawing.Size(137, 35);
            this.kryptonButton10.TabIndex = 37;
            this.kryptonButton10.Values.Image = global::FinalXML.Properties.Resources.UI_11_24;
            this.kryptonButton10.Values.Text = "Enviar SUNAT";
            this.kryptonButton10.Visible = false;
            // 
            // kryptonButton11
            // 
            this.kryptonButton11.Location = new System.Drawing.Point(568, 478);
            this.kryptonButton11.Name = "kryptonButton11";
            this.kryptonButton11.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.kryptonButton11.Size = new System.Drawing.Size(139, 35);
            this.kryptonButton11.TabIndex = 36;
            this.kryptonButton11.Values.Image = global::FinalXML.Properties.Resources._142_xml_export_20;
            this.kryptonButton11.Values.Text = "Generar XML";
            this.kryptonButton11.Visible = false;
            this.kryptonButton11.Click += new System.EventHandler(this.kryptonButton11_Click);
            // 
            // kryptonLabel37
            // 
            this.kryptonLabel37.Location = new System.Drawing.Point(204, 493);
            this.kryptonLabel37.Name = "kryptonLabel37";
            this.kryptonLabel37.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.SparkleBlue;
            this.kryptonLabel37.Size = new System.Drawing.Size(207, 20);
            this.kryptonLabel37.TabIndex = 39;
            this.kryptonLabel37.Values.Image = global::FinalXML.Properties.Resources.ok;
            this.kryptonLabel37.Values.Text = "Archivo generado correctamente ";
            this.kryptonLabel37.Visible = false;
            // 
            // dgListadoVentasCD
            // 
            this.dgListadoVentasCD.AllowUserToAddRows = false;
            this.dgListadoVentasCD.AllowUserToDeleteRows = false;
            this.dgListadoVentasCD.AllowUserToOrderColumns = true;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListadoVentasCD.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgListadoVentasCD.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgListadoVentasCD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.siglaD,
            this.serieD,
            this.numeracionD,
            this.numdocumentoD,
            this.codclienteD,
            this.clienteD,
            this.direccionD,
            this.femisionD,
            this.totalD,
            this.codsunatD,
            this.mensajesunatD,
            this.estadosunatD,
            this.xmlD,
            this.cdrD,
            this.pdfD,
            this.NomxmlD,
            this.NomcdrD,
            this.NompdfD});
            this.dgListadoVentasCD.GridStyles.Style = ComponentFactory.Krypton.Toolkit.DataGridViewStyle.Mixed;
            this.dgListadoVentasCD.GridStyles.StyleBackground = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonCustom1;
            this.dgListadoVentasCD.GridStyles.StyleColumn = ComponentFactory.Krypton.Toolkit.GridStyle.Sheet;
            this.dgListadoVentasCD.Location = new System.Drawing.Point(5, 77);
            this.dgListadoVentasCD.Name = "dgListadoVentasCD";
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListadoVentasCD.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgListadoVentasCD.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dgListadoVentasCD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgListadoVentasCD.Size = new System.Drawing.Size(1326, 381);
            this.dgListadoVentasCD.TabIndex = 1;
            this.dgListadoVentasCD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgListadoVentasCD_CellContentClick);
            // 
            // siglaD
            // 
            this.siglaD.DataPropertyName = "F5_CTD";
            this.siglaD.HeaderText = "SIGLA";
            this.siglaD.Name = "siglaD";
            // 
            // serieD
            // 
            this.serieD.DataPropertyName = "F5_CNUMSER";
            this.serieD.HeaderText = "SERIE";
            this.serieD.Name = "serieD";
            // 
            // numeracionD
            // 
            this.numeracionD.DataPropertyName = "F5_CNUMDOC";
            this.numeracionD.HeaderText = "NUMERACION";
            this.numeracionD.Name = "numeracionD";
            // 
            // numdocumentoD
            // 
            this.numdocumentoD.DataPropertyName = "NUMDOC";
            this.numdocumentoD.HeaderText = "NUM. DOCUMENTO";
            this.numdocumentoD.Name = "numdocumentoD";
            // 
            // codclienteD
            // 
            this.codclienteD.DataPropertyName = "F5_CCODCLI";
            this.codclienteD.HeaderText = "COD. CLIENTE";
            this.codclienteD.Name = "codclienteD";
            // 
            // clienteD
            // 
            this.clienteD.DataPropertyName = "F5_CNOMBRE";
            this.clienteD.HeaderText = "CLIENTE";
            this.clienteD.Name = "clienteD";
            // 
            // direccionD
            // 
            this.direccionD.DataPropertyName = "F5_CDIRECC";
            this.direccionD.HeaderText = "DIRECCIÓN";
            this.direccionD.Name = "direccionD";
            // 
            // femisionD
            // 
            this.femisionD.DataPropertyName = "F5_DFECDOC";
            this.femisionD.HeaderText = "FECHA EMISIÓN";
            this.femisionD.Name = "femisionD";
            // 
            // totalD
            // 
            this.totalD.DataPropertyName = "F5_NIMPORT";
            this.totalD.HeaderText = "TOTAL";
            this.totalD.Name = "totalD";
            // 
            // codsunatD
            // 
            this.codsunatD.DataPropertyName = "COD_ESTADO_SUNAT";
            this.codsunatD.HeaderText = "COD. SUNAT";
            this.codsunatD.Name = "codsunatD";
            // 
            // mensajesunatD
            // 
            this.mensajesunatD.DataPropertyName = "MENSAJE_SUNAT";
            this.mensajesunatD.HeaderText = "MENSAJE";
            this.mensajesunatD.Name = "mensajesunatD";
            // 
            // estadosunatD
            // 
            this.estadosunatD.DataPropertyName = "ESTADO_ENVIO";
            this.estadosunatD.HeaderText = "ESTADO";
            this.estadosunatD.Name = "estadosunatD";
            // 
            // xmlD
            // 
            this.xmlD.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.xmlD.HeaderText = "XML";
            this.xmlD.Name = "xmlD";
            this.xmlD.Text = "XML";
            this.xmlD.ToolTipText = "Descargar XML";
            this.xmlD.UseColumnTextForButtonValue = true;
            // 
            // cdrD
            // 
            this.cdrD.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cdrD.HeaderText = "CDR";
            this.cdrD.Name = "cdrD";
            this.cdrD.Text = "CDR";
            this.cdrD.ToolTipText = "Descargar CDR";
            this.cdrD.UseColumnTextForButtonValue = true;
            // 
            // pdfD
            // 
            this.pdfD.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.pdfD.HeaderText = "PDF";
            this.pdfD.Name = "pdfD";
            this.pdfD.Text = "PDF";
            this.pdfD.ToolTipText = "Descargar PDF";
            this.pdfD.UseColumnTextForButtonValue = true;
            // 
            // NomxmlD
            // 
            this.NomxmlD.HeaderText = "Nomxml";
            this.NomxmlD.Name = "NomxmlD";
            this.NomxmlD.Visible = false;
            // 
            // NomcdrD
            // 
            this.NomcdrD.HeaderText = "Nomcdr";
            this.NomcdrD.Name = "NomcdrD";
            this.NomcdrD.Visible = false;
            // 
            // NompdfD
            // 
            this.NompdfD.HeaderText = "Nompdf";
            this.NompdfD.Name = "NompdfD";
            this.NompdfD.Visible = false;
            // 
            // btnFiltrarCD
            // 
            this.btnFiltrarCD.Location = new System.Drawing.Point(1111, 23);
            this.btnFiltrarCD.Name = "btnFiltrarCD";
            this.btnFiltrarCD.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.btnFiltrarCD.Size = new System.Drawing.Size(90, 35);
            this.btnFiltrarCD.TabIndex = 26;
            this.btnFiltrarCD.Values.Image = global::FinalXML.Properties.Resources.examine_24;
            this.btnFiltrarCD.Values.Text = "Filtrar";
            this.btnFiltrarCD.Click += new System.EventHandler(this.btnFiltrarCD_Click);
            // 
            // dtpHastaCD
            // 
            this.dtpHastaCD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHastaCD.Location = new System.Drawing.Point(986, 31);
            this.dtpHastaCD.Name = "dtpHastaCD";
            this.dtpHastaCD.Size = new System.Drawing.Size(100, 20);
            this.dtpHastaCD.TabIndex = 27;
            // 
            // dtpDesdeCD
            // 
            this.dtpDesdeCD.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesdeCD.Location = new System.Drawing.Point(809, 31);
            this.dtpDesdeCD.Name = "dtpDesdeCD";
            this.dtpDesdeCD.Size = new System.Drawing.Size(100, 20);
            this.dtpDesdeCD.TabIndex = 28;
            // 
            // kryptonLabel38
            // 
            this.kryptonLabel38.Location = new System.Drawing.Point(756, 31);
            this.kryptonLabel38.Name = "kryptonLabel38";
            this.kryptonLabel38.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel38.TabIndex = 29;
            this.kryptonLabel38.Values.Text = "Desde:";
            // 
            // kryptonLabel39
            // 
            this.kryptonLabel39.Location = new System.Drawing.Point(927, 31);
            this.kryptonLabel39.Name = "kryptonLabel39";
            this.kryptonLabel39.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel39.TabIndex = 30;
            this.kryptonLabel39.Values.Text = "Hasta:";
            // 
            // kryptonPage3
            // 
            this.kryptonPage3.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage3.Controls.Add(this.kryptonPanel4);
            this.kryptonPage3.Flags = 65534;
            this.kryptonPage3.LastVisibleSet = true;
            this.kryptonPage3.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage3.Name = "kryptonPage3";
            this.kryptonPage3.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage3.Text = "Comunicación de Baja";
            this.kryptonPage3.ToolTipTitle = "Page ToolTip";
            this.kryptonPage3.UniqueName = "3801EB85FAE444120CB3E6AF1439D1C3";
            // 
            // kryptonPanel4
            // 
            this.kryptonPanel4.Controls.Add(this.kryptonButton7);
            this.kryptonPanel4.Controls.Add(this.txtNroTicket);
            this.kryptonPanel4.Controls.Add(this.kryptonLabel4);
            this.kryptonPanel4.Controls.Add(this.txtResult);
            this.kryptonPanel4.Controls.Add(this.kryptonLabel3);
            this.kryptonPanel4.Controls.Add(this.kryptonButton5);
            this.kryptonPanel4.Controls.Add(this.dglista2);
            this.kryptonPanel4.Controls.Add(this.GComunicacionBaja);
            this.kryptonPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kryptonPanel4.Location = new System.Drawing.Point(0, 0);
            this.kryptonPanel4.Name = "kryptonPanel4";
            this.kryptonPanel4.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPanel4.TabIndex = 147;
            // 
            // kryptonButton7
            // 
            this.kryptonButton7.Location = new System.Drawing.Point(75, 261);
            this.kryptonButton7.Name = "kryptonButton7";
            this.kryptonButton7.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.kryptonButton7.Size = new System.Drawing.Size(146, 31);
            this.kryptonButton7.TabIndex = 242;
            this.kryptonButton7.Values.Image = global::FinalXML.Properties.Resources.UI_11_241;
            this.kryptonButton7.Values.Text = "Consultar";
            this.kryptonButton7.Click += new System.EventHandler(this.kryptonButton6_Click);
            // 
            // txtNroTicket
            // 
            this.txtNroTicket.Location = new System.Drawing.Point(14, 229);
            this.txtNroTicket.Name = "txtNroTicket";
            this.txtNroTicket.ReadOnly = true;
            this.txtNroTicket.Size = new System.Drawing.Size(298, 20);
            this.txtNroTicket.TabIndex = 241;
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.Location = new System.Drawing.Point(14, 206);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(86, 20);
            this.kryptonLabel4.TabIndex = 240;
            this.kryptonLabel4.Values.Text = "Nro de Ticket:";
            // 
            // txtResult
            // 
            this.txtResult.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtResult.Location = new System.Drawing.Point(14, 80);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(298, 109);
            this.txtResult.TabIndex = 239;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(14, 52);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(115, 20);
            this.kryptonLabel3.TabIndex = 238;
            this.kryptonLabel3.Values.Text = "Resultado de Envío";
            // 
            // kryptonButton5
            // 
            this.kryptonButton5.Location = new System.Drawing.Point(623, 483);
            this.kryptonButton5.Name = "kryptonButton5";
            this.kryptonButton5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.kryptonButton5.Size = new System.Drawing.Size(223, 31);
            this.kryptonButton5.TabIndex = 237;
            this.kryptonButton5.Values.Image = global::FinalXML.Properties.Resources.view_icon_tm_16;
            this.kryptonButton5.Values.Text = "Generar Comunicación de baja";
            this.kryptonButton5.Click += new System.EventHandler(this.kryptonButton5_Click);
            // 
            // dglista2
            // 
            this.dglista2.AllowUserToAddRows = false;
            this.dglista2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dglista2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dglista2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.motivo});
            this.dglista2.Location = new System.Drawing.Point(355, 261);
            this.dglista2.Name = "dglista2";
            this.dglista2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dglista2.Size = new System.Drawing.Size(722, 150);
            this.dglista2.TabIndex = 133;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.FillWeight = 26.66996F;
            this.dataGridViewTextBoxColumn8.HeaderText = "ID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.FillWeight = 69.28292F;
            this.dataGridViewTextBoxColumn9.HeaderText = "Tipo Documento";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.FillWeight = 72.77803F;
            this.dataGridViewTextBoxColumn10.HeaderText = "Serie";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.FillWeight = 77.46198F;
            this.dataGridViewTextBoxColumn11.HeaderText = "Correlativo";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // motivo
            // 
            this.motivo.FillWeight = 253.8071F;
            this.motivo.HeaderText = "Motivo";
            this.motivo.Name = "motivo";
            // 
            // GComunicacionBaja
            // 
            this.GComunicacionBaja.Location = new System.Drawing.Point(334, 23);
            this.GComunicacionBaja.Name = "GComunicacionBaja";
            // 
            // GComunicacionBaja.Panel
            // 
            this.GComunicacionBaja.Panel.Controls.Add(this.cboEmpresaBaj);
            this.GComunicacionBaja.Panel.Controls.Add(this.kryptonLabel11);
            this.GComunicacionBaja.Panel.Controls.Add(this.kryptonLabel5);
            this.GComunicacionBaja.Panel.Controls.Add(this.FechaEmisionDocBaja);
            this.GComunicacionBaja.Panel.Controls.Add(this.kryptonButton6);
            this.GComunicacionBaja.Panel.Controls.Add(this.kryptonButton3);
            this.GComunicacionBaja.Panel.Controls.Add(this.kryptonButton4);
            this.GComunicacionBaja.Panel.Controls.Add(this.txtmotivo);
            this.GComunicacionBaja.Panel.Controls.Add(this.txtcorrelativo2);
            this.GComunicacionBaja.Panel.Controls.Add(this.label26);
            this.GComunicacionBaja.Panel.Controls.Add(this.textBox7);
            this.GComunicacionBaja.Panel.Controls.Add(this.comboBox1);
            this.GComunicacionBaja.Panel.Controls.Add(this.label27);
            this.GComunicacionBaja.Panel.Controls.Add(this.textBox1);
            this.GComunicacionBaja.Panel.Controls.Add(this.label25);
            this.GComunicacionBaja.Panel.Controls.Add(this.label31);
            this.GComunicacionBaja.Panel.Controls.Add(this.label24);
            this.GComunicacionBaja.Size = new System.Drawing.Size(900, 444);
            this.GComunicacionBaja.TabIndex = 145;
            this.GComunicacionBaja.Values.Heading = "Datos";
            // 
            // cboEmpresaBaj
            // 
            this.cboEmpresaBaj.DropDownWidth = 323;
            this.cboEmpresaBaj.Location = new System.Drawing.Point(153, 18);
            this.cboEmpresaBaj.Name = "cboEmpresaBaj";
            this.cboEmpresaBaj.Size = new System.Drawing.Size(323, 21);
            this.cboEmpresaBaj.TabIndex = 245;
            // 
            // kryptonLabel11
            // 
            this.kryptonLabel11.Location = new System.Drawing.Point(3, 18);
            this.kryptonLabel11.Name = "kryptonLabel11";
            this.kryptonLabel11.Size = new System.Drawing.Size(60, 20);
            this.kryptonLabel11.TabIndex = 244;
            this.kryptonLabel11.Values.Text = "Empresa:";
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.Location = new System.Drawing.Point(3, 160);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(149, 20);
            this.kryptonLabel5.TabIndex = 243;
            this.kryptonLabel5.Values.Text = "FechaEmisión Doumento:";
            // 
            // FechaEmisionDocBaja
            // 
            this.FechaEmisionDocBaja.CalendarTodayDate = new System.DateTime(2018, 10, 14, 0, 0, 0, 0);
            this.FechaEmisionDocBaja.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.FechaEmisionDocBaja.Location = new System.Drawing.Point(156, 160);
            this.FechaEmisionDocBaja.Name = "FechaEmisionDocBaja";
            this.FechaEmisionDocBaja.Size = new System.Drawing.Size(156, 21);
            this.FechaEmisionDocBaja.TabIndex = 237;
            // 
            // kryptonButton6
            // 
            this.kryptonButton6.Location = new System.Drawing.Point(326, 379);
            this.kryptonButton6.Name = "kryptonButton6";
            this.kryptonButton6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.kryptonButton6.Size = new System.Drawing.Size(146, 31);
            this.kryptonButton6.TabIndex = 236;
            this.kryptonButton6.Values.Image = global::FinalXML.Properties.Resources.UI_11_24;
            this.kryptonButton6.Values.Text = "&Nuevo Documento";
            this.kryptonButton6.Click += new System.EventHandler(this.kryptonButton6_Click);
            // 
            // kryptonButton3
            // 
            this.kryptonButton3.Location = new System.Drawing.Point(748, 277);
            this.kryptonButton3.Name = "kryptonButton3";
            this.kryptonButton3.Size = new System.Drawing.Size(90, 25);
            this.kryptonButton3.TabIndex = 1;
            this.kryptonButton3.Values.Image = global::FinalXML.Properties.Resources.delete;
            this.kryptonButton3.Values.Text = "&Eliminar";
            this.kryptonButton3.Click += new System.EventHandler(this.kryptonButton3_Click);
            // 
            // kryptonButton4
            // 
            this.kryptonButton4.Location = new System.Drawing.Point(349, 184);
            this.kryptonButton4.Name = "kryptonButton4";
            this.kryptonButton4.Size = new System.Drawing.Size(123, 25);
            this.kryptonButton4.TabIndex = 0;
            this.kryptonButton4.Values.Image = global::FinalXML.Properties.Resources._new;
            this.kryptonButton4.Values.Text = "       ";
            this.kryptonButton4.Click += new System.EventHandler(this.kryptonButton4_Click);
            // 
            // txtmotivo
            // 
            this.txtmotivo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmotivo.Location = new System.Drawing.Point(349, 87);
            this.txtmotivo.Multiline = true;
            this.txtmotivo.Name = "txtmotivo";
            this.txtmotivo.Size = new System.Drawing.Size(350, 79);
            this.txtmotivo.TabIndex = 119;
            // 
            // txtcorrelativo2
            // 
            this.txtcorrelativo2.Location = new System.Drawing.Point(153, 49);
            this.txtcorrelativo2.Name = "txtcorrelativo2";
            this.txtcorrelativo2.Size = new System.Drawing.Size(159, 20);
            this.txtcorrelativo2.TabIndex = 144;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(349, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(127, 20);
            this.label26.TabIndex = 142;
            this.label26.Values.Text = "Motivo de Anulación:";
            // 
            // textBox7
            // 
            this.textBox7.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox7.Location = new System.Drawing.Point(153, 108);
            this.textBox7.MaxLength = 4;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(159, 20);
            this.textBox7.TabIndex = 117;
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "Descripcion";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Factura",
            "Boleta"});
            this.comboBox1.Location = new System.Drawing.Point(153, 77);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(159, 21);
            this.comboBox1.TabIndex = 138;
            this.comboBox1.ValueMember = "Codigo";
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(3, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(140, 20);
            this.label27.TabIndex = 143;
            this.label27.Values.Text = "Correlativo Documento:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(153, 134);
            this.textBox1.MaxLength = 8;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(159, 20);
            this.textBox1.TabIndex = 118;
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(3, 139);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 20);
            this.label25.TabIndex = 139;
            this.label25.Values.Text = "Numeración:";
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(3, 108);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(40, 20);
            this.label31.TabIndex = 116;
            this.label31.Values.Text = "Serie:";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(3, 81);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(122, 20);
            this.label24.TabIndex = 137;
            this.label24.Values.Text = "Tipo de Documento:";
            // 
            // kryptonPage5
            // 
            this.kryptonPage5.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.kryptonPage5.Controls.Add(this.kryptonPanel6);
            this.kryptonPage5.Flags = 65534;
            this.kryptonPage5.LastVisibleSet = true;
            this.kryptonPage5.MinimumSize = new System.Drawing.Size(50, 50);
            this.kryptonPage5.Name = "kryptonPage5";
            this.kryptonPage5.Size = new System.Drawing.Size(1344, 538);
            this.kryptonPage5.Text = "Configuración";
            this.kryptonPage5.ToolTipTitle = "Page ToolTip";
            this.kryptonPage5.UniqueName = "C45C924A128F44C038A66276083848D6";
            // 
            // kryptonPanel6
            // 
            this.kryptonPanel6.Controls.Add(this.btnCancelEditEmi);
            this.kryptonPanel6.Controls.Add(this.btnEditarEmisor);
            this.kryptonPanel6.Controls.Add(this.grvEmisores);
            this.kryptonPanel6.Controls.Add(this.btnSaveEmisor);
            this.kryptonPanel6.Controls.Add(this.kryptonGroupBox6);
            this.kryptonPanel6.Location = new System.Drawing.Point(2, 3);
            this.kryptonPanel6.Name = "kryptonPanel6";
            this.kryptonPanel6.Size = new System.Drawing.Size(1340, 532);
            this.kryptonPanel6.TabIndex = 1;
            // 
            // btnCancelEditEmi
            // 
            this.btnCancelEditEmi.Cursor = System.Windows.Forms.Cursors.No;
            this.btnCancelEditEmi.Location = new System.Drawing.Point(1070, 254);
            this.btnCancelEditEmi.Name = "btnCancelEditEmi";
            this.btnCancelEditEmi.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.btnCancelEditEmi.Size = new System.Drawing.Size(129, 25);
            this.btnCancelEditEmi.TabIndex = 4;
            this.btnCancelEditEmi.Values.Image = global::FinalXML.Properties.Resources.cancel1;
            this.btnCancelEditEmi.Values.Text = "Cancelar";
            this.btnCancelEditEmi.Visible = false;
            this.btnCancelEditEmi.Click += new System.EventHandler(this.btnCancelEditEmi_Click);
            // 
            // btnEditarEmisor
            // 
            this.btnEditarEmisor.Location = new System.Drawing.Point(1205, 254);
            this.btnEditarEmisor.Name = "btnEditarEmisor";
            this.btnEditarEmisor.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.btnEditarEmisor.Size = new System.Drawing.Size(129, 25);
            this.btnEditarEmisor.TabIndex = 3;
            this.btnEditarEmisor.Values.Image = global::FinalXML.Properties.Resources.iconcopy;
            this.btnEditarEmisor.Values.Text = "Editar";
            this.btnEditarEmisor.Click += new System.EventHandler(this.btnEditarEmisor_Click);
            // 
            // grvEmisores
            // 
            this.grvEmisores.AllowUserToAddRows = false;
            this.grvEmisores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numruc,
            this.razsoc,
            this.Agencia,
            this.estemi,
            this.conemi,
            this.codubi,
            this.nomdep,
            this.nompro,
            this.nomdis,
            this.dirfis,
            this.sernam,
            this.basdat,
            this.tabcab,
            this.tabdet,
            this.estreg});
            this.grvEmisores.Location = new System.Drawing.Point(0, 289);
            this.grvEmisores.Name = "grvEmisores";
            this.grvEmisores.Size = new System.Drawing.Size(1337, 244);
            this.grvEmisores.TabIndex = 2;
            // 
            // numruc
            // 
            this.numruc.HeaderText = "RUC";
            this.numruc.Name = "numruc";
            // 
            // razsoc
            // 
            this.razsoc.HeaderText = "Razón Social";
            this.razsoc.Name = "razsoc";
            // 
            // Agencia
            // 
            this.Agencia.HeaderText = "codage";
            this.Agencia.Name = "Agencia";
            // 
            // estemi
            // 
            this.estemi.HeaderText = "Estado";
            this.estemi.Name = "estemi";
            // 
            // conemi
            // 
            this.conemi.HeaderText = "Condición";
            this.conemi.Name = "conemi";
            // 
            // codubi
            // 
            this.codubi.HeaderText = "Ubigeo";
            this.codubi.Name = "codubi";
            // 
            // nomdep
            // 
            this.nomdep.HeaderText = "Departamento";
            this.nomdep.Name = "nomdep";
            // 
            // nompro
            // 
            this.nompro.HeaderText = "Provincia";
            this.nompro.Name = "nompro";
            // 
            // nomdis
            // 
            this.nomdis.HeaderText = "Distrito";
            this.nomdis.Name = "nomdis";
            // 
            // dirfis
            // 
            this.dirfis.HeaderText = "Dirección Fiscal";
            this.dirfis.Name = "dirfis";
            // 
            // sernam
            // 
            this.sernam.HeaderText = "Servidor";
            this.sernam.Name = "sernam";
            // 
            // basdat
            // 
            this.basdat.HeaderText = "Base de datos";
            this.basdat.Name = "basdat";
            // 
            // tabcab
            // 
            this.tabcab.HeaderText = "Tabla Cabecera";
            this.tabcab.Name = "tabcab";
            // 
            // tabdet
            // 
            this.tabdet.HeaderText = "Tabla Detalle";
            this.tabdet.Name = "tabdet";
            // 
            // estreg
            // 
            this.estreg.HeaderText = "Estado Registro";
            this.estreg.Name = "estreg";
            // 
            // btnSaveEmisor
            // 
            this.btnSaveEmisor.Location = new System.Drawing.Point(12, 251);
            this.btnSaveEmisor.Name = "btnSaveEmisor";
            this.btnSaveEmisor.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Black;
            this.btnSaveEmisor.Size = new System.Drawing.Size(148, 26);
            this.btnSaveEmisor.TabIndex = 1;
            this.btnSaveEmisor.Values.Text = "Guardar";
            this.btnSaveEmisor.Click += new System.EventHandler(this.btnSaveEmisor_Click);
            // 
            // kryptonGroupBox6
            // 
            this.kryptonGroupBox6.Location = new System.Drawing.Point(12, 5);
            this.kryptonGroupBox6.Name = "kryptonGroupBox6";
            // 
            // kryptonGroupBox6.Panel
            // 
            this.kryptonGroupBox6.Panel.Controls.Add(this.lblEstadoEmisor);
            this.kryptonGroupBox6.Panel.Controls.Add(this.cboEstadoEmisor);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtpasssun);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtusersun);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel29);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel28);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtCodAge);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel27);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtuser);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel26);
            this.kryptonGroupBox6.Panel.Controls.Add(this.cboTablaDet);
            this.kryptonGroupBox6.Panel.Controls.Add(this.cboTablaCab);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel25);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel24);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtpass);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel23);
            this.kryptonGroupBox6.Panel.Controls.Add(this.cboBaseDatos);
            this.kryptonGroupBox6.Panel.Controls.Add(this.btnConectarServer);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel22);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtserver);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel21);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtnomdis);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel20);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtnomprv);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel19);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtnomdep);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel18);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel17);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtubigeo);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtdomfis);
            this.kryptonGroupBox6.Panel.Controls.Add(this.btnConsultarRuc);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel16);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtconemi);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtestemi);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel15);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel14);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtrazsoc);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel13);
            this.kryptonGroupBox6.Panel.Controls.Add(this.txtnumruc);
            this.kryptonGroupBox6.Panel.Controls.Add(this.kryptonLabel12);
            this.kryptonGroupBox6.Size = new System.Drawing.Size(1322, 240);
            this.kryptonGroupBox6.TabIndex = 0;
            this.kryptonGroupBox6.Values.Heading = "Emisor Electronico";
            // 
            // lblEstadoEmisor
            // 
            this.lblEstadoEmisor.Location = new System.Drawing.Point(9, 187);
            this.lblEstadoEmisor.Name = "lblEstadoEmisor";
            this.lblEstadoEmisor.Size = new System.Drawing.Size(50, 20);
            this.lblEstadoEmisor.TabIndex = 41;
            this.lblEstadoEmisor.Values.Text = "Estado:";
            // 
            // cboEstadoEmisor
            // 
            this.cboEstadoEmisor.DropDownWidth = 121;
            this.cboEstadoEmisor.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.cboEstadoEmisor.Location = new System.Drawing.Point(114, 187);
            this.cboEstadoEmisor.Name = "cboEstadoEmisor";
            this.cboEstadoEmisor.Size = new System.Drawing.Size(121, 21);
            this.cboEstadoEmisor.TabIndex = 40;
            // 
            // txtpasssun
            // 
            this.txtpasssun.Location = new System.Drawing.Point(112, 121);
            this.txtpasssun.Name = "txtpasssun";
            this.txtpasssun.PasswordChar = '*';
            this.txtpasssun.Size = new System.Drawing.Size(123, 23);
            this.txtpasssun.TabIndex = 39;
            // 
            // txtusersun
            // 
            this.txtusersun.Location = new System.Drawing.Point(112, 94);
            this.txtusersun.Name = "txtusersun";
            this.txtusersun.Size = new System.Drawing.Size(123, 23);
            this.txtusersun.TabIndex = 38;
            // 
            // kryptonLabel29
            // 
            this.kryptonLabel29.Location = new System.Drawing.Point(9, 126);
            this.kryptonLabel29.Name = "kryptonLabel29";
            this.kryptonLabel29.Size = new System.Drawing.Size(77, 20);
            this.kryptonLabel29.TabIndex = 37;
            this.kryptonLabel29.Values.Text = "Clave Sunat:";
            // 
            // kryptonLabel28
            // 
            this.kryptonLabel28.Location = new System.Drawing.Point(9, 96);
            this.kryptonLabel28.Name = "kryptonLabel28";
            this.kryptonLabel28.Size = new System.Drawing.Size(89, 20);
            this.kryptonLabel28.TabIndex = 36;
            this.kryptonLabel28.Values.Text = "Usuario Sunat:";
            // 
            // txtCodAge
            // 
            this.txtCodAge.Location = new System.Drawing.Point(685, 66);
            this.txtCodAge.MaxLength = 4;
            this.txtCodAge.Name = "txtCodAge";
            this.txtCodAge.Size = new System.Drawing.Size(153, 23);
            this.txtCodAge.TabIndex = 35;
            // 
            // kryptonLabel27
            // 
            this.kryptonLabel27.Location = new System.Drawing.Point(587, 66);
            this.kryptonLabel27.Name = "kryptonLabel27";
            this.kryptonLabel27.Size = new System.Drawing.Size(100, 20);
            this.kryptonLabel27.TabIndex = 34;
            this.kryptonLabel27.Values.Text = "Codigo Agencia:";
            // 
            // txtuser
            // 
            this.txtuser.Location = new System.Drawing.Point(315, 120);
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(88, 23);
            this.txtuser.TabIndex = 33;
            // 
            // kryptonLabel26
            // 
            this.kryptonLabel26.Location = new System.Drawing.Point(241, 123);
            this.kryptonLabel26.Name = "kryptonLabel26";
            this.kryptonLabel26.Size = new System.Drawing.Size(55, 20);
            this.kryptonLabel26.TabIndex = 32;
            this.kryptonLabel26.Values.Text = "Usuario:";
            // 
            // cboTablaDet
            // 
            this.cboTablaDet.DropDownWidth = 138;
            this.cboTablaDet.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Ribbon;
            this.cboTablaDet.Location = new System.Drawing.Point(685, 150);
            this.cboTablaDet.Name = "cboTablaDet";
            this.cboTablaDet.Size = new System.Drawing.Size(153, 21);
            this.cboTablaDet.TabIndex = 30;
            // 
            // cboTablaCab
            // 
            this.cboTablaCab.DropDownWidth = 138;
            this.cboTablaCab.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Ribbon;
            this.cboTablaCab.Location = new System.Drawing.Point(685, 123);
            this.cboTablaCab.Name = "cboTablaCab";
            this.cboTablaCab.Size = new System.Drawing.Size(153, 21);
            this.cboTablaCab.TabIndex = 29;
            // 
            // kryptonLabel25
            // 
            this.kryptonLabel25.Location = new System.Drawing.Point(587, 153);
            this.kryptonLabel25.Name = "kryptonLabel25";
            this.kryptonLabel25.Size = new System.Drawing.Size(84, 20);
            this.kryptonLabel25.TabIndex = 28;
            this.kryptonLabel25.Values.Text = "Tabla Detalle:";
            // 
            // kryptonLabel24
            // 
            this.kryptonLabel24.Location = new System.Drawing.Point(587, 123);
            this.kryptonLabel24.Name = "kryptonLabel24";
            this.kryptonLabel24.Size = new System.Drawing.Size(96, 20);
            this.kryptonLabel24.TabIndex = 26;
            this.kryptonLabel24.Values.Text = "Tabla Cabecera:";
            // 
            // txtpass
            // 
            this.txtpass.Location = new System.Drawing.Point(453, 123);
            this.txtpass.Name = "txtpass";
            this.txtpass.PasswordChar = '*';
            this.txtpass.Size = new System.Drawing.Size(121, 23);
            this.txtpass.TabIndex = 25;
            // 
            // kryptonLabel23
            // 
            this.kryptonLabel23.Location = new System.Drawing.Point(403, 123);
            this.kryptonLabel23.Name = "kryptonLabel23";
            this.kryptonLabel23.Size = new System.Drawing.Size(42, 20);
            this.kryptonLabel23.TabIndex = 24;
            this.kryptonLabel23.Values.Text = "Clave:";
            // 
            // cboBaseDatos
            // 
            this.cboBaseDatos.DropDownWidth = 138;
            this.cboBaseDatos.InputControlStyle = ComponentFactory.Krypton.Toolkit.InputControlStyle.Ribbon;
            this.cboBaseDatos.Location = new System.Drawing.Point(685, 96);
            this.cboBaseDatos.Name = "cboBaseDatos";
            this.cboBaseDatos.Size = new System.Drawing.Size(153, 21);
            this.cboBaseDatos.TabIndex = 23;
            this.cboBaseDatos.SelectedIndexChanged += new System.EventHandler(this.cboBaseDatos_SelectedIndexChanged);
            // 
            // btnConectarServer
            // 
            this.btnConectarServer.Location = new System.Drawing.Point(241, 156);
            this.btnConectarServer.Name = "btnConectarServer";
            this.btnConectarServer.Size = new System.Drawing.Size(333, 25);
            this.btnConectarServer.TabIndex = 22;
            this.btnConectarServer.Values.Text = "Conectar";
            this.btnConectarServer.Click += new System.EventHandler(this.btnConectarServer_Click);
            // 
            // kryptonLabel22
            // 
            this.kryptonLabel22.Location = new System.Drawing.Point(587, 97);
            this.kryptonLabel22.Name = "kryptonLabel22";
            this.kryptonLabel22.Size = new System.Drawing.Size(89, 20);
            this.kryptonLabel22.TabIndex = 21;
            this.kryptonLabel22.Values.Text = "Base de datos:";
            // 
            // txtserver
            // 
            this.txtserver.Location = new System.Drawing.Point(315, 94);
            this.txtserver.Name = "txtserver";
            this.txtserver.Size = new System.Drawing.Size(259, 23);
            this.txtserver.TabIndex = 20;
            // 
            // kryptonLabel21
            // 
            this.kryptonLabel21.Location = new System.Drawing.Point(241, 97);
            this.kryptonLabel21.Name = "kryptonLabel21";
            this.kryptonLabel21.Size = new System.Drawing.Size(58, 20);
            this.kryptonLabel21.TabIndex = 19;
            this.kryptonLabel21.Values.Text = "Servidor:";
            // 
            // txtnomdis
            // 
            this.txtnomdis.Location = new System.Drawing.Point(1130, 38);
            this.txtnomdis.Name = "txtnomdis";
            this.txtnomdis.ReadOnly = true;
            this.txtnomdis.Size = new System.Drawing.Size(185, 23);
            this.txtnomdis.TabIndex = 18;
            // 
            // kryptonLabel20
            // 
            this.kryptonLabel20.Location = new System.Drawing.Point(1077, 40);
            this.kryptonLabel20.Name = "kryptonLabel20";
            this.kryptonLabel20.Size = new System.Drawing.Size(53, 20);
            this.kryptonLabel20.TabIndex = 17;
            this.kryptonLabel20.Values.Text = "Distrito:";
            // 
            // txtnomprv
            // 
            this.txtnomprv.Location = new System.Drawing.Point(918, 37);
            this.txtnomprv.Name = "txtnomprv";
            this.txtnomprv.ReadOnly = true;
            this.txtnomprv.Size = new System.Drawing.Size(153, 23);
            this.txtnomprv.TabIndex = 16;
            // 
            // kryptonLabel19
            // 
            this.kryptonLabel19.Location = new System.Drawing.Point(849, 41);
            this.kryptonLabel19.Name = "kryptonLabel19";
            this.kryptonLabel19.Size = new System.Drawing.Size(63, 20);
            this.kryptonLabel19.TabIndex = 15;
            this.kryptonLabel19.Values.Text = "Provincia:";
            // 
            // txtnomdep
            // 
            this.txtnomdep.Location = new System.Drawing.Point(685, 37);
            this.txtnomdep.Name = "txtnomdep";
            this.txtnomdep.ReadOnly = true;
            this.txtnomdep.Size = new System.Drawing.Size(153, 23);
            this.txtnomdep.TabIndex = 14;
            // 
            // kryptonLabel18
            // 
            this.kryptonLabel18.Location = new System.Drawing.Point(587, 40);
            this.kryptonLabel18.Name = "kryptonLabel18";
            this.kryptonLabel18.Size = new System.Drawing.Size(92, 20);
            this.kryptonLabel18.TabIndex = 13;
            this.kryptonLabel18.Values.Text = "Departamento:";
            // 
            // kryptonLabel17
            // 
            this.kryptonLabel17.Location = new System.Drawing.Point(424, 41);
            this.kryptonLabel17.Name = "kryptonLabel17";
            this.kryptonLabel17.Size = new System.Drawing.Size(53, 20);
            this.kryptonLabel17.TabIndex = 12;
            this.kryptonLabel17.Values.Text = "Ubigeo:";
            // 
            // txtubigeo
            // 
            this.txtubigeo.Location = new System.Drawing.Point(476, 38);
            this.txtubigeo.Name = "txtubigeo";
            this.txtubigeo.ReadOnly = true;
            this.txtubigeo.Size = new System.Drawing.Size(98, 23);
            this.txtubigeo.TabIndex = 11;
            // 
            // txtdomfis
            // 
            this.txtdomfis.Location = new System.Drawing.Point(112, 67);
            this.txtdomfis.Name = "txtdomfis";
            this.txtdomfis.ReadOnly = true;
            this.txtdomfis.Size = new System.Drawing.Size(462, 23);
            this.txtdomfis.TabIndex = 10;
            // 
            // btnConsultarRuc
            // 
            this.btnConsultarRuc.Location = new System.Drawing.Point(238, 6);
            this.btnConsultarRuc.Name = "btnConsultarRuc";
            this.btnConsultarRuc.Size = new System.Drawing.Size(180, 25);
            this.btnConsultarRuc.TabIndex = 9;
            this.btnConsultarRuc.Values.Text = "Consultar";
            this.btnConsultarRuc.Click += new System.EventHandler(this.btnConsultarRuc_Click);
            // 
            // kryptonLabel16
            // 
            this.kryptonLabel16.Location = new System.Drawing.Point(9, 70);
            this.kryptonLabel16.Name = "kryptonLabel16";
            this.kryptonLabel16.Size = new System.Drawing.Size(97, 20);
            this.kryptonLabel16.TabIndex = 8;
            this.kryptonLabel16.Values.Text = "Dirección Fiscal:";
            // 
            // txtconemi
            // 
            this.txtconemi.Location = new System.Drawing.Point(302, 38);
            this.txtconemi.Name = "txtconemi";
            this.txtconemi.ReadOnly = true;
            this.txtconemi.Size = new System.Drawing.Size(116, 23);
            this.txtconemi.TabIndex = 7;
            // 
            // txtestemi
            // 
            this.txtestemi.Location = new System.Drawing.Point(94, 37);
            this.txtestemi.Name = "txtestemi";
            this.txtestemi.ReadOnly = true;
            this.txtestemi.Size = new System.Drawing.Size(138, 23);
            this.txtestemi.TabIndex = 6;
            // 
            // kryptonLabel15
            // 
            this.kryptonLabel15.Location = new System.Drawing.Point(238, 41);
            this.kryptonLabel15.Name = "kryptonLabel15";
            this.kryptonLabel15.Size = new System.Drawing.Size(68, 20);
            this.kryptonLabel15.TabIndex = 5;
            this.kryptonLabel15.Values.Text = "Condición:";
            // 
            // kryptonLabel14
            // 
            this.kryptonLabel14.Location = new System.Drawing.Point(9, 41);
            this.kryptonLabel14.Name = "kryptonLabel14";
            this.kryptonLabel14.Size = new System.Drawing.Size(50, 20);
            this.kryptonLabel14.TabIndex = 4;
            this.kryptonLabel14.Values.Text = "Estado:";
            // 
            // txtrazsoc
            // 
            this.txtrazsoc.Location = new System.Drawing.Point(506, 8);
            this.txtrazsoc.Name = "txtrazsoc";
            this.txtrazsoc.ReadOnly = true;
            this.txtrazsoc.Size = new System.Drawing.Size(809, 23);
            this.txtrazsoc.TabIndex = 3;
            // 
            // kryptonLabel13
            // 
            this.kryptonLabel13.Location = new System.Drawing.Point(424, 8);
            this.kryptonLabel13.Name = "kryptonLabel13";
            this.kryptonLabel13.Size = new System.Drawing.Size(82, 20);
            this.kryptonLabel13.TabIndex = 2;
            this.kryptonLabel13.Values.Text = "Razón Social:";
            // 
            // txtnumruc
            // 
            this.txtnumruc.Location = new System.Drawing.Point(94, 8);
            this.txtnumruc.MaxLength = 11;
            this.txtnumruc.Name = "txtnumruc";
            this.txtnumruc.Size = new System.Drawing.Size(138, 23);
            this.txtnumruc.TabIndex = 1;
            // 
            // kryptonLabel12
            // 
            this.kryptonLabel12.Location = new System.Drawing.Point(9, 11);
            this.kryptonLabel12.Name = "kryptonLabel12";
            this.kryptonLabel12.Size = new System.Drawing.Size(37, 20);
            this.kryptonLabel12.TabIndex = 0;
            this.kryptonLabel12.Values.Text = "RUC:";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 645);
            this.Controls.Add(this.kryptonPanel1);
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.Text = "INVERSIONES ARISTO EIRL";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonNavigator1)).EndInit();
            this.kryptonNavigator1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage1)).EndInit();
            this.kryptonPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel3)).EndInit();
            this.kryptonPanel3.ResumeLayout(false);
            this.kryptonPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox1.Panel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox1)).EndInit();
            this.kryptonGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGPedidos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage2)).EndInit();
            this.kryptonPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel2)).EndInit();
            this.kryptonPanel2.ResumeLayout(false);
            this.kryptonPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipdoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListadoVentas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage6)).EndInit();
            this.kryptonPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel7)).EndInit();
            this.kryptonPanel7.ResumeLayout(false);
            this.kryptonPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipdocAcu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaDocAcu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListadoVentasAcu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage4)).EndInit();
            this.kryptonPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel5)).EndInit();
            this.kryptonPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox3.Panel)).EndInit();
            this.kryptonGroupBox3.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox3)).EndInit();
            this.kryptonGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvResDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox4.Panel)).EndInit();
            this.kryptonGroupBox4.Panel.ResumeLayout(false);
            this.kryptonGroupBox4.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox4)).EndInit();
            this.kryptonGroupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox2.Panel)).EndInit();
            this.kryptonGroupBox2.Panel.ResumeLayout(false);
            this.kryptonGroupBox2.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox2)).EndInit();
            this.kryptonGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox5.Panel)).EndInit();
            this.kryptonGroupBox5.Panel.ResumeLayout(false);
            this.kryptonGroupBox5.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox5)).EndInit();
            this.kryptonGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage7)).EndInit();
            this.kryptonPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel8)).EndInit();
            this.kryptonPanel8.ResumeLayout(false);
            this.kryptonPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cboEstadoCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTipdocCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaDocCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListadoVentasCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage3)).EndInit();
            this.kryptonPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel4)).EndInit();
            this.kryptonPanel4.ResumeLayout(false);
            this.kryptonPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglista2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GComunicacionBaja.Panel)).EndInit();
            this.GComunicacionBaja.Panel.ResumeLayout(false);
            this.GComunicacionBaja.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GComunicacionBaja)).EndInit();
            this.GComunicacionBaja.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboEmpresaBaj)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPage5)).EndInit();
            this.kryptonPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel6)).EndInit();
            this.kryptonPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvEmisores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox6.Panel)).EndInit();
            this.kryptonGroupBox6.Panel.ResumeLayout(false);
            this.kryptonGroupBox6.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroupBox6)).EndInit();
            this.kryptonGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cboEstadoEmisor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTablaDet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboTablaCab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboBaseDatos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private ComponentFactory.Krypton.Navigator.KryptonNavigator kryptonNavigator1;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage1;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage2;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel3;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSalir;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnGeneraPDF;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnEnvioSunat;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnGeneraXML;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblmensaje;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgListadoVentas;
        private System.Windows.Forms.DataGridViewTextBoxColumn sigla;
        private System.Windows.Forms.DataGridViewTextBoxColumn serie;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeracion;
        private System.Windows.Forms.DataGridViewTextBoxColumn numdocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn codcliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn femision;
        private System.Windows.Forms.DataGridViewTextBoxColumn total;
        private System.Windows.Forms.DataGridViewTextBoxColumn codsunat;
        private System.Windows.Forms.DataGridViewTextBoxColumn mensajesunat;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadosunat;
        private System.Windows.Forms.DataGridViewButtonColumn xml;
        private System.Windows.Forms.DataGridViewButtonColumn cdr;
        private System.Windows.Forms.DataGridViewButtonColumn pdf;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nomxml;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nomcdr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nompdf;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnFiltrar;
        private System.Windows.Forms.DateTimePicker dtpHasta;
        private System.Windows.Forms.DateTimePicker dtpDesde;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonWrapLabel kryptonWrapLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonWrapLabel kryptonWrapLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker f2;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker f1;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kryptonGroupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton2;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView DGPedidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn idpedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn sigla1;
        private System.Windows.Forms.DataGridViewTextBoxColumn serie1;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeracion1;
        private System.Windows.Forms.DataGridViewTextBoxColumn numdocumento1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codcliente1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente1;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion1;
        private System.Windows.Forms.DataGridViewTextBoxColumn femision1;
        private System.Windows.Forms.DataGridViewTextBoxColumn total1;
        private System.Windows.Forms.CheckBox chkmoneda;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage3;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage4;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel5;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kryptonGroupBox3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnDelBol;
        private System.Windows.Forms.DataGridView grvResDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn numdoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn codcli;
        private System.Windows.Forms.DataGridViewTextBoxColumn client;
        private System.Windows.Forms.DataGridViewTextBoxColumn dircli;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecemi;
        private System.Windows.Forms.DataGridViewTextBoxColumn totdoc;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kryptonGroupBox4;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel10;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEmpresa;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel8;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSendResumen;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConsultarRes;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kryptonGroupBox2;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConsultarResumen;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kryptonGroupBox5;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtNumTicketResumen;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel9;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel7;
        private System.Windows.Forms.TextBox txtDetailRes;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage5;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCancelEditEmi;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnEditarEmisor;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView grvEmisores;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSaveEmisor;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox kryptonGroupBox6;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblEstadoEmisor;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEstadoEmisor;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtpasssun;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtusersun;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel29;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel28;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtCodAge;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel27;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtuser;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel26;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTablaDet;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTablaCab;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel25;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel24;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtpass;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel23;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboBaseDatos;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConectarServer;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel22;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtserver;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel21;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtnomdis;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel20;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtnomprv;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel19;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtnomdep;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel18;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel17;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtubigeo;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtdomfis;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnConsultarRuc;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel16;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtconemi;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtestemi;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel15;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel14;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtrazsoc;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel13;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txtnumruc;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel12;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTipdoc;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel30;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEmpresaDoc;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton7;
        private System.Windows.Forms.TextBox txtNroTicket;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private System.Windows.Forms.TextBox txtResult;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton5;
        private System.Windows.Forms.DataGridView dglista2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn motivo;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox GComunicacionBaja;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEmpresaBaj;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel11;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker FechaEmisionDocBaja;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton3;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton4;
        private System.Windows.Forms.TextBox txtmotivo;
        private System.Windows.Forms.TextBox txtcorrelativo2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel label26;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.ComboBox comboBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel label27;
        private System.Windows.Forms.TextBox textBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel label25;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel label31;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel label24;
        private System.Windows.Forms.DataGridViewTextBoxColumn numruc;
        private System.Windows.Forms.DataGridViewTextBoxColumn razsoc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Agencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn estemi;
        private System.Windows.Forms.DataGridViewTextBoxColumn conemi;
        private System.Windows.Forms.DataGridViewTextBoxColumn codubi;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomdep;
        private System.Windows.Forms.DataGridViewTextBoxColumn nompro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomdis;
        private System.Windows.Forms.DataGridViewTextBoxColumn dirfis;
        private System.Windows.Forms.DataGridViewTextBoxColumn sernam;
        private System.Windows.Forms.DataGridViewTextBoxColumn basdat;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabcab;
        private System.Windows.Forms.DataGridViewTextBoxColumn tabdet;
        private System.Windows.Forms.DataGridViewTextBoxColumn estreg;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage6;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel7;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTipdocAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel31;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel32;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEmpresaDocAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSalirAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnGeneraPDFAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnEnvioSunatAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnGeneraXMLAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel lblmensajeAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgListadoVentasAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnFiltrarA;
        private System.Windows.Forms.DateTimePicker dtpHastaAcu;
        private System.Windows.Forms.DateTimePicker dtpDesdeAcu;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel34;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel35;
        private System.Windows.Forms.DateTimePicker dtpFecFin;
        private System.Windows.Forms.DateTimePicker dtpFecIni;
        private ComponentFactory.Krypton.Navigator.KryptonPage kryptonPage7;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel8;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboTipdocCD;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel33;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel36;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEmpresaDocCD;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton8;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton9;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton10;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton11;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel37;
        private ComponentFactory.Krypton.Toolkit.KryptonDataGridView dgListadoVentasCD;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnFiltrarCD;
        private System.Windows.Forms.DateTimePicker dtpHastaCD;
        private System.Windows.Forms.DateTimePicker dtpDesdeCD;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel38;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel39;
        private System.Windows.Forms.DataGridViewTextBoxColumn siglaA;
        private System.Windows.Forms.DataGridViewTextBoxColumn serieA;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeracionA;
        private System.Windows.Forms.DataGridViewTextBoxColumn numdocumentoA;
        private System.Windows.Forms.DataGridViewTextBoxColumn codclienteA;
        private System.Windows.Forms.DataGridViewTextBoxColumn clienteA;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccionA;
        private System.Windows.Forms.DataGridViewTextBoxColumn femisionA;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalA;
        private System.Windows.Forms.DataGridViewTextBoxColumn codsunatA;
        private System.Windows.Forms.DataGridViewTextBoxColumn mensajesunatA;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadosunatA;
        private System.Windows.Forms.DataGridViewButtonColumn xmlA;
        private System.Windows.Forms.DataGridViewButtonColumn cdrA;
        private System.Windows.Forms.DataGridViewButtonColumn pdfA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomxmlA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomcdrA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NompdfA;
        private System.Windows.Forms.DataGridViewTextBoxColumn siglaD;
        private System.Windows.Forms.DataGridViewTextBoxColumn serieD;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeracionD;
        private System.Windows.Forms.DataGridViewTextBoxColumn numdocumentoD;
        private System.Windows.Forms.DataGridViewTextBoxColumn codclienteD;
        private System.Windows.Forms.DataGridViewTextBoxColumn clienteD;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccionD;
        private System.Windows.Forms.DataGridViewTextBoxColumn femisionD;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalD;
        private System.Windows.Forms.DataGridViewTextBoxColumn codsunatD;
        private System.Windows.Forms.DataGridViewTextBoxColumn mensajesunatD;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadosunatD;
        private System.Windows.Forms.DataGridViewButtonColumn xmlD;
        private System.Windows.Forms.DataGridViewButtonColumn cdrD;
        private System.Windows.Forms.DataGridViewButtonColumn pdfD;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomxmlD;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomcdrD;
        private System.Windows.Forms.DataGridViewTextBoxColumn NompdfD;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cboEstadoCD;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel40;
    }
}