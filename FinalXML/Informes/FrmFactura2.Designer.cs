﻿namespace FinalXML.Informes
{
    partial class FrmFactura2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DetalleDocumentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ContribuyenteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DocumentoElectronicoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ClsDatosReportesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.DetalleDocumentoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContribuyenteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentoElectronicoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClsDatosReportesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // DetalleDocumentoBindingSource
            // 
            this.DetalleDocumentoBindingSource.DataSource = typeof(FinalXML.DetalleDocumento);
            // 
            // ContribuyenteBindingSource
            // 
            this.ContribuyenteBindingSource.DataSource = typeof(FinalXML.Contribuyente);
            // 
            // DocumentoElectronicoBindingSource
            // 
            this.DocumentoElectronicoBindingSource.DataSource = typeof(FinalXML.DocumentoElectronico);
            // 
            // ClsDatosReportesBindingSource
            // 
            this.ClsDatosReportesBindingSource.DataSource = typeof(FinalXML.ClsDatosReportes);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(842, 504);
            this.reportViewer1.TabIndex = 0;
            // 
            // FrmFactura2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 504);
            this.Controls.Add(this.reportViewer1);
            this.Name = "FrmFactura2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmFactura2";
            this.Load += new System.EventHandler(this.FrmFactura2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DetalleDocumentoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ContribuyenteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DocumentoElectronicoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClsDatosReportesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource DetalleDocumentoBindingSource;
        private System.Windows.Forms.BindingSource ContribuyenteBindingSource;
        private System.Windows.Forms.BindingSource DocumentoElectronicoBindingSource;
        private System.Windows.Forms.BindingSource ClsDatosReportesBindingSource;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}