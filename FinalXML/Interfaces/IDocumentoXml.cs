﻿//using WinApp.Comun;
//using WinApp.Comun.Dto.Contratos;

namespace FinalXML.Interfaces
{
    public interface IDocumentoXml
    {
        IEstructuraXml Generar(IDocumentoElectronico request);
    }
}
