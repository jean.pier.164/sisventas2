﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalXML
{
    public class Conversion
    {
        private const int Millon = 1000000;
        private const long Billon = 1000000000000;

        public string enletras(string num)
        {
            string res, dec = "";
            Int64 entero;
            int decimales;
            double nro;

            try
            {
                nro = Convert.ToDouble(num);
            }
            catch
            {
                return "";
            }

            entero = Convert.ToInt64(Math.Truncate(nro));
            decimales = Convert.ToInt32(Math.Round(((nro - entero) * 100), 2));
            if (decimales > 0)
            {
                dec = " Y " + decimales.ToString() + "/100";
            }
            else {
                dec = " Y  00 /100".ToString();
            }

            res = toText(Convert.ToDouble(entero)) + dec;
            return res;
        }
        public static string Enletras(decimal num)
        {
            var entero = Convert.ToInt64(Math.Truncate(num));
            var decimales = Convert.ToInt32(Math.Round((num - entero) * 100, 2));
            var dec = decimales > 0 ? $" CON {decimales}/100" : " CON 00/100";

            var res = ToText(entero) + dec;
            return res;
        }
        private static string ToText(decimal value)
        {
            string num2Text;
            value = Math.Truncate(value);
            if (value == 0) num2Text = "CERO";
            else if (value == 1) num2Text = "UNO";
            else if (value == 2) num2Text = "DOS";
            else if (value == 3) num2Text = "TRES";
            else if (value == 4) num2Text = "CUATRO";
            else if (value == 5) num2Text = "CINCO";
            else if (value == 6) num2Text = "SEIS";
            else if (value == 7) num2Text = "SIETE";
            else if (value == 8) num2Text = "OCHO";
            else if (value == 9) num2Text = "NUEVE";
            else if (value == 10) num2Text = "DIEZ";
            else if (value == 11) num2Text = "ONCE";
            else if (value == 12) num2Text = "DOCE";
            else if (value == 13) num2Text = "TRECE";
            else if (value == 14) num2Text = "CATORCE";
            else if (value == 15) num2Text = "QUINCE";
            else if (value < 20) num2Text = $"DIECI{ToText(value - 10)}";
            else if (value == 20) num2Text = "VEINTE";
            else if (value < 30) num2Text = $"VEINTI{ToText(value - 20)}";
            else if (value == 30) num2Text = "TREINTA";
            else if (value == 40) num2Text = "CUARENTA";
            else if (value == 50) num2Text = "CINCUENTA";
            else if (value == 60) num2Text = "SESENTA";
            else if (value == 70) num2Text = "SETENTA";
            else if (value == 80) num2Text = "OCHENTA";
            else if (value == 90) num2Text = "NOVENTA";
            else if (value < 100) num2Text = $"{ToText(Math.Truncate(value / 10) * 10)} Y {ToText(value % 10)}";
            else if (value == 100) num2Text = "CIEN";
            else if (value < 200) num2Text = $"CIENTO {ToText(value - 100)}";
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) num2Text =
                $"{ToText(Math.Truncate(value / 100))}CIENTOS";
            else if (value == 500) num2Text = "QUINIENTOS";
            else if (value == 700) num2Text = "SETECIENTOS";
            else if (value == 900) num2Text = "NOVECIENTOS";
            else if (value < 1000) num2Text = $"{ToText(Math.Truncate(value / 100) * 100)} {ToText(value % 100)}";
            else if (value == 1000) num2Text = "MIL";
            else if (value < 2000) num2Text = $"MIL {ToText(value % 1000)}";
            else if (value < Millon)
            {
                num2Text = $"{ToText(Math.Truncate(value / 1000))} MIL";
                if ((value % 1000) > 0) num2Text = $"{num2Text} {ToText(value % 1000)}";
            }

            else if (value == Millon) num2Text = "UN MILLON";
            else if (value < 2000000) num2Text = $"UN MILLON {ToText(value % Millon)}";
            else if (value < Billon)
            {
                num2Text = $"{ToText(Math.Truncate(value / Millon))} MILLONES ";
                if ((value - Math.Truncate(value / Millon) * Millon) > 0) num2Text =
                    $"{num2Text} {ToText(value - Math.Truncate(value / Millon) * Millon)}";
            }

            else if (value == Billon) num2Text = "UN BILLON";
            else if (value < 2000000000000) num2Text =
                $"UN BILLON {ToText(value - Math.Truncate(value / Billon) * Billon)}";

            else
            {
                num2Text = $"{ToText(Math.Truncate(value / Billon))} BILLONES";
                if ((value - Math.Truncate(value / Billon) * Billon) > 0) num2Text =
                    $"{num2Text} {ToText(value - Math.Truncate(value / Billon) * Billon)}";
            }
            return num2Text;

        }

        private string toText(double value)
        {
            string Num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) Num2Text = "CERO";
            else if (value == 1) Num2Text = "UNO";
            else if (value == 2) Num2Text = "DOS";
            else if (value == 3) Num2Text = "TRES";
            else if (value == 4) Num2Text = "CUATRO";
            else if (value == 5) Num2Text = "CINCO";
            else if (value == 6) Num2Text = "SEIS";
            else if (value == 7) Num2Text = "SIETE";
            else if (value == 8) Num2Text = "OCHO";
            else if (value == 9) Num2Text = "NUEVE";
            else if (value == 10) Num2Text = "DIEZ";
            else if (value == 11) Num2Text = "ONCE";
            else if (value == 12) Num2Text = "DOCE";
            else if (value == 13) Num2Text = "TRECE";
            else if (value == 14) Num2Text = "CATORCE";
            else if (value == 15) Num2Text = "QUINCE";
            else if (value < 20) Num2Text = "DIECI" + toText(value - 10);
            else if (value == 20) Num2Text = "VEINTE";
            else if (value < 30) Num2Text = "VEINTI" + toText(value - 20);
            else if (value == 30) Num2Text = "TREINTA";
            else if (value == 40) Num2Text = "CUARENTA";
            else if (value == 50) Num2Text = "CINCUENTA";
            else if (value == 60) Num2Text = "SESENTA";
            else if (value == 70) Num2Text = "SETENTA";
            else if (value == 80) Num2Text = "OCHENTA";
            else if (value == 90) Num2Text = "NOVENTA";
            else if (value < 100) Num2Text = toText(Math.Truncate(value / 10) * 10) + " Y " + toText(value % 10);
            else if (value == 100) Num2Text = "CIEN";
            else if (value < 200) Num2Text = "CIENTO " + toText(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) Num2Text = toText(Math.Truncate(value / 100)) + "CIENTOS";
            else if (value == 500) Num2Text = "QUINIENTOS";
            else if (value == 700) Num2Text = "SETECIENTOS";
            else if (value == 900) Num2Text = "NOVECIENTOS";
            else if (value < 1000) Num2Text = toText(Math.Truncate(value / 100) * 100) + " " + toText(value % 100);
            else if (value == 1000) Num2Text = "MIL";
            else if (value < 2000) Num2Text = "MIL " + toText(value % 1000);
            else if (value < 1000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000)) + " MIL";
                if ((value % 1000) > 0) Num2Text = Num2Text + " " + toText(value % 1000);
            }

            else if (value == 1000000) Num2Text = "UN MILLON";
            else if (value < 2000000) Num2Text = "UN MILLON " + toText(value % 1000000);
            else if (value < 1000000000000)
            {
                Num2Text = toText(Math.Truncate(value / 1000000)) + " MILLONES ";
                if ((value - Math.Truncate(value / 1000000) * 1000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000) * 1000000);
            }

            else if (value == 1000000000000) Num2Text = "UN BILLON";
            else if (value < 2000000000000) Num2Text = "UN BILLON " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);

            else
            {
                Num2Text = toText(Math.Truncate(value / 1000000000000)) + " BILLONES";
                if ((value - Math.Truncate(value / 1000000000000) * 1000000000000) > 0) Num2Text = Num2Text + " " + toText(value - Math.Truncate(value / 1000000000000) * 1000000000000);
            }
            return Num2Text;

        }

        public string CodigoTipoNotaCredito(string codMotivo)
        {
            string motivoNota = "";

            Dictionary<string, string> dicTipoNota = new Dictionary<string, string>();

            dicTipoNota.Add("01", "ANULACION DE LA OPERACIÓN");
            dicTipoNota.Add("02", "ANULACIÓN POR ERROR EN EL RUC");
            dicTipoNota.Add("03", "CORECCIÓN POR ERROR EN LA DESCRIPCIÓN");
            dicTipoNota.Add("04", "DESCUENTO GLOBAL");
            dicTipoNota.Add("05", "DESCUENTO POR ITEM");
            dicTipoNota.Add("06", "DEVOLUCIÓN TOTAL");
            dicTipoNota.Add("07", "DEVOLUCIÓN POR ITEM");
            dicTipoNota.Add("08", "BONIFICACIÓN");
            dicTipoNota.Add("09", "DISMINUCIÓN EN EL VALOR");
            
            foreach (KeyValuePair<string, string> author in dicTipoNota)
            {
                if (author.Key == codMotivo)
                    motivoNota = author.Value;
            }

            return motivoNota;
        }

        public string CodigoTipoNotaDebito(string codMotivo)
        {
            string motivoNota = "";

            Dictionary<string, string> dicTipoNota = new Dictionary<string, string>();

            dicTipoNota.Add("01", "INTERESES POR MORA");
            dicTipoNota.Add("02", "AUMENTO EN EL VALOR");
            dicTipoNota.Add("03", "PENALIDADES");

            foreach (KeyValuePair<string, string> author in dicTipoNota)
            {
                if (author.Key == codMotivo)
                    motivoNota = author.Value;
            }

            return motivoNota;
        }
    }
}
