﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using FinalXML.Interfaces;

namespace FinalXML
{


    public class FacturaXml : IDocumentoXml
    {
        IEstructuraXml IDocumentoXml.Generar(IDocumentoElectronico request)
        {
            var documento = (DocumentoElectronico)request;
            var invoice = new Invoice
            {
                UblExtensions = new UBLExtensions
                {
                    Extension2 = new UBLExtension
                    {
                        ExtensionContent = new ExtensionContent
                        {
                            AdditionalInformation = new AdditionalInformation
                            {
                                AdditionalMonetaryTotals = new List<AdditionalMonetaryTotal>()
                                {
                                    new AdditionalMonetaryTotal()
                                    {
                                        ID = "1001",
                                        PayableAmount = new PayableAmount()
                                        {
                                            currencyID = documento.Moneda,
                                            value = documento.Gravadas
                                        }
                                    },
                                    new AdditionalMonetaryTotal
                                    {
                                        ID = "1002",
                                        PayableAmount = new PayableAmount
                                        {
                                            currencyID = documento.Moneda,
                                            value = documento.Inafectas
                                        }
                                    },
                                    new AdditionalMonetaryTotal
                                    {
                                        ID = "1003",
                                        PayableAmount = new PayableAmount
                                        {
                                            currencyID = documento.Moneda,
                                            value = documento.Exoneradas
                                        }
                                    },
                                    new AdditionalMonetaryTotal
                                    {
                                        ID = "1004",
                                        PayableAmount = new PayableAmount
                                        {
                                            currencyID = documento.Moneda,
                                            value = documento.Gratuitas
                                        }
                                    },
                                    new AdditionalMonetaryTotal {
                                        Percent= (documento.CalculoIgv * 100)
                                    }
                                },
                                AdditionalProperties = new List<AdditionalProperty>()
                                {
                                    new AdditionalProperty
                                    {
                                        ID = "1000",
                                        Value = documento.MontoEnLetras
                                    }
                                }
                            }
                        }
                    }
                },
                Id = documento.IdDocumento,
                IssueDate = DateTime.Parse(documento.FechaEmision),
                InvoiceTypeCode = documento.TipoDocumento,
                DocumentCurrencyCode = documento.Moneda,
                LineCountNumeric = documento.LineCountNumeric,
                Signature = new SignatureCac
                {
                    ID = documento.IdDocumento,
                    SignatoryParty = new SignatoryParty
                    {
                        PartyIdentification = new PartyIdentification
                        {
                            ID = new PartyIdentificationID
                            {
                                value = documento.Emisor.NroDocumento
                            }
                        },
                        PartyName = new PartyName
                        {
                            Name = documento.Emisor.NombreLegal
                        }
                    },
                    DigitalSignatureAttachment = new DigitalSignatureAttachment
                    {
                        ExternalReference = new ExternalReference
                        {
                            URI = $"{documento.Emisor.NroDocumento}-{documento.IdDocumento}"
                        }
                    }
                },
                AccountingSupplierParty = new AccountingSupplierParty
                {
                    CustomerAssignedAccountID = documento.Emisor.NroDocumento,
                    AdditionalAccountID = documento.Emisor.TipoDocumento,
                    CodDomicilioFiscal = documento.Emisor.CodDomicilioFiscal,
                    Party = new Party
                    {
                        PartyName = new PartyName
                        {
                            Name = documento.Emisor.NombreComercial
                        },
                        PostalAddress = new PostalAddress
                        {
                            ID = documento.Emisor.Ubigeo,
                            StreetName = documento.Emisor.Direccion,
                            CitySubdivisionName = documento.Emisor.Urbanizacion,
                            CountrySubentity = documento.Emisor.Departamento,
                            CityName = documento.Emisor.Provincia,
                            District = documento.Emisor.Distrito,
                            Country = new Country { IdentificationCode = "PE" }

                        },
                        PartyLegalEntity = new PartyLegalEntity
                        {
                            RegistrationName = documento.Emisor.NombreLegal
                        }
                    }
                },
                AccountingCustomerParty = new AccountingSupplierParty
                {
                    CustomerAssignedAccountID = documento.Receptor.NroDocumento,
                    AdditionalAccountID = documento.Receptor.TipoDocumento,
                    Party = new Party
                    {
                        PartyName = new PartyName
                        {
                            Name = documento.Receptor.NombreComercial ?? string.Empty
                        },
                        PartyLegalEntity = new PartyLegalEntity
                        {
                            RegistrationName = documento.Receptor.NombreLegal
                        }
                    }
                },
                UblVersionId = "2.1",
                CustomizationId = "2.0",
                LegalMonetaryTotal = new LegalMonetaryTotal
                {
                    PayableAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.TotalVenta
                    },
                    AllowanceTotalAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.DescuentoGlobal
                    }
                },
                TaxTotals = new List<TaxTotal>()
            };
            if (documento.Gravadas > 0)
            {
                invoice.TaxTotals.Add(new TaxTotal
                {
                    //Agregado para la nueva version
                    TaxableAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.Gravadas
                    },
                    TaxAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.TotalIgv
                    },
                    TaxSubtotal = new TaxSubtotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalIgv,
                        },
                        TaxCategory = new TaxCategory
                        {
                            Identifier = "S", //VALOR OBTENIDO DE LA TABLA 5
                            TaxScheme = new TaxScheme
                            {
                                ID = "1000",
                                Name = "IGV",
                                TaxTypeCode = "VAT"
                            }
                        }
                    }
                });
            }
            if(documento.Exoneradas > 0)
            {
                invoice.TaxTotals.Add(new TaxTotal
                {

                    TaxableAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.Exoneradas
                    },
                    TaxAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.TotalIgv
                    },
                    TaxSubtotal = new TaxSubtotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalIgv,
                        },
                        TaxCategory = new TaxCategory
                        {
                            Identifier = "E",
                            TaxScheme = new TaxScheme
                            {
                                ID = "9997",
                                Name = "EXO",
                                TaxTypeCode = "VAT"
                            }
                        }
                    }
                });
            }
            if (documento.TotalIsc > 0)
            {
                invoice.TaxTotals.Add(new TaxTotal
                {
                    TaxAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.TotalIsc,
                    },
                    TaxSubtotal = new TaxSubtotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalIsc
                        },
                        TaxCategory = new TaxCategory
                        {
                            Identifier = "S", //VALOR OBTENIDO DE LA TABLA 5
                            TaxScheme = new TaxScheme
                            {
                                ID = "2000",
                                Name = "ISC",
                                TaxTypeCode = "EXC"
                            }
                        }
                    }
                });
            }
            if (documento.TotalOtrosTributos > 0)
            {
                invoice.TaxTotals.Add(new TaxTotal
                {
                    TaxAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.TotalOtrosTributos,
                    },
                    TaxSubtotal = new TaxSubtotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalOtrosTributos
                        },
                        TaxCategory = new TaxCategory
                        {
                            Identifier = "S", //VALOR OBTENIDO DE LA TABLA 5
                            TaxScheme = new TaxScheme
                            {
                                ID = "9999",
                                Name = "OTROS",
                                TaxTypeCode = "OTH"
                            }
                        }
                    }
                });
            }

            /* Numero de Placa del Vehiculo - Gastos art.37° Renta */
            if (!string.IsNullOrEmpty(documento.PlacaVehiculo))
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.SunatCosts.RoadTransport
                    .LicensePlateId = documento.PlacaVehiculo;
            }

            /* Tipo de Operación - Catalogo N° 17 */
            if (!string.IsNullOrEmpty(documento.TipoOperacion)
                && documento.DatosGuiaTransportista == null)
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.SunatTransaction.Id = documento.TipoOperacion;
                // Si es Emisor Itinerante.
                if (documento.TipoOperacion == "05")
                {
                    invoice.UblExtensions.Extension2.ExtensionContent
                        .AdditionalInformation.AdditionalProperties.Add(new AdditionalProperty
                        {
                            ID = "3000", // En el catalogo aparece como 2005 pero es 3000
                            Value = "Venta realizada por emisor itinerante"
                        });
                }
            }

            foreach (var relacionado in documento.Relacionados)
            {
                invoice.DespatchDocumentReferences.Add(new InvoiceDocumentReference
                {
                    DocumentTypeCode = relacionado.TipoDocumento,
                    ID = relacionado.NroDocumento
                });
            }

            foreach (var relacionado in documento.OtrosDocumentosRelacionados)
            {
                invoice.AdditionalDocumentReferences.Add(new InvoiceDocumentReference
                {
                    DocumentTypeCode = relacionado.TipoDocumento,
                    ID = relacionado.NroDocumento
                });
            }

            if (documento.Gratuitas > 0)
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                        .AdditionalInformation.AdditionalProperties.Add(new AdditionalProperty
                        {
                            ID = "1002",
                            Value = "Articulos gratuitos"
                        });
            }
            var dctosPorItem = documento.Items.Sum(d => d.Descuento);
            if (documento.DescuentoGlobal > 0 || dctosPorItem > 0)
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.AdditionalMonetaryTotals.Add(new AdditionalMonetaryTotal
                    {
                        ID = "2005",
                        PayableAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.DescuentoGlobal + dctosPorItem
                        }
                    });
            }
            if (documento.MontoPercepcion > 0)
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.AdditionalMonetaryTotals.Add(new AdditionalMonetaryTotal
                    {
                        ID = "2001",
                        ReferenceAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalVenta
                        },
                        PayableAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.MontoPercepcion
                        },
                        TotalAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalVenta + documento.MontoPercepcion
                        }
                    });
            }
            if (documento.MontoAnticipo > 0)
            {
                invoice.PrepaidPayment = new BillingPayment
                {
                    Id = new PartyIdentificationID
                    {
                        schemeID = documento.TipoDocAnticipo,
                        value = documento.DocAnticipo
                    },
                    PaidAmount = new PayableAmount
                    {
                        currencyID = documento.MonedaAnticipo,
                        value = documento.MontoAnticipo
                    },
                    InstructionId = documento.Emisor.NroDocumento
                };
                invoice.LegalMonetaryTotal.PrepaidAmount = new PayableAmount
                {
                    currencyID = documento.MonedaAnticipo,
                    value = documento.MontoAnticipo
                };
            }

            // Datos Adicionales a la Factura.
            foreach (var adicional in documento.DatoAdicionales)
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.AdditionalProperties.Add(new AdditionalProperty
                    {
                        ID = adicional.Codigo,
                        Value = adicional.Contenido
                    });
            }

            if (documento.MontoDetraccion > 0)
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.AdditionalMonetaryTotals.Add(new AdditionalMonetaryTotal
                    {
                        ID = "2003",
                        PayableAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.MontoDetraccion
                        },
                        Percent = documento.CalculoDetraccion * 100
                    });
            }

            // Para datos de Guia de Remision Transportista.
            if (!string.IsNullOrEmpty(documento.DatosGuiaTransportista?.RucTransportista))
            {
                invoice.UblExtensions.Extension2.ExtensionContent
                    .AdditionalInformation.SunatEmbededDespatchAdvice = new SunatEmbededDespatchAdvice
                    {
                        DeliveryAddress = new PostalAddress
                        {
                            ID = documento.DatosGuiaTransportista.DireccionDestino.Ubigeo,
                            StreetName = documento.DatosGuiaTransportista.DireccionDestino.Direccion,
                            CitySubdivisionName = documento.DatosGuiaTransportista.DireccionDestino.Urbanizacion,
                            CityName = documento.DatosGuiaTransportista.DireccionDestino.Departamento,
                            CountrySubentity = documento.DatosGuiaTransportista.DireccionDestino.Provincia,
                            District = documento.DatosGuiaTransportista.DireccionDestino.Distrito,
                            Country = new Country
                            {
                                IdentificationCode = "PE"
                            }
                        },
                        OriginAddress = new PostalAddress
                        {
                            ID = documento.DatosGuiaTransportista.DireccionOrigen.Ubigeo,
                            StreetName = documento.DatosGuiaTransportista.DireccionOrigen.Direccion,
                            CitySubdivisionName = documento.DatosGuiaTransportista.DireccionOrigen.Urbanizacion,
                            CityName = documento.DatosGuiaTransportista.DireccionOrigen.Departamento,
                            CountrySubentity = documento.DatosGuiaTransportista.DireccionOrigen.Provincia,
                            District = documento.DatosGuiaTransportista.DireccionOrigen.Distrito,
                            Country = new Country
                            {
                                IdentificationCode = "PE"
                            }
                        },
                        SunatCarrierParty = new AccountingSupplierParty
                        {
                            CustomerAssignedAccountID = documento.DatosGuiaTransportista.RucTransportista,
                            AdditionalAccountID = "06",
                            Party = new Party
                            {
                                PartyLegalEntity = new PartyLegalEntity
                                {
                                    RegistrationName = documento.DatosGuiaTransportista.NombreTransportista
                                }
                            }
                        },
                        DriverParty = new AgentParty
                        {
                            PartyIdentification = new PartyIdentification
                            {
                                ID = new PartyIdentificationID
                                {
                                    value = documento.DatosGuiaTransportista.NroLicenciaConducir
                                }
                            }
                        },
                        SunatRoadTransport = new SunatRoadTransport
                        {
                            LicensePlateId = documento.DatosGuiaTransportista.PlacaVehiculo,
                            TransportAuthorizationCode = documento.DatosGuiaTransportista.CodigoAutorizacion,
                            BrandName = documento.DatosGuiaTransportista.MarcaVehiculo
                        },
                        TransportModeCode = documento.DatosGuiaTransportista.ModoTransporte,
                        GrossWeightMeasure = new InvoicedQuantity
                        {
                            unitCode = documento.DatosGuiaTransportista.UnidadMedida,
                            Value = documento.DatosGuiaTransportista.PesoBruto
                        }
                    };
            }

            foreach (var detalleDocumento in documento.Items)
            {
                var linea = new InvoiceLine
                {
                    ID = detalleDocumento.Id,
                    ItemClassificationCode = detalleDocumento.ItemClassificationCode,
                    InvoicedQuantity = new InvoicedQuantity
                    {
                        unitCode = detalleDocumento.UnidadMedida,
                        Value = detalleDocumento.Cantidad
                    },
                    LineExtensionAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = detalleDocumento.TotalVenta
                    },
                    PricingReference = new PricingReference
                    {
                        AlternativeConditionPrices = new List<AlternativeConditionPrice>()
                    },
                    Item = new Item
                    {
                        Description = detalleDocumento.Descripcion,
                        SellersItemIdentification = new SellersItemIdentification
                        {
                            ID = detalleDocumento.CodigoItem
                        },
                        AdditionalItemIdentification = new AdditionalItemIdentification
                        {
                            Id = detalleDocumento.PlacaVehiculo
                        }
                        
                    },
                    Price = new Price
                    {
                        PriceAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.PrecioUnitario
                        }
                    },
                    
                };
                /* 16 - Afectación al IGV por ítem */
                if (documento.Gravadas > 0)
                {
                    linea.TaxTotals.Add(new TaxTotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.Impuesto
                        },
                        //Agregado para la nueva version
                        TaxableAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.TotalVenta
                        },
                        TaxSubtotal = new TaxSubtotal
                        {
                            TaxAmount = new PayableAmount
                            {
                                currencyID = documento.Moneda,
                                value = detalleDocumento.Impuesto
                            },
                            TaxCategory = new TaxCategory
                            {
                                Identifier = "S",
                                TaxExemptionReasonCode = detalleDocumento.TipoImpuesto,
                                TaxScheme = new TaxScheme()
                                {
                                    ID = "1000",
                                    Name = "IGV",
                                    TaxTypeCode = "VAT"
                                }
                            }
                        }
                    });
                }
                /* Exoneradas */
                if (documento.Exoneradas > 0)
                {
                    linea.TaxTotals.Add(new TaxTotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.Impuesto
                        },
                        //Agregado para la nueva version
                        TaxableAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.TotalVenta
                        },
                        TaxSubtotal = new TaxSubtotal
                        {
                            TaxAmount = new PayableAmount
                            {
                                currencyID = documento.Moneda,
                                value = detalleDocumento.Impuesto
                            },
                            TaxCategory = new TaxCategory
                            {
                                Identifier = "E",
                                TaxExemptionReasonCode = detalleDocumento.TipoImpuesto,
                                TaxScheme = new TaxScheme()
                                {
                                    ID = "9997",
                                    Name = "EXO",
                                    TaxTypeCode = "VAT"
                                }
                            }
                        }
                    });
                }

                /* 17 - Sistema de ISC por ítem */
                if (detalleDocumento.ImpuestoSelectivo > 0)
                    linea.TaxTotals.Add(new TaxTotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.ImpuestoSelectivo
                        },
                        TaxSubtotal = new TaxSubtotal
                        {
                            TaxAmount = new PayableAmount
                            {
                                currencyID = documento.Moneda,
                                value = detalleDocumento.ImpuestoSelectivo
                            },
                            TaxCategory = new TaxCategory
                            {
                                TaxExemptionReasonCode = detalleDocumento.TipoImpuesto,
                                TierRange = "01",
                                TaxScheme = new TaxScheme()
                                {
                                    ID = "2000",
                                    Name = "ISC",
                                    TaxTypeCode = "EXC"
                                }
                            }
                        }
                    });

                linea.PricingReference.AlternativeConditionPrices.Add(new AlternativeConditionPrice
                {
                    PriceAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        // Comprobamos que sea una operacion gratuita.
                        value = documento.Gratuitas > 0 ? 0 : detalleDocumento.PrecioReferencial
                    },
                    PriceTypeCode = detalleDocumento.TipoPrecio
                });
                // Para operaciones no onerosas (gratuitas)
                if (documento.Gratuitas > 0)
                    linea.PricingReference.AlternativeConditionPrices.Add(new AlternativeConditionPrice
                    {
                        PriceAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.PrecioReferencial
                        },
                        PriceTypeCode = "02"
                    });

                // linea.AditionalItemIdentification.Id = documento.PlacaVehiculo;



                /* 51 - Descuentos por ítem */
                if (detalleDocumento.Descuento > 0)
                {
                    linea.AllowanceCharge.ChargeIndicator = false;
                    linea.AllowanceCharge.Amount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = detalleDocumento.Descuento
                    };
                }

                invoice.InvoiceLines.Add(linea);
            }

            return invoice;
        }
    }
}
