﻿using System;
using System.Collections.Generic;
using FinalXML.Interfaces;
//using WinApp.Comun;
//using WinApp.Comun.Dto.Contratos;
//using WinApp.Comun.Dto.Modelos;
//using WinApp.Estructuras.CommonAggregateComponents;
//using WinApp.Estructuras.CommonBasicComponents;
//using WinApp.Estructuras.CommonExtensionComponents;
//using WinApp.Estructuras.EstandarUbl;
//using WinApp.Estructuras.SunatAggregateComponents;

namespace FinalXML
{
    public class NotaDebitoXml : IDocumentoXml
    {
        IEstructuraXml IDocumentoXml.Generar(IDocumentoElectronico request)
        {
            var documento = (DocumentoElectronico)request;
            documento.MontoEnLetras = Conversion.Enletras(documento.TotalVenta);
            var debitNote = new DebitNote
            {
                UblExtensions = new UBLExtensions
                {
                    Extension2 = new UBLExtension
                    {
                        ExtensionContent = new ExtensionContent
                        {
                            AdditionalInformation = new AdditionalInformation
                            {
                                AdditionalMonetaryTotals = new List<AdditionalMonetaryTotal>()
                                {
                                    new AdditionalMonetaryTotal()
                                    {
                                        ID ="1001",
                                        PayableAmount = new PayableAmount()
                                        {
                                            currencyID = documento.Moneda,
                                            value = documento.Gravadas
                                        }

                                    },
                                    new AdditionalMonetaryTotal {
                                        Percent= (documento.CalculoIgv * 100)
                                    }
                                },

                                AdditionalProperties = new List<AdditionalProperty>()
                                {
                                    new AdditionalProperty
                                    {
                                        ID = "1000",
                                        Value = documento.MontoEnLetras
                                    }
                                }
                            }
                        }
                    }
                },
                Id = documento.IdDocumento,
                IssueDate = DateTime.Parse(documento.FechaEmision),
                DocumentCurrencyCode = documento.Moneda,
                Signature = new SignatureCac
                {
                    ID = documento.IdDocumento,
                    SignatoryParty = new SignatoryParty
                    {
                        PartyIdentification = new PartyIdentification
                        {
                            ID = new PartyIdentificationID
                            {
                                value = documento.Emisor.NroDocumento
                            }
                        },
                        PartyName = new PartyName
                        {
                            Name = documento.Emisor.NombreLegal
                        }
                    },
                    DigitalSignatureAttachment = new DigitalSignatureAttachment
                    {
                        ExternalReference = new ExternalReference
                        {
                            URI = $"{documento.Emisor.NroDocumento}-{documento.IdDocumento}"
                        }
                    }
                },
                AccountingSupplierParty = new AccountingSupplierParty
                {
                    CustomerAssignedAccountID = documento.Emisor.NroDocumento,
                    AdditionalAccountID = documento.Emisor.TipoDocumento,
                    Party = new Party
                    {
                        PartyName = new PartyName
                        {
                            Name = documento.Emisor.NombreComercial
                        },
                        PostalAddress = new PostalAddress
                        {
                            ID = documento.Emisor.Ubigeo,
                            StreetName = documento.Emisor.Direccion,
                            CitySubdivisionName = documento.Emisor.Urbanizacion,
                            CountrySubentity = documento.Emisor.Departamento,
                            CityName = documento.Emisor.Provincia,
                            District = documento.Emisor.Distrito,
                            Country = new Country { IdentificationCode = "PE" }
                        },
                        PartyLegalEntity = new PartyLegalEntity
                        {
                            RegistrationName = documento.Emisor.NombreLegal
                        }
                    }
                },
                AccountingCustomerParty = new AccountingSupplierParty
                {
                    CustomerAssignedAccountID = documento.Receptor.NroDocumento,
                    AdditionalAccountID = documento.Receptor.TipoDocumento,
                    Party = new Party
                    {
                        PartyName = new PartyName
                        {
                            Name = documento.Receptor.NombreComercial
                        },
                        PostalAddress = new PostalAddress
                        {
                            ID = documento.Receptor.Ubigeo,
                            StreetName = documento.Receptor.Direccion,
                            CitySubdivisionName = documento.Receptor.Urbanizacion,
                            CountrySubentity = documento.Receptor.Departamento,
                            CityName = documento.Receptor.Provincia,
                            District = documento.Receptor.Distrito,
                            Country = new Country { IdentificationCode = "PE" }
                        },
                        PartyLegalEntity = new PartyLegalEntity
                        {
                            RegistrationName = documento.Receptor.NombreLegal
                        }
                    }
                },
                UblVersionId = "2.1",
                CustomizationId = "2.0",
                RequestedMonetaryTotal = new LegalMonetaryTotal
                {
                    PayableAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.TotalVenta
                    },
                    AllowanceTotalAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = documento.DescuentoGlobal
                    }
                },
                TaxTotals = new List<TaxTotal>
                {
                    new TaxTotal
                    {
                        TaxableAmount = new PayableAmount{
                            currencyID=documento.Moneda,
                            value=documento.Gravadas
                        },
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = documento.TotalIgv
                        },
                        TaxSubtotal = new TaxSubtotal
                        {
                            TaxAmount = new PayableAmount
                            {
                                currencyID = documento.Moneda,
                                value = documento.TotalIgv,
                            },
                            TaxCategory = new TaxCategory
                            {
                                TaxScheme = new TaxScheme
                                {
                                    ID = "1000",
                                    Name = "IGV",
                                    TaxTypeCode = "VAT"
                                }
                            }
                        }
                    }
                }
            };

            foreach (var discrepancia in documento.Discrepancias)
            {
                debitNote.DiscrepancyResponses.Add(new DiscrepancyResponse
                {
                    ReferenceID = discrepancia.NroReferencia,
                    ResponseCode = discrepancia.Tipo,
                    Description = discrepancia.Descripcion
                });
            }

            foreach (var relacionado in documento.Relacionados)
            {
                debitNote.BillingReferences.Add(new BillingReference
                {
                    InvoiceDocumentReference = new InvoiceDocumentReference
                    {
                        ID = relacionado.NroDocumento,
                        DocumentTypeCode = relacionado.TipoDocumento
                    }
                });
            }

            foreach (var relacionado in documento.OtrosDocumentosRelacionados)
            {
                debitNote.AdditionalDocumentReferences.Add(new InvoiceDocumentReference
                {
                    DocumentTypeCode = relacionado.TipoDocumento,
                    ID = relacionado.NroDocumento
                });
            }

            foreach (var detalleDocumento in documento.Items)
            {
                var linea = new InvoiceLine
                {
                    ID = detalleDocumento.Id,
                    ItemClassificationCode=detalleDocumento.ItemClassificationCode,
                    DebitedQuantity = new InvoicedQuantity
                    {
                        unitCode = detalleDocumento.UnidadMedida,
                        Value = detalleDocumento.Cantidad
                    },
                    LineExtensionAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = detalleDocumento.TotalVenta
                    },
                    PricingReference = new PricingReference
                    {
                        AlternativeConditionPrices = new List<AlternativeConditionPrice>()
                    },
                    Item = new Item
                    {
                        Description = detalleDocumento.Descripcion,
                        SellersItemIdentification = new SellersItemIdentification
                        {
                            ID = detalleDocumento.CodigoItem
                        }
                    },
                    Price = new Price
                    {
                        PriceAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.PrecioUnitario
                        }
                    },
                };
                linea.PricingReference.AlternativeConditionPrices.Add(new AlternativeConditionPrice
                {
                    PriceAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        // Comprobamos que sea una operacion gratuita.
                        value = documento.Gratuitas > 0 ? 0 : detalleDocumento.PrecioReferencial
                    },
                    PriceTypeCode = detalleDocumento.TipoPrecio
                });
                /* 16 - Afectación al IGV por ítem */
                linea.TaxTotals.Add(new TaxTotal
                {
                    TaxAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = detalleDocumento.Impuesto
                    },                   
                    TaxableAmount = new PayableAmount
                    {
                        currencyID = documento.Moneda,
                        value = detalleDocumento.TotalVenta
                    },
                    TaxSubtotal = new TaxSubtotal
                    {
                        TaxAmount = new PayableAmount
                        {
                            currencyID = documento.Moneda,
                            value = detalleDocumento.Impuesto
                        },
                        TaxCategory = new TaxCategory
                        {
                            TaxExemptionReasonCode = detalleDocumento.TipoImpuesto,
                            TaxScheme = new TaxScheme()
                            {
                                ID = "1000",
                                Name = "IGV",
                                TaxTypeCode = "VAT"
                            }
                        }
                    }
                });
                debitNote.DebitNoteLines.Add(linea);
            }

            return debitNote;
        }
    }
}
