﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmdetalle_venta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmdetalle_venta))
        Me.generar_comprobantemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketdscomprobante = New BRAVOSPORT.marketdscomprobante()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.txtserie_documento = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cbtipo_documento = New System.Windows.Forms.ComboBox()
        Me.txtfecha = New System.Windows.Forms.DateTimePicker()
        Me.txtnombre_cliente = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtnum_documento = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtidcliente = New System.Windows.Forms.TextBox()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me.btnbuscar_cliente = New System.Windows.Forms.Button()
        Me.txtstock = New System.Windows.Forms.NumericUpDown()
        Me.txtcantidad = New System.Windows.Forms.NumericUpDown()
        Me.txtprecio_unitario = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtnombre_producto = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtidproducto = New System.Windows.Forms.TextBox()
        Me.btnguardar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnEnviarLuegoSunat = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.btnEnvioEmail = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.btnsalirr = New System.Windows.Forms.Button()
        Me.txtdinero = New System.Windows.Forms.TextBox()
        Me.lblvuelto = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lbltotal = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnEnviarSunat = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnmodificar = New System.Windows.Forms.Button()
        Me.cbosundm = New System.Windows.Forms.ComboBox()
        Me.btncalculadora = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.btnpedido = New System.Windows.Forms.Button()
        Me.chactivar = New System.Windows.Forms.CheckBox()
        Me.txtprecio_unitariom = New System.Windows.Forms.TextBox()
        Me.txtcanttotal = New System.Windows.Forms.TextBox()
        Me.txtcundm = New System.Windows.Forms.TextBox()
        Me.btnbuscarlistaprecio = New System.Windows.Forms.Button()
        Me.cboundm = New System.Windows.Forms.ComboBox()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.txtbuscar = New System.Windows.Forms.TextBox()
        Me.cbocampo = New System.Windows.Forms.ComboBox()
        Me.cboproduct = New System.Windows.Forms.ComboBox()
        Me.ProductoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Mmdsnombre = New BRAVOSPORT.mmdsnombre()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtpcompra = New System.Windows.Forms.TextBox()
        Me.txtflag = New System.Windows.Forms.TextBox()
        Me.ProductoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MarketmayoristaDataSet = New BRAVOSPORT.marketmayoristaDataSet()
        Me.MarketDataSetdosfechas = New BRAVOSPORT.marketDataSetdosfechas()
        Me.erroricono = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtidusuario = New System.Windows.Forms.TextBox()
        Me.txtusuario = New System.Windows.Forms.TextBox()
        Me.generar_comprobantemTableAdapter = New BRAVOSPORT.marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter()
        Me.recibos = New BRAVOSPORT.recibos()
        Me.MarketDataSetticket1 = New BRAVOSPORT.marketDataSetticket()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Generar_comprobanteTableAdapter1 = New BRAVOSPORT.marketDataSetticketTableAdapters.generar_comprobanteTableAdapter()
        Me.generar_comprobanteTableAdapter = New BRAVOSPORT.marketDataSetticketTableAdapters.generar_comprobanteTableAdapter()
        Me.generar_comprobanteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketDataSetticket = New BRAVOSPORT.marketDataSetticket()
        Me.ProductoTableAdapter = New BRAVOSPORT.marketmayoristaDataSetTableAdapters.productoTableAdapter()
        Me.Generar_comprobantemTableAdapter1 = New BRAVOSPORT.recibosTableAdapters.generar_comprobantemTableAdapter()
        Me.ProductoTableAdapter1 = New BRAVOSPORT.mmdsnombreTableAdapters.productoTableAdapter()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtstock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtcantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Mmdsnombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketmayoristaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketDataSetdosfechas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketDataSetticket1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.generar_comprobanteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketDataSetticket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'generar_comprobantemBindingSource
        '
        Me.generar_comprobantemBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobantemBindingSource.DataSource = Me.marketdscomprobante
        '
        'marketdscomprobante
        '
        Me.marketdscomprobante.DataSetName = "marketdscomprobante"
        Me.marketdscomprobante.EnforceConstraints = False
        Me.marketdscomprobante.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ReportViewer1)
        Me.GroupBox1.Controls.Add(Me.txtserie_documento)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.cbtipo_documento)
        Me.GroupBox1.Controls.Add(Me.txtfecha)
        Me.GroupBox1.Controls.Add(Me.txtnombre_cliente)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtnum_documento)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtidcliente)
        Me.GroupBox1.Controls.Add(Me.txtidventa)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox1.Location = New System.Drawing.Point(12, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(363, 575)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'ReportViewer1
        '
        Me.ReportViewer1.LocalReport.EnableExternalImages = True
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.ticket.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(13, 17)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(344, 539)
        Me.ReportViewer1.TabIndex = 24
        '
        'txtserie_documento
        '
        Me.txtserie_documento.Location = New System.Drawing.Point(95, 251)
        Me.txtserie_documento.Name = "txtserie_documento"
        Me.txtserie_documento.Size = New System.Drawing.Size(124, 25)
        Me.txtserie_documento.TabIndex = 48
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label10.Location = New System.Drawing.Point(23, 287)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 16)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Numero :"
        '
        'cbtipo_documento
        '
        Me.cbtipo_documento.Enabled = False
        Me.cbtipo_documento.FormattingEnabled = True
        Me.cbtipo_documento.Items.AddRange(New Object() {"Acuenta", "Boleta", "Factura"})
        Me.cbtipo_documento.Location = New System.Drawing.Point(95, 213)
        Me.cbtipo_documento.Name = "cbtipo_documento"
        Me.cbtipo_documento.Size = New System.Drawing.Size(124, 26)
        Me.cbtipo_documento.TabIndex = 19
        Me.cbtipo_documento.Text = "Boleta"
        '
        'txtfecha
        '
        Me.txtfecha.Enabled = False
        Me.txtfecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfecha.Location = New System.Drawing.Point(95, 182)
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.Size = New System.Drawing.Size(124, 25)
        Me.txtfecha.TabIndex = 18
        '
        'txtnombre_cliente
        '
        Me.txtnombre_cliente.Enabled = False
        Me.txtnombre_cliente.Location = New System.Drawing.Point(79, 88)
        Me.txtnombre_cliente.Name = "txtnombre_cliente"
        Me.txtnombre_cliente.Size = New System.Drawing.Size(225, 25)
        Me.txtnombre_cliente.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label5.Location = New System.Drawing.Point(23, 255)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 16)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Serie :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(23, 223)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Comp :"
        '
        'txtnum_documento
        '
        Me.txtnum_documento.Location = New System.Drawing.Point(95, 283)
        Me.txtnum_documento.Name = "txtnum_documento"
        Me.txtnum_documento.Size = New System.Drawing.Size(124, 25)
        Me.txtnum_documento.TabIndex = 40
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label3.Location = New System.Drawing.Point(21, 197)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Fecha :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(12, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Cliente :"
        '
        'txtidcliente
        '
        Me.txtidcliente.Enabled = False
        Me.txtidcliente.Location = New System.Drawing.Point(209, 88)
        Me.txtidcliente.Name = "txtidcliente"
        Me.txtidcliente.Size = New System.Drawing.Size(29, 25)
        Me.txtidcliente.TabIndex = 2
        '
        'txtidventa
        '
        Me.txtidventa.Enabled = False
        Me.txtidventa.Location = New System.Drawing.Point(79, 57)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(124, 25)
        Me.txtidventa.TabIndex = 0
        '
        'btnbuscar_cliente
        '
        Me.btnbuscar_cliente.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btnbuscar_cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscar_cliente.ForeColor = System.Drawing.Color.White
        Me.btnbuscar_cliente.Location = New System.Drawing.Point(195, 21)
        Me.btnbuscar_cliente.Name = "btnbuscar_cliente"
        Me.btnbuscar_cliente.Size = New System.Drawing.Size(176, 27)
        Me.btnbuscar_cliente.TabIndex = 23
        Me.btnbuscar_cliente.Text = "SELECCIONAR CLIENTE"
        Me.btnbuscar_cliente.UseVisualStyleBackColor = False
        '
        'txtstock
        '
        Me.txtstock.Enabled = False
        Me.txtstock.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstock.Location = New System.Drawing.Point(835, 42)
        Me.txtstock.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.txtstock.Name = "txtstock"
        Me.txtstock.Size = New System.Drawing.Size(69, 34)
        Me.txtstock.TabIndex = 29
        Me.txtstock.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtcantidad
        '
        Me.txtcantidad.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcantidad.Location = New System.Drawing.Point(764, 43)
        Me.txtcantidad.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.txtcantidad.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtcantidad.Name = "txtcantidad"
        Me.txtcantidad.Size = New System.Drawing.Size(61, 34)
        Me.txtcantidad.TabIndex = 28
        Me.txtcantidad.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtprecio_unitario
        '
        Me.txtprecio_unitario.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprecio_unitario.ForeColor = System.Drawing.Color.Magenta
        Me.txtprecio_unitario.Location = New System.Drawing.Point(694, 44)
        Me.txtprecio_unitario.Name = "txtprecio_unitario"
        Me.txtprecio_unitario.Size = New System.Drawing.Size(62, 34)
        Me.txtprecio_unitario.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label9.Location = New System.Drawing.Point(692, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 19)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "P.Venta :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(767, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 19)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Cant:"
        '
        'txtnombre_producto
        '
        Me.txtnombre_producto.Location = New System.Drawing.Point(59, 44)
        Me.txtnombre_producto.Name = "txtnombre_producto"
        Me.txtnombre_producto.Size = New System.Drawing.Size(627, 25)
        Me.txtnombre_producto.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(409, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 19)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Producto  :"
        '
        'txtidproducto
        '
        Me.txtidproducto.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidproducto.Location = New System.Drawing.Point(980, 33)
        Me.txtidproducto.Name = "txtidproducto"
        Me.txtidproducto.Size = New System.Drawing.Size(61, 20)
        Me.txtidproducto.TabIndex = 20
        Me.txtidproducto.Visible = False
        '
        'btnguardar
        '
        Me.btnguardar.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnguardar.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnguardar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnguardar.Location = New System.Drawing.Point(16, 71)
        Me.btnguardar.Name = "btnguardar"
        Me.btnguardar.Size = New System.Drawing.Size(171, 47)
        Me.btnguardar.TabIndex = 15
        Me.btnguardar.Text = "Agregar Articulos"
        Me.btnguardar.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnEnviarLuegoSunat)
        Me.GroupBox2.Controls.Add(Me.txtnombre_producto)
        Me.GroupBox2.Controls.Add(Me.GroupBox5)
        Me.GroupBox2.Controls.Add(Me.btnsalirr)
        Me.GroupBox2.Controls.Add(Me.txtdinero)
        Me.GroupBox2.Controls.Add(Me.lblvuelto)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.lbltotal)
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.cbosundm)
        Me.GroupBox2.Controls.Add(Me.btncalculadora)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.btnpedido)
        Me.GroupBox2.Controls.Add(Me.chactivar)
        Me.GroupBox2.Controls.Add(Me.txtprecio_unitariom)
        Me.GroupBox2.Controls.Add(Me.txtcanttotal)
        Me.GroupBox2.Controls.Add(Me.txtcundm)
        Me.GroupBox2.Controls.Add(Me.btnbuscarlistaprecio)
        Me.GroupBox2.Controls.Add(Me.cboundm)
        Me.GroupBox2.Controls.Add(Me.btnguardar)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.txtbuscar)
        Me.GroupBox2.Controls.Add(Me.cbocampo)
        Me.GroupBox2.Controls.Add(Me.cboproduct)
        Me.GroupBox2.Controls.Add(Me.txtprecio_unitario)
        Me.GroupBox2.Controls.Add(Me.txtstock)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtcantidad)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtpcompra)
        Me.GroupBox2.Controls.Add(Me.txtflag)
        Me.GroupBox2.Controls.Add(Me.txtidproducto)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox2.Location = New System.Drawing.Point(381, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1070, 609)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "VENTAS"
        '
        'btnEnviarLuegoSunat
        '
        Me.btnEnviarLuegoSunat.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarLuegoSunat.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarLuegoSunat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnEnviarLuegoSunat.Location = New System.Drawing.Point(753, 495)
        Me.btnEnviarLuegoSunat.Name = "btnEnviarLuegoSunat"
        Me.btnEnviarLuegoSunat.Size = New System.Drawing.Size(160, 70)
        Me.btnEnviarLuegoSunat.TabIndex = 84
        Me.btnEnviarLuegoSunat.Text = "Enviar luego a Sunat"
        Me.btnEnviarLuegoSunat.UseVisualStyleBackColor = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lblEmail)
        Me.GroupBox5.Controls.Add(Me.btnEnvioEmail)
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Controls.Add(Me.txtEmail)
        Me.GroupBox5.Location = New System.Drawing.Point(17, 527)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(739, 74)
        Me.GroupBox5.TabIndex = 86
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Envío Comprobante al cliente"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblEmail.Location = New System.Drawing.Point(14, 56)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(0, 15)
        Me.lblEmail.TabIndex = 3
        '
        'btnEnvioEmail
        '
        Me.btnEnvioEmail.Location = New System.Drawing.Point(609, 26)
        Me.btnEnvioEmail.Name = "btnEnvioEmail"
        Me.btnEnvioEmail.Size = New System.Drawing.Size(114, 28)
        Me.btnEnvioEmail.TabIndex = 2
        Me.btnEnvioEmail.Text = "ENVIAR"
        Me.btnEnvioEmail.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Email:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(78, 28)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(525, 25)
        Me.txtEmail.TabIndex = 0
        '
        'btnsalirr
        '
        Me.btnsalirr.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnsalirr.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsalirr.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnsalirr.Location = New System.Drawing.Point(753, 422)
        Me.btnsalirr.Name = "btnsalirr"
        Me.btnsalirr.Size = New System.Drawing.Size(160, 70)
        Me.btnsalirr.TabIndex = 20
        Me.btnsalirr.Text = "Nueva Venta"
        Me.btnsalirr.UseVisualStyleBackColor = False
        '
        'txtdinero
        '
        Me.txtdinero.ForeColor = System.Drawing.Color.Red
        Me.txtdinero.Location = New System.Drawing.Point(508, 459)
        Me.txtdinero.Name = "txtdinero"
        Me.txtdinero.Size = New System.Drawing.Size(75, 25)
        Me.txtdinero.TabIndex = 64
        '
        'lblvuelto
        '
        Me.lblvuelto.AutoSize = True
        Me.lblvuelto.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblvuelto.ForeColor = System.Drawing.Color.Fuchsia
        Me.lblvuelto.Location = New System.Drawing.Point(513, 491)
        Me.lblvuelto.Name = "lblvuelto"
        Me.lblvuelto.Size = New System.Drawing.Size(26, 29)
        Me.lblvuelto.TabIndex = 63
        Me.lblvuelto.Text = "0"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Fuchsia
        Me.Label15.Location = New System.Drawing.Point(407, 495)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(104, 24)
        Me.Label15.TabIndex = 61
        Me.Label15.Text = "VUELTO :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(408, 461)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 22)
        Me.Label14.TabIndex = 60
        Me.Label14.Text = "DINERO :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label12.Location = New System.Drawing.Point(407, 427)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 24)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "PAGAR :"
        '
        'lbltotal
        '
        Me.lbltotal.AutoSize = True
        Me.lbltotal.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lbltotal.Location = New System.Drawing.Point(504, 427)
        Me.lbltotal.Name = "lbltotal"
        Me.lbltotal.Size = New System.Drawing.Size(26, 29)
        Me.lbltotal.TabIndex = 31
        Me.lbltotal.Text = "0"
        '
        'GroupBox4
        '
        Me.GroupBox4.Location = New System.Drawing.Point(397, 427)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(243, 102)
        Me.GroupBox4.TabIndex = 85
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "GroupBox4"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnEnviarSunat)
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.btnmodificar)
        Me.GroupBox3.Location = New System.Drawing.Point(14, 419)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(377, 80)
        Me.GroupBox3.TabIndex = 84
        Me.GroupBox3.TabStop = False
        '
        'btnEnviarSunat
        '
        Me.btnEnviarSunat.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarSunat.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarSunat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnEnviarSunat.Location = New System.Drawing.Point(183, 4)
        Me.btnEnviarSunat.Name = "btnEnviarSunat"
        Me.btnEnviarSunat.Size = New System.Drawing.Size(176, 70)
        Me.btnEnviarSunat.TabIndex = 83
        Me.btnEnviarSunat.Text = "Enviar Sunat"
        Me.btnEnviarSunat.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Highlight
        Me.Button1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button1.Location = New System.Drawing.Point(5, 4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(172, 69)
        Me.Button1.TabIndex = 30
        Me.Button1.Text = "Quitar Articulos"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnmodificar
        '
        Me.btnmodificar.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnmodificar.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnmodificar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnmodificar.Location = New System.Drawing.Point(183, 41)
        Me.btnmodificar.Name = "btnmodificar"
        Me.btnmodificar.Size = New System.Drawing.Size(10, 40)
        Me.btnmodificar.TabIndex = 57
        Me.btnmodificar.Text = "Modificar"
        Me.btnmodificar.UseVisualStyleBackColor = False
        '
        'cbosundm
        '
        Me.cbosundm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbosundm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbosundm.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbosundm.FormattingEnabled = True
        Me.cbosundm.Items.AddRange(New Object() {"1/2", "1/4", "1", "2", "3", "4", "5", "6"})
        Me.cbosundm.Location = New System.Drawing.Point(119, 44)
        Me.cbosundm.Name = "cbosundm"
        Me.cbosundm.Size = New System.Drawing.Size(57, 24)
        Me.cbosundm.TabIndex = 81
        '
        'btncalculadora
        '
        Me.btncalculadora.BackgroundImage = CType(resources.GetObject("btncalculadora.BackgroundImage"), System.Drawing.Image)
        Me.btncalculadora.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btncalculadora.Location = New System.Drawing.Point(14, 17)
        Me.btncalculadora.Name = "btncalculadora"
        Me.btncalculadora.Size = New System.Drawing.Size(39, 56)
        Me.btncalculadora.TabIndex = 80
        Me.btncalculadora.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label18.Location = New System.Drawing.Point(59, 44)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(60, 19)
        Me.Label18.TabIndex = 77
        Me.Label18.Text = "Ctotal:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label16.Location = New System.Drawing.Point(195, 45)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(75, 19)
        Me.Label16.TabIndex = 76
        Me.Label16.Text = "Cundm :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label17.Location = New System.Drawing.Point(124, 44)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(65, 19)
        Me.Label17.TabIndex = 75
        Me.Label17.Text = "Undm :"
        '
        'btnpedido
        '
        Me.btnpedido.Location = New System.Drawing.Point(968, 593)
        Me.btnpedido.Name = "btnpedido"
        Me.btnpedido.Size = New System.Drawing.Size(10, 15)
        Me.btnpedido.TabIndex = 66
        Me.btnpedido.Text = "ImPedido"
        Me.btnpedido.UseVisualStyleBackColor = True
        '
        'chactivar
        '
        Me.chactivar.AutoSize = True
        Me.chactivar.Checked = True
        Me.chactivar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chactivar.Location = New System.Drawing.Point(771, 91)
        Me.chactivar.Name = "chactivar"
        Me.chactivar.Size = New System.Drawing.Size(79, 22)
        Me.chactivar.TabIndex = 73
        Me.chactivar.Text = "Pmayor"
        Me.chactivar.UseVisualStyleBackColor = True
        '
        'txtprecio_unitariom
        '
        Me.txtprecio_unitariom.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprecio_unitariom.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtprecio_unitariom.Location = New System.Drawing.Point(694, 84)
        Me.txtprecio_unitariom.Name = "txtprecio_unitariom"
        Me.txtprecio_unitariom.Size = New System.Drawing.Size(62, 34)
        Me.txtprecio_unitariom.TabIndex = 72
        '
        'txtcanttotal
        '
        Me.txtcanttotal.Location = New System.Drawing.Point(59, 43)
        Me.txtcanttotal.Name = "txtcanttotal"
        Me.txtcanttotal.Size = New System.Drawing.Size(59, 25)
        Me.txtcanttotal.TabIndex = 59
        '
        'txtcundm
        '
        Me.txtcundm.Location = New System.Drawing.Point(236, 44)
        Me.txtcundm.Name = "txtcundm"
        Me.txtcundm.Size = New System.Drawing.Size(55, 25)
        Me.txtcundm.TabIndex = 58
        '
        'btnbuscarlistaprecio
        '
        Me.btnbuscarlistaprecio.Location = New System.Drawing.Point(856, 82)
        Me.btnbuscarlistaprecio.Name = "btnbuscarlistaprecio"
        Me.btnbuscarlistaprecio.Size = New System.Drawing.Size(57, 36)
        Me.btnbuscarlistaprecio.TabIndex = 54
        Me.btnbuscarlistaprecio.Text = "..."
        Me.btnbuscarlistaprecio.UseVisualStyleBackColor = True
        '
        'cboundm
        '
        Me.cboundm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboundm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboundm.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboundm.FormattingEnabled = True
        Me.cboundm.Items.AddRange(New Object() {"UND", "DOC", "/2doc", "/4doc", "TR", "/2Tr", "PQ", "/2Pq", "CAJ", "/2Caj", "DSP", "BLS", "KG", "ZC", "/2Zc", "FRS", "TRO"})
        Me.cboundm.Location = New System.Drawing.Point(178, 44)
        Me.cboundm.Name = "cboundm"
        Me.cboundm.Size = New System.Drawing.Size(57, 24)
        Me.cboundm.TabIndex = 56
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.inexistente.ForeColor = System.Drawing.Color.Red
        Me.inexistente.Location = New System.Drawing.Point(346, 233)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(170, 22)
        Me.inexistente.TabIndex = 52
        Me.inexistente.Text = "Datos Inexistente"
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Location = New System.Drawing.Point(16, 119)
        Me.datalistado.MultiSelect = False
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(897, 294)
        Me.datalistado.TabIndex = 51
        '
        'txtbuscar
        '
        Me.txtbuscar.Location = New System.Drawing.Point(27, 45)
        Me.txtbuscar.Name = "txtbuscar"
        Me.txtbuscar.Size = New System.Drawing.Size(10, 25)
        Me.txtbuscar.TabIndex = 41
        '
        'cbocampo
        '
        Me.cbocampo.FormattingEnabled = True
        Me.cbocampo.Items.AddRange(New Object() {"codigo", "codigom"})
        Me.cbocampo.Location = New System.Drawing.Point(16, 44)
        Me.cbocampo.Name = "cbocampo"
        Me.cbocampo.Size = New System.Drawing.Size(10, 26)
        Me.cbocampo.TabIndex = 40
        Me.cbocampo.Text = "codigo"
        '
        'cboproduct
        '
        Me.cboproduct.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboproduct.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboproduct.DataSource = Me.ProductoBindingSource1
        Me.cboproduct.DisplayMember = "nombre"
        Me.cboproduct.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboproduct.FormattingEnabled = True
        Me.cboproduct.Location = New System.Drawing.Point(194, 82)
        Me.cboproduct.Name = "cboproduct"
        Me.cboproduct.Size = New System.Drawing.Size(492, 26)
        Me.cboproduct.TabIndex = 0
        '
        'ProductoBindingSource1
        '
        Me.ProductoBindingSource1.DataMember = "producto"
        Me.ProductoBindingSource1.DataSource = Me.Mmdsnombre
        '
        'Mmdsnombre
        '
        Me.Mmdsnombre.DataSetName = "mmdsnombre"
        Me.Mmdsnombre.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(831, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 19)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Stock :"
        '
        'txtpcompra
        '
        Me.txtpcompra.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpcompra.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtpcompra.Location = New System.Drawing.Point(980, 119)
        Me.txtpcompra.Name = "txtpcompra"
        Me.txtpcompra.Size = New System.Drawing.Size(74, 21)
        Me.txtpcompra.TabIndex = 82
        Me.txtpcompra.Text = "1"
        Me.txtpcompra.Visible = False
        '
        'txtflag
        '
        Me.txtflag.Location = New System.Drawing.Point(980, 67)
        Me.txtflag.Name = "txtflag"
        Me.txtflag.Size = New System.Drawing.Size(67, 25)
        Me.txtflag.TabIndex = 55
        Me.txtflag.Visible = False
        '
        'ProductoBindingSource
        '
        Me.ProductoBindingSource.DataMember = "producto"
        Me.ProductoBindingSource.DataSource = Me.MarketmayoristaDataSet
        '
        'MarketmayoristaDataSet
        '
        Me.MarketmayoristaDataSet.DataSetName = "marketmayoristaDataSet"
        Me.MarketmayoristaDataSet.EnforceConstraints = False
        Me.MarketmayoristaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketDataSetdosfechas
        '
        Me.MarketDataSetdosfechas.DataSetName = "marketDataSetdosfechas"
        Me.MarketDataSetdosfechas.EnforceConstraints = False
        Me.MarketDataSetdosfechas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'erroricono
        '
        Me.erroricono.ContainerControl = Me
        '
        'txtidusuario
        '
        Me.txtidusuario.Enabled = False
        Me.txtidusuario.Location = New System.Drawing.Point(18, -4)
        Me.txtidusuario.Name = "txtidusuario"
        Me.txtidusuario.Size = New System.Drawing.Size(10, 20)
        Me.txtidusuario.TabIndex = 6
        Me.txtidusuario.Visible = False
        '
        'txtusuario
        '
        Me.txtusuario.Enabled = False
        Me.txtusuario.Location = New System.Drawing.Point(32, -4)
        Me.txtusuario.Name = "txtusuario"
        Me.txtusuario.Size = New System.Drawing.Size(10, 20)
        Me.txtusuario.TabIndex = 7
        Me.txtusuario.Visible = False
        '
        'generar_comprobantemTableAdapter
        '
        Me.generar_comprobantemTableAdapter.ClearBeforeFill = True
        '
        'recibos
        '
        Me.recibos.DataSetName = "recibos"
        Me.recibos.EnforceConstraints = False
        Me.recibos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketDataSetticket1
        '
        Me.MarketDataSetticket1.DataSetName = "marketDataSetticket"
        Me.MarketDataSetticket1.EnforceConstraints = False
        Me.MarketDataSetticket1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "generar_comprobante"
        Me.BindingSource1.DataSource = Me.MarketDataSetticket1
        '
        'Generar_comprobanteTableAdapter1
        '
        Me.Generar_comprobanteTableAdapter1.ClearBeforeFill = True
        '
        'generar_comprobanteTableAdapter
        '
        Me.generar_comprobanteTableAdapter.ClearBeforeFill = True
        '
        'generar_comprobanteBindingSource
        '
        Me.generar_comprobanteBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobanteBindingSource.DataSource = Me.recibos
        '
        'marketDataSetticket
        '
        Me.marketDataSetticket.DataSetName = "marketDataSetticket"
        Me.marketDataSetticket.EnforceConstraints = False
        Me.marketDataSetticket.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProductoTableAdapter
        '
        Me.ProductoTableAdapter.ClearBeforeFill = True
        '
        'Generar_comprobantemTableAdapter1
        '
        Me.Generar_comprobantemTableAdapter1.ClearBeforeFill = True
        '
        'ProductoTableAdapter1
        '
        Me.ProductoTableAdapter1.ClearBeforeFill = True
        '
        'frmdetalle_venta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(1370, 633)
        Me.Controls.Add(Me.btnbuscar_cliente)
        Me.Controls.Add(Me.txtusuario)
        Me.Controls.Add(Me.txtidusuario)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.ForeColor = System.Drawing.Color.Cornsilk
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmdetalle_venta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VENTAS-REALIZAR VENTAS"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtstock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtcantidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Mmdsnombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketmayoristaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketDataSetdosfechas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketDataSetticket1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.generar_comprobanteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketDataSetticket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbtipo_documento As System.Windows.Forms.ComboBox
    Friend WithEvents txtfecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtnombre_cliente As System.Windows.Forms.TextBox
    Friend WithEvents btnguardar As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtnum_documento As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtidcliente As System.Windows.Forms.TextBox
    Friend WithEvents txtidventa As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents erroricono As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtstock As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtcantidad As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtprecio_unitario As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtnombre_producto As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtidproducto As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnsalirr As System.Windows.Forms.Button
    Friend WithEvents lbltotal As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboproduct As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtserie_documento As System.Windows.Forms.TextBox
    Friend WithEvents txtusuario As System.Windows.Forms.TextBox
    Friend WithEvents txtidusuario As System.Windows.Forms.TextBox
    Friend WithEvents cbocampo As System.Windows.Forms.ComboBox
    Friend WithEvents txtbuscar As System.Windows.Forms.TextBox
    Friend WithEvents btnbuscar_cliente As System.Windows.Forms.Button
    Friend WithEvents inexistente As System.Windows.Forms.Label
    Friend WithEvents datalistado As System.Windows.Forms.DataGridView
    Friend WithEvents btnbuscarlistaprecio As Button
    Friend WithEvents txtflag As TextBox
    Friend WithEvents cboundm As ComboBox
    Friend WithEvents btnmodificar As Button
    Friend WithEvents txtcundm As TextBox
    Friend WithEvents txtcanttotal As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtdinero As TextBox
    Friend WithEvents lblvuelto As Label
    Friend WithEvents btnpedido As Button
    Friend WithEvents txtprecio_unitariom As TextBox
    Friend WithEvents chactivar As CheckBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents generar_comprobantemTableAdapter As marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents Label18 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents btncalculadora As Button
    Friend WithEvents cbosundm As ComboBox
    Friend WithEvents MarketDataSetdosfechas As marketDataSetdosfechas
    Friend WithEvents recibos As recibos
    Friend WithEvents txtpcompra As TextBox
    Friend WithEvents MarketDataSetticket1 As marketDataSetticket
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents Generar_comprobanteTableAdapter1 As marketDataSetticketTableAdapters.generar_comprobanteTableAdapter
    Friend WithEvents marketdscomprobante As marketdscomprobante
    Friend WithEvents generar_comprobantemBindingSource As BindingSource
    Friend WithEvents generar_comprobanteTableAdapter As marketDataSetticketTableAdapters.generar_comprobanteTableAdapter
    Friend WithEvents generar_comprobanteBindingSource As BindingSource
    Friend WithEvents marketDataSetticket As marketDataSetticket
    Friend WithEvents MarketmayoristaDataSet As marketmayoristaDataSet
    Friend WithEvents ProductoBindingSource As BindingSource
    Friend WithEvents ProductoTableAdapter As marketmayoristaDataSetTableAdapters.productoTableAdapter
    Private WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents btnEnviarSunat As Button
    Friend WithEvents Generar_comprobantemTableAdapter1 As recibosTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents btnEnvioEmail As Button
    Friend WithEvents lblEmail As Label
    Friend WithEvents Mmdsnombre As mmdsnombre
    Friend WithEvents ProductoBindingSource1 As BindingSource
    Friend WithEvents ProductoTableAdapter1 As mmdsnombreTableAdapters.productoTableAdapter
    Friend WithEvents btnEnviarLuegoSunat As Button
End Class
