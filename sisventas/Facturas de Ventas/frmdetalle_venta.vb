﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System
Imports FinalXML
Imports FinalXML.Interfaces
Imports FinalXML.InterMySql
Imports Microsoft.Reporting.WinForms
Imports Gma.QrCodeNet.Encoding
Imports Gma.QrCodeNet.Encoding.Windows.Render
Imports System.Drawing.Imaging
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading.Tasks

Public Class frmdetalle_venta

    Shared conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Private dt As New DataTable
    Private dtn As New DataTable

    Dim SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Dim SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Dim SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Shared USEREMAIL As String = ConfigurationManager.AppSettings.Get("USEREMAIL")

    Shared NROCAJAFE As String = ConfigurationManager.AppSettings.Get("NROCAJAFE")

    Shared rutaArchivoXmlEmail As String = ""
    Shared rutaPdfEmail As String = ""

    Dim _documento As DocumentoElectronico
    Dim ConvertLetras As Conversion = New Conversion
    Dim herramientas As Herramientas = New Herramientas

    Dim RutaArchivo As String
    Shared recursos As String
    Shared cadenaConexion As String = ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString

    'Public Property RutaArchivo As String
    '    Get
    '    End Get
    '    Set
    '    End Set
    'End Property

    Private Sub mostrar_detalleventa()
        Try
            Dim func As New fdetalle_venta
            dt = func.mostrar_detalleventa

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
                ocultar_columnas()

            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


    End Sub

    Private Sub frmdetalle_venta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Mmdsnombre.producto' Puede moverla o quitarla según sea necesario.
        Me.ProductoTableAdapter1.Fill(Me.Mmdsnombre.producto)


        recursos = herramientas.GetResourcesPath()

        txtbuscar.Focus()
        txtdinero.Text = 0

        txtcanttotal.Text = "1"
        txtcundm.Text = "1"
        cboundm.Text = "UND"
        cbosundm.Text = "1"

        mostrarusuario()
        mostrar_detalleventa()
        txtidcliente.Text = "3067"
        txtnombre_cliente.Text = "CLIENTE CASUAL"
        txtserie_documento.Text = DateTime.Now.ToShortTimeString()
        txtnum_documento.Text = DateTime.Now.ToString()


        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("ImporteVenta").Value IsNot Nothing) AndAlso
                    (row.Cells("ImporteVenta").Value IsNot DBNull.Value))
                Select row.Cells("ImporteVenta").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
            lbltotal.Text = String.Format(resultado)



        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        Try
            Dim dts As New vventa
            Dim func As New fventa

            dts.gidcliente = txtidcliente.Text
            dts.gfecha_venta = txtfecha.Value
            dts.gtipo_documento = cbtipo_documento.Text
            dts.gnum_documento = txtnum_documento.Text
            dts.gidusuario = txtidusuario.Text
            dts.gserie_documento = txtserie_documento.Text

            If func.insertar(dts) Then

                ' MessageBox.Show("Comprobante Generado", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ' mostrar()


                Dim SQL As String
                Dim MiConexion As New SqlConnection(cadenaConexion)
                Dim Rs As SqlDataReader
                Dim Com As New SqlCommand

                Com.Connection = MiConexion
                MiConexion.Open()

                SQL = "select top 1 idventa from ventas order by idventa desc"
                Com = New SqlCommand(SQL, MiConexion)

                Rs = Com.ExecuteReader()

                Rs.Read()
                txtidventa.Text = Rs(0) 'aca me pone el primer campo del select 

                Rs.Close()

            Else
                MessageBox.Show("Venta no fue registrada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'mostrar()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)

        Dim ReportDataSource As ReportDataSource = New ReportDataSource()
        ReportDataSource.Value = generar_comprobantemBindingSource
        ReportDataSource.Name = "recibos"
        ReportViewer1.LocalReport.DataSources.Add(ReportDataSource)

        Me.ReportViewer1.RefreshReport()

        btnEnviarSunat.Enabled = True

    End Sub
    Public Sub limpiar()
        txtidproducto.Text = ""
        txtnombre_producto.Text = ""
        txtprecio_unitario.Text = ""
        txtcantidad.Text = 1
        txtstock.Text = 0
    End Sub
    Private Sub mostrarusuario()

        Dim func As New fusuario
        dt = func.mostrarusu

    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fdetalle_venta
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



        buscar()

    End Sub

    Private Sub buscar()
        Try
            Dim ds As New DataSet
            ds.Tables.Add(dt.Copy)
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = "idventa=  '" & txtidventa.Text & "'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()
            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ocultar_columnas()
        datalistado.Columns(0).Visible = False
        datalistado.Columns(1).Visible = False
        datalistado.Columns(2).Visible = False

    End Sub



    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        limpiar()
        mostrar()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim result As DialogResult
        ' = MessageBox.Show("Realmente desea quitar los articulos de venta?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        ' result = DialogResult.OK Then

        Try
            For Each row As DataGridViewRow In datalistado.Rows
                'Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)
                Dim marcado As Boolean = Convert.ToBoolean(row.Cells(0).Value)
                If marcado Then
                    'Dim onekey As Integer = Convert.ToInt32(row.Cells("iddetalle_venta").Value)
                    Dim onekey As Integer = Convert.ToInt32(datalistado.CurrentRow.Cells("iddetalle_venta").Value)
                    Dim vdb As New vdetalle_venta
                    Dim func As New fdetalle_venta

                    vdb.giddetalle_venta = onekey
                    vdb.gidventa = datalistado.SelectedCells.Item(1).Value
                    vdb.gidproducto = datalistado.SelectedCells.Item(2).Value
                    vdb.gcantidad = datalistado.SelectedCells.Item(4).Value

                    If func.eliminar(vdb) Then
                        If func.aumentar_stock(vdb) Then

                        End If
                    Else
                        ' MessageBox.Show("Articulo fue quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                End If
            Next

            Call mostrar_detalleventa()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'Else
        'MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Call mostrar()
        'End If
        Try
            Dim query As IEnumerable(Of Object) =
                    From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                    Where (
                        (row.Cells("subtotal").Value IsNot Nothing) AndAlso
                        (row.Cells("subtotal").Value IsNot DBNull.Value)) Select row.Cells("subtotal").Value()

            Dim resultado As Decimal =
                    query.Sum(Function(row) Convert.ToDecimal(row))
            lbltotal.Text = String.Format(resultado)





            frmproducto.btneditar.PerformClick()

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
        Try
            Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)
            Me.ReportViewer1.RefreshReport()




        Catch ex As Exception
            Me.ReportViewer1.RefreshReport()
        End Try
        txtidproducto.Text = ""
        txtnombre_producto.Text = ""
        txtprecio_unitario.Text = ""
        mostrar_detalleventa()

        lblvuelto.Text = txtdinero.Text - lbltotal.Text
    End Sub
    Private Sub btnguardar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If txtcantidad.Value > 1 Then
            cbosundm.Text = txtcantidad.Value

        End If

        txtcanttotal.Text = txtcantidad.Value * txtcundm.Text

        If txtcantidad.Value > txtstock.Value Then

            btnguardar.Visible = 1

        Else
            btnguardar.Visible = 1

            If Me.ValidateChildren = True And txtidproducto.Text <> "" And txtcantidad.Text <> "" And txtprecio_unitario.Text <> "" Then

                If cbosundm.Text.Equals("") Then
                    MessageBox.Show("Seleccione el tipo de unidad de medida del producto", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'btnbuscarlistaprecio.Select()
                    btnbuscarlistaprecio.BackColor = Color.Red
                Else
                    btnbuscarlistaprecio.BackColor = SystemColors.ControlLight
                End If

                Try
                    Dim dts As New vdetalle_venta
                    Dim func As New fdetalle_venta
                    dts.gidventa = txtidventa.Text
                    dts.gidproducto = txtidproducto.Text
                    dts.gcantidad = txtcantidad.Text

                    If chactivar.Checked Then
                        dts.gprecio_unitario = txtprecio_unitariom.Text
                    Else
                        dts.gprecio_unitario = txtprecio_unitario.Text
                    End If


                    dts.gundm = cboundm.Text
                    dts.gcundm = txtcundm.Text
                    dts.gsundm = cbosundm.Text
                    dts.gprecio_compra = txtpcompra.Text


                    If func.insertar(dts) Then
                        If func.disminuir_stock(dts) Then

                        End If
                        'MessageBox.Show("Articulo fue añadido Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        mostrar_detalleventa()
                        txtcantidad.Value = "1"
                        txtcanttotal.Text = "1"
                        txtcundm.Text = "1"
                        cboundm.Text = "UND"
                        cbosundm.Text = "1"


                    Else
                        'MessageBox.Show("Articulo  no fue añadido Correctamente, intente de nuevos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        mostrar_detalleventa()


                    End If
                Catch ex As Exception
                    'MessageBox.Show("Genera Comprobante para Ingresar productos", "Presiona Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)

                End Try
            Else
                MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            Try
                Dim query As IEnumerable(Of Object) =
                    From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                    Where (
                        (row.Cells("subtotal").Value IsNot Nothing) AndAlso
                        (row.Cells("subtotal").Value IsNot DBNull.Value)) Select row.Cells("subtotal").Value()

                Dim resultado As Decimal =
                    query.Sum(Function(row) Convert.ToDecimal(row))
                lbltotal.Text = String.Format(resultado)




            Catch ex As Exception
                MessageBox.Show(ex.Message)

            End Try
        End If


        mostrar_detalleventa()
        txtbuscar.Focus()
        txtbuscar.SelectAll()

        Try
            Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)
            Me.ReportViewer1.RefreshReport()

        Catch ex As Exception
            Me.ReportViewer1.RefreshReport()
        End Try

        lblvuelto.Text = txtdinero.Text - lbltotal.Text
    End Sub
    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmRecibo._documento = _documento
        frmRecibo.txtidventa.Text = txtidventa.Text
        frmRecibo.ShowDialog()
    End Sub
    Private Sub txtprecio_unitario_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtprecio_unitario.Validating
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Me.erroricono.SetError(sender, "")
        Else
            Me.erroricono.SetError(sender, "Ingrese precio unitario")
        End If
    End Sub


    Private Sub btneditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar detalle de Venta ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtnombre_producto.Text <> "" Then
            Try
                Dim dts As New vdetalle_venta
                Dim func As New fdetalle_venta

                dts.giddetalle_venta = datalistado.SelectedCells.Item(1).Value

                dts.gidventa = txtidventa.Text
                dts.gidproducto = txtidproducto.Text

                dts.gcantidad = txtcantidad.Text

                dts.gprecio_unitario = txtprecio_unitario.Text




                If func.editar(dts) Then
                    MessageBox.Show("Detalle Venta Modificada Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else

                    MessageBox.Show("Detalle Venta no fue Modificada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()
                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub


    Private Sub cboproduct_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboproduct.SelectedIndexChanged
        mostrar_nproductos()
    End Sub

    Private Sub mostrar_nproductos()

        Dim func As New fnproductos
        dtn = func.mostrarnproducto
    End Sub

    Private Sub txtbuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbuscar.TextChanged

        'buscarxcodigo()

        If txtbuscar.Text = "777777" Then
            btnsalirr.PerformClick()
        End If

    End Sub


    Private Sub btnbuscar_cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbuscar_cliente.Click
        frmcliente.txtflag.Text = "1"
        frmcliente.ShowDialog()

        Try
            Dim dts As New vventa
            Dim func As New fventa

            dts.gidventa = txtidventa.Text
            dts.gidcliente = txtidcliente.Text
            dts.gfecha_venta = txtfecha.Value
            dts.gtipo_documento = cbtipo_documento.Text
            dts.gnum_documento = txtnum_documento.Text

            If func.editar(dts) Then


            Else


            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)

            Dim ReportDataSource As ReportDataSource = New ReportDataSource()
            ReportDataSource.Value = generar_comprobantemBindingSource
            ReportDataSource.Name = "recibos"
            ReportViewer1.LocalReport.DataSources.Add(ReportDataSource)

            Me.ReportViewer1.RefreshReport()




        Catch ex As Exception
            Me.ReportViewer1.RefreshReport()
        End Try


    End Sub
    Private Sub datalistado_CellClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidproducto.Text = datalistado.SelectedCells.Item(2).Value
        txtnombre_producto.Text = datalistado.SelectedCells.Item(3).Value
        txtcantidad.Text = datalistado.SelectedCells.Item(4).Value
        cbosundm.Text = datalistado.SelectedCells.Item(5).Value
        cboundm.Text = datalistado.SelectedCells.Item(6).Value
        txtcundm.Text = datalistado.SelectedCells.Item(7).Value
        txtprecio_unitario.Text = datalistado.SelectedCells.Item(8).Value

        txtcanttotal.Text = txtcantidad.Text * txtcundm.Text
    End Sub



    Private Sub btnbuscarlistaprecio_Click(sender As Object, e As EventArgs) Handles btnbuscarlistaprecio.Click

        frmvistalistaprecios.txtflag.Text = "1"

        frmvistalistaprecios.ShowDialog()
        txtcanttotal.Text = txtcantidad.Text * txtcundm.Text


    End Sub

    Private Sub btnmodificar_Click(sender As Object, e As EventArgs) Handles btnmodificar.Click


        If Me.ValidateChildren = True And txtprecio_unitario.Text <> "" Then
            Try
                Dim dts As New vproducto
                Dim func As New fproducto
                dts.gidproducto = txtidproducto.Text
                'dts.gprecio_venta = txtprecio_unitario.Text
                dts.gprecio_ventam = txtprecio_unitariom.Text
                If func.editarp(dts) Then


                Else
                    MessageBox.Show("Producto No Agregado", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub Button2_Click_2(sender As Object, e As EventArgs)
        lblvuelto.Text = txtdinero.Text - lbltotal.Text
    End Sub

    Private Sub btnpedido_Click(sender As Object, e As EventArgs) Handles btnpedido.Click
        notapedido.txtidventa.Text = txtidventa.Text
        notapedido.ShowDialog()
    End Sub


    Public Function CriarDataSet() As comprobanteds
        Dim Ds = New comprobanteds()

        For C As Integer = 1 To 200
        Next

        Return Ds
    End Function
    Private Sub btnimpresion_Click(sender As Object, e As EventArgs)

        Using ds = CriarDataSet()
            Using Relatorio = New Microsoft.Reporting.WinForms.LocalReport()
                Relatorio.ReportPath = "Reportes\rptticketn.rdlc"
                Relatorio.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("comprobanteds", DirectCast(ds.generar_comprobante, DataTable)))

                Using Rpd = New PrintReportSample.ReportPrintDocument(Relatorio)
                    Rpd.Print()
                End Using
            End Using
        End Using
    End Sub











    Private Sub txtbuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtbuscar.KeyPress
        If e.KeyChar = Chr(13) Then
            btnguardar.PerformClick()
        End If
    End Sub



    Private Sub btncalculadora_Click(sender As Object, e As EventArgs) Handles btncalculadora.Click
        Dim Proceso As New Process()
        Proceso.StartInfo.FileName = "calc.exe"
        Proceso.StartInfo.Arguments = ""
        Proceso.Start()



    End Sub

    Private Sub btneditar_Click_1(sender As Object, e As EventArgs)



        Try
            Dim dts As New vventa
            Dim func As New fventa

            dts.gidventa = txtidventa.Text
            dts.gidcliente = txtidcliente.Text
            dts.gfecha_venta = txtfecha.Value
            dts.gtipo_documento = cbtipo_documento.Text
            dts.gnum_documento = txtnum_documento.Text

            If func.editar(dts) Then


            Else


            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)
            Me.ReportViewer1.RefreshReport()




        Catch ex As Exception
            Me.ReportViewer1.RefreshReport()
        End Try

    End Sub



    Private Sub btnsalirr_Click(sender As Object, e As EventArgs) Handles btnsalirr.Click

        'Gugardar cambios del comprobante antes de iniciar otro comprobante
        'Dim dtsm As New vventa
        'Dim funcm As New fventa

        'dtsm.gidcliente = txtidcliente.Text
        'dtsm.gfecha_venta = txtfecha.Value
        'dtsm.gtipo_documento = cbtipo_documento.Text
        'dtsm.gnum_documento = txtnum_documento.Text
        'dtsm.gidusuario = txtidusuario.Text
        'dtsm.gserie_documento = txtserie_documento.Text
        'dtsm.gidventa = txtidventa.Text

        'If funcm.editar(dtsm) Then

        'End If

        'TODO: esta línea de código carga datos en la tabla 'MarketDataSetdosfechas.producto' Puede moverla o quitarla según sea necesario.
        'Me.ProductoTableAdapter.Fill(Me.MarketDataSetdosfechas.producto)
        txtbuscar.Focus()
        txtdinero.Text = 0

        txtcanttotal.Text = "1"
        txtcundm.Text = "1"
        cboundm.Text = "UND"

        mostrarusuario()
        mostrar_detalleventa()
        txtidcliente.Text = "3067"
        txtnombre_cliente.Text = "CLIENTE CASUAL"
        txtserie_documento.Text = DateTime.Now.ToShortTimeString()
        txtnum_documento.Text = DateTime.Now.ToString()

        Try
            Dim dts As New vventa
            Dim func As New fventa

            dts.gidcliente = txtidcliente.Text
            dts.gfecha_venta = txtfecha.Value
            dts.gtipo_documento = cbtipo_documento.Text
            dts.gnum_documento = txtnum_documento.Text
            dts.gidusuario = txtidusuario.Text
            dts.gserie_documento = txtserie_documento.Text

            If func.insertar(dts) Then

                'MessageBox.Show("NUEVO COMPROBANTE", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)


                Dim SQL As String
                Dim MiConexion As New SqlConnection(cadenaConexion)
                Dim Rs As SqlDataReader
                Dim Com As New SqlCommand

                Com.Connection = MiConexion
                MiConexion.Open()

                SQL = "select top 1 idventa from ventas order by idventa desc"
                Com = New SqlCommand(SQL, MiConexion)

                Rs = Com.ExecuteReader()

                Rs.Read()
                txtidventa.Text = Rs(0) 'aca me pone el primer campo del select 

                Rs.Close()


                mostrar_detalleventa()
                Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)
                Me.ReportViewer1.RefreshReport()
            Else
                MessageBox.Show("Venta no fue registrada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                mostrar_detalleventa()
            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
        lblEmail.Text = ""
        txtEmail.Text = ""

        btnEnviarSunat.Enabled = True

    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick

    End Sub

    Private Async Sub Button2_Click(sender As Object, e As EventArgs) Handles btnEnviarSunat.Click

        Dim idCliente As String
        Dim idVenta As String
        idCliente = txtidcliente.Text
        idVenta = txtidventa.Text

        Dim dtc As New vcliente
        Dim dtv As New vventa
        Dim dtse As New vComprobanteSerie
        Dim dtdVe As New vdetalle_venta

        Dim dtcliente As New DataTable
        Dim tipoDocumento As vclienteTipoDocumento = New vclienteTipoDocumento
        Dim dtventa As New DataTable
        Dim dtSerie As New DataTable
        Dim dtDetVenta As New DataTable

        dtc.gidcliente = idCliente

        Dim funcCli As New fcliente
        Dim funcVen As New fventa
        Dim funcSerie As New fComprobanteSerie
        Dim funcDetVen As New fdetalle_venta

        dtcliente = funcCli.verificarCliente(dtc)

        If dtcliente.Rows.Count > 0 Then

            dtc.gidcliente = idCliente
            dtc.gnombres = dtcliente.Rows(0)(1).ToString
            dtc.gapellidos = dtcliente.Rows(0)(2).ToString
            dtc.gdireccion = dtcliente.Rows(0)(3).ToString
            dtc.gdni = dtcliente.Rows(0)(5).ToString
            tipoDocumento.gcodigoSunat = dtcliente.Rows(0)(7).ToString
            dtc.gtipoDocumento = tipoDocumento

            dtv.gidventa = idVenta
            dtventa = funcVen.verificarVenta(dtv)



            dtdVe.gidventa = idVenta
            dtDetVenta = funcDetVen.mostrar_detalleventa_fe(dtdVe)

            If dtDetVenta.Rows.Count = 0 Then
                MessageBox.Show("Ingrese al menos un producto a la venta.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            'Verificamos si es boleta o factura
            If Not tipoDocumento.gcodigoSunat.ToString.Trim.Equals("6") Then
                'Boleta 03

                dtse.gcodigoSunat = "03"
                dtse.gnrocaja = NROCAJAFE
                dtSerie = funcSerie.obtenerNumeracion(dtse)

                If dtSerie.Rows.Count > 0 Then

                    Dim numeracion As Integer
                    Dim correlativo As String

                    dtse.gid = dtSerie.Rows(0)(0).ToString
                    dtse.gcodigoSunat = dtSerie.Rows(0)(1).ToString
                    dtse.gdescripcion = dtSerie.Rows(0)(2).ToString
                    dtse.gserie = dtSerie.Rows(0)(3).ToString & NROCAJAFE
                    dtse.gnumeracion = dtSerie.Rows(0)(4).ToString
                    numeracion = dtSerie.Rows(0)(4).ToString

                    correlativo = numeracion.ToString("00000000")

                    If dtventa.Rows.Count > 0 Then

                        dtv.gtipo_comprobante = "03"
                        dtv.gserie_comprobante = dtse.gserie
                        dtv.gnumero_comprobante = correlativo

                        If funcVen.editarNumeracion(dtv) Then

                            Try
                                _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
                                Dim Items As List(Of DetalleDocumento) = New List(Of DetalleDocumento)()
                                Dim ven As DetalleDocumento = Nothing
                                Cursor.Current = Cursors.WaitCursor

                                _documento.Moneda = "PEN"

                                If dtDetVenta IsNot Nothing Then
                                    Dim i As Integer = 0

                                    For Each row As DataRow In dtDetVenta.Rows

                                        If i > 0 Then Items.Add(ven)


                                        Dim precioSinIgv As Decimal
                                        'Dim igv As Decimal
                                        Dim impuesto As Decimal

                                        ven = New DetalleDocumento()
                                        ven.Id = (i + 1)
                                        ven.CodigoItem = Convert.ToString(row(2))
                                        ven.Descripcion = Convert.ToString(row(3)).Trim()
                                        ven.Cantidad = Math.Abs(Convert.ToDecimal(row(4)))

                                        If ven.Cantidad = 0 Then ven.Cantidad = 1

                                        ven.PrecioUnitario = Math.Abs(Convert.ToDecimal(row(8)))
                                        ven.PrecioReferencial = Math.Abs(Convert.ToDecimal(row(8)))

                                        precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row(9)))) / 1.18, 2)
                                        impuesto = Math.Abs(Convert.ToDecimal(row(9))) - precioSinIgv


                                        If _documento.Moneda = "PEN" Then
                                            ven.Suma = Math.Abs((Convert.ToDecimal(row(9))))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        ElseIf _documento.Moneda = "USD" Then

                                            ven.Suma = Math.Abs(Convert.ToDecimal(row(9)))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        End If


                                        ven.Impuesto = impuesto
                                        ven.TotalVenta = (ven.SubTotalVenta)
                                        ven.TipoPrecio = "01"
                                        ven.UnidadCliente = Convert.ToString(row(6)).Trim()

                                        If ven.Impuesto <> 0 Then
                                            ven.TipoImpuesto = "10"
                                        Else
                                            ven.TipoImpuesto = "20"

                                        End If

                                        ven.OtroImpuesto = 0
                                        'ven.UnidadMedida = "NIU"
                                        ven.UnidadMedida = Convert.ToString(row(6)).Trim()
                                        ven.Descuento = 0
                                        ven.ImpuestoSelectivo = 0

                                        i += 1

                                        If dtDetVenta.Rows.Count = i Then Items.Add(ven)

                                    Next
                                End If

                                _documento.Items = Items
                                _documento.Receptor.NroDocumento = dtventa.Rows(0)(24).ToString.Trim
                                _documento.Receptor.NombreLegal = dtventa.Rows(0)(20).ToString.Trim & " " & dtventa.Rows(0)(21).ToString.Trim
                                _documento.Receptor.Direccion = dtventa.Rows(0)(22).ToString.Trim
                                _documento.FechaVencimiento = Now
                                Dim fec As String
                                fec = dtventa.Rows(0)(2).ToString
                                _documento.FechaEmision = Convert.ToDateTime(dtventa.Rows(0)(2)).ToString("yyyy-MM-dd")

                                CalcularTotales()

                                Dim str1 As String = dtv.gserie_comprobante
                                Dim str2 As String = dtv.gnumero_comprobante

                                _documento.IdDocumento = str1 & "-" & str2
                                _documento.TipoDocumento = "03"
                                _documento.TipoOperacion = "0101"
                                _documento.Receptor.TipoDocumento = "6"

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim response As DocumentoResponse = New DocumentoResponse With {
                                        .Exito = False
                                    }

                                response = Await New GenerarFactura(serializador).Post(_documento)

                                If Not response.Exito Then Throw New ApplicationException(response.MensajeError)

                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_documento.IdDocumento}.xml")

                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma))


                            Catch a As Exception
                                MessageBox.Show(a.Message)
                                Exit Sub
                            Finally
                                Cursor.Current = Cursors.[Default]
                            End Try

                            MessageBox.Show("La Boleta " & dtse.gserie & "-" & correlativo & " se generó y se enviará por resumen diario.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            frmRecibo._documento = _documento
                            frmRecibo.txtidventa.Text = idVenta
                            frmRecibo.ShowDialog()

                            rutaArchivoXmlEmail = RutaArchivo
                            rutaPdfEmail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, frmRecibo._rutaPdf)

                            dtv.gcod_estado_envio_sunat = 0
                            dtv.gmensaje_envio_sunat = 0
                            dtv.garchivo_xml = ""
                            dtv.garchivo_cdr = ""
                            dtv.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"
                            dtv.gestado_envio_sunat = 0
                            funcVen.editarDatosEnvioDocumentoSunat(dtv)

                            btnEnviarSunat.Enabled = False

                        Else
                            MessageBox.Show("Error al actualizar la numeración en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                    Else
                        MessageBox.Show("No se encontró la venta en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Else
                    MessageBox.Show("No se encontró numeracion para boletas en la base de datos. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            Else
                'Factura 01

                dtse.gcodigoSunat = "01"
                dtse.gnrocaja = NROCAJAFE
                dtSerie = funcSerie.obtenerNumeracion(dtse)

                If dtSerie.Rows.Count > 0 Then

                    Dim numeracion As Integer
                    Dim correlativo As String

                    dtse.gid = dtSerie.Rows(0)(0).ToString
                    dtse.gcodigoSunat = dtSerie.Rows(0)(1).ToString
                    dtse.gdescripcion = dtSerie.Rows(0)(2).ToString
                    dtse.gserie = dtSerie.Rows(0)(3).ToString & NROCAJAFE
                    dtse.gnumeracion = dtSerie.Rows(0)(4).ToString
                    numeracion = dtSerie.Rows(0)(4).ToString

                    correlativo = numeracion.ToString("00000000")

                    If dtventa.Rows.Count > 0 Then

                        dtv.gtipo_comprobante = "01"
                        dtv.gserie_comprobante = dtse.gserie
                        dtv.gnumero_comprobante = correlativo

                        If funcVen.editarNumeracion(dtv) Then

                            Try
                                _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
                                Dim Items As List(Of DetalleDocumento) = New List(Of DetalleDocumento)()
                                Dim ven As DetalleDocumento = Nothing
                                Cursor.Current = Cursors.WaitCursor


                                'CVentas1 = AdmCVenta.LeerVenta("", CVentas.Sigla, CVentas.Serie, CVentas.Numeracion)

                                _documento.Moneda = "PEN"

                                'If CVentas1.Moneda = "MN" Then
                                '        _documento.Moneda = "PEN"
                                '    ElseIf CVentas1.Moneda = "US" Then
                                '        _documento.Moneda = "USD"
                                '    End If

                                'dt_DetalleVenta = AdmCVenta.LeerDetalle(cboEmpresaDoc.SelectedValue.ToString(), CVentas.Sigla, CVentas.Serie, CVentas.Numeracion)

                                If dtDetVenta IsNot Nothing Then
                                    Dim i As Integer = 0

                                    For Each row As DataRow In dtDetVenta.Rows

                                        If i > 0 Then Items.Add(ven)


                                        Dim precioSinIgv As Decimal
                                        'Dim igv As Decimal
                                        Dim impuesto As Decimal

                                        ven = New DetalleDocumento()
                                        ven.Id = (i + 1)
                                        ven.CodigoItem = Convert.ToString(row(2))
                                        ven.Descripcion = Convert.ToString(row(3)).Trim()
                                        ven.Cantidad = Math.Abs(Convert.ToDecimal(row(4)))

                                        If ven.Cantidad = 0 Then ven.Cantidad = 1

                                        ven.PrecioUnitario = Math.Abs(Convert.ToDecimal(row(8)))
                                        ven.PrecioReferencial = Math.Abs(Convert.ToDecimal(row(8)))

                                        precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row(9)))) / 1.18, 2)
                                        impuesto = Math.Abs(Convert.ToDecimal(row(9))) - precioSinIgv


                                        If _documento.Moneda = "PEN" Then
                                            ven.Suma = Math.Abs((Convert.ToDecimal(row(9))))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        ElseIf _documento.Moneda = "USD" Then

                                            ven.Suma = Math.Abs(Convert.ToDecimal(row(9)))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        End If


                                        ven.Impuesto = impuesto
                                        ven.TotalVenta = (ven.SubTotalVenta)
                                        ven.TipoPrecio = "01"
                                        ven.UnidadCliente = Convert.ToString(row(6)).Trim()

                                        If ven.Impuesto <> 0 Then
                                            ven.TipoImpuesto = "10"
                                        Else
                                            ven.TipoImpuesto = "20"

                                        End If

                                        ven.OtroImpuesto = 0
                                        'ven.UnidadMedida = "NIU"
                                        ven.UnidadMedida = Convert.ToString(row(6)).Trim()
                                        ven.Descuento = 0
                                        ven.ImpuestoSelectivo = 0

                                        i += 1

                                        If dtDetVenta.Rows.Count = i Then Items.Add(ven)

                                    Next
                                End If

                                _documento.Items = Items
                                _documento.Receptor.NroDocumento = dtventa.Rows(0)(24).ToString.Trim
                                _documento.Receptor.NombreLegal = dtventa.Rows(0)(20).ToString.Trim & " " & dtventa.Rows(0)(21).ToString.Trim
                                _documento.Receptor.Direccion = dtventa.Rows(0)(22).ToString.Trim
                                _documento.FechaVencimiento = Now
                                Dim fec As String
                                fec = dtventa.Rows(0)(2).ToString
                                _documento.FechaEmision = Convert.ToDateTime(dtventa.Rows(0)(2)).ToString("yyyy-MM-dd")

                                CalcularTotales()

                                Dim str1 As String = dtv.gserie_comprobante
                                Dim str2 As String = dtv.gnumero_comprobante

                                _documento.IdDocumento = str1 & "-" & str2
                                _documento.TipoDocumento = "01"
                                _documento.TipoOperacion = "0101"
                                _documento.Receptor.TipoDocumento = "6"

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim response As DocumentoResponse = New DocumentoResponse With {
                                        .Exito = False
                                    }

                                response = Await New GenerarFactura(serializador).Post(_documento)

                                If Not response.Exito Then Throw New ApplicationException(response.MensajeError)

                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_documento.IdDocumento}.xml")

                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma))


                            Catch a As Exception
                                MessageBox.Show(a.Message)
                                Exit Sub
                            Finally
                                Cursor.Current = Cursors.[Default]
                            End Try


                            Try
                                Cursor = Cursors.WaitCursor

                                If Not AccesoInternet() Then
                                    MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                                    Return
                                End If

                                If String.IsNullOrEmpty(_documento.IdDocumento) Then
                                    Throw New InvalidOperationException("La Serie y el Correlativo no pueden estar vacíos")
                                    Exit Sub
                                End If

                                Dim tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo))
                                Dim firmadoRequest = New FirmadoRequest With {
                                    .TramaXmlSinFirma = tramaXmlSinFirma,
                                    .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                                    .PasswordCertificado = PASSWORDCERTIFICADO,
                                    .UnSoloNodoExtension = False
                                }

                                Dim certificador As ICertificador = New Certificador()
                                Dim respuestaFirmado = Await New Firmar(certificador).Post(firmadoRequest)
                                _documento.ResumenFirma = respuestaFirmado.ResumenFirma
                                _documento.FirmaDigital = respuestaFirmado.ValorFirma
                                If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)
                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"CF_{_documento.IdDocumento}.xml")
                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))

                                Dim oContribuyente = CrearEmisor()

                                Dim enviarDocumentoRequest = New EnviarDocumentoRequest With {
                                    .Ruc = oContribuyente.NroDocumento,
                                    .UsuarioSol = oContribuyente.UsuarioSol,
                                    .ClaveSol = oContribuyente.ClaveSol,
                                    .EndPointUrl = SunatFact,
                                    .IdDocumento = _documento.IdDocumento,
                                    .TipoDocumento = _documento.TipoDocumento,
                                    .TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                                }

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim servicioSunatDocumentos As IServicioSunatDocumentos = New ServicioSunatDocumentos()
                                Dim respuestaEnvio As RespuestaComunConArchivo
                                respuestaEnvio = Await New EnviarDocumento(serializador, servicioSunatDocumentos).Post(enviarDocumentoRequest)
                                Dim rpta = CType(respuestaEnvio, EnviarDocumentoResponse)
                                MessageBox.Show(rpta.MensajeRespuesta & " Siendo las " + DateTime.Now, "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                Try

                                    If rpta.Exito AndAlso Not String.IsNullOrEmpty(rpta.TramaZipCdr) Then
                                        File.WriteAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml", Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))
                                        File.WriteAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip", Convert.FromBase64String(rpta.TramaZipCdr))
                                        _documento.FirmaDigital = respuestaFirmado.ValorFirma


                                    End If

                                    dtv.gcod_estado_envio_sunat = rpta.CodigoRespuesta
                                    dtv.gmensaje_envio_sunat = rpta.MensajeRespuesta
                                    dtv.garchivo_xml = rpta.NombreArchivo & ".xml"
                                    dtv.garchivo_cdr = "R-" & rpta.NombreArchivo & ".zip"
                                    dtv.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"

                                    If rpta.Exito Then
                                        If rpta.CodigoRespuesta = "0" Then

                                            If dtv IsNot Nothing AndAlso dtv.gnumero_comprobante <> "" Then
                                                dtv.gestado_envio_sunat = 0
                                                funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                            End If
                                        ElseIf rpta.CodigoRespuesta Is Nothing Then
                                            Dim msg = String.Concat(rpta.MensajeRespuesta)
                                            Dim faultCode = "Client."

                                            If msg.Contains(faultCode) Then
                                                Dim posicion = msg.IndexOf(faultCode, StringComparison.Ordinal)
                                                Dim codigoError = msg.Substring(posicion + faultCode.Length, 4)
                                                msg = codigoError
                                            End If

                                            dtv.gestado_envio_sunat = 1
                                            dtv.gcod_estado_envio_sunat = msg
                                            funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                        End If

                                        frmRecibo._documento = _documento
                                        frmRecibo.txtidventa.Text = idVenta
                                        frmRecibo.ShowDialog()

                                        btnEnviarSunat.Enabled = False

                                        rutaArchivoXmlEmail = RutaArchivo
                                        rutaPdfEmail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, frmRecibo._rutaPdf)

                                    Else
                                        MessageBox.Show(rpta.MensajeError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End If

                                Catch ex As Exception

                                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Exit Sub
                                End Try

                                If Not respuestaEnvio.Exito Then Throw New ApplicationException(respuestaEnvio.MensajeError)

                            Catch ex As Exception
                                MessageBox.Show(ex.Message)

                                Exit Sub
                            Finally
                                'btnGeneraXML.Enabled = True
                                'btnEnvioSunat.Enabled = False
                                Cursor = Cursors.[Default]
                            End Try

                            'MessageBox.Show("La Factura " & dtse.gserie & "-" & correlativo & " se generó y envió a sunat.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Else
                            MessageBox.Show("Error al actualizar la numeración en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                    Else
                        MessageBox.Show("No se encontró la venta en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Else
                    MessageBox.Show("No se encontró numeracion para boletas en la base de datos. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            End If

        Else
            MessageBox.Show("Venta no enviada a sunat. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        lblvuelto.Text = txtdinero.Text - lbltotal.Text

    End Sub

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub CalcularTotales()
        _documento.TotalIgv = _documento.Items.Sum(Function(d) d.Impuesto)
        _documento.TotalIsc = _documento.Items.Sum(Function(d) d.ImpuestoSelectivo)
        _documento.TotalOtrosTributos = _documento.Items.Sum(Function(d) d.OtroImpuesto)
        _documento.Gravadas = _documento.Items.Where(Function(d) d.TipoImpuesto.StartsWith("1")).Sum(Function(d) d.SubTotalVenta)
        _documento.Exoneradas = _documento.Items.Where(Function(d) d.TipoImpuesto.Contains("20")).Sum(Function(d) d.Suma)
        _documento.Inafectas = _documento.Items.Where(Function(d) d.TipoImpuesto.StartsWith("3") OrElse d.TipoImpuesto.Contains("40")).Sum(Function(d) d.Suma)
        _documento.Gratuitas = _documento.Items.Where(Function(d) d.TipoImpuesto.Contains("21")).Sum(Function(d) d.Suma)
        _documento.LineCountNumeric = Convert.ToString(_documento.Items.Count())

        If _documento.TotalIsc > 0 Then
            _documento.TotalIgv = (_documento.Gravadas + _documento.TotalIsc) * _documento.CalculoIgv
        End If

        _documento.TotalVenta = _documento.Gravadas + _documento.Exoneradas + _documento.Inafectas + _documento.TotalIgv + _documento.TotalIsc + _documento.TotalOtrosTributos
        _documento.MontoEnLetras = ConvertLetras.enletras(_documento.TotalVenta.ToString())

        If _documento.CalculoIgv > 0 Then
            _documento.SubTotalVenta = _documento.TotalVenta - _documento.TotalIgv
        Else
            _documento.SubTotalVenta = _documento.TotalVenta
        End If
    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Sub RefreshTicketVenta()

        Try

            'TODO: esta línea de código carga datos en la tabla 'recibos.generar_comprobantem' Puede moverla o quitarla según sea necesario.
            'Me.generar_comprobantemTableAdapter.Fill(Me.recibos.generar_comprobantem, idventa:=txtidventa.Text)

            Dim dtv As New vventa
            Dim funcVen As New fventa
            Dim dtventa As New DataTable

            dtv.gidventa = txtidventa.Text
            'dtventa = funcVen.verificarVenta(dtv)

            Dim parametro As List(Of ReportParameter) = New List(Of ReportParameter)

            'If dtventa.Rows.Count > 0 Then

            Dim herramientas As Herramientas = New Herramientas()
            Dim recursos = herramientas.GetResourcesPath2()
            Dim nomdocumento = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" +
                _documento.IdDocumento
            Dim datosAdicionales_CDB As String = ""
            Dim CodigoCertificado As String = ""
            datosAdicionales_CDB = _documento.Emisor.NroDocumento & "|" & _documento.TipoDocumento & "|" & _documento.IdDocumento & "|" &
                _documento.TotalIgv & "|" & _documento.TotalVenta & "|" & _documento.FechaEmision & "|" &
                _documento.Receptor.TipoDocumento & "|" & _documento.Receptor.NroDocumento
            CodigoCertificado = datosAdicionales_CDB & "|" & _documento.FirmaDigital
            Dim qrEncoder = New QrEncoder(ErrorCorrectionLevel.H)
            Dim qrCode = qrEncoder.Encode(datosAdicionales_CDB)
            Dim renderer = New GraphicsRenderer(New FixedModuleSize(5, QuietZoneModules.Two), Brushes.Black, Brushes.White)

            Using stream = New FileStream(recursos & "\" + nomdocumento & ".jpeg", FileMode.Create)
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Jpeg, stream)
            End Using

            parametro.Add(New ReportParameter("pLogo", "file:///" & recursos & "/" + nomdocumento & ".jpeg"))

            'End If

            Me.ReportViewer1.ProcessingMode = ProcessingMode.Local
            ReportViewer1.LocalReport.EnableExternalImages = True

            ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)
            ReportViewer1.ZoomMode = ZoomMode.Percent
            ReportViewer1.ZoomPercent = 100
            ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.recibos.rdlc"
            ReportViewer1.LocalReport.SetParameters(parametro)

            generar_comprobantemBindingSource.RemoveFilter()
            'Me.generar_comprobantemTableAdapter.Fill(Me.recibos.generar_comprobantem, idventa:=txtidventa.Text)


            Me.ReportViewer1.LocalReport.DataSources.Clear()
            Dim ReportDataSource As ReportDataSource = New ReportDataSource()
            ReportDataSource.Value = generar_comprobantemBindingSource
            ReportDataSource.Name = "recibos"
            ReportViewer1.LocalReport.DataSources.Add(ReportDataSource)

            Me.ReportViewer1.RefreshReport()

            Dim warnings As Warning()
            Dim streamids As String()
            Dim mimeType As String
            Dim encoding As String
            Dim filenameExtension As String
            Dim bytes As Byte() = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)

            If _documento.TipoDocumento.Equals("01") Then
                Using fs As FileStream = New FileStream("FACTURAS_PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                    fs.Write(bytes, 0, bytes.Length)
                End Using
            Else
                If _documento.TipoDocumento.Equals("03") Then
                    Using fs As FileStream = New FileStream("BOLETAS_PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                        fs.Write(bytes, 0, bytes.Length)
                    End Using
                Else
                    Using fs As FileStream = New FileStream("PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                        fs.Write(bytes, 0, bytes.Length)
                    End Using
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error al mostrar recibo", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.ReportViewer1.RefreshReport()
        End Try

    End Sub

    Private Async Sub EnviarEmailComprobante()

        btnEnvioEmail.Enabled = False

        Dim SmtpServer As New SmtpClient()

        Dim vSmtpServer As New vSmtpServer()

        SmtpServer = vSmtpServer.obtenerSmtpCliente()

        Dim mail As New MailMessage()

        mail = New MailMessage()
        Dim addr() As String = txtEmail.Text.Split(";")

        Dim idDocE As String = _documento.IdDocumento
        Dim recepE As String = _documento.Receptor.NombreLegal
        lblEmail.Text = "Enviando email... " & idDocE

        Try
            mail.From = New MailAddress(USEREMAIL, RAZONSOCIAL, System.Text.Encoding.UTF8)
            Dim i As Byte
            For i = 0 To addr.Length - 1
                mail.To.Add(addr(i))
            Next
            mail.Subject = "Comprobante Electrónico " & _documento.IdDocumento & " - " & RUC & " " & RAZONSOCIAL
            'mail.Body = TextBox4.Text
            'If ListBox1.Items.Count <> 0 Then
            '    For i = 0 To ListBox1.Items.Count - 1
            '        mail.Attachments.Add(New Attachment(ListBox1.Items.Item(i)))
            '    Next
            'End If
            'Dim logo As New LinkedResource(Path)
            'logo.ContentId = "Logo"
            Dim htmlview As String = ""
            Dim saludo As String = ""

            mail.Attachments.Add(New Attachment(rutaArchivoXmlEmail))
            mail.Attachments.Add(New Attachment(rutaPdfEmail))

            'htmlview = "<html><body><table border=2><tr width=100%><td><img src=cid:Logo alt=companyname /></td><td>MY COMPANY DESCRIPTION</td></tr></table><hr/></body></html>"

            saludo = "Estimado " & recepE & ".<br/>"
            saludo += "Se adjunta su comprobante electronico " & idDocE

            Dim alternateView1 As AlternateView = AlternateView.CreateAlternateViewFromString(htmlview + saludo, Nothing, MediaTypeNames.Text.Html)
            'alternateView1.LinkedResources.Add(logo)
            mail.AlternateViews.Add(alternateView1)
            mail.IsBodyHtml = True
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            mail.ReplyTo = New MailAddress(txtEmail.Text)
            'SmtpServer.Send(mail)
            Await SmtpServer.SendMailAsync(mail)
            lblEmail.ForeColor = Color.DarkGreen
            lblEmail.Text = "Se envió correctamente el email con el comprobante " & idDocE & " - " & recepE
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblEmail.ForeColor = Color.Red
            lblEmail.Text = "Error al enviar email. Comprobante " & idDocE
        End Try

        btnEnvioEmail.Enabled = True

    End Sub

    Private Sub btnEnvioEmail_Click(sender As Object, e As EventArgs) Handles btnEnvioEmail.Click
        If Not _documento Is Nothing Then
            If Not _documento.IdDocumento.Equals("") Then
                If txtEmail.Text.Trim.Length > 0 Then
                    EnviarEmailComprobante()
                Else
                    MessageBox.Show("Debe ingresar un email válido", "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            Else
                MessageBox.Show("No se ha generado aun el comprobante electrónico", "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("No se ha generado aun el comprobante electrónico", "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub txtdinero_TextChanged(sender As Object, e As EventArgs) Handles txtdinero.TextChanged
        lblvuelto.Text = txtdinero.Text - lbltotal.Text
    End Sub

    Private Sub cbosundm_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbosundm.SelectedIndexChanged

    End Sub

    Private Async Sub btnEnviarLuegoSunat_Click(sender As Object, e As EventArgs) Handles btnEnviarLuegoSunat.Click

        Dim idCliente As String
        Dim idVenta As String
        idCliente = txtidcliente.Text
        idVenta = txtidventa.Text

        Dim dtc As New vcliente
        Dim dtv As New vventa
        Dim dtse As New vComprobanteSerie
        Dim dtdVe As New vdetalle_venta

        Dim dtcliente As New DataTable
        Dim tipoDocumento As vclienteTipoDocumento = New vclienteTipoDocumento
        Dim dtventa As New DataTable
        Dim dtSerie As New DataTable
        Dim dtDetVenta As New DataTable

        dtc.gidcliente = idCliente

        Dim funcCli As New fcliente
        Dim funcVen As New fventa
        Dim funcSerie As New fComprobanteSerie
        Dim funcDetVen As New fdetalle_venta

        dtcliente = funcCli.verificarCliente(dtc)

        If dtcliente.Rows.Count > 0 Then

            dtc.gidcliente = idCliente
            dtc.gnombres = dtcliente.Rows(0)(1).ToString
            dtc.gapellidos = dtcliente.Rows(0)(2).ToString
            dtc.gdireccion = dtcliente.Rows(0)(3).ToString
            dtc.gdni = dtcliente.Rows(0)(5).ToString
            tipoDocumento.gcodigoSunat = dtcliente.Rows(0)(7).ToString
            dtc.gtipoDocumento = tipoDocumento

            dtv.gidventa = idVenta
            dtventa = funcVen.verificarVenta(dtv)

            dtdVe.gidventa = idVenta
            dtDetVenta = funcDetVen.mostrar_detalleventa_fe(dtdVe)

            If dtDetVenta.Rows.Count = 0 Then
                MessageBox.Show("Ingrese al menos un producto a la venta.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            'Verificamos si es boleta o factura
            If Not tipoDocumento.gcodigoSunat.ToString.Trim.Equals("6") Then
                'Boleta 03
                MessageBox.Show("Esta opción solo aplica para Facturas", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Else
                'Factura 01

                dtse.gcodigoSunat = "01"
                dtse.gnrocaja = NROCAJAFE
                dtSerie = funcSerie.obtenerNumeracion(dtse)

                If dtSerie.Rows.Count > 0 Then

                    Dim numeracion As Integer
                    Dim correlativo As String

                    dtse.gid = dtSerie.Rows(0)(0).ToString
                    dtse.gcodigoSunat = dtSerie.Rows(0)(1).ToString
                    dtse.gdescripcion = dtSerie.Rows(0)(2).ToString
                    dtse.gserie = dtSerie.Rows(0)(3).ToString & NROCAJAFE
                    dtse.gnumeracion = dtSerie.Rows(0)(4).ToString
                    numeracion = dtSerie.Rows(0)(4).ToString

                    correlativo = numeracion.ToString("00000000")

                    If dtventa.Rows.Count > 0 Then

                        dtv.gtipo_comprobante = "01"
                        dtv.gserie_comprobante = dtse.gserie
                        dtv.gnumero_comprobante = correlativo

                        If funcVen.editarNumeracion(dtv) Then

                            Try
                                _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
                                Dim Items As List(Of DetalleDocumento) = New List(Of DetalleDocumento)()
                                Dim ven As DetalleDocumento = Nothing
                                Cursor.Current = Cursors.WaitCursor


                                'CVentas1 = AdmCVenta.LeerVenta("", CVentas.Sigla, CVentas.Serie, CVentas.Numeracion)

                                _documento.Moneda = "PEN"

                                'If CVentas1.Moneda = "MN" Then
                                '        _documento.Moneda = "PEN"
                                '    ElseIf CVentas1.Moneda = "US" Then
                                '        _documento.Moneda = "USD"
                                '    End If

                                'dt_DetalleVenta = AdmCVenta.LeerDetalle(cboEmpresaDoc.SelectedValue.ToString(), CVentas.Sigla, CVentas.Serie, CVentas.Numeracion)

                                If dtDetVenta IsNot Nothing Then
                                    Dim i As Integer = 0

                                    For Each row As DataRow In dtDetVenta.Rows

                                        If i > 0 Then Items.Add(ven)


                                        Dim precioSinIgv As Decimal
                                        'Dim igv As Decimal
                                        Dim impuesto As Decimal

                                        ven = New DetalleDocumento()
                                        ven.Id = (i + 1)
                                        ven.CodigoItem = Convert.ToString(row(2))
                                        ven.Descripcion = Convert.ToString(row(3)).Trim()
                                        ven.Cantidad = Math.Abs(Convert.ToDecimal(row(4)))

                                        If ven.Cantidad = 0 Then ven.Cantidad = 1

                                        ven.PrecioUnitario = Math.Abs(Convert.ToDecimal(row(8)))
                                        ven.PrecioReferencial = Math.Abs(Convert.ToDecimal(row(8)))

                                        precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row(9)))) / 1.18, 2)
                                        impuesto = Math.Abs(Convert.ToDecimal(row(9))) - precioSinIgv


                                        If _documento.Moneda = "PEN" Then
                                            ven.Suma = Math.Abs((Convert.ToDecimal(row(9))))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        ElseIf _documento.Moneda = "USD" Then

                                            ven.Suma = Math.Abs(Convert.ToDecimal(row(9)))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        End If


                                        ven.Impuesto = impuesto
                                        ven.TotalVenta = (ven.SubTotalVenta)
                                        ven.TipoPrecio = "01"
                                        ven.UnidadCliente = Convert.ToString(row(6)).Trim()

                                        If ven.Impuesto <> 0 Then
                                            ven.TipoImpuesto = "10"
                                        Else
                                            ven.TipoImpuesto = "20"

                                        End If

                                        ven.OtroImpuesto = 0
                                        'ven.UnidadMedida = "NIU"
                                        ven.UnidadMedida = Convert.ToString(row(6)).Trim()
                                        ven.Descuento = 0
                                        ven.ImpuestoSelectivo = 0

                                        i += 1

                                        If dtDetVenta.Rows.Count = i Then Items.Add(ven)

                                    Next
                                End If

                                _documento.Items = Items
                                _documento.Receptor.NroDocumento = dtventa.Rows(0)(24).ToString.Trim
                                _documento.Receptor.NombreLegal = dtventa.Rows(0)(20).ToString.Trim & " " & dtventa.Rows(0)(21).ToString.Trim
                                _documento.Receptor.Direccion = dtventa.Rows(0)(22).ToString.Trim
                                _documento.FechaVencimiento = Now
                                Dim fec As String
                                fec = dtventa.Rows(0)(2).ToString
                                _documento.FechaEmision = Convert.ToDateTime(dtventa.Rows(0)(2)).ToString("yyyy-MM-dd")

                                CalcularTotales()

                                Dim str1 As String = dtv.gserie_comprobante
                                Dim str2 As String = dtv.gnumero_comprobante

                                _documento.IdDocumento = str1 & "-" & str2
                                _documento.TipoDocumento = "01"
                                _documento.TipoOperacion = "0101"
                                _documento.Receptor.TipoDocumento = "6"

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim response As DocumentoResponse = New DocumentoResponse With {
                                        .Exito = False
                                    }

                                response = Await New GenerarFactura(serializador).Post(_documento)

                                If Not response.Exito Then Throw New ApplicationException(response.MensajeError)

                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_documento.IdDocumento}.xml")

                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma))


                            Catch a As Exception
                                MessageBox.Show(a.Message)
                                Exit Sub
                            Finally
                                Cursor.Current = Cursors.[Default]
                            End Try


                            Try
                                Cursor = Cursors.WaitCursor

                                If Not AccesoInternet() Then
                                    MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                                    Return
                                End If

                                'If String.IsNullOrEmpty(_documento.IdDocumento) Then
                                '    Throw New InvalidOperationException("La Serie y el Correlativo no pueden estar vacíos")
                                '    Exit Sub
                                'End If

                                'Dim tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo))
                                'Dim firmadoRequest = New FirmadoRequest With {
                                '    .TramaXmlSinFirma = tramaXmlSinFirma,
                                '    .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                                '    .PasswordCertificado = PASSWORDCERTIFICADO,
                                '    .UnSoloNodoExtension = False
                                '}

                                'Dim certificador As ICertificador = New Certificador()
                                'Dim respuestaFirmado = Await New Firmar(certificador).Post(firmadoRequest)
                                '_documento.ResumenFirma = respuestaFirmado.ResumenFirma
                                '_documento.FirmaDigital = respuestaFirmado.ValorFirma
                                'If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)
                                'RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"CF_{_documento.IdDocumento}.xml")
                                'File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))

                                'Dim oContribuyente = CrearEmisor()

                                'Dim enviarDocumentoRequest = New EnviarDocumentoRequest With {
                                '    .Ruc = oContribuyente.NroDocumento,
                                '    .UsuarioSol = oContribuyente.UsuarioSol,
                                '    .ClaveSol = oContribuyente.ClaveSol,
                                '    .EndPointUrl = SunatFact,
                                '    .IdDocumento = _documento.IdDocumento,
                                '    .TipoDocumento = _documento.TipoDocumento,
                                '    .TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                                '}

                                'Dim serializador As ISerializador = New FinalXML.Serializador()
                                'Dim servicioSunatDocumentos As IServicioSunatDocumentos = New ServicioSunatDocumentos()
                                'Dim respuestaEnvio As RespuestaComunConArchivo
                                'respuestaEnvio = Await New EnviarDocumento(serializador, servicioSunatDocumentos).Post(enviarDocumentoRequest)
                                'Dim rpta = CType(respuestaEnvio, EnviarDocumentoResponse)

                                'MessageBox.Show(rpta.MensajeRespuesta & " Siendo las " + DateTime.Now, "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                MessageBox.Show("El comprobante " & dtv.gserie_comprobante & " - " & dtv.gnumero_comprobante & " se registro en la base de datos y queda pendiente su envío a SUNAT.Siendo las " + DateTime.Now, "Registro de Comprobante", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                'Try

                                'If rpta.Exito AndAlso Not String.IsNullOrEmpty(rpta.TramaZipCdr) Then
                                '        File.WriteAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml", Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))
                                '        File.WriteAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip", Convert.FromBase64String(rpta.TramaZipCdr))
                                '        _documento.FirmaDigital = respuestaFirmado.ValorFirma


                                '    End If

                                'dtv.gcod_estado_envio_sunat = rpta.CodigoRespuesta
                                '    dtv.gmensaje_envio_sunat = rpta.MensajeRespuesta
                                '    dtv.garchivo_xml = rpta.NombreArchivo & ".xml"
                                '    dtv.garchivo_cdr = "R-" & rpta.NombreArchivo & ".zip"
                                '    dtv.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"

                                'If rpta.Exito Then
                                'If rpta.CodigoRespuesta = "0" Then

                                '            If dtv IsNot Nothing AndAlso dtv.gnumero_comprobante <> "" Then
                                '                dtv.gestado_envio_sunat = 0
                                '                funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                '            End If
                                '        ElseIf rpta.CodigoRespuesta Is Nothing Then
                                '            Dim msg = String.Concat(rpta.MensajeRespuesta)
                                '            Dim faultCode = "Client."

                                '            If msg.Contains(faultCode) Then
                                '                Dim posicion = msg.IndexOf(faultCode, StringComparison.Ordinal)
                                '                Dim codigoError = msg.Substring(posicion + faultCode.Length, 4)
                                '                msg = codigoError
                                '            End If

                                '            dtv.gestado_envio_sunat = 1
                                '            dtv.gcod_estado_envio_sunat = msg
                                '            funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                '        End If

                                frmRecibo._documento = _documento
                                        frmRecibo.txtidventa.Text = idVenta
                                        frmRecibo.ShowDialog()

                                        btnEnviarSunat.Enabled = False

                                        rutaArchivoXmlEmail = RutaArchivo
                                        rutaPdfEmail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, frmRecibo._rutaPdf)

                                'Else
                                '    MessageBox.Show(rpta.MensajeError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                'End If

                                'Catch ex As Exception

                                '    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                '    Exit Sub
                                'End Try

                                'If Not respuestaEnvio.Exito Then Throw New ApplicationException(respuestaEnvio.MensajeError)

                            Catch ex As Exception
                                MessageBox.Show(ex.Message)

                                Exit Sub
                            Finally
                                'btnGeneraXML.Enabled = True
                                'btnEnvioSunat.Enabled = False
                                Cursor = Cursors.[Default]
                            End Try

                            'MessageBox.Show("La Factura " & dtse.gserie & "-" & correlativo & " se generó y envió a sunat.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Else
                            MessageBox.Show("Error al actualizar la numeración en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                    Else
                        MessageBox.Show("No se encontró la venta en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Else
                    MessageBox.Show("No se encontró numeracion para boletas en la base de datos. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            End If

        Else
            MessageBox.Show("Venta no enviada a sunat. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        lblvuelto.Text = txtdinero.Text - lbltotal.Text

    End Sub
End Class