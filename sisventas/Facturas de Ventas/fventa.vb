﻿Imports System.Data.SqlClient
Public Class fventa
    Inherits conexion
    Dim cmd As New SqlCommand
    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_venta")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function


    Public Function insertar(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_venta")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idcliente", dts.gidcliente)
            cmd.Parameters.AddWithValue("@fecha_venta", dts.gfecha_venta)
            cmd.Parameters.AddWithValue("@tipo_documento", dts.gtipo_documento)
            cmd.Parameters.AddWithValue("@num_documento", dts.gnum_documento)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)
            cmd.Parameters.AddWithValue("@serie_documento", dts.gserie_documento)
            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function insertarNotaCredito(ByVal dts As vNotaCredito) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("insertar_nota_credito")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idcliente", dts.gidcliente)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)
            cmd.Parameters.AddWithValue("@idventa", dts.gidventa)
            cmd.Parameters.AddWithValue("@fecha_venta", dts.gfecha_venta)
            cmd.Parameters.AddWithValue("@tipo_documento", dts.gtipo_documento)
            cmd.Parameters.AddWithValue("@serie_documento", dts.gserie_documento)
            cmd.Parameters.AddWithValue("@num_documento", dts.gnum_documento)
            cmd.Parameters.AddWithValue("@tipo_documento_rel", dts.gtipo_comprobante_rel)
            cmd.Parameters.AddWithValue("@serie_documento_rel", dts.gserie_comprobante_rel)
            cmd.Parameters.AddWithValue("@num_documento_rel", dts.gnumero_comprobante_rel)
            cmd.Parameters.AddWithValue("@tipo_nota", dts.gtipo_nota)
            cmd.Parameters.AddWithValue("@tipo_nota_descripcion", dts.gtipo_nota_descripcion)
            cmd.Parameters.AddWithValue("@cod_estado_envio_sunat", dts.gcod_estado_envio_sunat)
            cmd.Parameters.AddWithValue("@mensaje_envio_sunat", dts.gmensaje_envio_sunat)
            cmd.Parameters.AddWithValue("@estado_envio_sunat", dts.gestado_envio_sunat)
            cmd.Parameters.AddWithValue("@archivo_xml", dts.garchivo_xml)
            cmd.Parameters.AddWithValue("@archivo_cdr", dts.garchivo_cdr)
            cmd.Parameters.AddWithValue("@archivo_pdf", dts.garchivo_pdf)

            'If cmd.ExecuteNonQuery Then
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)

            Return dt
            'Else
            '    Return Nothing
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function insertarNotaDebito(ByVal dts As vNotaDebito) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("insertar_nota_debito")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idcliente", dts.gidcliente)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)
            cmd.Parameters.AddWithValue("@idventa", dts.gidventa)
            cmd.Parameters.AddWithValue("@fecha_venta", dts.gfecha_venta)
            cmd.Parameters.AddWithValue("@tipo_documento", dts.gtipo_documento)
            cmd.Parameters.AddWithValue("@serie_documento", dts.gserie_documento)
            cmd.Parameters.AddWithValue("@num_documento", dts.gnum_documento)
            cmd.Parameters.AddWithValue("@tipo_documento_rel", dts.gtipo_comprobante_rel)
            cmd.Parameters.AddWithValue("@serie_documento_rel", dts.gserie_comprobante_rel)
            cmd.Parameters.AddWithValue("@num_documento_rel", dts.gnumero_comprobante_rel)
            cmd.Parameters.AddWithValue("@tipo_nota", dts.gtipo_nota)
            cmd.Parameters.AddWithValue("@tipo_nota_descripcion", dts.gtipo_nota_descripcion)
            cmd.Parameters.AddWithValue("@cod_estado_envio_sunat", dts.gcod_estado_envio_sunat)
            cmd.Parameters.AddWithValue("@mensaje_envio_sunat", dts.gmensaje_envio_sunat)
            cmd.Parameters.AddWithValue("@estado_envio_sunat", dts.gestado_envio_sunat)
            cmd.Parameters.AddWithValue("@archivo_xml", dts.garchivo_xml)
            cmd.Parameters.AddWithValue("@archivo_cdr", dts.garchivo_cdr)
            cmd.Parameters.AddWithValue("@archivo_pdf", dts.garchivo_pdf)

            'If cmd.ExecuteNonQuery Then
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)

            Return dt
            'Else
            '    Return Nothing
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function editar(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_venta")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idventa", dts.gidventa)
            cmd.Parameters.AddWithValue("@idcliente", dts.gidcliente)
            cmd.Parameters.AddWithValue("@fecha_venta", dts.gfecha_venta)
            cmd.Parameters.AddWithValue("@tipo_documento", dts.gtipo_documento)
            cmd.Parameters.AddWithValue("@num_documento", dts.gnum_documento)
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()
        End Try

    End Function

    Public Function eliminar(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("eliminar_venta")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.Add("@idventa", SqlDbType.NVarChar, 50).Value = dts.gidventa
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Public Function verificarVenta(ByVal dts As vventa) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("verificar_venta")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.Add("@idventa", SqlDbType.NVarChar, 50).Value = dts.gidventa
            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function editarNumeracion(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_numeracion_venta")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idventa", dts.gidventa)
            cmd.Parameters.AddWithValue("@tipo_comprobante", dts.gtipo_comprobante)
            cmd.Parameters.AddWithValue("@serie_comprobante", dts.gserie_comprobante)
            cmd.Parameters.AddWithValue("@numero_comprobante", dts.gnumero_comprobante)
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()
        End Try

    End Function

    Public Function editarDatosEnvioDocumentoSunat(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_estado_envio_documento_sunat")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idventa", dts.gidventa)
            cmd.Parameters.AddWithValue("@cod_estado_envio_sunat", dts.gcod_estado_envio_sunat)
            cmd.Parameters.AddWithValue("@mensaje_envio_sunat", dts.gmensaje_envio_sunat)
            cmd.Parameters.AddWithValue("@estado_envio_sunat", dts.gestado_envio_sunat)
            cmd.Parameters.AddWithValue("@archivo_xml", dts.garchivo_xml)
            cmd.Parameters.AddWithValue("@archivo_cdr", dts.garchivo_cdr)
            cmd.Parameters.AddWithValue("@archivo_pdf", dts.garchivo_pdf)
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()
        End Try

    End Function

    Public Function anularEnvioComprobanteSunat(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("anular_envio_comprobante_sunat")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idventa", dts.gidventa)
            cmd.Parameters.AddWithValue("@motivo", dts.gmotivoAnulacionEnvioSunat)
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()
        End Try

    End Function

    Public Function anularEnvioComprobanteSunatSerie(ByVal dts As vventa) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("anular_envio_comprobante_sunat_serie")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@serie_comprobante", dts.gserie_comprobante)
            cmd.Parameters.AddWithValue("@numero_comprobante", dts.gnumero_comprobante)
            cmd.Parameters.AddWithValue("@motivo", dts.gmotivoAnulacionEnvioSunat)
            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()
        End Try

    End Function

End Class
