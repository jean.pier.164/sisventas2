﻿Public Class vdetalle_venta

    Dim iddetalle_venta, idventa, idproducto, cantidad As Integer
    Dim precio_unitario, precio_compra, cundm As Double
    Dim undm, sundm As String




    Public Property giddetalle_venta
        Get
            Return iddetalle_venta
        End Get
        Set(ByVal value)
            iddetalle_venta = value
        End Set
    End Property




    Public Property gidventa
        Get
            Return idventa
        End Get
        Set(ByVal value)
            idventa = value
        End Set
    End Property




    Public Property gidproducto
        Get
            Return idproducto
        End Get
        Set(ByVal value)
            idproducto = value
        End Set
    End Property
    Public Property gcantidad
        Get
            Return cantidad
        End Get
        Set(ByVal value)
            cantidad = value
        End Set
    End Property
    Public Property gprecio_unitario
        Get
            Return precio_unitario
        End Get
        Set(ByVal value)
            precio_unitario = value
        End Set
    End Property
    Public Property gundm
        Get
            Return undm
        End Get
        Set(ByVal value)
            undm = value
        End Set
    End Property
    Public Property gcundm
        Get
            Return cundm
        End Get
        Set(ByVal value)
            cundm = value
        End Set
    End Property

    Public Property gsundm
        Get
            Return sundm
        End Get
        Set(ByVal value)
            sundm = value
        End Set
    End Property


    Public Property gprecio_compra
        Get
            Return precio_compra
        End Get
        Set(ByVal value)
            precio_compra = value
        End Set
    End Property
    Public Sub New()

    End Sub



    Public Sub New(ByVal iddetalle_venta As Integer, ByVal idventa As Integer, ByVal idproducto As Integer, ByVal cantidad As Integer, ByVal precio_unitario As Double, ByVal undm As String, ByVal cundm As Double, ByVal sundm As String, ByVal precio_compra As Double)

        giddetalle_venta = iddetalle_venta
        gidventa = idventa
        gidproducto = idproducto
        gcantidad = cantidad
        gprecio_unitario = precio_unitario
        gundm = undm
        gcundm = cundm
        gsundm = sundm
        gprecio_compra = precio_compra

    End Sub

End Class
