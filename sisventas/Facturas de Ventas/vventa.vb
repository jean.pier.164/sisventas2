﻿Public Class vventa
    Dim idventa, idcliente, idusuario As Integer
    Dim fecha_venta As DateTime
    Dim tipo_documento, num_documento, serie_documento As String

    Dim tipo_comprobante, serie_comprobante, numero_comprobante As String
    Dim cod_estado_envio_sunat As Integer
    Dim estado_envio_sunat As Integer
    Dim mensaje_envio_sunat, archivo_xml, archivo_cdr, archivo_pdf As String
    Dim fecha_envio_sunat As DateTime
    Dim motivoAnulacionEnvioSunat As String


    Public Property gidventa
        Get
            Return idventa
        End Get
        Set(ByVal value)
            idventa = value
        End Set
    End Property

    Public Property gidcliente
        Get
            Return idcliente
        End Get
        Set(ByVal value)
            idcliente = value
        End Set
    End Property

    Public Property gfecha_venta
        Get
            Return fecha_venta
        End Get
        Set(ByVal value)
            fecha_venta = value
        End Set
    End Property

    Public Property gtipo_documento
        Get
            Return tipo_documento
        End Get
        Set(ByVal value)
            tipo_documento = value
        End Set
    End Property

    Public Property gnum_documento
        Get
            Return num_documento
        End Get
        Set(ByVal value)
            num_documento = value
        End Set
    End Property

    Public Property gidusuario
        Get
            Return idusuario
        End Get
        Set(ByVal value)
            idusuario = value
        End Set
    End Property

    Public Property gserie_documento
        Get
            Return serie_documento
        End Get
        Set(ByVal value)
            serie_documento = value
        End Set
    End Property

    Public Property gtipo_comprobante
        Get
            Return tipo_comprobante
        End Get
        Set(ByVal value)
            tipo_comprobante = value
        End Set
    End Property

    Public Property gserie_comprobante
        Get
            Return serie_comprobante
        End Get
        Set(ByVal value)
            serie_comprobante = value
        End Set
    End Property

    Public Property gnumero_comprobante
        Get
            Return numero_comprobante
        End Get
        Set(ByVal value)
            numero_comprobante = value
        End Set
    End Property

    Public Property gcod_estado_envio_sunat
        Get
            Return cod_estado_envio_sunat
        End Get
        Set(ByVal value)
            cod_estado_envio_sunat = value
        End Set
    End Property

    Public Property gestado_envio_sunat
        Get
            Return estado_envio_sunat
        End Get
        Set(ByVal value)
            estado_envio_sunat = value
        End Set
    End Property

    Public Property gmensaje_envio_sunat
        Get
            Return mensaje_envio_sunat
        End Get
        Set(ByVal value)
            mensaje_envio_sunat = value
        End Set
    End Property

    Public Property garchivo_xml
        Get
            Return archivo_xml
        End Get
        Set(ByVal value)
            archivo_xml = value
        End Set
    End Property

    Public Property garchivo_cdr
        Get
            Return archivo_cdr
        End Get
        Set(ByVal value)
            archivo_cdr = value
        End Set
    End Property

    Public Property garchivo_pdf
        Get
            Return archivo_pdf
        End Get
        Set(ByVal value)
            archivo_pdf = value
        End Set
    End Property

    Public Property gfecha_envio_sunat
        Get
            Return fecha_envio_sunat
        End Get
        Set(ByVal value)
            fecha_envio_sunat = value
        End Set
    End Property

    Public Property gmotivoAnulacionEnvioSunat
        Get
            Return motivoAnulacionEnvioSunat
        End Get
        Set(ByVal value)
            motivoAnulacionEnvioSunat = value
        End Set
    End Property

    Public Sub New()

    End Sub



    Public Sub New(ByVal idventa As Integer, ByVal idcliente As Integer, ByVal fecha_venta As DateTime, ByVal tipo_documento As String, ByVal num_documento As String, ByVal idusuario As Integer, ByVal serie_documento As String)

        gidventa = idventa
        gidcliente = idcliente
        gfecha_venta = fecha_venta
        gtipo_documento = tipo_documento
        gnum_documento = num_documento
        gidusuario = idusuario
        gserie_documento = serie_documento

    End Sub
End Class
