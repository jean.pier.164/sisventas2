﻿Public Class vcompras

    Dim idcompras, idusuario As Integer
    Dim fecha_compra As Date
    Dim tipo_documento, serie_documento, num_documento As String


    Public Property gidcompras
        Get
            Return idcompras
        End Get
        Set(ByVal value)
            idcompras = value
        End Set
    End Property


    Public Property gidusuario
        Get
            Return idusuario
        End Get
        Set(ByVal value)
            idusuario = value
        End Set
    End Property

    Public Property gfecha_compra
        Get
            Return fecha_compra
        End Get
        Set(ByVal value)
            fecha_compra = value
        End Set
    End Property


    Public Property gtipo_documento
        Get
            Return tipo_documento
        End Get
        Set(ByVal value)
            tipo_documento = value
        End Set
    End Property


    Public Property gserie_documento
        Get
            Return serie_documento
        End Get
        Set(ByVal value)
            serie_documento = value
        End Set
    End Property


    Public Property gnum_documento
        Get
            Return num_documento
        End Get
        Set(ByVal value)
            num_documento = value
        End Set
    End Property








    Public Sub New()

    End Sub



    Public Sub New(ByVal idcompras As Integer, ByVal fecha_compra As Date, ByVal tipo_documento As String, ByVal serie_documento As String, ByVal num_documento As String, ByVal idusuario As Integer)

        gidcompras = idcompras
        gfecha_compra = fecha_compra
        gtipo_documento = tipo_documento
        gserie_documento = serie_documento
        gnum_documento = num_documento
        gidusuario = idusuario
    End Sub

End Class
