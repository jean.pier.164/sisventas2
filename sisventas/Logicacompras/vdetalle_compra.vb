﻿Public Class vdetalle_compra
    Dim iddetalle_compra, idcompras, idproductoscompra As Integer
    Dim cantidad, precio_unit As Double



    Public Property giddetalle_compra
        Get
            Return iddetalle_compra
        End Get
        Set(ByVal value)
            iddetalle_compra = value
        End Set
    End Property




    Public Property gidcompras
        Get
            Return idcompras
        End Get
        Set(ByVal value)
            idcompras = value
        End Set
    End Property




    Public Property gidproductoscompra
        Get
            Return idproductoscompra
        End Get
        Set(ByVal value)
            idproductoscompra = value
        End Set
    End Property
    Public Property gcantidad
        Get
            Return cantidad
        End Get
        Set(ByVal value)
            cantidad = value
        End Set
    End Property
    Public Property gprecio_unit
        Get
            Return precio_unit
        End Get
        Set(ByVal value)
            precio_unit = value
        End Set
    End Property



    Public Sub New()

    End Sub



    Public Sub New(ByVal iddetalle_compra As Integer, ByVal idcompras As Integer, ByVal idproductoscompra As Integer, ByVal cantidad As Double, ByVal precio_unit As Double)

        giddetalle_compra = iddetalle_compra
        gidcompras = idcompras
        gidproductoscompra = idproductoscompra
        gcantidad = cantidad
        gprecio_unit = precio_unit

    End Sub
End Class
