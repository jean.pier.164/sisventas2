﻿Imports System.IO

Public Class frmAddDetalleNota
    Private dt As New DataTable
    Public detNota As vNotaDebitoDetalle = Nothing

    Private Sub btnFacturasPDF_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        'System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FACTURAS_PDF\\"))
        If txtdescProd.Text.Trim.Length = 0 Then
            MessageBox.Show("Ingrese la descripción del producto")
            Exit Sub
        End If

        If txtPrecio.Text.Trim.Length = 0 Then
            MessageBox.Show("Ingrese el precio del producto")
            Exit Sub
        End If

        Dim Aleatorio As Integer
        Randomize() ' inicializar la semilla  
        Aleatorio = CLng((500 - 1000) * Rnd() + 1000)

        detNota = New vNotaDebitoDetalle
        detNota.gcantidad = cmbCantidad.Text
        detNota.gcundm = 1
        detNota.gsundm = 1
        detNota.gundm = txtUndMedida.Text
        detNota.gdesproducto = txtdescProd.Text.ToUpper.Trim
        detNota.gprecio_compra = txtPrecio.Text
        detNota.gprecio_unitario = txtPrecio.Text
        detNota.gidproducto = 1
        detNota.giddetalle_nota_debito = Aleatorio
        frmnota_debito.detNota = detNota

        Me.Dispose()
    End Sub

    Private Sub TextBox3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        e.Handled = Not (Char.IsDigit(e.KeyChar) Or e.KeyChar = ".")
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Dispose()
    End Sub

    Private Sub frmAddDetalleNota_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'cmbCantidad.SelectedText = "1"
        'cmbCantidad.SelectedValue = "1"
        cmbCantidad.SelectedIndex = 0
    End Sub
End Class