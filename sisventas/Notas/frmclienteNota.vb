﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Xml
Imports Newtonsoft.Json.Linq

Public Class frmclienteNota

    Private dt As New DataTable

    Private Sub frmcliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()
        'txtnombre.Text = ""
        'txtapellidos.Text = "SN"
        'txtdireccion.Text = "SN"
        'txttelefono.Text = "SN"
        'txtdni.Text = ""
        txtidcliente.Text = "SN"

        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add("1", "DNI")
        comboSource.Add("6", "RUC")
        comboSource.Add("4", "CARNET DE EXTRANJERIA")
        comboSource.Add("7", "PASAPORTE")

        'cmbTipoDocCliente.DataSource = New BindingSource(comboSource, Nothing)
        'cmbTipoDocCliente.DisplayMember = "Value"
        'cmbTipoDocCliente.ValueMember = "Key"

    End Sub
    Public Sub limpiar()


        'txtnombre.Text = ""
        'txtapellidos.Text = ""
        'txtdireccion.Text = ""
        'txttelefono.Text = ""
        'txtdni.Text = ""
        txtidcliente.Text = ""

    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fcliente
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub ocultar_columnas()
        datalistado.Columns(1).Visible = False

    End Sub

    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        limpiar()
        mostrar()

    End Sub


    Private Sub datalistado_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        If txtflag.Text = "1" Then
            frmnota_credito.sIdCliente = datalistado.SelectedCells.Item(1).Value
            frmnota_credito.sNombreCliente = datalistado.SelectedCells.Item(2).Value
            frmnota_credito.sNroDocCliente = datalistado.SelectedCells.Item(6).Value

            frmnota_debito.sIdCliente = datalistado.SelectedCells.Item(1).Value
            frmnota_debito.sNombreCliente = datalistado.SelectedCells.Item(2).Value
            frmnota_debito.sNroDocCliente = datalistado.SelectedCells.Item(6).Value

            'frmnota_credito.setNombreCliente(datalistado.SelectedCells.Item(2).Value)

            'Dim str As String = frmnota_credito.txtidcliente.Text

            'MessageBox.Show(frmnota_credito.txtidcliente.Text)

            If Not IsDBNull(datalistado.SelectedCells.Item(9).Value) Then
                frmnota_credito.sEmailCliente = datalistado.SelectedCells.Item(9).Value
                frmnota_debito.sEmailCliente = datalistado.SelectedCells.Item(9).Value
            End If

            Me.Hide()

        End If
    End Sub

    Private Sub txtbuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbuscar.TextChanged
        buscar()
    End Sub

End Class