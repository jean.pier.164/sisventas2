﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmnota_credito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.generar_comprobantemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketdscomprobante = New BRAVOSPORT.marketdscomprobante()
        Me.txtidcliente = New System.Windows.Forms.TextBox()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me.btnbuscar_cliente = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtidproducto = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbTipoNota = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtIdTipoVenta = New System.Windows.Forms.TextBox()
        Me.txtNumeracionVenta = New System.Windows.Forms.TextBox()
        Me.txtSerieVenta = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.txtfechaEmisionComprobante = New System.Windows.Forms.TextBox()
        Me.txtcomprobante = New System.Windows.Forms.TextBox()
        Me.txttipodoccliente = New System.Windows.Forms.TextBox()
        Me.txtnroDocCliente = New System.Windows.Forms.TextBox()
        Me.txtnombreCliente = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnEnviarLuegoSunat = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.btnEnvioEmail = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnmodificar = New System.Windows.Forms.Button()
        Me.btnEnviarSunat = New System.Windows.Forms.Button()
        Me.btnsalirr = New System.Windows.Forms.Button()
        Me.btnpedido = New System.Windows.Forms.Button()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.txtpcompra = New System.Windows.Forms.TextBox()
        Me.txtflag = New System.Windows.Forms.TextBox()
        Me.ProductoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Mmdsnombre = New BRAVOSPORT.mmdsnombre()
        Me.ProductoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MarketmayoristaDataSet = New BRAVOSPORT.marketmayoristaDataSet()
        Me.MarketDataSetdosfechas = New BRAVOSPORT.marketDataSetdosfechas()
        Me.erroricono = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtidusuario = New System.Windows.Forms.TextBox()
        Me.txtusuario = New System.Windows.Forms.TextBox()
        Me.generar_comprobantemTableAdapter = New BRAVOSPORT.marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter()
        Me.recibos = New BRAVOSPORT.recibos()
        Me.MarketDataSetticket1 = New BRAVOSPORT.marketDataSetticket()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Generar_comprobanteTableAdapter1 = New BRAVOSPORT.marketDataSetticketTableAdapters.generar_comprobanteTableAdapter()
        Me.generar_comprobanteTableAdapter = New BRAVOSPORT.marketDataSetticketTableAdapters.generar_comprobanteTableAdapter()
        Me.generar_comprobanteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketDataSetticket = New BRAVOSPORT.marketDataSetticket()
        Me.ProductoTableAdapter = New BRAVOSPORT.marketmayoristaDataSetTableAdapters.productoTableAdapter()
        Me.Generar_comprobantemTableAdapter1 = New BRAVOSPORT.recibosTableAdapters.generar_comprobantemTableAdapter()
        Me.ProductoTableAdapter1 = New BRAVOSPORT.mmdsnombreTableAdapters.productoTableAdapter()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Mmdsnombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketmayoristaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketDataSetdosfechas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketDataSetticket1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.generar_comprobanteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketDataSetticket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'generar_comprobantemBindingSource
        '
        Me.generar_comprobantemBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobantemBindingSource.DataSource = Me.marketdscomprobante
        '
        'marketdscomprobante
        '
        Me.marketdscomprobante.DataSetName = "marketdscomprobante"
        Me.marketdscomprobante.EnforceConstraints = False
        Me.marketdscomprobante.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txtidcliente
        '
        Me.txtidcliente.Enabled = False
        Me.txtidcliente.Location = New System.Drawing.Point(661, 102)
        Me.txtidcliente.Name = "txtidcliente"
        Me.txtidcliente.Size = New System.Drawing.Size(29, 25)
        Me.txtidcliente.TabIndex = 2
        Me.txtidcliente.Visible = False
        '
        'txtidventa
        '
        Me.txtidventa.Enabled = False
        Me.txtidventa.Location = New System.Drawing.Point(423, 31)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(124, 25)
        Me.txtidventa.TabIndex = 0
        Me.txtidventa.Visible = False
        '
        'btnbuscar_cliente
        '
        Me.btnbuscar_cliente.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btnbuscar_cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscar_cliente.ForeColor = System.Drawing.Color.White
        Me.btnbuscar_cliente.Location = New System.Drawing.Point(176, 29)
        Me.btnbuscar_cliente.Name = "btnbuscar_cliente"
        Me.btnbuscar_cliente.Size = New System.Drawing.Size(176, 27)
        Me.btnbuscar_cliente.TabIndex = 23
        Me.btnbuscar_cliente.Text = "SELECCIONAR CLIENTE"
        Me.btnbuscar_cliente.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(38, 33)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 19)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Cliente:"
        '
        'txtidproducto
        '
        Me.txtidproducto.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidproducto.Location = New System.Drawing.Point(833, 16)
        Me.txtidproducto.Name = "txtidproducto"
        Me.txtidproducto.Size = New System.Drawing.Size(61, 20)
        Me.txtidproducto.TabIndex = 20
        Me.txtidproducto.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbTipoNota)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtIdTipoVenta)
        Me.GroupBox2.Controls.Add(Me.txtNumeracionVenta)
        Me.GroupBox2.Controls.Add(Me.txtSerieVenta)
        Me.GroupBox2.Controls.Add(Me.txtidventa)
        Me.GroupBox2.Controls.Add(Me.txtidcliente)
        Me.GroupBox2.Controls.Add(Me.TextBox1)
        Me.GroupBox2.Controls.Add(Me.txtfechaEmisionComprobante)
        Me.GroupBox2.Controls.Add(Me.txtcomprobante)
        Me.GroupBox2.Controls.Add(Me.txttipodoccliente)
        Me.GroupBox2.Controls.Add(Me.txtnroDocCliente)
        Me.GroupBox2.Controls.Add(Me.txtnombreCliente)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.btnbuscar_cliente)
        Me.GroupBox2.Controls.Add(Me.btnEnviarLuegoSunat)
        Me.GroupBox2.Controls.Add(Me.GroupBox5)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.btnpedido)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtpcompra)
        Me.GroupBox2.Controls.Add(Me.txtflag)
        Me.GroupBox2.Controls.Add(Me.txtidproducto)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox2.Location = New System.Drawing.Point(18, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(932, 609)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "NOTA DE CRÉDITO"
        '
        'cmbTipoNota
        '
        Me.cmbTipoNota.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTipoNota.FormattingEnabled = True
        Me.cmbTipoNota.Location = New System.Drawing.Point(244, 255)
        Me.cmbTipoNota.Name = "cmbTipoNota"
        Me.cmbTipoNota.Size = New System.Drawing.Size(352, 26)
        Me.cmbTipoNota.TabIndex = 103
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(38, 257)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 19)
        Me.Label2.TabIndex = 102
        Me.Label2.Text = "Tipo:"
        '
        'txtIdTipoVenta
        '
        Me.txtIdTipoVenta.Location = New System.Drawing.Point(711, 176)
        Me.txtIdTipoVenta.Name = "txtIdTipoVenta"
        Me.txtIdTipoVenta.Size = New System.Drawing.Size(76, 25)
        Me.txtIdTipoVenta.TabIndex = 101
        Me.txtIdTipoVenta.Visible = False
        '
        'txtNumeracionVenta
        '
        Me.txtNumeracionVenta.Location = New System.Drawing.Point(614, 176)
        Me.txtNumeracionVenta.Name = "txtNumeracionVenta"
        Me.txtNumeracionVenta.Size = New System.Drawing.Size(76, 25)
        Me.txtNumeracionVenta.TabIndex = 100
        Me.txtNumeracionVenta.Visible = False
        '
        'txtSerieVenta
        '
        Me.txtSerieVenta.Location = New System.Drawing.Point(520, 176)
        Me.txtSerieVenta.Name = "txtSerieVenta"
        Me.txtSerieVenta.Size = New System.Drawing.Size(76, 25)
        Me.txtSerieVenta.TabIndex = 99
        Me.txtSerieVenta.Visible = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(589, 102)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(57, 25)
        Me.TextBox1.TabIndex = 98
        Me.TextBox1.Visible = False
        '
        'txtfechaEmisionComprobante
        '
        Me.txtfechaEmisionComprobante.Location = New System.Drawing.Point(244, 212)
        Me.txtfechaEmisionComprobante.Name = "txtfechaEmisionComprobante"
        Me.txtfechaEmisionComprobante.ReadOnly = True
        Me.txtfechaEmisionComprobante.Size = New System.Drawing.Size(147, 25)
        Me.txtfechaEmisionComprobante.TabIndex = 97
        '
        'txtcomprobante
        '
        Me.txtcomprobante.Location = New System.Drawing.Point(244, 176)
        Me.txtcomprobante.Name = "txtcomprobante"
        Me.txtcomprobante.ReadOnly = True
        Me.txtcomprobante.Size = New System.Drawing.Size(234, 25)
        Me.txtcomprobante.TabIndex = 96
        '
        'txttipodoccliente
        '
        Me.txttipodoccliente.Location = New System.Drawing.Point(507, 102)
        Me.txttipodoccliente.Name = "txttipodoccliente"
        Me.txttipodoccliente.Size = New System.Drawing.Size(66, 25)
        Me.txttipodoccliente.TabIndex = 95
        Me.txttipodoccliente.Visible = False
        '
        'txtnroDocCliente
        '
        Me.txtnroDocCliente.Location = New System.Drawing.Point(244, 102)
        Me.txtnroDocCliente.Name = "txtnroDocCliente"
        Me.txtnroDocCliente.ReadOnly = True
        Me.txtnroDocCliente.Size = New System.Drawing.Size(234, 25)
        Me.txtnroDocCliente.TabIndex = 94
        '
        'txtnombreCliente
        '
        Me.txtnombreCliente.Location = New System.Drawing.Point(244, 70)
        Me.txtnombreCliente.Name = "txtnombreCliente"
        Me.txtnombreCliente.ReadOnly = True
        Me.txtnombreCliente.Size = New System.Drawing.Size(478, 25)
        Me.txtnombreCliente.TabIndex = 93
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label12.Location = New System.Drawing.Point(74, 215)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(115, 18)
        Me.Label12.TabIndex = 92
        Me.Label12.Text = "Fecha Emisión:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label11.Location = New System.Drawing.Point(74, 179)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(108, 18)
        Me.Label11.TabIndex = 91
        Me.Label11.Text = "Comprobante:"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(176, 137)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(215, 27)
        Me.Button2.TabIndex = 90
        Me.Button2.Text = "SELECCIONAR COMPROBANTE"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label9.Location = New System.Drawing.Point(38, 140)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 19)
        Me.Label9.TabIndex = 89
        Me.Label9.Text = "Comprobante:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(74, 105)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(114, 18)
        Me.Label8.TabIndex = 88
        Me.Label8.Text = "Doc. Identidad:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 11.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(74, 73)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(164, 18)
        Me.Label7.TabIndex = 87
        Me.Label7.Text = "Nombre/Razón Social:"
        '
        'btnEnviarLuegoSunat
        '
        Me.btnEnviarLuegoSunat.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarLuegoSunat.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarLuegoSunat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnEnviarLuegoSunat.Location = New System.Drawing.Point(765, 534)
        Me.btnEnviarLuegoSunat.Name = "btnEnviarLuegoSunat"
        Me.btnEnviarLuegoSunat.Size = New System.Drawing.Size(160, 63)
        Me.btnEnviarLuegoSunat.TabIndex = 84
        Me.btnEnviarLuegoSunat.Text = "Enviar luego a Sunat"
        Me.btnEnviarLuegoSunat.UseVisualStyleBackColor = False
        Me.btnEnviarLuegoSunat.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lblEmail)
        Me.GroupBox5.Controls.Add(Me.btnEnvioEmail)
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Controls.Add(Me.txtEmail)
        Me.GroupBox5.Location = New System.Drawing.Point(17, 543)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(730, 54)
        Me.GroupBox5.TabIndex = 86
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Envío Comprobante al cliente"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmail.ForeColor = System.Drawing.Color.DarkGreen
        Me.lblEmail.Location = New System.Drawing.Point(14, 56)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(0, 15)
        Me.lblEmail.TabIndex = 3
        '
        'btnEnvioEmail
        '
        Me.btnEnvioEmail.Location = New System.Drawing.Point(609, 26)
        Me.btnEnvioEmail.Name = "btnEnvioEmail"
        Me.btnEnvioEmail.Size = New System.Drawing.Size(114, 28)
        Me.btnEnvioEmail.TabIndex = 2
        Me.btnEnvioEmail.Text = "ENVIAR"
        Me.btnEnvioEmail.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Email:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(78, 28)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(525, 25)
        Me.txtEmail.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.btnmodificar)
        Me.GroupBox3.Controls.Add(Me.btnEnviarSunat)
        Me.GroupBox3.Controls.Add(Me.btnsalirr)
        Me.GroupBox3.Location = New System.Drawing.Point(17, 478)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(770, 56)
        Me.GroupBox3.TabIndex = 84
        Me.GroupBox3.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Highlight
        Me.Button1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button1.Location = New System.Drawing.Point(6, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(172, 41)
        Me.Button1.TabIndex = 30
        Me.Button1.Text = "Quitar Articulo"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnmodificar
        '
        Me.btnmodificar.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnmodificar.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnmodificar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnmodificar.Location = New System.Drawing.Point(284, 27)
        Me.btnmodificar.Name = "btnmodificar"
        Me.btnmodificar.Size = New System.Drawing.Size(10, 27)
        Me.btnmodificar.TabIndex = 57
        Me.btnmodificar.Text = "Modificar"
        Me.btnmodificar.UseVisualStyleBackColor = False
        Me.btnmodificar.Visible = False
        '
        'btnEnviarSunat
        '
        Me.btnEnviarSunat.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarSunat.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarSunat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnEnviarSunat.Location = New System.Drawing.Point(364, 18)
        Me.btnEnviarSunat.Name = "btnEnviarSunat"
        Me.btnEnviarSunat.Size = New System.Drawing.Size(176, 41)
        Me.btnEnviarSunat.TabIndex = 83
        Me.btnEnviarSunat.Text = "Enviar Sunat"
        Me.btnEnviarSunat.UseVisualStyleBackColor = False
        '
        'btnsalirr
        '
        Me.btnsalirr.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnsalirr.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsalirr.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnsalirr.Location = New System.Drawing.Point(597, 18)
        Me.btnsalirr.Name = "btnsalirr"
        Me.btnsalirr.Size = New System.Drawing.Size(160, 36)
        Me.btnsalirr.TabIndex = 20
        Me.btnsalirr.Text = "Nueva NC"
        Me.btnsalirr.UseVisualStyleBackColor = False
        Me.btnsalirr.Visible = False
        '
        'btnpedido
        '
        Me.btnpedido.Location = New System.Drawing.Point(968, 593)
        Me.btnpedido.Name = "btnpedido"
        Me.btnpedido.Size = New System.Drawing.Size(10, 15)
        Me.btnpedido.TabIndex = 66
        Me.btnpedido.Text = "ImPedido"
        Me.btnpedido.UseVisualStyleBackColor = True
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.inexistente.ForeColor = System.Drawing.Color.Red
        Me.inexistente.Location = New System.Drawing.Point(358, 319)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(170, 22)
        Me.inexistente.TabIndex = 52
        Me.inexistente.Text = "Datos Inexistente"
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Location = New System.Drawing.Point(16, 303)
        Me.datalistado.MultiSelect = False
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(897, 169)
        Me.datalistado.TabIndex = 51
        '
        'txtpcompra
        '
        Me.txtpcompra.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpcompra.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtpcompra.Location = New System.Drawing.Point(833, 102)
        Me.txtpcompra.Name = "txtpcompra"
        Me.txtpcompra.Size = New System.Drawing.Size(74, 21)
        Me.txtpcompra.TabIndex = 82
        Me.txtpcompra.Text = "1"
        Me.txtpcompra.Visible = False
        '
        'txtflag
        '
        Me.txtflag.Location = New System.Drawing.Point(833, 50)
        Me.txtflag.Name = "txtflag"
        Me.txtflag.Size = New System.Drawing.Size(67, 25)
        Me.txtflag.TabIndex = 55
        Me.txtflag.Visible = False
        '
        'ProductoBindingSource1
        '
        Me.ProductoBindingSource1.DataMember = "producto"
        Me.ProductoBindingSource1.DataSource = Me.Mmdsnombre
        '
        'Mmdsnombre
        '
        Me.Mmdsnombre.DataSetName = "mmdsnombre"
        Me.Mmdsnombre.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProductoBindingSource
        '
        Me.ProductoBindingSource.DataMember = "producto"
        Me.ProductoBindingSource.DataSource = Me.MarketmayoristaDataSet
        '
        'MarketmayoristaDataSet
        '
        Me.MarketmayoristaDataSet.DataSetName = "marketmayoristaDataSet"
        Me.MarketmayoristaDataSet.EnforceConstraints = False
        Me.MarketmayoristaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketDataSetdosfechas
        '
        Me.MarketDataSetdosfechas.DataSetName = "marketDataSetdosfechas"
        Me.MarketDataSetdosfechas.EnforceConstraints = False
        Me.MarketDataSetdosfechas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'erroricono
        '
        Me.erroricono.ContainerControl = Me
        '
        'txtidusuario
        '
        Me.txtidusuario.Enabled = False
        Me.txtidusuario.Location = New System.Drawing.Point(18, -4)
        Me.txtidusuario.Name = "txtidusuario"
        Me.txtidusuario.Size = New System.Drawing.Size(10, 20)
        Me.txtidusuario.TabIndex = 6
        Me.txtidusuario.Visible = False
        '
        'txtusuario
        '
        Me.txtusuario.Enabled = False
        Me.txtusuario.Location = New System.Drawing.Point(32, -4)
        Me.txtusuario.Name = "txtusuario"
        Me.txtusuario.Size = New System.Drawing.Size(10, 20)
        Me.txtusuario.TabIndex = 7
        Me.txtusuario.Visible = False
        '
        'generar_comprobantemTableAdapter
        '
        Me.generar_comprobantemTableAdapter.ClearBeforeFill = True
        '
        'recibos
        '
        Me.recibos.DataSetName = "recibos"
        Me.recibos.EnforceConstraints = False
        Me.recibos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketDataSetticket1
        '
        Me.MarketDataSetticket1.DataSetName = "marketDataSetticket"
        Me.MarketDataSetticket1.EnforceConstraints = False
        Me.MarketDataSetticket1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "generar_comprobante"
        Me.BindingSource1.DataSource = Me.MarketDataSetticket1
        '
        'Generar_comprobanteTableAdapter1
        '
        Me.Generar_comprobanteTableAdapter1.ClearBeforeFill = True
        '
        'generar_comprobanteTableAdapter
        '
        Me.generar_comprobanteTableAdapter.ClearBeforeFill = True
        '
        'generar_comprobanteBindingSource
        '
        Me.generar_comprobanteBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobanteBindingSource.DataSource = Me.recibos
        '
        'marketDataSetticket
        '
        Me.marketDataSetticket.DataSetName = "marketDataSetticket"
        Me.marketDataSetticket.EnforceConstraints = False
        Me.marketDataSetticket.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProductoTableAdapter
        '
        Me.ProductoTableAdapter.ClearBeforeFill = True
        '
        'Generar_comprobantemTableAdapter1
        '
        Me.Generar_comprobantemTableAdapter1.ClearBeforeFill = True
        '
        'ProductoTableAdapter1
        '
        Me.ProductoTableAdapter1.ClearBeforeFill = True
        '
        'frmnota_credito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(964, 633)
        Me.Controls.Add(Me.txtusuario)
        Me.Controls.Add(Me.txtidusuario)
        Me.Controls.Add(Me.GroupBox2)
        Me.ForeColor = System.Drawing.Color.Cornsilk
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmnota_credito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NOTA DE CREDITO"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Mmdsnombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketmayoristaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketDataSetdosfechas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketDataSetticket1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.generar_comprobanteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketDataSetticket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtidcliente As System.Windows.Forms.TextBox
    Friend WithEvents txtidventa As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents erroricono As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtidproducto As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnsalirr As System.Windows.Forms.Button
    Friend WithEvents txtusuario As System.Windows.Forms.TextBox
    Friend WithEvents txtidusuario As System.Windows.Forms.TextBox
    Friend WithEvents btnbuscar_cliente As System.Windows.Forms.Button
    Friend WithEvents inexistente As System.Windows.Forms.Label
    Friend WithEvents datalistado As System.Windows.Forms.DataGridView
    Friend WithEvents txtflag As TextBox
    Friend WithEvents btnmodificar As Button
    Friend WithEvents btnpedido As Button
    Friend WithEvents generar_comprobantemTableAdapter As marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents MarketDataSetdosfechas As marketDataSetdosfechas
    Friend WithEvents recibos As recibos
    Friend WithEvents txtpcompra As TextBox
    Friend WithEvents MarketDataSetticket1 As marketDataSetticket
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents Generar_comprobanteTableAdapter1 As marketDataSetticketTableAdapters.generar_comprobanteTableAdapter
    Friend WithEvents marketdscomprobante As marketdscomprobante
    Friend WithEvents generar_comprobantemBindingSource As BindingSource
    Friend WithEvents generar_comprobanteTableAdapter As marketDataSetticketTableAdapters.generar_comprobanteTableAdapter
    Friend WithEvents generar_comprobanteBindingSource As BindingSource
    Friend WithEvents marketDataSetticket As marketDataSetticket
    Friend WithEvents MarketmayoristaDataSet As marketmayoristaDataSet
    Friend WithEvents ProductoBindingSource As BindingSource
    Friend WithEvents ProductoTableAdapter As marketmayoristaDataSetTableAdapters.productoTableAdapter
    Friend WithEvents btnEnviarSunat As Button
    Friend WithEvents Generar_comprobantemTableAdapter1 As recibosTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents btnEnvioEmail As Button
    Friend WithEvents lblEmail As Label
    Friend WithEvents Mmdsnombre As mmdsnombre
    Friend WithEvents ProductoBindingSource1 As BindingSource
    Friend WithEvents ProductoTableAdapter1 As mmdsnombreTableAdapters.productoTableAdapter
    Friend WithEvents btnEnviarLuegoSunat As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txttipodoccliente As TextBox
    Friend WithEvents txtnroDocCliente As TextBox
    Friend WithEvents txtnombreCliente As TextBox
    Friend WithEvents txtfechaEmisionComprobante As TextBox
    Friend WithEvents txtcomprobante As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents txtIdTipoVenta As TextBox
    Friend WithEvents txtNumeracionVenta As TextBox
    Friend WithEvents txtSerieVenta As TextBox
    Friend WithEvents cmbTipoNota As ComboBox
    Friend WithEvents Label2 As Label
End Class
