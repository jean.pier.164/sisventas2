﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO
Imports System
Imports FinalXML
Imports FinalXML.Interfaces
Imports FinalXML.InterMySql
Imports Microsoft.Reporting.WinForms
Imports Gma.QrCodeNet.Encoding
Imports Gma.QrCodeNet.Encoding.Windows.Render
Imports System.Drawing.Imaging
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading.Tasks
Imports System.Reflection
Imports System.ComponentModel

Public Class frmnota_debito

    Shared conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Shared dt As New DataTable
    Shared dtNota As New DataTable
    Private dtn As New DataTable

    Dim SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Dim SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Dim SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Shared USEREMAIL As String = ConfigurationManager.AppSettings.Get("USEREMAIL")

    Shared NROCAJAFE As String = ConfigurationManager.AppSettings.Get("NROCAJAFE")

    Shared rutaArchivoXmlEmail As String = ""
    Shared rutaPdfEmail As String = ""

    Dim _documento As DocumentoElectronico
    Dim ConvertLetras As Conversion = New Conversion
    Dim herramientas As Herramientas = New Herramientas

    Public Shared sIdCliente As String
    Public Shared sNombreCliente As String
    Public Shared sNroDocCliente As String
    Public Shared sEmailCliente As String

    Public Shared sIdVenta As String
    Public Shared sFechaVenta As String
    Public Shared sIdTipoVenta As String
    Public Shared sSerieVenta As String
    Public Shared sNumeracionVenta As String

    Shared listIdDetalleVenta As New List(Of Integer)
    Public Shared detNota As vNotaDebitoDetalle = Nothing
    Shared ListdetNota As New List(Of vNotaDebitoDetalle)

    Dim RutaArchivo As String
    Shared recursos As String
    Shared cadenaConexion As String = ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString

    Private Sub mostrar_detalleventa(ByVal detnota As vNotaDebitoDetalle)
        Try
            'Dim func As New fdetalle_venta
            'dt = func.mostrar_detalleventa(txtidventa.Text)
            'dt.a
            If Not detnota Is Nothing Then
                ListdetNota.Add(detnota)
                dtNota = ConvertToDataTable(ListdetNota)
            End If

            If dtNota.Rows.Count <> 0 Then
                datalistado.DataSource = dtNota


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
                ocultar_columnas()

            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


    End Sub

    Private Sub frmdetalle_venta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'Mmdsnombre.producto' Puede moverla o quitarla según sea necesario.
        Me.ProductoTableAdapter1.Fill(Me.Mmdsnombre.producto)


        recursos = herramientas.GetResourcesPath()

        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add("01", "INTERESES POR MORA")
        comboSource.Add("02", "AUMENTO EN EL VALOR")
        comboSource.Add("03", "PENALIDADES")
        'comboSource.Add("04", "DESCUENTO GLOBAL")
        'comboSource.Add("05", "DESCUENTO POR ITEM")
        'comboSource.Add("06", "DEVOLUCIÓN TOTAL")
        'comboSource.Add("07", "DEVOLUCIÓN POR ITEM")
        'comboSource.Add("08", "BONIFICACIÓN")
        'comboSource.Add("09", "DISMINUCIÓN EN EL VALOR")

        cmbTipoNota.DataSource = New BindingSource(comboSource, Nothing)
        cmbTipoNota.DisplayMember = "Value"
        cmbTipoNota.ValueMember = "Key"

        cmbTipoNota.SelectedIndex = 0
        'txtbuscar.Focus()
        'txtdinero.Text = 0

        'txtcanttotal.Text = "1"
        'txtcundm.Text = "1"
        'cboundm.Text = "UND"
        'cbosundm.Text = "1"
        'txtidventa.Text = 1

        mostrarusuario()
        'mostrar_detalleventa()
        'txtidcliente.Text = "3067"
        'txtnombre_cliente.Text = "CLIENTE CASUAL"
        'txtserie_documento.Text = DateTime.Now.ToShortTimeString()
        'txtnum_documento.Text = DateTime.Now.ToString()


        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("ImporteVenta").Value IsNot Nothing) AndAlso
                    (row.Cells("ImporteVenta").Value IsNot DBNull.Value))
                Select row.Cells("ImporteVenta").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
            'lbltotal.Text = String.Format(resultado)



        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        'If Not txtidventa.Text.Equals("") Then
        '    Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)

        '    Dim ReportDataSource As ReportDataSource = New ReportDataSource()
        '    ReportDataSource.Value = generar_comprobantemBindingSource
        '    ReportDataSource.Name = "recibos"
        '    ReportViewer1.LocalReport.DataSources.Add(ReportDataSource)

        '    Me.ReportViewer1.RefreshReport()
        'End If

        btnEnviarSunat.Enabled = True

    End Sub

    Public Sub limpiar()
        txtidproducto.Text = ""
        txtnombreCliente.Text = ""
        txtnroDocCliente.Text = ""
        txtcomprobante.Text = ""
        txtfechaEmisionComprobante.Text = ""
        txtidcliente.Text = ""
        txtidventa.Text = ""
        txttipodoccliente.Text = ""

    End Sub

    Private Sub mostrarusuario()

        Dim func As New fusuario
        dt = func.mostrarusuNC()
        txtidusuario.Text = dt.Rows(0).Item("idusuario").ToString
        txtusuario.Text = dt.Rows(0).Item("login").ToString
    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fdetalle_venta
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



        buscar()

    End Sub

    Private Sub buscar()
        Try
            Dim ds As New DataSet
            ds.Tables.Add(dt.Copy)
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = "idventa=  '" & txtidventa.Text & "'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()
            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ocultar_columnas()
        datalistado.Columns(0).Visible = False
        datalistado.Columns(1).Visible = False
        datalistado.Columns(2).Visible = False
        datalistado.Columns(3).Visible = False
        datalistado.Columns(7).Visible = False
        datalistado.Columns(8).Visible = False
    End Sub



    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        limpiar()
        mostrar()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim result As DialogResult
        ' = MessageBox.Show("Realmente desea quitar los articulos de venta?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        ' result = DialogResult.OK Then
        Dim idDetalleVenta As Integer

        If datalistado.SelectedRows.Count > 0 Then
            'you may want to add a confirmation message, and if the user confirms delete
            idDetalleVenta = Convert.ToInt32(datalistado.CurrentRow.Cells("giddetalle_nota_debito").Value)
            listIdDetalleVenta.Add(idDetalleVenta)
            datalistado.Rows.Remove(datalistado.SelectedRows(0))

            If ListdetNota.Count > 0 Then
                Dim count As Integer = 0
                For Each detnot As vNotaDebitoDetalle In ListdetNota
                    If detnot.giddetalle_nota_debito = idDetalleVenta Then
                        ListdetNota.RemoveAt(count)
                    End If
                    count = count + 1
                Next
            End If

        Else
            MessageBox.Show("Seleccione un producto a eliminar!")
        End If

        Exit Sub



        'Try
        '    For Each row As DataGridViewRow In datalistado.Rows
        '        'Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)
        '        Dim marcado As Boolean = Convert.ToBoolean(row.Cells(0).Value)
        '        If marcado Then
        '            'Dim onekey As Integer = Convert.ToInt32(row.Cells("iddetalle_venta").Value)
        '            Dim onekey As Integer = Convert.ToInt32(datalistado.CurrentRow.Cells("iddetalle_venta").Value)
        '            Dim vdb As New vdetalle_venta
        '            Dim func As New fdetalle_venta

        '            vdb.giddetalle_venta = onekey
        '            vdb.gidventa = datalistado.SelectedCells.Item(1).Value
        '            vdb.gidproducto = datalistado.SelectedCells.Item(2).Value
        '            vdb.gcantidad = datalistado.SelectedCells.Item(4).Value

        '            If func.eliminar(vdb) Then
        '                If func.aumentar_stock(vdb) Then

        '                End If
        '            Else
        '                ' MessageBox.Show("Articulo fue quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '            End If

        '        End If
        '    Next

        '    Call mostrar_detalleventa(Nothing)
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
        'Else
        'MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'Call mostrar()
        'End If
        Try
            Dim query As IEnumerable(Of Object) =
                    From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                    Where (
                        (row.Cells("subtotal").Value IsNot Nothing) AndAlso
                        (row.Cells("subtotal").Value IsNot DBNull.Value)) Select row.Cells("subtotal").Value()

            Dim resultado As Decimal =
                    query.Sum(Function(row) Convert.ToDecimal(row))
            'lbltotal.Text = String.Format(resultado)


            frmproducto.btneditar.PerformClick()

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
        'Try
        '    Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)
        '    Me.ReportViewer1.RefreshReport()




        'Catch ex As Exception
        '    Me.ReportViewer1.RefreshReport()
        'End Try
        txtidproducto.Text = ""
        'txtnombre_producto.Text = ""
        'txtprecio_unitario.Text = ""
        'mostrar_detalleventa(Nothing)

        'lblvuelto.Text = txtdinero.Text - lbltotal.Text
    End Sub

    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmRecibo._documento = _documento
        frmRecibo.txtidventa.Text = txtidventa.Text
        frmRecibo.ShowDialog()
    End Sub
    Private Sub txtprecio_unitario_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Me.erroricono.SetError(sender, "")
        Else
            Me.erroricono.SetError(sender, "Ingrese precio unitario")
        End If
    End Sub

    Private Sub cboproduct_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mostrar_nproductos()
    End Sub

    Private Sub mostrar_nproductos()

        Dim func As New fnproductos
        dtn = func.mostrarnproducto
    End Sub

    Private Sub btnbuscar_cliente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbuscar_cliente.Click
        frmclienteNota.txtflag.Text = "1"
        frmclienteNota.ShowDialog()

        txtidcliente.Text = sIdCliente
        txtnombreCliente.Text = sNombreCliente
        txtnroDocCliente.Text = sNroDocCliente
        txtEmail.Text = sEmailCliente

        'Try
        '    Me.generar_comprobantemTableAdapter.Fill(dataTable:=Me.marketdscomprobante.generar_comprobantem, idventa:=txtidventa.Text)

        '    Dim ReportDataSource As ReportDataSource = New ReportDataSource()
        '    ReportDataSource.Value = generar_comprobantemBindingSource
        '    ReportDataSource.Name = "recibos"
        '    ReportViewer1.LocalReport.DataSources.Add(ReportDataSource)

        '    Me.ReportViewer1.RefreshReport()

        'Catch ex As Exception
        '    Me.ReportViewer1.RefreshReport()
        'End Try

    End Sub

    Private Sub datalistado_CellClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidproducto.Text = datalistado.SelectedCells.Item(2).Value
        'txtnombre_producto.Text = datalistado.SelectedCells.Item(3).Value
        'txtcantidad.Text = datalistado.SelectedCells.Item(4).Value
        'cbosundm.Text = datalistado.SelectedCells.Item(5).Value
        'cboundm.Text = datalistado.SelectedCells.Item(6).Value
        'txtcundm.Text = datalistado.SelectedCells.Item(7).Value
        'txtprecio_unitario.Text = datalistado.SelectedCells.Item(8).Value

        'txtcanttotal.Text = txtcantidad.Text * txtcundm.Text
    End Sub

    Private Sub btnbuscarlistaprecio_Click(sender As Object, e As EventArgs)

        frmvistalistaprecios.txtflag.Text = "1"

        frmvistalistaprecios.ShowDialog()
        'txtcanttotal.Text = txtcantidad.Text * txtcundm.Text


    End Sub

    Private Sub btnpedido_Click(sender As Object, e As EventArgs) Handles btnpedido.Click
        notapedido.txtidventa.Text = txtidventa.Text
        notapedido.ShowDialog()
    End Sub


    Public Function CriarDataSet() As comprobanteds
        Dim Ds = New comprobanteds()

        For C As Integer = 1 To 200
        Next

        Return Ds
    End Function
    Private Sub btnimpresion_Click(sender As Object, e As EventArgs)

        Using ds = CriarDataSet()
            Using Relatorio = New Microsoft.Reporting.WinForms.LocalReport()
                Relatorio.ReportPath = "Reportes\rptticketn.rdlc"
                Relatorio.DataSources.Add(New Microsoft.Reporting.WinForms.ReportDataSource("comprobanteds", DirectCast(ds.generar_comprobante, DataTable)))

                Using Rpd = New PrintReportSample.ReportPrintDocument(Relatorio)
                    Rpd.Print()
                End Using
            End Using
        End Using
    End Sub

    Private Sub btnsalirr_Click(sender As Object, e As EventArgs) Handles btnsalirr.Click

        'Gugardar cambios del comprobante antes de iniciar otro comprobante
        'Dim dtsm As New vventa
        'Dim funcm As New fventa

        'dtsm.gidcliente = txtidcliente.Text
        'dtsm.gfecha_venta = txtfecha.Value
        'dtsm.gtipo_documento = cbtipo_documento.Text
        'dtsm.gnum_documento = txtnum_documento.Text
        'dtsm.gidusuario = txtidusuario.Text
        'dtsm.gserie_documento = txtserie_documento.Text
        'dtsm.gidventa = txtidventa.Text

        'If funcm.editar(dtsm) Then

        'End If

        'TODO: esta línea de código carga datos en la tabla 'MarketDataSetdosfechas.producto' Puede moverla o quitarla según sea necesario.
        'Me.ProductoTableAdapter.Fill(Me.MarketDataSetdosfechas.producto)
        'txtbuscar.Focus()
        'txtdinero.Text = 0

        'txtcanttotal.Text = "1"
        'txtcundm.Text = "1"
        'cboundm.Text = "UND"

        mostrarusuario()
        mostrar_detalleventa(Nothing)
        'txtidcliente.Text = "3067"
        'txtnombre_cliente.Text = "CLIENTE CASUAL"
        'txtserie_documento.Text = DateTime.Now.ToShortTimeString()
        'txtnum_documento.Text = DateTime.Now.ToString()

        _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
        lblEmail.Text = ""
        txtEmail.Text = ""

        btnEnviarSunat.Enabled = True

    End Sub

    Private Async Sub Button2_Click(sender As Object, e As EventArgs) Handles btnEnviarSunat.Click

        Dim idCliente As String
        Dim idVenta As String
        idCliente = txtidcliente.Text
        idVenta = txtidventa.Text

        Dim dtc As New vcliente
        Dim dtv As New vventa
        Dim dtse As New vComprobanteSerie
        Dim dtdVe As New vdetalle_venta

        Dim dtcliente As New DataTable
        Dim tipoDocumento As vclienteTipoDocumento = New vclienteTipoDocumento
        Dim dtventa As New DataTable
        Dim dtSerie As New DataTable
        Dim dtDetVenta As New DataTable

        dtc.gidcliente = idCliente

        Dim funcCli As New fcliente
        Dim funcVen As New fventa
        Dim funcSerie As New fComprobanteSerie
        Dim funcDetVen As New fdetalle_venta

        dtcliente = funcCli.verificarCliente(dtc)

        If dtcliente.Rows.Count > 0 Then

            dtc.gidcliente = idCliente
            dtc.gnombres = dtcliente.Rows(0)(1).ToString
            dtc.gapellidos = dtcliente.Rows(0)(2).ToString
            dtc.gdireccion = dtcliente.Rows(0)(3).ToString
            dtc.gdni = dtcliente.Rows(0)(5).ToString
            tipoDocumento.gcodigoSunat = dtcliente.Rows(0)(7).ToString
            dtc.gtipoDocumento = tipoDocumento

            dtv.gidventa = idVenta
            dtventa = funcVen.verificarVenta(dtv)



            dtdVe.gidventa = idVenta
            'dtDetVenta = funcDetVen.mostrar_detalleventa_fe(dtdVe)

            'If listIdDetalleVenta.Count > 0 Then
            '    For Each idDetVenta As Integer In listIdDetalleVenta
            '        Dim foundRow As DataRow() = dtDetVenta.Select("iddetalle_venta = " & idDetVenta)
            '        If foundRow.Count > 0 Then
            '            For Each row As DataRow In foundRow
            '                'Console.WriteLine("{0}, {1}", row(0), row(1))
            '                'dtDetVenta.Rows(row(0)).Delete()
            '                dtDetVenta.Rows.Remove(row)
            '            Next
            '        End If
            '    Next
            'End If

            If ListdetNota.Count = 0 Then
                MessageBox.Show("Ingrese al menos un producto a la nota de debito.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            'Verificamos si es boleta o factura
            'If Not tipoDocumento.gcodigoSunat.ToString.Trim.Equals("6") Then
            'Boleta 03

            dtse.gcodigoSunat = "08"
            dtse.gnrocaja = NROCAJAFE
            dtse.gcodTipoComprobante = dtventa.Rows(0)(7).ToString
            dtSerie = funcSerie.obtenerNumeracionNotas(dtse)

            If dtSerie.Rows.Count > 0 Then

                Dim numeracion As Integer
                Dim correlativo As String

                dtse.gid = dtSerie.Rows(0)(0).ToString
                dtse.gcodigoSunat = dtSerie.Rows(0)(1).ToString
                dtse.gdescripcion = dtSerie.Rows(0)(2).ToString
                dtse.gserie = dtSerie.Rows(0)(3).ToString & NROCAJAFE
                dtse.gnumeracion = dtSerie.Rows(0)(4).ToString
                numeracion = dtSerie.Rows(0)(4).ToString

                correlativo = numeracion.ToString("00000000")

                If dtventa.Rows.Count > 0 Then

                    dtv.gtipo_comprobante = "08" 'Nota de DEBITO
                    dtv.gserie_comprobante = dtse.gserie
                    dtv.gnumero_comprobante = correlativo

                    'If funcVen.editarNumeracion(dtv) Then

                    Try

                        _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
                        Dim Items As List(Of DetalleDocumento) = New List(Of DetalleDocumento)()

                        Dim ven As DetalleDocumento = Nothing
                        Cursor.Current = Cursors.WaitCursor


                        _documento.Moneda = "PEN"

                        If ListdetNota IsNot Nothing Then
                            Dim i As Integer = 0

                            For Each row As vNotaDebitoDetalle In ListdetNota

                                If i > 0 Then Items.Add(ven)


                                Dim precioSinIgv As Decimal
                                'Dim igv As Decimal
                                Dim impuesto As Decimal

                                ven = New DetalleDocumento()
                                ven.Id = (i + 1)
                                ven.CodigoItem = Convert.ToString(row.giddetalle_nota_debito)
                                ven.Descripcion = Convert.ToString(row.gdesproducto).Trim()
                                ven.Cantidad = Math.Abs(Convert.ToDecimal(row.gcantidad))

                                If ven.Cantidad = 0 Then ven.Cantidad = 1

                                ven.PrecioUnitario = Math.Abs(Convert.ToDecimal(row.gprecio_unitario))
                                ven.PrecioReferencial = Math.Abs(Convert.ToDecimal(row.gprecio_unitario))

                                precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row.gprecio_unitario))) / 1.18, 2)
                                impuesto = Math.Abs(Convert.ToDecimal(row.gprecio_unitario)) - precioSinIgv


                                If _documento.Moneda = "PEN" Then
                                    ven.Suma = Math.Abs((Convert.ToDecimal(row.gprecio_unitario)))
                                    ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row.gprecio_unitario) - impuesto))

                                ElseIf _documento.Moneda = "USD" Then

                                    ven.Suma = Math.Abs(Convert.ToDecimal(row.gprecio_unitario))
                                    ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row.gprecio_unitario) - impuesto))

                                End If


                                ven.Impuesto = impuesto
                                ven.TotalVenta = (ven.SubTotalVenta)
                                ven.TipoPrecio = "01"
                                ven.UnidadCliente = Convert.ToString(row.gundm).Trim()

                                If ven.Impuesto <> 0 Then
                                    ven.TipoImpuesto = "10"
                                Else
                                    ven.TipoImpuesto = "20"

                                End If

                                ven.OtroImpuesto = 0
                                'ven.UnidadMedida = "NIU"
                                ven.UnidadMedida = Convert.ToString(row.gundm).Trim()
                                ven.Descuento = 0
                                ven.ImpuestoSelectivo = 0

                                i += 1

                                'If dtDetVenta.Rows.Count = i Then
                                Items.Add(ven)
                                'End If

                            Next
                        End If

                        _documento.Items = Items
                        _documento.Receptor.NroDocumento = dtventa.Rows(0)(24).ToString.Trim
                        _documento.Receptor.NombreLegal = dtventa.Rows(0)(20).ToString.Trim & " " & dtventa.Rows(0)(21).ToString.Trim
                        _documento.Receptor.Direccion = dtventa.Rows(0)(22).ToString.Trim
                        _documento.FechaVencimiento = Now
                        'Dim fec As String
                        'fec = dtventa.Rows(0)(2).ToString
                        '_documento.FechaEmision = Convert.ToDateTime(dtventa.Rows(0)(2)).ToString("yyyy-MM-dd")
                        _documento.FechaEmision = Now

                        CalcularTotales()

                        'Serie y numeración nota de credito
                        Dim str1 As String = dtv.gserie_comprobante
                        Dim str2 As String = dtv.gnumero_comprobante

                        Dim NuevoTipoDocumento As String = ""
                        Dim pad As Char = "0"
                        'Serie y numeracion comprobante de venta
                        Dim strCP1 As String = dtventa.Rows(0)(8).ToString
                        Dim strCP2 As String = dtventa.Rows(0)(9).ToString
                        NuevoTipoDocumento = dtventa.Rows(0)(7).ToString

                        '_documento.IdDocumento = str1 & "-" & str2
                        '_documento.TipoDocumento = "03"
                        '_documento.TipoOperacion = "0101"
                        _documento.Receptor.TipoDocumento = tipoDocumento.gcodigoSunat

                        Dim NuevaSerie As String = ""


                        _documento.IdDocumento = str1 + "-" + str2.PadLeft(8, pad).Trim()
                        _documento.TipoDocumento = "08"

                        Dim docRelacionado As New DocumentoRelacionado With {
                                    .NroDocumento = strCP1 & "-" & strCP2,
                                    .TipoDocumento = NuevoTipoDocumento
                                }
                        _documento.Relacionados.Add(docRelacionado)

                        _documento.Discrepancias.Add(New Discrepancia With {
                                    .Tipo = cmbTipoNota.SelectedValue,
                                    .Descripcion = cmbTipoNota.Text,
                                    .NroReferencia = strCP1 & "-" & strCP2
                                })

                        Dim serializador As ISerializador = New FinalXML.Serializador()
                        Dim response As DocumentoResponse = New DocumentoResponse With {
                                        .Exito = False
                                    }

                        response = Await New GenerarNotaDedito(serializador).Post(_documento)

                        If Not response.Exito Then Throw New ApplicationException(response.MensajeError)

                        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_documento.IdDocumento}.xml")

                        File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma))


                    Catch a As Exception
                        MessageBox.Show(a.Message)
                        Exit Sub
                    Finally
                        Cursor.Current = Cursors.[Default]
                    End Try

                    Try
                        Cursor = Cursors.WaitCursor

                        If Not AccesoInternet() Then
                            MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                            Return
                        End If

                        If String.IsNullOrEmpty(_documento.IdDocumento) Then
                            Throw New InvalidOperationException("La Serie y el Correlativo no pueden estar vacíos")
                            Exit Sub
                        End If

                        Dim tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo))
                        Dim firmadoRequest = New FirmadoRequest With {
                                    .TramaXmlSinFirma = tramaXmlSinFirma,
                                    .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                                    .PasswordCertificado = PASSWORDCERTIFICADO,
                                    .UnSoloNodoExtension = False
                                }

                        Dim certificador As ICertificador = New Certificador()
                        Dim respuestaFirmado = Await New Firmar(certificador).Post(firmadoRequest)
                        _documento.ResumenFirma = respuestaFirmado.ResumenFirma
                        _documento.FirmaDigital = respuestaFirmado.ValorFirma
                        If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)
                        RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"CF_{_documento.IdDocumento}.xml")
                        File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))

                        Dim oContribuyente = CrearEmisor()

                        Dim enviarDocumentoRequest = New EnviarDocumentoRequest With {
                                    .Ruc = oContribuyente.NroDocumento,
                                    .UsuarioSol = oContribuyente.UsuarioSol,
                                    .ClaveSol = oContribuyente.ClaveSol,
                                    .EndPointUrl = SunatFact,
                                    .IdDocumento = _documento.IdDocumento,
                                    .TipoDocumento = _documento.TipoDocumento,
                                    .TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                                }

                        Dim serializador As ISerializador = New FinalXML.Serializador()
                        Dim servicioSunatDocumentos As IServicioSunatDocumentos = New ServicioSunatDocumentos()
                        Dim respuestaEnvio As RespuestaComunConArchivo
                        respuestaEnvio = Await New EnviarDocumento(serializador, servicioSunatDocumentos).Post(enviarDocumentoRequest)
                        Dim rpta = CType(respuestaEnvio, EnviarDocumentoResponse)
                        MessageBox.Show(rpta.MensajeRespuesta & " Siendo las " + DateTime.Now, "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Try

                            If rpta.Exito AndAlso Not String.IsNullOrEmpty(rpta.TramaZipCdr) Then
                                File.WriteAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml", Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))
                                File.WriteAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip", Convert.FromBase64String(rpta.TramaZipCdr))
                                _documento.FirmaDigital = respuestaFirmado.ValorFirma


                            End If

                            dtv.gcod_estado_envio_sunat = rpta.CodigoRespuesta
                            dtv.gmensaje_envio_sunat = rpta.MensajeRespuesta
                            dtv.garchivo_xml = rpta.NombreArchivo & ".xml"
                            dtv.garchivo_cdr = "R-" & rpta.NombreArchivo & ".zip"
                            dtv.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"

                            If rpta.Exito Then
                                Dim idNotaCredito As Integer = 0
                                If rpta.CodigoRespuesta = "0" Then

                                    If dtv IsNot Nothing AndAlso dtv.gnumero_comprobante <> "" Then
                                        dtv.gestado_envio_sunat = 0
                                        'funcVen.editarDatosEnvioDocumentoSunat(dtv)

                                        Dim notaDebito As New vNotaDebito
                                        notaDebito.gidcliente = idCliente
                                        notaDebito.gidusuario = txtidusuario.Text
                                        notaDebito.gidventa = idVenta
                                        notaDebito.gfecha_venta = Date.Now
                                        notaDebito.gtipo_documento = _documento.TipoDocumento
                                        notaDebito.gserie_documento = dtv.gserie_comprobante
                                        notaDebito.gnum_documento = dtv.gnumero_comprobante
                                        notaDebito.gtipo_comprobante_rel = dtventa.Rows(0)(7).ToString
                                        notaDebito.gserie_comprobante_rel = dtventa.Rows(0)(8).ToString
                                        notaDebito.gnumero_comprobante_rel = dtventa.Rows(0)(9).ToString
                                        notaDebito.gtipo_nota = cmbTipoNota.SelectedValue
                                        notaDebito.gtipo_nota_descripcion = cmbTipoNota.Text
                                        notaDebito.gcod_estado_envio_sunat = rpta.CodigoRespuesta
                                        notaDebito.gmensaje_envio_sunat = rpta.MensajeRespuesta
                                        notaDebito.gestado_envio_sunat = 0
                                        notaDebito.garchivo_xml = rpta.NombreArchivo & ".xml"
                                        notaDebito.garchivo_cdr = "R-" & rpta.NombreArchivo & ".zip"
                                        notaDebito.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"

                                        Dim dtRsptaNota As DataTable
                                        dtRsptaNota = funcVen.insertarNotaDebito(notaDebito)

                                        If (dtRsptaNota.Rows.Count > 0) Then
                                            idNotaCredito = dtRsptaNota.Rows(0)(0)
                                            'MessageBox.Show("Se registró la nota de credito " & idNotaCredito)
                                            funcSerie.actualizarNumeracionNotas(dtse)

                                            If dtDetVenta IsNot Nothing Then
                                                Dim i As Integer = 0
                                                Dim detNotCredito As vNotaDebitoDetalle

                                                For Each row As vNotaDebitoDetalle In ListdetNota
                                                    detNotCredito = New vNotaDebitoDetalle

                                                    detNotCredito.gidnotadebito = idNotaCredito
                                                    detNotCredito.gidproducto = Convert.ToString(row.gidproducto)
                                                    detNotCredito.gdesproducto = row.gdesproducto
                                                    detNotCredito.gcantidad = Math.Abs(Convert.ToDecimal(row.gcantidad))
                                                    detNotCredito.gprecio_unitario = Math.Abs(Convert.ToDecimal(row.gprecio_unitario))
                                                    detNotCredito.gundm = Convert.ToString(row.gundm).Trim()
                                                    detNotCredito.gsundm = Convert.ToString(row.gsundm).Trim()
                                                    detNotCredito.gcundm = Convert.ToString(row.gcundm).Trim()
                                                    detNotCredito.gprecio_compra = Math.Abs(Convert.ToDecimal(row.gprecio_compra))

                                                    funcDetVen.insertarDetalleNotaDebito(detNotCredito)

                                                Next
                                            End If
                                        Else
                                            MessageBox.Show("Error al registrar la nota de DEBITO")
                                        End If

                                    End If
                                ElseIf rpta.CodigoRespuesta Is Nothing Then
                                    Dim msg = String.Concat(rpta.MensajeRespuesta)
                                    Dim faultCode = "Client."

                                    If msg.Contains(faultCode) Then
                                        Dim posicion = msg.IndexOf(faultCode, StringComparison.Ordinal)
                                        Dim codigoError = msg.Substring(posicion + faultCode.Length, 4)
                                        msg = codigoError
                                    End If

                                    dtv.gestado_envio_sunat = 1
                                    dtv.gcod_estado_envio_sunat = msg
                                    'funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                    'MessageBox.Show("No se registró la nota de credito. Motivo: " & rpta.MensajeRespuesta)
                                End If

                                frmReciboNota._documento = _documento
                                frmReciboNota.txtidventa.Text = idNotaCredito
                                frmReciboNota.ShowDialog()

                                btnEnviarSunat.Enabled = False

                                rutaArchivoXmlEmail = RutaArchivo
                                rutaPdfEmail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, frmReciboNota._rutaPdf)

                            Else
                                MessageBox.Show(rpta.MensajeError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                            End If

                        Catch ex As Exception

                            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Exit Sub
                        End Try

                        If Not respuestaEnvio.Exito Then Throw New ApplicationException(respuestaEnvio.MensajeError)

                    Catch ex As Exception
                        MessageBox.Show(ex.Message)

                        Exit Sub
                    Finally
                        'btnGeneraXML.Enabled = True
                        'btnEnvioSunat.Enabled = False
                        Cursor = Cursors.[Default]
                    End Try


                    'MessageBox.Show("La Nota de crédito " & dtse.gserie & "-" & correlativo & " se generó y se enviará por resumen diario.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'frmRecibo._documento = _documento
                    'frmRecibo.txtidventa.Text = idVenta
                    'frmRecibo.ShowDialog()

                    'rutaArchivoXmlEmail = RutaArchivo
                    'rutaPdfEmail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, frmRecibo._rutaPdf)

                    'dtv.gcod_estado_envio_sunat = 0
                    'dtv.gmensaje_envio_sunat = 0
                    'dtv.garchivo_xml = ""
                    'dtv.garchivo_cdr = ""
                    'dtv.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"
                    'dtv.gestado_envio_sunat = 0
                    'funcVen.editarDatosEnvioDocumentoSunat(dtv)

                    'btnEnviarSunat.Enabled = False

                    'Else
                    '    MessageBox.Show("Error al actualizar la numeración en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'End If

                Else
                    MessageBox.Show("No se encontró la venta en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            Else
                MessageBox.Show("No se encontró numeracion para boletas en la base de datos. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Else
            MessageBox.Show("Venta no enviada a sunat. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub CalcularTotales()
        _documento.TotalIgv = _documento.Items.Sum(Function(d) d.Impuesto)
        _documento.TotalIsc = _documento.Items.Sum(Function(d) d.ImpuestoSelectivo)
        _documento.TotalOtrosTributos = _documento.Items.Sum(Function(d) d.OtroImpuesto)
        _documento.Gravadas = _documento.Items.Where(Function(d) d.TipoImpuesto.StartsWith("1")).Sum(Function(d) d.SubTotalVenta)
        _documento.Exoneradas = _documento.Items.Where(Function(d) d.TipoImpuesto.Contains("20")).Sum(Function(d) d.Suma)
        _documento.Inafectas = _documento.Items.Where(Function(d) d.TipoImpuesto.StartsWith("3") OrElse d.TipoImpuesto.Contains("40")).Sum(Function(d) d.Suma)
        _documento.Gratuitas = _documento.Items.Where(Function(d) d.TipoImpuesto.Contains("21")).Sum(Function(d) d.Suma)
        _documento.LineCountNumeric = Convert.ToString(_documento.Items.Count())

        If _documento.TotalIsc > 0 Then
            _documento.TotalIgv = (_documento.Gravadas + _documento.TotalIsc) * _documento.CalculoIgv
        End If

        _documento.TotalVenta = _documento.Gravadas + _documento.Exoneradas + _documento.Inafectas + _documento.TotalIgv + _documento.TotalIsc + _documento.TotalOtrosTributos
        _documento.MontoEnLetras = ConvertLetras.enletras(_documento.TotalVenta.ToString())

        If _documento.CalculoIgv > 0 Then
            _documento.SubTotalVenta = _documento.TotalVenta - _documento.TotalIgv
        Else
            _documento.SubTotalVenta = _documento.TotalVenta
        End If
    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Async Sub EnviarEmailComprobante()

        btnEnvioEmail.Enabled = False

        Dim SmtpServer As New SmtpClient()

        Dim vSmtpServer As New vSmtpServer()

        SmtpServer = vSmtpServer.obtenerSmtpCliente()

        Dim mail As New MailMessage()

        mail = New MailMessage()
        Dim addr() As String = txtEmail.Text.Split(";")

        Dim idDocE As String = _documento.IdDocumento
        Dim recepE As String = _documento.Receptor.NombreLegal
        lblEmail.Text = "Enviando email... " & idDocE

        Try
            mail.From = New MailAddress(USEREMAIL, RAZONSOCIAL, System.Text.Encoding.UTF8)
            Dim i As Byte
            For i = 0 To addr.Length - 1
                mail.To.Add(addr(i))
            Next
            mail.Subject = "Comprobante Electrónico " & _documento.IdDocumento & " - " & RUC & " " & RAZONSOCIAL
            'mail.Body = TextBox4.Text
            'If ListBox1.Items.Count <> 0 Then
            '    For i = 0 To ListBox1.Items.Count - 1
            '        mail.Attachments.Add(New Attachment(ListBox1.Items.Item(i)))
            '    Next
            'End If
            'Dim logo As New LinkedResource(Path)
            'logo.ContentId = "Logo"
            Dim htmlview As String = ""
            Dim saludo As String = ""

            mail.Attachments.Add(New Attachment(rutaArchivoXmlEmail))
            mail.Attachments.Add(New Attachment(rutaPdfEmail))

            'htmlview = "<html><body><table border=2><tr width=100%><td><img src=cid:Logo alt=companyname /></td><td>MY COMPANY DESCRIPTION</td></tr></table><hr/></body></html>"

            saludo = "Estimado " & recepE & ".<br/>"
            saludo += "Se adjunta su comprobante electronico " & idDocE

            Dim alternateView1 As AlternateView = AlternateView.CreateAlternateViewFromString(htmlview + saludo, Nothing, MediaTypeNames.Text.Html)
            'alternateView1.LinkedResources.Add(logo)
            mail.AlternateViews.Add(alternateView1)
            mail.IsBodyHtml = True
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            mail.ReplyTo = New MailAddress(txtEmail.Text)
            'SmtpServer.Send(mail)
            Await SmtpServer.SendMailAsync(mail)
            lblEmail.ForeColor = Color.DarkGreen
            lblEmail.Text = "Se envió correctamente el email con el comprobante " & idDocE & " - " & recepE
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
            lblEmail.ForeColor = Color.Red
            lblEmail.Text = "Error al enviar email. Comprobante " & idDocE
        End Try

        btnEnvioEmail.Enabled = True

    End Sub

    Private Sub btnEnvioEmail_Click(sender As Object, e As EventArgs) Handles btnEnvioEmail.Click
        If Not _documento Is Nothing Then
            If Not _documento.IdDocumento.Equals("") Then
                If txtEmail.Text.Trim.Length > 0 Then
                    EnviarEmailComprobante()
                Else
                    MessageBox.Show("Debe ingresar un email válido", "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            Else
                MessageBox.Show("No se ha generado aun el comprobante electrónico", "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("No se ha generado aun el comprobante electrónico", "Error al enviar email", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Async Sub btnEnviarLuegoSunat_Click(sender As Object, e As EventArgs) Handles btnEnviarLuegoSunat.Click

        Dim idCliente As String
        Dim idVenta As String
        idCliente = txtidcliente.Text
        idVenta = txtidventa.Text

        Dim dtc As New vcliente
        Dim dtv As New vventa
        Dim dtse As New vComprobanteSerie
        Dim dtdVe As New vdetalle_venta

        Dim dtcliente As New DataTable
        Dim tipoDocumento As vclienteTipoDocumento = New vclienteTipoDocumento
        Dim dtventa As New DataTable
        Dim dtSerie As New DataTable
        Dim dtDetVenta As New DataTable

        dtc.gidcliente = idCliente

        Dim funcCli As New fcliente
        Dim funcVen As New fventa
        Dim funcSerie As New fComprobanteSerie
        Dim funcDetVen As New fdetalle_venta

        dtcliente = funcCli.verificarCliente(dtc)

        If dtcliente.Rows.Count > 0 Then

            dtc.gidcliente = idCliente
            dtc.gnombres = dtcliente.Rows(0)(1).ToString
            dtc.gapellidos = dtcliente.Rows(0)(2).ToString
            dtc.gdireccion = dtcliente.Rows(0)(3).ToString
            dtc.gdni = dtcliente.Rows(0)(5).ToString
            tipoDocumento.gcodigoSunat = dtcliente.Rows(0)(7).ToString
            dtc.gtipoDocumento = tipoDocumento

            dtv.gidventa = idVenta
            dtventa = funcVen.verificarVenta(dtv)

            dtdVe.gidventa = idVenta
            dtDetVenta = funcDetVen.mostrar_detalleventa_fe(dtdVe)

            If dtDetVenta.Rows.Count = 0 Then
                MessageBox.Show("Ingrese al menos un producto a la venta.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            'Verificamos si es boleta o factura
            If Not tipoDocumento.gcodigoSunat.ToString.Trim.Equals("6") Then
                'Boleta 03
                MessageBox.Show("Esta opción solo aplica para Facturas", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Else
                'Factura 01

                dtse.gcodigoSunat = "01"
                dtse.gnrocaja = NROCAJAFE
                dtSerie = funcSerie.obtenerNumeracion(dtse)

                If dtSerie.Rows.Count > 0 Then

                    Dim numeracion As Integer
                    Dim correlativo As String

                    dtse.gid = dtSerie.Rows(0)(0).ToString
                    dtse.gcodigoSunat = dtSerie.Rows(0)(1).ToString
                    dtse.gdescripcion = dtSerie.Rows(0)(2).ToString
                    dtse.gserie = dtSerie.Rows(0)(3).ToString & NROCAJAFE
                    dtse.gnumeracion = dtSerie.Rows(0)(4).ToString
                    numeracion = dtSerie.Rows(0)(4).ToString

                    correlativo = numeracion.ToString("00000000")

                    If dtventa.Rows.Count > 0 Then

                        dtv.gtipo_comprobante = "01"
                        dtv.gserie_comprobante = dtse.gserie
                        dtv.gnumero_comprobante = correlativo

                        If funcVen.editarNumeracion(dtv) Then

                            Try
                                _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
                                Dim Items As List(Of DetalleDocumento) = New List(Of DetalleDocumento)()
                                Dim ven As DetalleDocumento = Nothing
                                Cursor.Current = Cursors.WaitCursor


                                'CVentas1 = AdmCVenta.LeerVenta("", CVentas.Sigla, CVentas.Serie, CVentas.Numeracion)

                                _documento.Moneda = "PEN"

                                'If CVentas1.Moneda = "MN" Then
                                '        _documento.Moneda = "PEN"
                                '    ElseIf CVentas1.Moneda = "US" Then
                                '        _documento.Moneda = "USD"
                                '    End If

                                'dt_DetalleVenta = AdmCVenta.LeerDetalle(cboEmpresaDoc.SelectedValue.ToString(), CVentas.Sigla, CVentas.Serie, CVentas.Numeracion)

                                If dtDetVenta IsNot Nothing Then
                                    Dim i As Integer = 0

                                    For Each row As DataRow In dtDetVenta.Rows

                                        If i > 0 Then Items.Add(ven)


                                        Dim precioSinIgv As Decimal
                                        'Dim igv As Decimal
                                        Dim impuesto As Decimal

                                        ven = New DetalleDocumento()
                                        ven.Id = (i + 1)
                                        ven.CodigoItem = Convert.ToString(row(2))
                                        ven.Descripcion = Convert.ToString(row(3)).Trim()
                                        ven.Cantidad = Math.Abs(Convert.ToDecimal(row(4)))

                                        If ven.Cantidad = 0 Then ven.Cantidad = 1

                                        ven.PrecioUnitario = Math.Abs(Convert.ToDecimal(row(8)))
                                        ven.PrecioReferencial = Math.Abs(Convert.ToDecimal(row(8)))

                                        precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row(9)))) / 1.18, 2)
                                        impuesto = Math.Abs(Convert.ToDecimal(row(9))) - precioSinIgv


                                        If _documento.Moneda = "PEN" Then
                                            ven.Suma = Math.Abs((Convert.ToDecimal(row(9))))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        ElseIf _documento.Moneda = "USD" Then

                                            ven.Suma = Math.Abs(Convert.ToDecimal(row(9)))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        End If


                                        ven.Impuesto = impuesto
                                        ven.TotalVenta = (ven.SubTotalVenta)
                                        ven.TipoPrecio = "01"
                                        ven.UnidadCliente = Convert.ToString(row(6)).Trim()

                                        If ven.Impuesto <> 0 Then
                                            ven.TipoImpuesto = "10"
                                        Else
                                            ven.TipoImpuesto = "20"

                                        End If

                                        ven.OtroImpuesto = 0
                                        'ven.UnidadMedida = "NIU"
                                        ven.UnidadMedida = Convert.ToString(row(6)).Trim()
                                        ven.Descuento = 0
                                        ven.ImpuestoSelectivo = 0

                                        i += 1

                                        If dtDetVenta.Rows.Count = i Then Items.Add(ven)

                                    Next
                                End If

                                _documento.Items = Items
                                _documento.Receptor.NroDocumento = dtventa.Rows(0)(24).ToString.Trim
                                _documento.Receptor.NombreLegal = dtventa.Rows(0)(20).ToString.Trim & " " & dtventa.Rows(0)(21).ToString.Trim
                                _documento.Receptor.Direccion = dtventa.Rows(0)(22).ToString.Trim
                                _documento.FechaVencimiento = Now
                                Dim fec As String
                                fec = dtventa.Rows(0)(2).ToString
                                _documento.FechaEmision = Convert.ToDateTime(dtventa.Rows(0)(2)).ToString("yyyy-MM-dd")

                                CalcularTotales()

                                Dim str1 As String = dtv.gserie_comprobante
                                Dim str2 As String = dtv.gnumero_comprobante

                                _documento.IdDocumento = str1 & "-" & str2
                                _documento.TipoDocumento = "01"
                                _documento.TipoOperacion = "0101"
                                _documento.Receptor.TipoDocumento = "6"

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim response As DocumentoResponse = New DocumentoResponse With {
                                        .Exito = False
                                    }

                                response = Await New GenerarFactura(serializador).Post(_documento)

                                If Not response.Exito Then Throw New ApplicationException(response.MensajeError)

                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_documento.IdDocumento}.xml")

                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma))


                            Catch a As Exception
                                MessageBox.Show(a.Message)
                                Exit Sub
                            Finally
                                Cursor.Current = Cursors.[Default]
                            End Try


                            Try
                                Cursor = Cursors.WaitCursor

                                If Not AccesoInternet() Then
                                    MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                                    Return
                                End If

                                MessageBox.Show("El comprobante " & dtv.gserie_comprobante & " - " & dtv.gnumero_comprobante & " se registro en la base de datos y queda pendiente su envío a SUNAT.Siendo las " + DateTime.Now, "Registro de Comprobante", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                frmRecibo._documento = _documento
                                frmRecibo.txtidventa.Text = idVenta
                                frmRecibo.ShowDialog()

                                btnEnviarSunat.Enabled = False

                                rutaArchivoXmlEmail = RutaArchivo
                                rutaPdfEmail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, frmRecibo._rutaPdf)

                            Catch ex As Exception
                                MessageBox.Show(ex.Message)

                                Exit Sub
                            Finally
                                'btnGeneraXML.Enabled = True
                                'btnEnvioSunat.Enabled = False
                                Cursor = Cursors.[Default]
                            End Try

                            'MessageBox.Show("La Factura " & dtse.gserie & "-" & correlativo & " se generó y envió a sunat.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        Else
                            MessageBox.Show("Error al actualizar la numeración en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                    Else
                        MessageBox.Show("No se encontró la venta en la base de datos(IDVenta: " & idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Else
                    MessageBox.Show("No se encontró numeracion para boletas en la base de datos. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            End If

        Else
            MessageBox.Show("Venta no enviada a sunat. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        'lblvuelto.Text = txtdinero.Text - lbltotal.Text

    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click

        Dim idCliente As String
        idCliente = txtidcliente.Text().Trim()
        listIdDetalleVenta = New List(Of Integer)

        If Not idCliente.Equals("") Then
            frmcListadoComprobantesSunatCliente.txtIdCliente.Text = idCliente
            frmcListadoComprobantesSunatCliente.ShowDialog()

            txtidventa.Text = sIdVenta
            txtfechaEmisionComprobante.Text = sFechaVenta
            txtSerieVenta.Text = sSerieVenta
            txtNumeracionVenta.Text = sNumeracionVenta
            txtIdTipoVenta.Text = sIdTipoVenta
            txtcomprobante.Text = sSerieVenta & "-" & sNumeracionVenta

            'mostrar_detalleventa()

        Else
            MessageBox.Show("Seleccione un cliente", "Nota de Crédito", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub btnAgregarDetalle_Click(sender As Object, e As EventArgs) Handles btnAgregarDetalle.Click

        Dim idCliente As String
        idCliente = txtidcliente.Text().Trim()

        If Not idCliente.Equals("") Then
            frmAddDetalleNota.ShowDialog()

            If Not detNota Is Nothing Then
                mostrar_detalleventa(detNota)
            End If
        Else
            MessageBox.Show("Seleccione un cliente", "Nota de Crédito", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    'Public Shared Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
    '    Dim table As New DataTable()
    '    Dim fields() As FieldInfo = GetType(T).GetFields()
    '    For Each field As FieldInfo In fields
    '        table.Columns.Add(field.Name, field.FieldType)
    '    Next
    '    For Each item As T In list
    '        Dim row As DataRow = table.NewRow()
    '        For Each field As FieldInfo In fields
    '            row(field.Name) = field.GetValue(item)
    '        Next
    '        table.Rows.Add(row)
    '    Next
    '    Return table
    'End Function

    Public Shared Function ConvertToDataTable(Of T)(ByVal list As IList(Of T)) As DataTable
        Dim td As New DataTable
        Dim entityType As Type = GetType(T)
        Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(entityType)

        For Each prop As PropertyDescriptor In properties
            td.Columns.Add(prop.Name)
        Next

        For Each item As T In list
            Dim row As DataRow = td.NewRow()

            For Each prop As PropertyDescriptor In properties
                row(prop.Name) = prop.GetValue(item)
            Next

            td.Rows.Add(row)
        Next

        Return td
    End Function

End Class