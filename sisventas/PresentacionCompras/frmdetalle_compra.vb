﻿Public Class frmdetalle_compra
    Private dt As New DataTable


    Private Sub frmdetalle_compra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar_compra()
        'Try
        '    Dim query As IEnumerable(Of Object) = _
        '        From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)() _
        '        Where ( _
        '            (row.Cells("ImporteVenta").Value IsNot Nothing) AndAlso _
        '            (row.Cells("ImporteVenta").Value IsNot DBNull.Value)) Select row.Cells("ImporteVenta").Value()

        '    Dim resultado As Decimal = _
        '        query.Sum(Function(row) Convert.ToDecimal(row))
        '    lbltotal.Text = String.Format("Total Pagar: {0:N2}", resultado)


        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)

        'End Try
    End Sub



    Public Sub limpiar()
        'btnguardar.Visible = True
        txtidproducto.Text = ""
        txtnombre_producto.Text = ""


        txtcantidad.Text = 0



    End Sub
    Private Sub mostrar_compra()
        Try
            Dim func As New fdetalle_compra
            dt = func.mostrar_compra

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = "idcompras=  '" & txtidcompra.Text & "'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub ocultar_columnas()

        datalistado.Columns(1).Visible = False
        datalistado.Columns(2).Visible = False
        datalistado.Columns(3).Visible = False
    End Sub


    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtidproducto.Text <> "" And txtcantidad.Text <> "" And txtprecio_unitario.Text <> "" Then
            Try
                Dim dts As New vdetalle_compra
                Dim func As New fdetalle_compra

                dts.gidcompras = txtidcompra.Text
                dts.gidproductoscompra = txtidproducto.Text
                dts.gcantidad = txtcantidad.Text
                dts.gprecio_unit = txtprecio_unitario.Text



        

                If func.insertar(dts) Then



                 
                    MessageBox.Show("Articulo fue añadido Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar_compra()


                Else
                    MessageBox.Show("Articulo  no fue añadido Correctamente, intente de nuevos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar_compra()


                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnbuscar_producto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbuscar_producto.Click
        frmproductocompra.txtflag.Text = "1"
        frmproductocompra.ShowDialog()
    End Sub

    Private Sub datalistado_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick

    End Sub

    Private Sub datalistado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellContentClick

    End Sub

    Private Sub btnimprimir_Click(sender As Object, e As EventArgs) Handles btnimprimir.Click

    End Sub
End Class