﻿Public Class frmproductocompra
    Private dt As New DataTable
    Private Sub frmproductocompra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()


    End Sub
    Public Sub limpiar()
        btnguardar.Visible = True
        btneditar.Visible = False
        txtnombre.Text = ""
  
        txtprecio.Text = "0"

        txtidproducto.Text = ""

      

    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fproductosc
            dt = func.mostrar
            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        btnnuevo.Visible = True
        btneditar.Visible = False
    End Sub



    Private Sub txtidproducto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtidproducto.TextChanged

    End Sub

    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtnombre.Text <> "" Then

            Try
                Dim dts As New vproductoc
                Dim func As New fproductosc

                dts.gnombrec = txtnombre.Text
                dts.gprecio = txtprecio.Text






                If func.insertar(dts) Then
                    MessageBox.Show("Producto Registrado Correctamente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else
                    MessageBox.Show("Producto no fue registrado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btneditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos del Producto ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtnombre.Text <> "" Then
            Try


                Dim dts As New vproductoc
                Dim func As New fproductosc
                dts.gidproductoscompra = txtidproducto.Text
                dts.gnombrec = txtnombre.Text
               
                dts.gprecio = txtprecio.Text
       



                If func.editar(dts) Then
                    MessageBox.Show("Producto Modificado Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else
                    MessageBox.Show("Producto no fue Modificado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub datalistado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellContentClick

    End Sub

    Private Sub datalistado_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        If txtflag.Text = "1" Then
            frmdetalle_compra.txtidproducto.Text = datalistado.SelectedCells.Item(1).Value
            frmdetalle_compra.txtnombre_producto.Text = datalistado.SelectedCells.Item(2).Value

            frmdetalle_compra.txtprecio_unitario.Text = datalistado.SelectedCells.Item(3).Value

            Me.Close()



        End If
    End Sub
End Class