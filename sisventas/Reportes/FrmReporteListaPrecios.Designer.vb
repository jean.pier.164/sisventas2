﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReporteListaPrecios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource5 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.marketdslistaprecios = New BRAVOSPORT.marketdslistaprecios()
        Me.mlistapreciospBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mlistapreciospTableAdapter = New BRAVOSPORT.marketdslistapreciosTableAdapters.mlistapreciospTableAdapter()
        CType(Me.marketdslistaprecios, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mlistapreciospBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource5.Name = "ListaPrecios"
        ReportDataSource5.Value = Me.mlistapreciospBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource5)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.ReporteListaPrecios.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(784, 873)
        Me.ReportViewer1.TabIndex = 2
        '
        'marketdslistaprecios
        '
        Me.marketdslistaprecios.DataSetName = "marketdslistaprecios"
        Me.marketdslistaprecios.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'mlistapreciospBindingSource
        '
        Me.mlistapreciospBindingSource.DataMember = "mlistapreciosp"
        Me.mlistapreciospBindingSource.DataSource = Me.marketdslistaprecios
        '
        'mlistapreciospTableAdapter
        '
        Me.mlistapreciospTableAdapter.ClearBeforeFill = True
        '
        'FrmReporteListaPrecios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 873)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmReporteListaPrecios"
        Me.Text = "REPORTE LISTA DE PRECIOS"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.marketdslistaprecios, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mlistapreciospBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents mlistapreciospBindingSource As BindingSource
    Friend WithEvents marketdslistaprecios As marketdslistaprecios
    Friend WithEvents mlistapreciospTableAdapter As marketdslistapreciosTableAdapters.mlistapreciospTableAdapter
End Class
