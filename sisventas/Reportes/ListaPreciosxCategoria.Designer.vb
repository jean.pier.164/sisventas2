﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ListaPreciosxCategoria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.mdslistapreciosxcategoria = New BRAVOSPORT.mdslistapreciosxcategoria()
        Me.listaspreciosxcategoriaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.listaspreciosxcategoriaTableAdapter = New BRAVOSPORT.mdslistapreciosxcategoriaTableAdapters.listaspreciosxcategoriaTableAdapter()
        CType(Me.mdslistapreciosxcategoria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.listaspreciosxcategoriaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource2.Name = "dslistapreciosxcategoria"
        ReportDataSource2.Value = Me.listaspreciosxcategoriaBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.ListaPreciosxCategoria.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(726, 361)
        Me.ReportViewer1.TabIndex = 3
        '
        'mdslistapreciosxcategoria
        '
        Me.mdslistapreciosxcategoria.DataSetName = "mdslistapreciosxcategoria"
        Me.mdslistapreciosxcategoria.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'listaspreciosxcategoriaBindingSource
        '
        Me.listaspreciosxcategoriaBindingSource.DataMember = "listaspreciosxcategoria"
        Me.listaspreciosxcategoriaBindingSource.DataSource = Me.mdslistapreciosxcategoria
        '
        'listaspreciosxcategoriaTableAdapter
        '
        Me.listaspreciosxcategoriaTableAdapter.ClearBeforeFill = True
        '
        'ListaPreciosxCategoria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(726, 361)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ListaPreciosxCategoria"
        Me.Text = "ListaPreciosxCategoria"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.mdslistapreciosxcategoria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.listaspreciosxcategoriaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents listaspreciosxcategoriaBindingSource As BindingSource
    Friend WithEvents mdslistapreciosxcategoria As mdslistapreciosxcategoria
    Friend WithEvents listaspreciosxcategoriaTableAdapter As mdslistapreciosxcategoriaTableAdapters.listaspreciosxcategoriaTableAdapter
End Class
