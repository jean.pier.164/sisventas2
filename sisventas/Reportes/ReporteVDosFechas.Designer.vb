﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReporteVDosFechas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.marketDataSetdosfechas = New BRAVOSPORT.marketDataSetdosfechas()
        Me.cdosfechasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cdosfechasTableAdapter = New BRAVOSPORT.marketDataSetdosfechasTableAdapters.cdosfechasTableAdapter()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.TextBox()
        CType(Me.marketDataSetdosfechas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cdosfechasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource2.Name = "datasetvdosfechas"
        ReportDataSource2.Value = Me.cdosfechasBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.rptvdosfechas.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(714, 307)
        Me.ReportViewer1.TabIndex = 0
        '
        'marketDataSetdosfechas
        '
        Me.marketDataSetdosfechas.DataSetName = "marketDataSetdosfechas"
        Me.marketDataSetdosfechas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'cdosfechasBindingSource
        '
        Me.cdosfechasBindingSource.DataMember = "cdosfechas"
        Me.cdosfechasBindingSource.DataSource = Me.marketDataSetdosfechas
        '
        'cdosfechasTableAdapter
        '
        Me.cdosfechasTableAdapter.ClearBeforeFill = True
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(201, 113)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(100, 20)
        Me.txtfechai.TabIndex = 1
        Me.txtfechai.Visible = False
        '
        'txtfechafi
        '
        Me.txtfechafi.Location = New System.Drawing.Point(396, 112)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(100, 20)
        Me.txtfechafi.TabIndex = 2
        Me.txtfechafi.Visible = False
        '
        'ReporteVDosFechas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(714, 307)
        Me.Controls.Add(Me.txtfechafi)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteVDosFechas"
        Me.Text = "ReporteVDosFechas"
        CType(Me.marketDataSetdosfechas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cdosfechasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents cdosfechasBindingSource As BindingSource
    Friend WithEvents marketDataSetdosfechas As marketDataSetdosfechas
    Friend WithEvents cdosfechasTableAdapter As marketDataSetdosfechasTableAdapters.cdosfechasTableAdapter
    Friend WithEvents txtfechai As TextBox
    Friend WithEvents txtfechafi As TextBox
End Class
