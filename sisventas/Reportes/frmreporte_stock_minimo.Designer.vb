﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreporte_stock_minimo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.sisventasdatasetstockminimo = New BRAVOSPORT.sisventasdatasetstockminimo()
        Me.stock_minimoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.stock_minimoTableAdapter = New BRAVOSPORT.sisventasdatasetstockminimoTableAdapters.stock_minimoTableAdapter()
        CType(Me.sisventasdatasetstockminimo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stock_minimoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "datasetstockminimo"
        ReportDataSource1.Value = Me.stock_minimoBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "sisventas.rptstockminimo.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.PageCountMode = Microsoft.Reporting.WinForms.PageCountMode.Actual
        Me.ReportViewer1.Size = New System.Drawing.Size(797, 529)
        Me.ReportViewer1.TabIndex = 0
        '
        'sisventasdatasetstockminimo
        '
        Me.sisventasdatasetstockminimo.DataSetName = "sisventasdatasetstockminimo"
        Me.sisventasdatasetstockminimo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'stock_minimoBindingSource
        '
        Me.stock_minimoBindingSource.DataMember = "stock_minimo"
        Me.stock_minimoBindingSource.DataSource = Me.sisventasdatasetstockminimo
        '
        'stock_minimoTableAdapter
        '
        Me.stock_minimoTableAdapter.ClearBeforeFill = True
        '
        'frmreporte_stock_minimo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 529)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmreporte_stock_minimo"
        Me.Text = "STOCK MINIMO"
        CType(Me.sisventasdatasetstockminimo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stock_minimoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents stock_minimoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents sisventasdatasetstockminimo As BRAVOSPORT.sisventasdatasetstockminimo
    Friend WithEvents stock_minimoTableAdapter As BRAVOSPORT.sisventasdatasetstockminimoTableAdapters.stock_minimoTableAdapter
End Class
