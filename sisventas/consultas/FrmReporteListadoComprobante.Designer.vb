﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReporteListadoComprobante
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.listado_comprobantes_sunatBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ListadoComprobante = New BRAVOSPORT.ListadoComprobante()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.TextBox()
        Me.listado_comprobantes_sunatTableAdapter = New BRAVOSPORT.ListadoComprobanteTableAdapters.listado_comprobantes_sunatTableAdapter()
        CType(Me.listado_comprobantes_sunatBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ListadoComprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'listado_comprobantes_sunatBindingSource
        '
        Me.listado_comprobantes_sunatBindingSource.DataMember = "listado_comprobantes_sunat"
        Me.listado_comprobantes_sunatBindingSource.DataSource = Me.ListadoComprobante
        '
        'ListadoComprobante
        '
        Me.ListadoComprobante.DataSetName = "ListadoComprobante"
        Me.ListadoComprobante.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "dsListadoComprobante"
        ReportDataSource1.Value = Me.listado_comprobantes_sunatBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.Listado.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.PageCountMode = Microsoft.Reporting.WinForms.PageCountMode.Actual
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(1369, 591)
        Me.ReportViewer1.TabIndex = 1
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(28, 66)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(19, 20)
        Me.txtfechai.TabIndex = 2
        Me.txtfechai.Visible = False
        '
        'txtfechafi
        '
        Me.txtfechafi.Location = New System.Drawing.Point(53, 66)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(12, 20)
        Me.txtfechafi.TabIndex = 3
        Me.txtfechafi.Visible = False
        '
        'listado_comprobantes_sunatTableAdapter
        '
        Me.listado_comprobantes_sunatTableAdapter.ClearBeforeFill = True
        '
        'FrmReporteListadoComprobante
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1369, 591)
        Me.Controls.Add(Me.txtfechafi)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmReporteListadoComprobante"
        Me.Text = "FrmReporteListadoComprobante"
        CType(Me.listado_comprobantes_sunatBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ListadoComprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents txtfechai As TextBox
    Friend WithEvents txtfechafi As TextBox
    Friend WithEvents listado_comprobantes_sunatBindingSource As BindingSource
    Friend WithEvents ListadoComprobante As ListadoComprobante
    Friend WithEvents listado_comprobantes_sunatTableAdapter As ListadoComprobanteTableAdapters.listado_comprobantes_sunatTableAdapter
End Class
