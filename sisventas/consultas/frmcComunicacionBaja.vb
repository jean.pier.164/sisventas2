﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports FinalXML
Imports FinalXML.Interfaces
Imports FinalXML.InterMySql
'Imports FinalXML.Interfaces

Public Class frmcComunicacionBaja

    Private dt As New DataTable

    Dim _resumen As ResumenDiario
    Shared tramaXmlSinFirma As String
    Dim _documento As DocumentoElectronico
    Dim RutaArchivo As String
    Dim IdDocumento As String
    Shared recursos As String
    Dim herramientas As Herramientas = New Herramientas

    Shared SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Shared SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Shared SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Shared NROCAJAFE As String = ConfigurationManager.AppSettings.Get("NROCAJAFE")

    Shared nroTicketRD As String
    Shared idDocumentoRD As String
    Shared counter2 As Int32 = 1
    Shared CodTipoDocumento As String

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar(DateTime.Now, DateTime.Now)

        recursos = herramientas.GetResourcesPath()

    End Sub

    Private Sub mostrar(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime)
        Try
            Dim func As New fcventas
            dt = func.mostrarResumenesDiarios(fechaInicio, fechaFin)

            dglista2.Columns.Item("Eliminar").Visible = False

            'If dt.Rows.Count <> 0 Then
            '    dglista2.DataSource = dt
            '    dglista2.ColumnHeadersVisible = True
            'Else
            '    dglista2.DataSource = Nothing
            '    dglista2.ColumnHeadersVisible = False
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        mostrar(txtFechaEmision.Value, txtFechaEmision.Value)

        Dim mycount As Integer

        mycount = dglista2.Rows.Count()

    End Sub

    Private Async Sub Button1_Click(sender As Object, e As EventArgs) Handles btnConsultaRD.Click

        Dim dtse As New vComprobanteSerie
        Dim dtCb As New vComunicacionBaja

        Dim dtSerie As New DataTable

        Dim funcSerie As New fComprobanteSerie

        Dim contribuyente = CrearEmisor()

        dtse.gcodigoSunat = "RA"
        dtse.gnrocaja = NROCAJAFE
        dtSerie = funcSerie.obtenerNumeracion(dtse)

        Try
            Cursor.Current = Cursors.WaitCursor

            If dglista2.Rows.Count > 0 Then

                If Not AccesoInternet() Then
                    MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    Return
                End If

                Dim Serie As Integer = dtSerie.Rows(0)(4)

                Dim correl = Serie

                Dim documentoBaja = New ComunicacionBaja With {
                    .IdDocumento = String.Format("RA-{0:yyyyMMdd}-" & correl, DateTime.Today),
                    .FechaEmision = DateTime.Today.ToString("yyyy-MM-dd"),
                    .FechaReferencia = txtFechaEmision.Value.ToString("yyyy-MM-dd"),
                    .Emisor = contribuyente,
                    .Bajas = New List(Of DocumentoBaja)()
                }
                Dim nomdoc = "RA-" & String.Format("{0:yyyyMMdd}-" & correl, DateTime.Today)

                For Each row As DataGridViewRow In dglista2.Rows
                    Dim baja As DocumentoBaja = New DocumentoBaja()
                    baja.Id = Convert.ToInt32(row.Cells(0).Value)
                    baja.TipoDocumento = Convert.ToString(row.Cells(1).Value)
                    baja.Serie = Convert.ToString(row.Cells(2).Value)
                    baja.Correlativo = Convert.ToString(row.Cells(3).Value)
                    baja.MotivoBaja = Convert.ToString(row.Cells(4).Value)
                    documentoBaja.Bajas.Add(baja)
                Next

                Dim invoice = GeneradorXML.GenerarVoidedDocuments(documentoBaja)
                Dim serializador3 = New FinalXML.Serializador()
                tramaXmlSinFirma = serializador3.GenerarXml(invoice)
                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{documentoBaja.IdDocumento}.xml")
                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(tramaXmlSinFirma))

                IdDocumento = nomdoc

                _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }

                _documento.IdDocumento = IdDocumento
                _documento.TipoDocumento = "RA"
                If String.IsNullOrEmpty(_documento.IdDocumento) Then Throw New InvalidOperationException("La Serie y el Correlativo no pueden estar vacíos")

                'tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo))
                'Dim firmadoRequest = New FirmadoRequest With {
                '    .TramaXmlSinFirma = tramaXmlSinFirma,
                '    .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                '    .PasswordCertificado = PASSWORDCERTIFICADO,
                '    .UnSoloNodoExtension = True
                '}

                'Dim enviar As FirmarController = New FirmarController()
                'Dim respuestaFirmado = enviar.FirmadoResponse(firmadoRequest)
                'RutaArchivo = Path.Combine($"{Program.CarpetaXml}\\{contribuyente.NroDocumento}-{documentoBaja.IdDocumento}.xml")
                'File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))
                'If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)

                Dim tramaXmlSinFirma2 = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo))
                Dim firmadoRequest = New FirmadoRequest With {
                                    .TramaXmlSinFirma = tramaXmlSinFirma2,
                                    .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                                    .PasswordCertificado = PASSWORDCERTIFICADO,
                                    .UnSoloNodoExtension = False
                                }

                Dim certificador As ICertificador = New Certificador()
                Dim respuestaFirmado = Await New Firmar(certificador).Post(firmadoRequest)
                _documento.ResumenFirma = respuestaFirmado.ResumenFirma
                _documento.FirmaDigital = respuestaFirmado.ValorFirma
                If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)
                RutaArchivo = Path.Combine(Program.CarpetaXml, contribuyente.NroDocumento & $"-{documentoBaja.IdDocumento}.xml")
                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))

                Dim oContribuyente = CrearEmisor()
                Dim enviarDocumentoRequest = New EnviarDocumentoRequest With {
                    .Ruc = oContribuyente.NroDocumento,
                    .UsuarioSol = oContribuyente.UsuarioSol,
                    .ClaveSol = oContribuyente.ClaveSol,
                    .EndPointUrl = SunatFact,
                    .IdDocumento = _documento.IdDocumento,
                    .TipoDocumento = _documento.TipoDocumento,
                    .TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                }

                Dim respuestaEnvio = New EnviarDocumentoResponse()
                Dim enviaResumen As EnviarResumenController = New EnviarResumenController()
                respuestaEnvio = enviaResumen.EnviarResumenResponse(enviarDocumentoRequest)
                Dim rpta = CType(respuestaEnvio, EnviarDocumentoResponse)

                txtDetailRes.Text = rpta.NroTicket
                If rpta.Exito Then txtNumTicket.Text = rpta.NroTicket.ToString()
                If Not respuestaEnvio.Exito Then Throw New ApplicationException(respuestaEnvio.MensajeError)
                DialogResult = DialogResult.OK


                Try
                    Cursor = Cursors.WaitCursor
                    If String.IsNullOrEmpty(txtNumTicket.Text) Then Return

                    Dim consultaTicketRequest = New ConsultaTicketRequest With {
                        .Ruc = oContribuyente.NroDocumento,
                        .UsuarioSol = oContribuyente.UsuarioSol,
                        .ClaveSol = oContribuyente.ClaveSol,
                        .EndPointUrl = SunatFact,
                        .IdDocumento = IdDocumento,
                        .NroTicket = txtNumTicket.Text
                    }

                    Dim serializador As ISerializador = New FinalXML.Serializador()
                    Dim servicioSunatDocumentos As IServicioSunatDocumentos = New InterMySql.ServicioSunatDocumentos()
                    Dim rptatk As EnviarDocumentoResponse = Await New ConsultarTicket(ServicioSunatDocumentos, Serializador).Post(consultaTicketRequest)

                    'Dim respuestaEnvio = New EnviarDocumentoResponse()
                    'Dim ConsultaTiket As ConsultarTicket = New ConsultarTicket()
                    'respuestaEnvio = ConsultaTiket.EnviarDocumentoResponse(consultaTicketRequest)

                    If Not respuestaEnvio.Exito Then
                        Throw New ApplicationException(respuestaEnvio.MensajeError)
                    Else
                        File.WriteAllBytes($"{Program.CarpetaCdr}\\{IdDocumento}.zip", Convert.FromBase64String(rptatk.TramaZipCdr))

                        dtCb.gcorrelativo = Serie
                        dtCb.gcodigoEstadoEnvioSunat = "0"
                        dtCb.gmensajeEnvioSunat = txtNumTicket.Text
                        dtCb.gnombreArchivoZip = rptatk.NombreArchivo & ".zip"
                        dtCb.gnombreArchivo = rptatk.NombreArchivo
                        dtCb.gnombreDocumento = IdDocumento

                        funcSerie.insertarComunicacionBaja(dtCb)

                        Dim dtv As New vventa
                        Dim funcVen As New fventa

                        For Each row As DataGridViewRow In dglista2.Rows

                            dtv.gserie_comprobante = Convert.ToString(row.Cells(2).Value)
                            dtv.gnumero_comprobante = Convert.ToString(row.Cells(3).Value)
                            dtv.gmotivoAnulacionEnvioSunat = Convert.ToString(row.Cells(4).Value)
                            funcVen.anularEnvioComprobanteSunatSerie(dtv)
                        Next

                        txtDetailRes.Text = rptatk.MensajeRespuesta
                    End If

                Catch ex As Exception
                    txtDetailRes.Text = ex.Message
                Finally
                    Cursor = Cursors.[Default]
                End Try

            Else
                MessageBox.Show("No hay Registros para Generar Documento")
                Return
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            Cursor.Current = Cursors.[Default]
        End Try

    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dglista2.CellClick
        Dim i, j As Integer
        i = dglista2.CurrentRow.Index
        nroTicketRD = dglista2.Item(4, i).Value
        'idDocumentoRD = dglista2.Item(7, i).Value
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        Try

            If txtSerie.Text = "" Then
                MessageBox.Show("Ingrese Serie")
                txtSerie.Focus()
                Return
            End If

            If txtMotivo.Text = "" Then
                MessageBox.Show("Ingrese Motivo de Anulación")
                txtMotivo.Focus()
                Return
            End If

            If cmbTipoDoc.SelectedIndex = 0 Then
                CodTipoDocumento = "01"
            ElseIf cmbTipoDoc.SelectedIndex = 1 Then
                CodTipoDocumento = "03"
            End If

            If txtNumeracion.Text = "" Then
                MessageBox.Show("Ingrese Numeración")
                txtNumeracion.Focus()
                Return
            End If

            dglista2.Rows.Add(counter2, CodTipoDocumento, txtSerie.Text, txtNumeracion.Text, txtMotivo.Text)
            counter2 += 1
            txtMotivo.Text = ""
        Catch a As Exception
            MessageBox.Show(a.Message)
        End Try
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        Try

            If dglista2.Rows.Count > 0 Then
                dglista2.Rows.RemoveAt(dglista2.CurrentRow.Index)
            Else
                MessageBox.Show("No hay registros por eliminar")
            End If

        Catch a As Exception
            MessageBox.Show(a.Message)
        End Try
    End Sub

End Class