﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmcListaResumenDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.zip = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nomZip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtNumTicketResumen = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDetailRes = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtcount = New System.Windows.Forms.TextBox()
        Me.txtstotalcompra = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnconsultar = New System.Windows.Forms.Button()
        Me.txtfechai = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnConsultaRD = New System.Windows.Forms.Button()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column9, Me.Column1, Me.Column2, Me.Column3, Me.zip, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.nomZip})
        Me.datalistado.Location = New System.Drawing.Point(243, 46)
        Me.datalistado.MultiSelect = False
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(1089, 469)
        Me.datalistado.TabIndex = 0
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "id"
        Me.Column9.HeaderText = "Id"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 45
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "correlativo"
        Me.Column1.HeaderText = "Correlativo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 103
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "cod_estado_envio_sunat"
        Me.Column2.HeaderText = "Codigo Envío Sunat"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 111
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "mensaje_envio_sunat"
        Me.Column3.HeaderText = "Mensaje Envío Sunat"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 118
        '
        'zip
        '
        Me.zip.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.zip.HeaderText = "Archivo Respuesta Sunat"
        Me.zip.Name = "zip"
        Me.zip.ReadOnly = True
        Me.zip.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.zip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.zip.Text = "Archivo Zip"
        Me.zip.ToolTipText = "Descargar Archivo Zip"
        Me.zip.UseColumnTextForButtonValue = True
        Me.zip.Width = 141
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "nombre_archivo"
        Me.Column5.HeaderText = "Nombre Archivo"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 123
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "nombre_documento"
        Me.Column6.HeaderText = "Nombre Documento"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 145
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "fecha"
        Me.Column7.HeaderText = "Fecha"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 72
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "fecha_registro"
        Me.Column8.HeaderText = "Fecha Registro"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 117
        '
        'nomZip
        '
        Me.nomZip.DataPropertyName = "nombre_archivo_zip"
        Me.nomZip.HeaderText = "NomArchivoZip"
        Me.nomZip.Name = "nomZip"
        Me.nomZip.ReadOnly = True
        Me.nomZip.Visible = False
        Me.nomZip.Width = 129
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Location = New System.Drawing.Point(306, 115)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(117, 16)
        Me.inexistente.TabIndex = 3
        Me.inexistente.Text = "Datos Inexistente"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtcount)
        Me.GroupBox2.Controls.Add(Me.txtstotalcompra)
        Me.GroupBox2.Controls.Add(Me.txtfechafi)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.btnconsultar)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.txtfechai)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1338, 521)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consultas"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.txtDetailRes)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 146)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 254)
        Me.GroupBox1.TabIndex = 42
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Envío Resumen"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtNumTicketResumen)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 171)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(219, 74)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Consulta"
        '
        'txtNumTicketResumen
        '
        Me.txtNumTicketResumen.Location = New System.Drawing.Point(80, 32)
        Me.txtNumTicketResumen.Name = "txtNumTicketResumen"
        Me.txtNumTicketResumen.Size = New System.Drawing.Size(133, 22)
        Me.txtNumTicketResumen.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "N° Ticket:"
        '
        'txtDetailRes
        '
        Me.txtDetailRes.BackColor = System.Drawing.Color.White
        Me.txtDetailRes.Location = New System.Drawing.Point(6, 33)
        Me.txtDetailRes.Multiline = True
        Me.txtDetailRes.Name = "txtDetailRes"
        Me.txtDetailRes.ReadOnly = True
        Me.txtDetailRes.Size = New System.Drawing.Size(219, 127)
        Me.txtDetailRes.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(912, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "N° de Registros :"
        '
        'txtcount
        '
        Me.txtcount.Location = New System.Drawing.Point(1030, 20)
        Me.txtcount.Name = "txtcount"
        Me.txtcount.Size = New System.Drawing.Size(63, 22)
        Me.txtcount.TabIndex = 40
        '
        'txtstotalcompra
        '
        Me.txtstotalcompra.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstotalcompra.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtstotalcompra.Location = New System.Drawing.Point(1032, 678)
        Me.txtstotalcompra.Name = "txtstotalcompra"
        Me.txtstotalcompra.Size = New System.Drawing.Size(193, 30)
        Me.txtstotalcompra.TabIndex = 39
        '
        'txtfechafi
        '
        Me.txtfechafi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechafi.Location = New System.Drawing.Point(616, 20)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(110, 22)
        Me.txtfechafi.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(510, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 16)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Fecha Final :"
        '
        'btnconsultar
        '
        Me.btnconsultar.Location = New System.Drawing.Point(749, 18)
        Me.btnconsultar.Name = "btnconsultar"
        Me.btnconsultar.Size = New System.Drawing.Size(75, 27)
        Me.btnconsultar.TabIndex = 8
        Me.btnconsultar.Text = "Consultar"
        Me.btnconsultar.UseVisualStyleBackColor = True
        '
        'txtfechai
        '
        Me.txtfechai.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechai.Location = New System.Drawing.Point(340, 18)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(121, 22)
        Me.txtfechai.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(240, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 16)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Fecha Inicio :"
        '
        'btnConsultaRD
        '
        Me.btnConsultaRD.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnConsultaRD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultaRD.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnConsultaRD.Location = New System.Drawing.Point(1099, 539)
        Me.btnConsultaRD.Name = "btnConsultaRD"
        Me.btnConsultaRD.Size = New System.Drawing.Size(245, 24)
        Me.btnConsultaRD.TabIndex = 46
        Me.btnConsultaRD.Text = "Consultar Estado Resumen Diario"
        Me.btnConsultaRD.UseVisualStyleBackColor = False
        '
        'frmcListaResumenDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1362, 575)
        Me.Controls.Add(Me.btnConsultaRD)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmcListaResumenDiario"
        Me.Text = "Consulta de  Resumenes Diarios"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents datalistado As System.Windows.Forms.DataGridView
    Friend WithEvents inexistente As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnconsultar As System.Windows.Forms.Button
    Friend WithEvents txtfechafi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtfechai As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtstotalcompra As System.Windows.Forms.TextBox
    Friend WithEvents txtcount As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtDetailRes As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumTicketResumen As TextBox
    Friend WithEvents btnConsultaRD As Button
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents zip As DataGridViewButtonColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents nomZip As DataGridViewTextBoxColumn
End Class
