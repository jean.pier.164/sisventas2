﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports FinalXML
Imports FinalXML.Interfaces
Imports FinalXML.InterMySql
'Imports FinalXML.Interfaces

Public Class frmcListaResumenDiario

    Private dt As New DataTable

    Dim _resumen As ResumenDiario
    Dim TramaXmlSinFirma As String
    Dim RutaArchivo As String
    Dim IdDocumento As String
    Shared recursos As String
    Dim herramientas As Herramientas = New Herramientas

    Shared SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Shared SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Shared SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Shared NROCAJAFE As String = ConfigurationManager.AppSettings.Get("NROCAJAFE")

    Shared nroTicketRD As String
    Shared idDocumentoRD As String

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar(DateTime.Now, DateTime.Now)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

        _resumen = New ResumenDiario()
        recursos = herramientas.GetResourcesPath()

    End Sub

    Private Sub mostrar(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime)
        Try
            Dim func As New fcventas
            dt = func.mostrarResumenesDiarios(fechaInicio, fechaFin)

            'datalistado.Columns.Item("Eliminar").Visible = False

            'If dt.Rows.Count <> 0 Then
            datalistado.DataSource = dt

            datalistado.ColumnHeadersVisible = True
            inexistente.Visible = False
            'Else
            '    datalistado.DataSource = Nothing


            '    datalistado.ColumnHeadersVisible = False
            '    inexistente.Visible = True
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click

        mostrar(txtfechai.Value, txtfechafi.Value)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

    End Sub

    Private Async Sub EnviarResumen()

        Dim dtse As New vComprobanteSerie
        Dim dtv As New vventa
        Dim dtdVe As New vdetalle_venta
        Dim dtRd As New vresumenDiario

        Dim dtSerie As New DataTable
        Dim dtDetVenta As New DataTable

        Dim funcSerie As New fComprobanteSerie
        Dim funcVen As New fventa
        Dim funcDetVen As New fdetalle_venta

        Dim contribuyente = CrearEmisor()

        dtse.gcodigoSunat = "RC"
        dtse.gnrocaja = NROCAJAFE
        dtSerie = funcSerie.obtenerNumeracion(dtse)

        If datalistado.RowCount > 0 Then

            If nroTicketRD <> "" Then

                If Not AccesoInternet() Then
                    MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    Return
                End If

                txtNumTicketResumen.Text = nroTicketRD
                Try

                    Dim oContribuyenteRpta = CrearEmisor()

                    Dim consultaTicketRequest = New ConsultaTicketRequest With {
                        .Ruc = oContribuyenteRpta.NroDocumento.ToString,
                        .UsuarioSol = oContribuyenteRpta.UsuarioSol,
                        .ClaveSol = oContribuyenteRpta.ClaveSol,
                        .EndPointUrl = SunatFact,
                        .IdDocumento = idDocumentoRD,
                        .NroTicket = nroTicketRD
                    }

                    Dim serializador As ISerializador = New FinalXML.Serializador()
                    Dim servicioSunatDocumentos As IServicioSunatDocumentos = New InterMySql.ServicioSunatDocumentos()
                    Dim rptatk As EnviarDocumentoResponse = Await New ConsultarTicket(servicioSunatDocumentos, serializador).Post(consultaTicketRequest)

                    If Not rptatk.Exito Then
                        Throw New ApplicationException(rptatk.MensajeError)
                    Else
                        txtDetailRes.Text = rptatk.MensajeRespuesta
                    End If

                Catch ex As Exception
                    txtDetailRes.Text = ex.Message
                End Try

            Else
                MessageBox.Show("Seleccione nuevamente un resumen diario..!")
            End If
        Else
            MessageBox.Show("No se ha seleccionado un resumen diario..!")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnConsultaRD.Click

        EnviarResumen()

    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        Dim i, j As Integer
        i = datalistado.CurrentRow.Index
        nroTicketRD = datalistado.Item(4, i).Value
        idDocumentoRD = datalistado.Item(7, i).Value
    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick

        Try
            If datalistado.Columns(e.ColumnIndex).Name.Equals("zip") Then
                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CDR\" & datalistado.CurrentRow.Cells(nomZip.Name).Value.ToString())
                Dim p As System.Diagnostics.Process = New System.Diagnostics.Process()
                p.StartInfo.FileName = RutaArchivo
                p.Start()
            End If

        Catch a As Exception
            MessageBox.Show(a.Message)
        End Try

    End Sub
End Class