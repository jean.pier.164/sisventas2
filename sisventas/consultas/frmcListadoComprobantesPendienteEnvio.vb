﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports FinalXML
Imports FinalXML.Interfaces
Imports FinalXML.InterMySql
'Imports FinalXML.Interfaces

Public Class frmcListadoComprobantesPendienteEnvio

    Private dt As New DataTable

    Dim _resumen As ResumenDiario
    Dim TramaXmlSinFirma As String
    Dim RutaArchivo As String
    Dim IdDocumento As String
    Shared recursos As String
    Dim herramientas As Herramientas = New Herramientas
    Shared idVentaAnular As String
    Shared comprobanteAnular As String

    Shared SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Shared SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Shared SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Dim _documento As DocumentoElectronico
    Dim ConvertLetras As Conversion = New Conversion

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar(DateTime.Now, DateTime.Now)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("total").Value IsNot Nothing) AndAlso
                    (row.Cells("total").Value IsNot DBNull.Value)) Select row.Cells("total").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        _resumen = New ResumenDiario()
        recursos = herramientas.GetResourcesPath()

    End Sub

    Private Sub mostrar(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime)
        Try
            Dim func As New fcventas
            dt = func.mostrarListadoComprobantesSunatPendientes(fechaInicio, fechaFin)

            'datalistado.Columns.Item("Eliminar").Visible = False

            'If dt.Rows.Count <> 0 Then
            datalistado.DataSource = dt

            datalistado.ColumnHeadersVisible = True
            inexistente.Visible = False
            'Else
            '    datalistado.DataSource = Nothing


            '    datalistado.ColumnHeadersVisible = False
            '    inexistente.Visible = True
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click

        mostrar(txtfechai.Value, txtfechafi.Value)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount


        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("total").Value IsNot Nothing) AndAlso
                    (row.Cells("total").Value IsNot DBNull.Value)) Select row.Cells("total").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        Dim i, j As Integer
        i = datalistado.CurrentRow.Index
        idVentaAnular = datalistado.Item(1, i).Value
        comprobanteAnular = datalistado.Item(6, i).Value & "-" & datalistado.Item(7, i).Value
    End Sub

    Private Async Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick

        Try

            If datalistado.Columns(e.ColumnIndex).Name.Equals("enviarSunat") Then

                Dim _idVenta As String
                _idVenta = datalistado.CurrentRow.Cells(idventa.Name).Value.ToString()

                Dim idCliente As String
                'Dim idVenta As String
                'idCliente = txtidcliente.Text
                'idVenta = txtidventa.Text

                Dim dtc As New vcliente
                Dim dtv As New vventa
                Dim dtse As New vComprobanteSerie
                Dim dtdVe As New vdetalle_venta

                Dim dtcliente As New DataTable
                Dim tipoDocumento As vclienteTipoDocumento = New vclienteTipoDocumento
                Dim dtventa As New DataTable
                'Dim dtSerie As New DataTable
                Dim dtDetVenta As New DataTable

                Dim funcCli As New fcliente
                Dim funcVen As New fventa
                Dim funcSerie As New fComprobanteSerie
                Dim funcDetVen As New fdetalle_venta

                dtv.gidventa = _idVenta
                dtventa = funcVen.verificarVenta(dtv)

                dtc.gidcliente = dtventa.Rows(0)("idcliente").ToString
                dtcliente = funcCli.verificarCliente(dtc)

                If dtcliente.Rows.Count > 0 Then

                    dtc.gidcliente = dtventa.Rows(0)("idcliente").ToString
                    dtc.gnombres = dtcliente.Rows(0)(1).ToString
                    dtc.gapellidos = dtcliente.Rows(0)(2).ToString
                    dtc.gdireccion = dtcliente.Rows(0)(3).ToString
                    dtc.gdni = dtcliente.Rows(0)(5).ToString
                    tipoDocumento.gcodigoSunat = dtcliente.Rows(0)(7).ToString
                    dtc.gtipoDocumento = tipoDocumento

                    dtdVe.gidventa = _idVenta
                    dtDetVenta = funcDetVen.mostrar_detalleventa_fe(dtdVe)

                    If dtDetVenta.Rows.Count = 0 Then
                        MessageBox.Show("Ingrese al menos un producto a la venta.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If

                    'Verificamos si es boleta o factura
                    If Not tipoDocumento.gcodigoSunat.ToString.Trim.Equals("6") Then
                        'Boleta 03
                        MessageBox.Show("Solo se pueden enviar facturas pendientes. Para Boletas se envían desde Resumen diario", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)

                    Else
                        'Factura 01

                        'dtse.gcodigoSunat = "01"
                        'dtSerie = funcSerie.obtenerNumeracion(dtse)

                        'If dtSerie.Rows.Count > 0 Then

                        Dim serie As String
                        Dim correlativo As String

                        'dtse.gid = dtSerie.Rows(0)(0).ToString
                        'dtse.gcodigoSunat = dtSerie.Rows(0)(1).ToString
                        'dtse.gdescripcion = dtSerie.Rows(0)(2).ToString
                        'dtse.gserie = dtSerie.Rows(0)(3).ToString
                        'dtse.gnumeracion = dtSerie.Rows(0)(4).ToString

                        serie = dtventa.Rows(0)("serie_comprobante").ToString
                        correlativo = dtventa.Rows(0)("numero_comprobante").ToString

                        If dtventa.Rows.Count > 0 Then

                            dtv.gtipo_comprobante = "01"
                            dtv.gserie_comprobante = serie
                            dtv.gnumero_comprobante = correlativo

                            'If funcVen.editarNumeracion(dtv) Then

                            Try
                                _documento = New DocumentoElectronico With {
                                    .Emisor = CrearEmisor()
                                }
                                Dim Items As List(Of DetalleDocumento) = New List(Of DetalleDocumento)()
                                Dim ven As DetalleDocumento = Nothing
                                Cursor.Current = Cursors.WaitCursor

                                _documento.Moneda = "PEN"

                                If dtDetVenta IsNot Nothing Then
                                    Dim i As Integer = 0

                                    For Each row As DataRow In dtDetVenta.Rows

                                        If i > 0 Then Items.Add(ven)


                                        Dim precioSinIgv As Decimal
                                        'Dim igv As Decimal
                                        Dim impuesto As Decimal

                                        ven = New DetalleDocumento()
                                        ven.Id = (i + 1)
                                        ven.CodigoItem = Convert.ToString(row(2))
                                        ven.Descripcion = Convert.ToString(row(3)).Trim()
                                        ven.Cantidad = Math.Abs(Convert.ToDecimal(row(4)))

                                        If ven.Cantidad = 0 Then ven.Cantidad = 1

                                        ven.PrecioUnitario = Math.Abs(Convert.ToDecimal(row(8)))
                                        ven.PrecioReferencial = Math.Abs(Convert.ToDecimal(row(8)))

                                        precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row(9)))) / 1.18, 2)
                                        impuesto = Math.Abs(Convert.ToDecimal(row(9))) - precioSinIgv


                                        If _documento.Moneda = "PEN" Then
                                            ven.Suma = Math.Abs((Convert.ToDecimal(row(9))))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        ElseIf _documento.Moneda = "USD" Then

                                            ven.Suma = Math.Abs(Convert.ToDecimal(row(9)))
                                            ven.SubTotalVenta = Math.Abs((Convert.ToDecimal(row(9)) - impuesto))

                                        End If


                                        ven.Impuesto = impuesto
                                        ven.TotalVenta = (ven.SubTotalVenta)
                                        ven.TipoPrecio = "01"
                                        ven.UnidadCliente = Convert.ToString(row(6)).Trim()

                                        If ven.Impuesto <> 0 Then
                                            ven.TipoImpuesto = "10"
                                        Else
                                            ven.TipoImpuesto = "20"

                                        End If

                                        ven.OtroImpuesto = 0
                                        'ven.UnidadMedida = "NIU"
                                        ven.UnidadMedida = Convert.ToString(row(6)).Trim()
                                        ven.Descuento = 0
                                        ven.ImpuestoSelectivo = 0

                                        i += 1

                                        If dtDetVenta.Rows.Count = i Then Items.Add(ven)

                                    Next
                                End If

                                _documento.Items = Items
                                _documento.Receptor.NroDocumento = dtventa.Rows(0)(24).ToString.Trim
                                _documento.Receptor.NombreLegal = dtventa.Rows(0)(20).ToString.Trim & " " & dtventa.Rows(0)(21).ToString.Trim
                                _documento.Receptor.Direccion = dtventa.Rows(0)(22).ToString.Trim
                                _documento.FechaVencimiento = Now
                                Dim fec As String
                                fec = dtventa.Rows(0)(2).ToString
                                _documento.FechaEmision = Convert.ToDateTime(dtventa.Rows(0)(2)).ToString("yyyy-MM-dd")

                                CalcularTotales()

                                Dim str1 As String = dtv.gserie_comprobante
                                Dim str2 As String = dtv.gnumero_comprobante

                                _documento.IdDocumento = str1 & "-" & str2
                                _documento.TipoDocumento = "01"
                                _documento.TipoOperacion = "0101"
                                _documento.Receptor.TipoDocumento = "6"

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim response As DocumentoResponse = New DocumentoResponse With {
                                        .Exito = False
                                    }

                                response = Await New GenerarFactura(serializador).Post(_documento)

                                If Not response.Exito Then Throw New ApplicationException(response.MensajeError)

                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_documento.IdDocumento}.xml")

                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(response.TramaXmlSinFirma))


                            Catch a As Exception
                                MessageBox.Show(a.Message)
                                Exit Sub
                            Finally
                                Cursor.Current = Cursors.[Default]
                            End Try


                            Try
                                Cursor = Cursors.WaitCursor

                                If Not AccesoInternet() Then
                                    MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                                    Return
                                End If

                                If String.IsNullOrEmpty(_documento.IdDocumento) Then
                                    Throw New InvalidOperationException("La Serie y el Correlativo no pueden estar vacíos")
                                    Exit Sub
                                End If

                                Dim tramaXmlSinFirma = Convert.ToBase64String(File.ReadAllBytes(RutaArchivo))
                                Dim firmadoRequest = New FirmadoRequest With {
                                    .TramaXmlSinFirma = tramaXmlSinFirma,
                                    .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                                    .PasswordCertificado = PASSWORDCERTIFICADO,
                                    .UnSoloNodoExtension = False
                                }

                                Dim certificador As ICertificador = New Certificador()
                                Dim respuestaFirmado = Await New Firmar(certificador).Post(firmadoRequest)
                                _documento.ResumenFirma = respuestaFirmado.ResumenFirma
                                _documento.FirmaDigital = respuestaFirmado.ValorFirma
                                If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)
                                RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"CF_{_documento.IdDocumento}.xml")
                                File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))

                                Dim oContribuyente = CrearEmisor()

                                Dim enviarDocumentoRequest = New EnviarDocumentoRequest With {
                                    .Ruc = oContribuyente.NroDocumento,
                                    .UsuarioSol = oContribuyente.UsuarioSol,
                                    .ClaveSol = oContribuyente.ClaveSol,
                                    .EndPointUrl = SunatFact,
                                    .IdDocumento = _documento.IdDocumento,
                                    .TipoDocumento = _documento.TipoDocumento,
                                    .TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
                                }

                                Dim serializador As ISerializador = New FinalXML.Serializador()
                                Dim servicioSunatDocumentos As IServicioSunatDocumentos = New ServicioSunatDocumentos()
                                Dim respuestaEnvio As RespuestaComunConArchivo
                                respuestaEnvio = Await New EnviarDocumento(serializador, servicioSunatDocumentos).Post(enviarDocumentoRequest)
                                Dim rpta = CType(respuestaEnvio, EnviarDocumentoResponse)
                                MessageBox.Show(rpta.MensajeRespuesta & " Siendo las " + DateTime.Now, "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                Try

                                    If rpta.Exito AndAlso Not String.IsNullOrEmpty(rpta.TramaZipCdr) Then
                                        File.WriteAllBytes($"{Program.CarpetaXml}\\{rpta.NombreArchivo}.xml", Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))
                                        File.WriteAllBytes($"{Program.CarpetaCdr}\\R-{rpta.NombreArchivo}.zip", Convert.FromBase64String(rpta.TramaZipCdr))
                                        _documento.FirmaDigital = respuestaFirmado.ValorFirma


                                    End If

                                    dtv.gcod_estado_envio_sunat = rpta.CodigoRespuesta
                                    dtv.gmensaje_envio_sunat = rpta.MensajeRespuesta
                                    dtv.garchivo_xml = rpta.NombreArchivo & ".xml"
                                    dtv.garchivo_cdr = "R-" & rpta.NombreArchivo & ".zip"
                                    dtv.garchivo_pdf = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" + _documento.IdDocumento & ".pdf"

                                    If rpta.Exito Then
                                        If rpta.CodigoRespuesta = "0" Then

                                            If dtv IsNot Nothing AndAlso dtv.gnumero_comprobante <> "" Then
                                                dtv.gestado_envio_sunat = 0
                                                funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                            End If
                                        ElseIf rpta.CodigoRespuesta Is Nothing Then
                                            Dim msg = String.Concat(rpta.MensajeRespuesta)
                                            Dim faultCode = "Client."

                                            If msg.Contains(faultCode) Then
                                                Dim posicion = msg.IndexOf(faultCode, StringComparison.Ordinal)
                                                Dim codigoError = msg.Substring(posicion + faultCode.Length, 4)
                                                msg = codigoError
                                            End If

                                            dtv.gestado_envio_sunat = 1
                                            dtv.gcod_estado_envio_sunat = msg
                                            funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                        End If

                                        frmRecibo._documento = _documento
                                        frmRecibo.txtidventa.Text = _idVenta
                                        frmRecibo.ShowDialog()

                                    Else
                                        MessageBox.Show(rpta.MensajeError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                                    End If

                                Catch ex As Exception

                                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Exit Sub
                                End Try

                                If Not respuestaEnvio.Exito Then Throw New ApplicationException(respuestaEnvio.MensajeError)

                            Catch ex As Exception
                                MessageBox.Show(ex.Message)

                                Exit Sub
                            Finally
                                'btnGeneraXML.Enabled = True
                                'btnEnvioSunat.Enabled = False
                                Cursor = Cursors.[Default]
                            End Try

                            'MessageBox.Show("La Factura " & dtse.gserie & "-" & correlativo & " se generó y envió a sunat.", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Information)

                            'Else
                            '    MessageBox.Show("Error al actualizar la numeración en la base de datos(IDVenta: " & _idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            'End If

                        Else
                            MessageBox.Show("No se encontró la venta en la base de datos(IDVenta: " & _idVenta & "). Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If

                        'Else
                        '    MessageBox.Show("No se encontró numeracion para boletas en la base de datos. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'End If

                    End If

                Else
                    MessageBox.Show("Venta no enviada a sunat. Cliente no existe", "Enviando Registro a Sunat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

                'MessageBox.Show("Envío Sunat " & _idVenta)
                mostrar(txtfechai.Value, txtfechafi.Value)
            End If

        Catch a As Exception
            MessageBox.Show(a.Message)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        FrmReporteListadoComprobante.txtfechai.Text = Me.txtfechai.Text
        FrmReporteListadoComprobante.txtfechafi.Text = Me.txtfechafi.Text

        FrmReporteListadoComprobante.ShowDialog()

    End Sub

    Private Sub CalcularTotales()
        _documento.TotalIgv = _documento.Items.Sum(Function(d) d.Impuesto)
        _documento.TotalIsc = _documento.Items.Sum(Function(d) d.ImpuestoSelectivo)
        _documento.TotalOtrosTributos = _documento.Items.Sum(Function(d) d.OtroImpuesto)
        _documento.Gravadas = _documento.Items.Where(Function(d) d.TipoImpuesto.StartsWith("1")).Sum(Function(d) d.SubTotalVenta)
        _documento.Exoneradas = _documento.Items.Where(Function(d) d.TipoImpuesto.Contains("20")).Sum(Function(d) d.Suma)
        _documento.Inafectas = _documento.Items.Where(Function(d) d.TipoImpuesto.StartsWith("3") OrElse d.TipoImpuesto.Contains("40")).Sum(Function(d) d.Suma)
        _documento.Gratuitas = _documento.Items.Where(Function(d) d.TipoImpuesto.Contains("21")).Sum(Function(d) d.Suma)
        _documento.LineCountNumeric = Convert.ToString(_documento.Items.Count())

        If _documento.TotalIsc > 0 Then
            _documento.TotalIgv = (_documento.Gravadas + _documento.TotalIsc) * _documento.CalculoIgv
        End If

        _documento.TotalVenta = _documento.Gravadas + _documento.Exoneradas + _documento.Inafectas + _documento.TotalIgv + _documento.TotalIsc + _documento.TotalOtrosTributos
        _documento.MontoEnLetras = ConvertLetras.enletras(_documento.TotalVenta.ToString())

        If _documento.CalculoIgv > 0 Then
            _documento.SubTotalVenta = _documento.TotalVenta - _documento.TotalIgv
        Else
            _documento.SubTotalVenta = _documento.TotalVenta
        End If
    End Sub
End Class