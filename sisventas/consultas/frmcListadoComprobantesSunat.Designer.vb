﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmcListadoComprobantesSunat
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cod_tipo_comprobante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pdf = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.xml = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.cdr = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomArchivoPDF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomArchivoXML = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomArchivoCDR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtcount = New System.Windows.Forms.TextBox()
        Me.txtstotalcompra = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnconsultar = New System.Windows.Forms.Button()
        Me.txtfechai = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtssventa = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.cod_tipo_comprobante, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.total, Me.Column13, Me.Column14, Me.Column15, Me.Column16, Me.pdf, Me.xml, Me.cdr, Me.Column20, Me.NomArchivoPDF, Me.NomArchivoXML, Me.NomArchivoCDR})
        Me.datalistado.Location = New System.Drawing.Point(18, 46)
        Me.datalistado.MultiSelect = False
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(1418, 469)
        Me.datalistado.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "idventa"
        Me.Column1.HeaderText = "Id Venta"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 78
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "descripcion"
        Me.Column2.HeaderText = "Comprobante"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 119
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "usuario"
        Me.Column3.HeaderText = "Usuario Reg."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 105
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "fecha_venta"
        Me.Column4.HeaderText = "Fecha Venta"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 103
        '
        'cod_tipo_comprobante
        '
        Me.cod_tipo_comprobante.DataPropertyName = "tipo_comprobante"
        Me.cod_tipo_comprobante.HeaderText = "Cod. Tipo Comprobante"
        Me.cod_tipo_comprobante.Name = "cod_tipo_comprobante"
        Me.cod_tipo_comprobante.ReadOnly = True
        Me.cod_tipo_comprobante.Width = 168
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "serie_comprobante"
        Me.Column6.HeaderText = "Serie Comprobante"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 143
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "numero_comprobante"
        Me.Column7.HeaderText = "Numero Comprobante"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 158
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "descripcion_tipo_doc"
        Me.Column8.HeaderText = "Tipo Doc. Cliente"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 130
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "dni"
        Me.Column9.HeaderText = "Doc. Identidad"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 114
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "nombre"
        Me.Column10.HeaderText = "Nombres Cliente"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 126
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "apellidos"
        Me.Column11.HeaderText = "Apellidos Cliente"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 129
        '
        'total
        '
        Me.total.DataPropertyName = "total"
        Me.total.HeaderText = "total"
        Me.total.Name = "total"
        Me.total.ReadOnly = True
        Me.total.Width = 61
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "descrip_estado_envio"
        Me.Column13.HeaderText = "Envío Anulado"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 114
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "motivo_anulacion_envio"
        Me.Column14.HeaderText = "Motivo Anulación"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Width = 131
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "cod_estado_envio_sunat"
        Me.Column15.HeaderText = "Cod. Estado Envío Sunat"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Width = 138
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "mensaje_envio_sunat"
        Me.Column16.HeaderText = "Mensaje Envío Sunat"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        Me.Column16.Width = 118
        '
        'pdf
        '
        Me.pdf.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.pdf.HeaderText = "Archivo PDF"
        Me.pdf.Name = "pdf"
        Me.pdf.ReadOnly = True
        Me.pdf.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.pdf.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.pdf.Text = "Archivo PDF"
        Me.pdf.ToolTipText = "Descargar Archivo PDF"
        Me.pdf.UseColumnTextForButtonValue = True
        Me.pdf.Width = 102
        '
        'xml
        '
        Me.xml.HeaderText = "Archivo XML"
        Me.xml.Name = "xml"
        Me.xml.ReadOnly = True
        Me.xml.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.xml.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.xml.Text = "Archivo XML"
        Me.xml.ToolTipText = "Descargar Archivo XML"
        Me.xml.UseColumnTextForButtonValue = True
        Me.xml.Width = 104
        '
        'cdr
        '
        Me.cdr.HeaderText = "Archivo CDR"
        Me.cdr.Name = "cdr"
        Me.cdr.ReadOnly = True
        Me.cdr.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cdr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cdr.Text = "Archivo CDR"
        Me.cdr.ToolTipText = "Descargar Archivo CDR"
        Me.cdr.UseColumnTextForButtonValue = True
        Me.cdr.Width = 103
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "fecha_envio_sunat"
        Me.Column20.HeaderText = "Fecha Envío a Sunat"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        Me.Column20.Width = 116
        '
        'NomArchivoPDF
        '
        Me.NomArchivoPDF.DataPropertyName = "archivo_pdf"
        Me.NomArchivoPDF.HeaderText = "NomArchivoPDF"
        Me.NomArchivoPDF.Name = "NomArchivoPDF"
        Me.NomArchivoPDF.ReadOnly = True
        Me.NomArchivoPDF.Visible = False
        Me.NomArchivoPDF.Width = 136
        '
        'NomArchivoXML
        '
        Me.NomArchivoXML.DataPropertyName = "archivo_xml"
        Me.NomArchivoXML.HeaderText = "NomArchivoXML"
        Me.NomArchivoXML.Name = "NomArchivoXML"
        Me.NomArchivoXML.ReadOnly = True
        Me.NomArchivoXML.Visible = False
        Me.NomArchivoXML.Width = 138
        '
        'NomArchivoCDR
        '
        Me.NomArchivoCDR.DataPropertyName = "archivo_cdr"
        Me.NomArchivoCDR.HeaderText = "NomArchivoCDR"
        Me.NomArchivoCDR.Name = "NomArchivoCDR"
        Me.NomArchivoCDR.ReadOnly = True
        Me.NomArchivoCDR.Visible = False
        Me.NomArchivoCDR.Width = 137
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Location = New System.Drawing.Point(306, 115)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(117, 16)
        Me.inexistente.TabIndex = 3
        Me.inexistente.Text = "Datos Inexistente"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtcount)
        Me.GroupBox2.Controls.Add(Me.txtstotalcompra)
        Me.GroupBox2.Controls.Add(Me.txtfechafi)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.btnconsultar)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.txtfechai)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1452, 521)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consultas"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1130, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(98, 24)
        Me.Button1.TabIndex = 42
        Me.Button1.Text = "Imprimir"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(912, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "N° de Registros :"
        '
        'txtcount
        '
        Me.txtcount.Location = New System.Drawing.Point(1030, 20)
        Me.txtcount.Name = "txtcount"
        Me.txtcount.Size = New System.Drawing.Size(63, 22)
        Me.txtcount.TabIndex = 40
        '
        'txtstotalcompra
        '
        Me.txtstotalcompra.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstotalcompra.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtstotalcompra.Location = New System.Drawing.Point(1032, 678)
        Me.txtstotalcompra.Name = "txtstotalcompra"
        Me.txtstotalcompra.Size = New System.Drawing.Size(193, 30)
        Me.txtstotalcompra.TabIndex = 39
        '
        'txtfechafi
        '
        Me.txtfechafi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechafi.Location = New System.Drawing.Point(616, 20)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(110, 22)
        Me.txtfechafi.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(510, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 16)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Fecha Final :"
        '
        'btnconsultar
        '
        Me.btnconsultar.Location = New System.Drawing.Point(749, 18)
        Me.btnconsultar.Name = "btnconsultar"
        Me.btnconsultar.Size = New System.Drawing.Size(75, 27)
        Me.btnconsultar.TabIndex = 8
        Me.btnconsultar.Text = "Consultar"
        Me.btnconsultar.UseVisualStyleBackColor = True
        '
        'txtfechai
        '
        Me.txtfechai.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechai.Location = New System.Drawing.Point(340, 18)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(121, 22)
        Me.txtfechai.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(240, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 16)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Fecha Inicio :"
        '
        'txtssventa
        '
        Me.txtssventa.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtssventa.ForeColor = System.Drawing.Color.Blue
        Me.txtssventa.Location = New System.Drawing.Point(454, 541)
        Me.txtssventa.Name = "txtssventa"
        Me.txtssventa.Size = New System.Drawing.Size(105, 30)
        Me.txtssventa.TabIndex = 41
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(270, 543)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(178, 23)
        Me.Label4.TabIndex = 45
        Me.Label4.Text = "Suma total de Ventas :"
        '
        'frmcListadoComprobantesSunat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1476, 575)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtssventa)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmcListadoComprobantesSunat"
        Me.Text = "Listado Comprobantes Enviados Sunat"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents datalistado As System.Windows.Forms.DataGridView
    Friend WithEvents inexistente As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnconsultar As System.Windows.Forms.Button
    Friend WithEvents txtfechafi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtfechai As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtstotalcompra As System.Windows.Forms.TextBox
    Friend WithEvents txtssventa As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtcount As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents cod_tipo_comprobante As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents total As DataGridViewTextBoxColumn
    Friend WithEvents Column13 As DataGridViewTextBoxColumn
    Friend WithEvents Column14 As DataGridViewTextBoxColumn
    Friend WithEvents Column15 As DataGridViewTextBoxColumn
    Friend WithEvents Column16 As DataGridViewTextBoxColumn
    Friend WithEvents pdf As DataGridViewButtonColumn
    Friend WithEvents xml As DataGridViewButtonColumn
    Friend WithEvents cdr As DataGridViewButtonColumn
    Friend WithEvents Column20 As DataGridViewTextBoxColumn
    Friend WithEvents NomArchivoPDF As DataGridViewTextBoxColumn
    Friend WithEvents NomArchivoXML As DataGridViewTextBoxColumn
    Friend WithEvents NomArchivoCDR As DataGridViewTextBoxColumn
    Friend WithEvents Button1 As Button
End Class
