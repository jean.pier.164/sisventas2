﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports FinalXML
Imports FinalXML.Interfaces
Imports FinalXML.InterMySql
'Imports FinalXML.Interfaces

Public Class frmcListadoComprobantesSunat

    Private dt As New DataTable

    Dim _resumen As ResumenDiario
    Dim TramaXmlSinFirma As String
    Dim RutaArchivo As String
    Dim IdDocumento As String
    Shared recursos As String
    Dim herramientas As Herramientas = New Herramientas
    Shared idVentaAnular As String
    Shared comprobanteAnular As String

    Shared SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Shared SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Shared SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar(DateTime.Now, DateTime.Now)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("total").Value IsNot Nothing) AndAlso
                    (row.Cells("total").Value IsNot DBNull.Value)) Select row.Cells("total").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        _resumen = New ResumenDiario()
        recursos = herramientas.GetResourcesPath()

    End Sub

    Private Sub mostrar(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime)
        Try
            Dim func As New fcventas
            dt = func.mostrarListadoComprobantesSunat(fechaInicio, fechaFin)

            'datalistado.Columns.Item("Eliminar").Visible = False

            'If dt.Rows.Count <> 0 Then
            datalistado.DataSource = dt

            datalistado.ColumnHeadersVisible = True
            inexistente.Visible = False
            'Else
            '    datalistado.DataSource = Nothing


            '    datalistado.ColumnHeadersVisible = False
            '    inexistente.Visible = True
            'End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click

        mostrar(txtfechai.Value, txtfechafi.Value)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount


        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("total").Value IsNot Nothing) AndAlso
                    (row.Cells("total").Value IsNot DBNull.Value)) Select row.Cells("total").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        Dim i, j As Integer
        i = datalistado.CurrentRow.Index
        idVentaAnular = datalistado.Item(1, i).Value
        comprobanteAnular = datalistado.Item(6, i).Value & "-" & datalistado.Item(7, i).Value
    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick

        Try
            If datalistado.Columns(e.ColumnIndex).Name.Equals("pdf") Then

                If (datalistado.CurrentRow.Cells("cod_tipo_comprobante").Value.ToString = "01") Then
                    RutaArchivo = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, ("FACTURAS_PDF\" + datalistado.CurrentRow.Cells(NomArchivoPDF.Name).Value.ToString))
                End If

                If (datalistado.CurrentRow.Cells("cod_tipo_comprobante").Value.ToString = "03") Then
                    RutaArchivo = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, ("BOLETAS_PDF\" + datalistado.CurrentRow.Cells(NomArchivoPDF.Name).Value.ToString))
                End If

                If (datalistado.CurrentRow.Cells("cod_tipo_comprobante").Value.ToString = "07") Then
                    RutaArchivo = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, ("NOTA_CREDITO_PDF\" + datalistado.CurrentRow.Cells(NomArchivoPDF.Name).Value.ToString))
                End If

                If (datalistado.CurrentRow.Cells("cod_tipo_comprobante").Value.ToString = "08") Then
                    RutaArchivo = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, ("NOTA_DEBITO_PDF\" + datalistado.CurrentRow.Cells(NomArchivoPDF.Name).Value.ToString))
                End If

                'RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CDR\" & datalistado.CurrentRow.Cells(NomArchivoPDF.Name).Value.ToString())
                Dim p As System.Diagnostics.Process = New System.Diagnostics.Process()
                p.StartInfo.FileName = RutaArchivo
                p.Start()
            End If

            If datalistado.Columns(e.ColumnIndex).Name.Equals("xml") Then
                RutaArchivo = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "XML\" & datalistado.CurrentRow.Cells(NomArchivoXML.Name).Value.ToString())
                Dim p As System.Diagnostics.Process = New System.Diagnostics.Process()
                p.StartInfo.FileName = RutaArchivo
                p.Start()
            End If

            If datalistado.Columns(e.ColumnIndex).Name.Equals("cdr") Then
                RutaArchivo = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "CDR\" & datalistado.CurrentRow.Cells(NomArchivoCDR.Name).Value.ToString())
                Dim p As System.Diagnostics.Process = New System.Diagnostics.Process()
                p.StartInfo.FileName = RutaArchivo
                p.Start()
            End If

        Catch a As Exception
            MessageBox.Show(a.Message)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        FrmReporteListadoComprobante.txtfechai.Text = Me.txtfechai.Text
        FrmReporteListadoComprobante.txtfechafi.Text = Me.txtfechafi.Text

        FrmReporteListadoComprobante.ShowDialog()

    End Sub
End Class