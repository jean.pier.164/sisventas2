﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmcRegistroAnularBoleta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtMotivoAnular = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtComprobanteAnular = New System.Windows.Forms.TextBox()
        Me.btnAceptarAnular = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblIdVentaAnular = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(50, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Motivo:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblIdVentaAnular)
        Me.GroupBox1.Controls.Add(Me.txtComprobanteAnular)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtMotivoAnular)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(384, 126)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Anulación Envío Sunat"
        '
        'txtMotivoAnular
        '
        Me.txtMotivoAnular.Location = New System.Drawing.Point(97, 49)
        Me.txtMotivoAnular.Multiline = True
        Me.txtMotivoAnular.Name = "txtMotivoAnular"
        Me.txtMotivoAnular.Size = New System.Drawing.Size(254, 70)
        Me.txtMotivoAnular.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Comprobante:"
        '
        'txtComprobanteAnular
        '
        Me.txtComprobanteAnular.Enabled = False
        Me.txtComprobanteAnular.Location = New System.Drawing.Point(98, 20)
        Me.txtComprobanteAnular.Name = "txtComprobanteAnular"
        Me.txtComprobanteAnular.Size = New System.Drawing.Size(134, 20)
        Me.txtComprobanteAnular.TabIndex = 3
        '
        'btnAceptarAnular
        '
        Me.btnAceptarAnular.Location = New System.Drawing.Point(110, 144)
        Me.btnAceptarAnular.Name = "btnAceptarAnular"
        Me.btnAceptarAnular.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptarAnular.TabIndex = 2
        Me.btnAceptarAnular.Text = "Aceptar"
        Me.btnAceptarAnular.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(249, 144)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblIdVentaAnular
        '
        Me.lblIdVentaAnular.AutoSize = True
        Me.lblIdVentaAnular.Location = New System.Drawing.Point(339, 20)
        Me.lblIdVentaAnular.Name = "lblIdVentaAnular"
        Me.lblIdVentaAnular.Size = New System.Drawing.Size(39, 13)
        Me.lblIdVentaAnular.TabIndex = 4
        Me.lblIdVentaAnular.Text = "Label3"
        Me.lblIdVentaAnular.Visible = False
        '
        'frmcRegistroAnularBoleta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(416, 181)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptarAnular)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmcRegistroAnularBoleta"
        Me.Text = "Anular Boleta Envío Sunat"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtComprobanteAnular As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtMotivoAnular As TextBox
    Friend WithEvents btnAceptarAnular As Button
    Friend WithEvents btnCancelar As Button
    Friend WithEvents lblIdVentaAnular As Label
End Class
