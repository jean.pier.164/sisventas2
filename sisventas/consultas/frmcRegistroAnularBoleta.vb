﻿Public Class frmcRegistroAnularBoleta
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Dispose()
    End Sub

    Private Sub btnAceptarAnular_Click(sender As Object, e As EventArgs) Handles btnAceptarAnular.Click

        Dim dtv As New vventa

        Dim funcVen As New fventa

        If txtComprobanteAnular.Text <> "" Then

            dtv.gidventa = lblIdVentaAnular.Text
            dtv.gmotivoAnulacionEnvioSunat = txtMotivoAnular.Text

            If funcVen.anularEnvioComprobanteSunat(dtv) Then
                MessageBox.Show("Se anuló el envío de la boleta a sunat.", "Anulación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Dispose()
            End If

        Else
            MessageBox.Show("Ingrese un motivo para anular la boleta.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub
End Class