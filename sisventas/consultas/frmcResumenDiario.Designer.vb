﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmcResumenDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.Eliminar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtNumTicketResumen = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDetailRes = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtcount = New System.Windows.Forms.TextBox()
        Me.txtstotalcompra = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnconsultar = New System.Windows.Forms.Button()
        Me.txtfechai = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtssventa = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnEnviarResumenDiario = New System.Windows.Forms.Button()
        Me.btnAnularEnvío = New System.Windows.Forms.Button()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Eliminar})
        Me.datalistado.Location = New System.Drawing.Point(243, 46)
        Me.datalistado.MultiSelect = False
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(1089, 469)
        Me.datalistado.TabIndex = 0
        '
        'Eliminar
        '
        Me.Eliminar.HeaderText = "Eliminar"
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        Me.Eliminar.Width = 67
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Location = New System.Drawing.Point(306, 115)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(117, 16)
        Me.inexistente.TabIndex = 3
        Me.inexistente.Text = "Datos Inexistente"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtcount)
        Me.GroupBox2.Controls.Add(Me.txtstotalcompra)
        Me.GroupBox2.Controls.Add(Me.txtfechafi)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.btnconsultar)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.txtfechai)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1338, 521)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consultas"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.txtDetailRes)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 146)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(231, 254)
        Me.GroupBox1.TabIndex = 42
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Envío Resumen"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtNumTicketResumen)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 171)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(219, 74)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Consulta"
        '
        'txtNumTicketResumen
        '
        Me.txtNumTicketResumen.Location = New System.Drawing.Point(80, 32)
        Me.txtNumTicketResumen.Name = "txtNumTicketResumen"
        Me.txtNumTicketResumen.Size = New System.Drawing.Size(133, 22)
        Me.txtNumTicketResumen.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "N° Ticket:"
        '
        'txtDetailRes
        '
        Me.txtDetailRes.BackColor = System.Drawing.Color.White
        Me.txtDetailRes.Location = New System.Drawing.Point(6, 33)
        Me.txtDetailRes.Multiline = True
        Me.txtDetailRes.Name = "txtDetailRes"
        Me.txtDetailRes.ReadOnly = True
        Me.txtDetailRes.Size = New System.Drawing.Size(219, 127)
        Me.txtDetailRes.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(912, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "N° de Registros :"
        '
        'txtcount
        '
        Me.txtcount.Location = New System.Drawing.Point(1030, 20)
        Me.txtcount.Name = "txtcount"
        Me.txtcount.Size = New System.Drawing.Size(63, 22)
        Me.txtcount.TabIndex = 40
        '
        'txtstotalcompra
        '
        Me.txtstotalcompra.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstotalcompra.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtstotalcompra.Location = New System.Drawing.Point(1032, 678)
        Me.txtstotalcompra.Name = "txtstotalcompra"
        Me.txtstotalcompra.Size = New System.Drawing.Size(193, 30)
        Me.txtstotalcompra.TabIndex = 39
        '
        'txtfechafi
        '
        Me.txtfechafi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechafi.Location = New System.Drawing.Point(616, 20)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(110, 22)
        Me.txtfechafi.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(510, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 16)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Fecha Final :"
        '
        'btnconsultar
        '
        Me.btnconsultar.Location = New System.Drawing.Point(749, 18)
        Me.btnconsultar.Name = "btnconsultar"
        Me.btnconsultar.Size = New System.Drawing.Size(75, 27)
        Me.btnconsultar.TabIndex = 8
        Me.btnconsultar.Text = "Consultar"
        Me.btnconsultar.UseVisualStyleBackColor = True
        '
        'txtfechai
        '
        Me.txtfechai.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechai.Location = New System.Drawing.Point(340, 18)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(121, 22)
        Me.txtfechai.TabIndex = 4
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(240, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 16)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Fecha Inicio :"
        '
        'txtssventa
        '
        Me.txtssventa.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtssventa.ForeColor = System.Drawing.Color.Blue
        Me.txtssventa.Location = New System.Drawing.Point(454, 541)
        Me.txtssventa.Name = "txtssventa"
        Me.txtssventa.Size = New System.Drawing.Size(105, 30)
        Me.txtssventa.TabIndex = 41
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Narrow", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(270, 543)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(178, 23)
        Me.Label4.TabIndex = 45
        Me.Label4.Text = "Suma total de Ventas :"
        '
        'btnEnviarResumenDiario
        '
        Me.btnEnviarResumenDiario.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarResumenDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarResumenDiario.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnEnviarResumenDiario.Location = New System.Drawing.Point(1169, 539)
        Me.btnEnviarResumenDiario.Name = "btnEnviarResumenDiario"
        Me.btnEnviarResumenDiario.Size = New System.Drawing.Size(175, 24)
        Me.btnEnviarResumenDiario.TabIndex = 46
        Me.btnEnviarResumenDiario.Text = "Enviar Resumen Diario Boletas"
        Me.btnEnviarResumenDiario.UseVisualStyleBackColor = False
        '
        'btnAnularEnvío
        '
        Me.btnAnularEnvío.BackColor = System.Drawing.Color.Red
        Me.btnAnularEnvío.Font = New System.Drawing.Font("Arial Rounded MT Bold", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnularEnvío.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnAnularEnvío.Location = New System.Drawing.Point(1356, 237)
        Me.btnAnularEnvío.Name = "btnAnularEnvío"
        Me.btnAnularEnvío.Size = New System.Drawing.Size(75, 54)
        Me.btnAnularEnvío.TabIndex = 47
        Me.btnAnularEnvío.Text = "Anular Envío a Sunat"
        Me.btnAnularEnvío.UseVisualStyleBackColor = False
        '
        'frmcResumenDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1439, 575)
        Me.Controls.Add(Me.btnAnularEnvío)
        Me.Controls.Add(Me.btnEnviarResumenDiario)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtssventa)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmcResumenDiario"
        Me.Text = "Envío Resumen Diario"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents datalistado As System.Windows.Forms.DataGridView
    Friend WithEvents Eliminar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents inexistente As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnconsultar As System.Windows.Forms.Button
    Friend WithEvents txtfechafi As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtfechai As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtstotalcompra As System.Windows.Forms.TextBox
    Friend WithEvents txtssventa As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtcount As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtDetailRes As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtNumTicketResumen As TextBox
    Friend WithEvents btnEnviarResumenDiario As Button
    Friend WithEvents btnAnularEnvío As Button
End Class
