﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.IO
Imports FinalXML
'Imports FinalXML.Interfaces

Public Class frmcResumenDiario

    Private dt As New DataTable

    Dim _resumen As ResumenDiario
    Dim TramaXmlSinFirma As String
    Dim RutaArchivo As String
    Dim IdDocumento As String
    Shared recursos As String
    Dim herramientas As Herramientas = New Herramientas
    Shared idVentaAnular As String
    Shared comprobanteAnular As String

    Shared SunatFact As String = ConfigurationManager.AppSettings.Get("SUNATCPE")
    Shared SunatGuia As String = ConfigurationManager.AppSettings.Get("SUNATGUI")
    Shared SunatOtro As String = ConfigurationManager.AppSettings.Get("SUNATOCE")

    Shared RUC As String = ConfigurationManager.AppSettings.Get("RUC")
    Shared TIPODOC As String = ConfigurationManager.AppSettings.Get("TIPODOC")
    Shared DIRECCION As String = ConfigurationManager.AppSettings.Get("DIRECCION")
    Shared DEPARTAMENTO As String = ConfigurationManager.AppSettings.Get("DEPARTAMENTO")
    Shared PROVINCIA As String = ConfigurationManager.AppSettings.Get("PROVINCIA")
    Shared DISTRITO As String = ConfigurationManager.AppSettings.Get("DISTRITO")
    Shared RAZONSOCIAL As String = ConfigurationManager.AppSettings.Get("RAZONSOCIAL")
    Shared NOMBRECOMERCIAL As String = ConfigurationManager.AppSettings.Get("NOMBRECOMERCIAL")
    Shared UBIGEO As String = ConfigurationManager.AppSettings.Get("UBIGEO")
    Shared USUARIOSOL As String = ConfigurationManager.AppSettings.Get("USUARIOSOL")
    Shared CLAVESOL As String = ConfigurationManager.AppSettings.Get("CLAVESOL")

    Shared PASSWORDCERTIFICADO As String = ConfigurationManager.AppSettings.Get("PASSWORDCERTIFICADO")
    Shared NOMBREARCHIVOCERTIFICADO As String = ConfigurationManager.AppSettings.Get("NOMBREARCHIVOCERTIFICADO")

    Shared NROCAJAFE As String = ConfigurationManager.AppSettings.Get("NROCAJAFE")

    Private Shared Function CrearEmisor() As Contribuyente
        Return New Contribuyente With {
            .NroDocumento = RUC,
            .TipoDocumento = TIPODOC,
            .Direccion = DIRECCION,
            .Departamento = DEPARTAMENTO,
            .Provincia = PROVINCIA,
            .Distrito = DISTRITO,
            .NombreLegal = RAZONSOCIAL,
            .NombreComercial = NOMBRECOMERCIAL,
            .Ubigeo = UBIGEO,
            .UsuarioSol = USUARIOSOL,
            .ClaveSol = CLAVESOL
        }
    End Function

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar(DateTime.Now, DateTime.Now)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("total").Value IsNot Nothing) AndAlso
                    (row.Cells("total").Value IsNot DBNull.Value)) Select row.Cells("total").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        _resumen = New ResumenDiario()
        recursos = herramientas.GetResourcesPath()

    End Sub

    Private Sub mostrar(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime)
        Try
            Dim func As New fcventas
            dt = func.mostrarBoletasEnvioResumenDiario(fechaInicio, fechaFin)

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click

        Dim fechaInicio As DateTime
        Dim fechaFin As DateTime

        mostrar(txtfechai.Value, txtfechafi.Value)

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount


        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("total").Value IsNot Nothing) AndAlso
                    (row.Cells("total").Value IsNot DBNull.Value)) Select row.Cells("total").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Async Sub EnviarResumen()

        Dim dtse As New vComprobanteSerie
        Dim dtv As New vventa
        Dim dtdVe As New vdetalle_venta
        Dim dtRd As New vresumenDiario

        Dim dtSerie As New DataTable
        Dim dtDetVenta As New DataTable

        Dim funcSerie As New fComprobanteSerie
        Dim funcVen As New fventa
        Dim funcDetVen As New fdetalle_venta

        Dim contribuyente = CrearEmisor()

        dtse.gcodigoSunat = "RC"
        dtse.gnrocaja = NROCAJAFE
        dtSerie = funcSerie.obtenerNumeracion(dtse)

        If datalistado.RowCount > 0 Then

            If Not AccesoInternet() Then
                MessageBox.Show("No hay conexión con el servidor " & vbLf & " Verifique si existe conexión a internet e intente nuevamente.", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                Return
            End If

            Dim Serie As Integer = dtSerie.Rows(0)(4)

            Dim Fecha As DateTime = txtfechai.Value.Date
            _resumen.IdDocumento = String.Format("RC-{0}{1}{2}-{3}", Fecha.Year, String.Format("{0:00}", Fecha.Month), String.Format("{0:00}", Fecha.Day), Serie)
            _resumen.Emisor = contribuyente
            _resumen.FechaEmision = Fecha.ToString("yyyy-MM-dd")
            _resumen.FechaReferencia = Fecha.ToString("yyyy-MM-dd")
            Dim tipdoc As String = "", sersun As String = "", numsun As String = ""
            Dim impuesto As Decimal = 0, Gravadas As Decimal = 0, Exoneradas As Decimal = 0
            Dim Items As List(Of GrupoResumen) = New List(Of GrupoResumen)()
            Dim ven As GrupoResumen = New GrupoResumen()
            Dim i As Integer = 0

            For Each row As DataGridViewRow In datalistado.Rows
                i += 1
                tipdoc = row.Cells("tipo_comprobante").Value.ToString()
                sersun = row.Cells("serie_comprobante").Value.ToString()
                numsun = row.Cells("numero_comprobante").Value.ToString()

                'CVentas1 = AdmCVenta.LeerVentaResumen(_resumen.Emisor.NroDocumento, tipdoc, sersun, numsun)

                'dt_DetalleVenta = AdmCVenta.LeerDetalleResumen(cboEmpresa.SelectedValue.ToString(), CVentas1.Sigla, CVentas1.Serie, CVentas1.Numeracion)

                dtdVe.gidventa = row.Cells("idventa").Value.ToString()
                dtDetVenta = funcDetVen.mostrar_detalleventa_fe(dtdVe)
                impuesto = 0
                Gravadas = 0
                Exoneradas = 0
                If dtDetVenta IsNot Nothing Then

                    For Each row_det As DataRow In dtDetVenta.Rows

                        Dim precioSinIgv As Decimal
                        Dim igv As Decimal


                        precioSinIgv = Math.Round(Math.Abs((Convert.ToDecimal(row_det(9)))) / 1.18, 2)
                        impuesto += Math.Abs(Convert.ToDecimal(row_det(9))) - precioSinIgv
                        'impuesto = Convert.ToDecimal(row_det(6).ToString())

                        If Math.Abs((Convert.ToDecimal(row_det(9)))) <> 0 Then
                            Gravadas += Math.Abs((Convert.ToDecimal(row_det(9)) - impuesto))
                        Else
                            Exoneradas += Math.Abs((Convert.ToDecimal(row_det(9)) - impuesto))
                        End If
                    Next
                End If

                ven = New GrupoResumen With {
                    .Id = i,
                    .TipoDocumento = "03",
                    .Moneda = "PEN",
                    .NumeroDocumento = sersun & "-" + numsun,
                    .TotalVenta = Gravadas + Exoneradas + impuesto,
                    .TotalDescuentos = 0,
                    .TotalIgv = impuesto,
                    .TotalIsc = 0,
                    .TotalOtrosImpuestos = 0,
                    .Gravadas = Gravadas,
                    .Exoneradas = Exoneradas,
                    .Inafectas = 0,
                    .Exportacion = 0,
                    .Gratuitas = 0,
                    .DocumentoCliente = row.Cells("dni").Value.ToString().Trim(),
                    .TipoDocumentoCliente = "1",
                    .Operacion = "1" '1: Adicionar, 2:Modificar, 3:Anulado, 4:Anulado en el día (anulado antes de informar comprobante)
                }
                Items.Add(ven)
            Next

            _resumen.Resumenes = Items

            Dim ResumenDiario = GeneradorXML.GenerarSummaryDocuments(_resumen)
            Dim serializador3 = New Serializador()

            TramaXmlSinFirma = serializador3.GenerarXml(ResumenDiario)
            RutaArchivo = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\" & $"{_resumen.IdDocumento}.xml")
            File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(TramaXmlSinFirma))
            IdDocumento = _resumen.IdDocumento
            Dim firmadoRequest = New FirmadoRequest With {
                .TramaXmlSinFirma = TramaXmlSinFirma,
                .CertificadoDigital = Convert.ToBase64String(File.ReadAllBytes(recursos & "\" & NOMBREARCHIVOCERTIFICADO)),
                .PasswordCertificado = PASSWORDCERTIFICADO,
                .UnSoloNodoExtension = True
            }

            'Dim enviar As FirmarController = New FirmarController()
            'Dim respuestaFirmado = enviar.FirmadoResponse(firmadoRequest)

            'RutaArchivo = Path.Combine($"{Program.CarpetaXml}\\{contribuyente.NroDocumento.ToString()}-{_resumen.IdDocumento}.xml")
            'File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))


            Dim certificador As Interfaces.ICertificador = New InterMySql.Certificador()
            Dim respuestaFirmado = Await New Firmar(certificador).Post(firmadoRequest)
            '_resumen.ResumenFirma = respuestaFirmado.ResumenFirma
            '_resumen.FirmaDigital = respuestaFirmado.ValorFirma
            If Not respuestaFirmado.Exito Then Throw New ApplicationException(respuestaFirmado.MensajeError)
            RutaArchivo = Path.Combine($"{Program.CarpetaXml}\\{contribuyente.NroDocumento.ToString()}-{_resumen.IdDocumento}.xml")
            File.WriteAllBytes(RutaArchivo, Convert.FromBase64String(respuestaFirmado.TramaXmlFirmado))

            Dim oContribuyente = CrearEmisor()

            Dim enviarDocumentoRequest = New EnviarDocumentoRequest With {
                .Ruc = oContribuyente.NroDocumento.ToString(),
                .UsuarioSol = oContribuyente.UsuarioSol,
                .ClaveSol = oContribuyente.ClaveSol,
                .EndPointUrl = SunatFact,
                .IdDocumento = _resumen.IdDocumento,
                .TipoDocumento = "RC",
                .TramaXmlFirmado = respuestaFirmado.TramaXmlFirmado
            }
            Dim serializador = New Serializador()
            Dim servicioSunatDocumentos As Interfaces.IServicioSunatDocumentos = New InterMySql.ServicioSunatDocumentos()

            Dim respuestaEnvio As RespuestaComunConArchivo
            respuestaEnvio = Await New EnviarResumen(serializador, servicioSunatDocumentos).Post(enviarDocumentoRequest)

            Dim rpta = CType(respuestaEnvio, EnviarResumenResponse)
            txtDetailRes.Text = rpta.NroTicket

            If rpta.Exito Then

                txtNumTicketResumen.Text = rpta.NroTicket.ToString()
                'Dim newocor = AdmCEmpresa.SetCorrelativoMasivo(contribuyente.CodigoEmpresa, "RC", Serie)

                If String.IsNullOrEmpty(txtNumTicketResumen.Text) Then Return

                Try

                    Dim oContribuyenteRpta = CrearEmisor()

                    Dim consultaTicketRequest = New ConsultaTicketRequest With {
                        .Ruc = oContribuyenteRpta.NroDocumento.ToString,
                        .UsuarioSol = oContribuyenteRpta.UsuarioSol,
                        .ClaveSol = oContribuyenteRpta.ClaveSol,
                        .EndPointUrl = SunatFact,
                        .IdDocumento = IdDocumento,
                        .NroTicket = txtNumTicketResumen.Text
                    }
                    'Dim respuestaEnvioTk = New EnviarDocumentoResponse()
                    'Dim ConsultaTiketRes As ConsultarTicket = New ConsultarTicket()
                    'respuestaEnvioTk = ConsultaTiketRes.EnviarDocumentoResponse(consultaTicketRequest)

                    Dim serializadortk = New Serializador()
                    Dim servicioSunatDocumentostk As Interfaces.IServicioSunatDocumentos = New InterMySql.ServicioSunatDocumentos()
                    'Dim respuestaEnvioTk As RespuestaComunConArchivo
                    'respuestaEnvioTk = Await New ConsultarTicket(serializador, servicioSunatDocumentos).Post(consultaTicketRequest)
                    Dim rptatk As EnviarDocumentoResponse = Await New ConsultarTicket(servicioSunatDocumentos, serializador).Post(consultaTicketRequest)
                    'Dim rptatk = CType(respuestaEnvioTk, EnviarDocumentoResponse)

                    If Not rptatk.Exito Then
                        Throw New ApplicationException(rptatk.MensajeError)
                    Else
                        File.WriteAllBytes($"{Program.CarpetaCdr}\\{rptatk.NombreArchivo}.zip", Convert.FromBase64String(rptatk.TramaZipCdr))

                        dtRd.gcorrelativo = Serie
                        dtRd.gcodigoEstadoEnvioSunat = "0"
                        dtRd.gmensajeEnvioSunat = rptatk.MensajeRespuesta + " | Ticket: " + rpta.NroTicket.ToString
                        dtRd.gnombreArchivoZip = rptatk.NombreArchivo & ".zip"
                        dtRd.gnombreArchivo = rptatk.NombreArchivo
                        dtRd.gnombreDocumento = IdDocumento

                        funcSerie.insertarResumenDiario(dtRd)
                        txtDetailRes.Text = rptatk.MensajeRespuesta

                        For Each row As DataGridViewRow In datalistado.Rows

                            dtv.gidventa = row.Cells("idventa").Value.ToString()
                            dtv.gnumero_comprobante = row.Cells("numero_comprobante").Value.ToString()
                            dtv.gcod_estado_envio_sunat = "0"
                            dtv.gmensaje_envio_sunat = rptatk.MensajeRespuesta + " | Ticket: " + rpta.NroTicket.ToString
                            dtv.garchivo_xml = rpta.NombreArchivo & ".xml"
                            dtv.garchivo_cdr = "R-" & rpta.NombreArchivo & ".zip"
                            dtv.garchivo_pdf = oContribuyente.NroDocumento & "-" + DateTime.Parse(row.Cells("fecha_venta").Value.ToString()).ToString("yyyy-MM-dd") & "-" + row.Cells("dni").Value.ToString() & ".pdf"

                            If dtv IsNot Nothing AndAlso dtv.gnumero_comprobante <> "" Then
                                dtv.gestado_envio_sunat = 0
                                funcVen.editarDatosEnvioDocumentoSunat(dtv)
                                'AdmCVenta.UpdateAcumulado(CVentas1)
                            End If
                        Next

                    End If

                Catch ex As Exception
                    txtDetailRes.Text = ex.Message
                End Try

            End If

            If Not respuestaEnvio.Exito Then Throw New ApplicationException(respuestaEnvio.MensajeError)
            MessageBox.Show("Se ha enviado correctamente el archivo de resumen..!")
        Else
            MessageBox.Show("No se han encontrado boletas para añadir al resumen..!")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnEnviarResumenDiario.Click

        Dim result As Integer = MessageBox.Show("Desea realmente enviar el resumen diario de boletas", "Envio de Resumen Diario", MessageBoxButtons.YesNoCancel)
        If result = DialogResult.Yes Then
            EnviarResumen()
        End If

    End Sub

    Private Function AccesoInternet() As Boolean
        Try
            Dim host As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry("www.google.com")
            Return True
        Catch es As Exception
            Return False
        End Try
    End Function

    Private Sub btnAnularEnvío_Click(sender As Object, e As EventArgs) Handles btnAnularEnvío.Click

        Dim dtv As New vventa

        Dim funcVen As New fventa

        If datalistado.RowCount > 0 Then

            If idVentaAnular <> "" Then

                Dim testDialog As New frmcRegistroAnularBoleta()
                testDialog.lblIdVentaAnular.Text = idVentaAnular
                testDialog.txtComprobanteAnular.Text = comprobanteAnular
                testDialog.ShowDialog()

                If (testDialog.DialogResult = Windows.Forms.DialogResult.Cancel) Then
                    btnconsultar_Click(sender, e)
                End If

            Else
                MessageBox.Show("Seleccione una boleta a anular..!")
            End If
        Else
            MessageBox.Show("No hay boletas para seleccionar..!")
        End If

    End Sub

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        Dim i, j As Integer
        i = datalistado.CurrentRow.Index
        idVentaAnular = datalistado.Item(1, i).Value
        comprobanteAnular = datalistado.Item(6, i).Value & "-" & datalistado.Item(7, i).Value
    End Sub
End Class