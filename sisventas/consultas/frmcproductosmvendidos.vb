﻿Imports System.Configuration
Imports System.Data.SqlClient




Public Class frmcproductosmvendidos
    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)

    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from producto", conexion)


    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable
    Private Sub frmcproductosmvendidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        mostrar_cantidadproductosvendidos()

        cbonombre_producto.Text = "TODOS"
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select nombre from producto order by nombre")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbonombre_producto.Items.Add(variable.Item(0))
        End While

        conexion.Close()



    End Sub

    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimir.Click

    End Sub
    Private Sub mostrar_productosmvendidos()
        Try
            Dim func As New fproductosmvendidos
            dt = func.mostrar_productosmvendidos()

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub


    Private Sub mostrar_cantidadproductosvendidos()
        Try
            Dim func As New fcantidadproductosvendidos
            dt = func.mostrar_cantidadproductosvendidos



            If dt.Rows.Count <> 0 Then
                datalistadomayor.DataSource = dt


                datalistadomayor.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistadomayor.DataSource = Nothing


                datalistadomayor.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub
    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click
        mostrar_productosmvendidos()

        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("Cantidad").Value IsNot Nothing) AndAlso
                    (row.Cells("Cantidad").Value IsNot DBNull.Value)) Select row.Cells("Cantidad").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
            txttotalcantidad.Text = String.Format("Cantidad Total: {0:N2}", resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("Importe").Value IsNot Nothing) AndAlso
                    (row.Cells("Importe").Value IsNot DBNull.Value)) Select row.Cells("Importe").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
            txtimportetotal.Text = String.Format("Importe Total: {0:N2}", resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Sub cbonombre_producto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbonombre_producto.SelectedIndexChanged

    End Sub

    Private Sub datalistadomayor_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistadomayor.CellContentClick

    End Sub
End Class