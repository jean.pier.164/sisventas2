﻿Public Class frmstockminimo
    Private dt As New DataTable
    Private Sub frmstockminimo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar_stockminimo()
    End Sub



    Private Sub mostrar_stockminimo()
        Try
            Dim func As New fstock_minimo
            dt = func.mostrar_stockminimo()



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True

            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimir.Click
        frmreporte_stock_minimo.ShowDialog()
    End Sub
End Class