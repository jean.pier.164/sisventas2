﻿Imports System.Data.SqlClient
Public Class fComprobanteSerie
    Inherits conexion

    Dim cmd As New SqlCommand

    Public Function editar(ByVal dts As vComprobanteSerie) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_comprobante_serie")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@cod_sunat", dts.gcodigoSunat)
            cmd.Parameters.AddWithValue("@numeracion", dts.gnumeracion)
            cmd.Parameters.AddWithValue("@nrocaja", dts.gnrocaja)

            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function obtenerNumeracion(ByVal dts As vComprobanteSerie) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("obtener_numeracion_comprobante")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.Add("@cod_sunat", SqlDbType.NVarChar, 10).Value = dts.gcodigoSunat
            cmd.Parameters.Add("@nrocaja", SqlDbType.NVarChar, 10).Value = dts.gnrocaja
            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function obtenerNumeracionNotas(ByVal dts As vComprobanteSerie) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("obtener_numeracion_comprobante_notas")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.Add("@cod_sunat", SqlDbType.NVarChar, 10).Value = dts.gcodigoSunat
            cmd.Parameters.Add("@nrocaja", SqlDbType.NVarChar, 10).Value = dts.gnrocaja
            cmd.Parameters.Add("@tipocp", SqlDbType.NVarChar, 10).Value = dts.gcodTipoComprobante
            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function actualizarNumeracionNotas(ByVal dts As vComprobanteSerie) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("actualizar_numeracion_comprobante_notas")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.Add("@cod_sunat", SqlDbType.NVarChar, 10).Value = dts.gcodigoSunat
            cmd.Parameters.Add("@nrocaja", SqlDbType.NVarChar, 10).Value = dts.gnrocaja
            cmd.Parameters.Add("@tipocp", SqlDbType.NVarChar, 10).Value = dts.gcodTipoComprobante
            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function insertarResumenDiario(ByVal dts As vresumenDiario) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_resumen_diario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@correlativo", dts.gcorrelativo)
            cmd.Parameters.AddWithValue("@cod_estado_envio_sunat", dts.gcodigoEstadoEnvioSunat)
            cmd.Parameters.AddWithValue("@mensaje_envio_sunat", dts.gmensajeEnvioSunat)
            cmd.Parameters.AddWithValue("@nombre_archivo_zip", dts.gnombreArchivoZip)
            cmd.Parameters.AddWithValue("@nombre_archivo", dts.gnombreArchivo)
            cmd.Parameters.AddWithValue("@nombre_documento", dts.gnombreDocumento)
            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function insertarComunicacionBaja(ByVal dts As vComunicacionBaja) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_comunicacion_baja")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@correlativo", dts.gcorrelativo)
            cmd.Parameters.AddWithValue("@cod_estado_envio_sunat", dts.gcodigoEstadoEnvioSunat)
            cmd.Parameters.AddWithValue("@mensaje_envio_sunat", dts.gmensajeEnvioSunat)
            cmd.Parameters.AddWithValue("@nombre_archivo_zip", dts.gnombreArchivoZip)
            cmd.Parameters.AddWithValue("@nombre_archivo", dts.gnombreArchivo)
            cmd.Parameters.AddWithValue("@nombre_documento", dts.gnombreDocumento)
            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

End Class
