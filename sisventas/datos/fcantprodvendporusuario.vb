﻿Imports System.Data.SqlClient
Public Class fcantprodvendporusuario
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar_cantprodvendporusuario() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("cantproductosvendidosporusuariom")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@log", frmcCantidadProductosporUsuario.cbologin.Text)
            cmd.Parameters.AddWithValue("@fechai", frmcCantidadProductosporUsuario.txtfechai.Value)
            cmd.Parameters.AddWithValue("@fechafi", frmcCantidadProductosporUsuario.txtfechafi.Value)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function

End Class
