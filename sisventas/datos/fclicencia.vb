﻿Imports System.Data.SqlClient

Public Class fclicencia

    Inherits conexion
    Dim cmd As New SqlCommand
    Public adaptador As SqlDataAdapter
    Dim acmd As New SqlCommand

    Public Function consultarLicencia() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("sp_licencia_expiracion")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

End Class
