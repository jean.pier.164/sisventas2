﻿Imports System.Data.SqlClient
Public Class fcventas
    Inherits conexion
    Dim cmd As New SqlCommand
    Public adaptador As SqlDataAdapter
    Dim acmd As New SqlCommand

    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("cventas")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function mostrarBoletasEnvioResumenDiario(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("cventas_listado_para_resumen_diario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@fechaInicio", fechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechaFin)

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function mostrarResumenesDiarios(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("cventas_listado_resumen_diario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@fechaInicio", fechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechaFin)

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function mostrarListadoComprobantesSunat(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("listado_comprobantes_sunat")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@fechaInicio", fechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechaFin)

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function mostrarListadoComprobantesSunatVentasCliente(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime, ByVal idcliente As Integer) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("listado_comprobantes_sunat_ventas_cliente")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@fechaInicio", fechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechaFin)
            cmd.Parameters.AddWithValue("@idcliente", idcliente)

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function mostrarListadoComprobantesSunatPendientes(ByVal fechaInicio As DateTime, ByVal fechaFin As DateTime) As DataTable
        Try
            conectado()
            cmd = New SqlCommand("listado_comprobantes_sunat_pendientes")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@fechaInicio", fechaInicio)
            cmd.Parameters.AddWithValue("@fechaFin", fechaFin)

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

End Class
