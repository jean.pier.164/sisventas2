﻿Imports System.Data.SqlClient
Public Class flistaprecios
    Inherits conexion
    Dim cmd As New SqlCommand
    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_listaprecios")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function
    Public Function mostrarm() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mlistapreciosp")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function
    Public Function mlistapreciosp() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mlistaprecios")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idproducto", frmdetalle_venta.txtidproducto.Text)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function



    Public Function insertar(ByVal dts As vlistaprecios) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_listaprecios")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idproducto", dts.gidproducto)
            cmd.Parameters.AddWithValue("@undm", dts.gundm)
            cmd.Parameters.AddWithValue("@cant", dts.gcant)
            cmd.Parameters.AddWithValue("@pventa", dts.gpventa)
            cmd.Parameters.AddWithValue("@sundm", dts.gsundm)
            cmd.Parameters.AddWithValue("@pcompra", dts.gpcompra)


            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function
    Public Function editar(ByVal dts As vlistaprecios) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_listaprecios")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idprecios", dts.gidprecios)
            cmd.Parameters.AddWithValue("@idproducto", dts.gidproducto)
            cmd.Parameters.AddWithValue("@undm", dts.gundm)
            cmd.Parameters.AddWithValue("@cant", dts.gcant)
            cmd.Parameters.AddWithValue("@pventa", dts.gpventa)
            cmd.Parameters.AddWithValue("@sundm", dts.gsundm)
            cmd.Parameters.AddWithValue("@pcompra", dts.gpcompra)



            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function eliminar(ByVal dts As vlistaprecios) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("eliminar_listaprecios")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.Add("@idprecios", SqlDbType.NVarChar, 50).Value = dts.gidprecios

            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        End Try
    End Function
End Class
