﻿Public Class vComprobanteSerie
    Dim idComprobanteSerie As Integer
    Dim codigoSunat, descripcion, serie As String
    Dim numeracion As Integer
    Dim nrocaja As String
    Dim codTipoComprobante As String


    'seeter y getter

    Public Property gid
        Get
            Return idComprobanteSerie

        End Get
        Set(ByVal value)


            idComprobanteSerie = value

        End Set
    End Property

    Public Property gcodigoSunat

        Get
            Return codigoSunat


        End Get
        Set(ByVal value)
            codigoSunat = value
        End Set
    End Property


    Public Property gdescripcion
        Get
            Return descripcion
        End Get
        Set(ByVal value)
            descripcion = value
        End Set
    End Property

    Public Property gserie
        Get
            Return serie
        End Get
        Set(ByVal value)
            serie = value
        End Set
    End Property

    Public Property gnumeracion
        Get
            Return numeracion
        End Get
        Set(ByVal value)
            numeracion = value
        End Set
    End Property

    Public Property gnrocaja

        Get
            Return nrocaja


        End Get
        Set(ByVal value)
            nrocaja = value
        End Set
    End Property

    Public Property gcodTipoComprobante

        Get
            Return codTipoComprobante


        End Get
        Set(ByVal value)
            codTipoComprobante = value
        End Set
    End Property

    'constructores

    Public Sub New()

    End Sub

    Public Sub New(ByVal idTipo As Integer, ByVal nombres As String, ByVal codigoSunat As String, ByVal descripcion As String,
                   ByVal serie As String, ByVal numeracion As Integer, ByVal nrocaja As String, ByVal codTipoComprobante As String)
        gid = idTipo
        gcodigoSunat = codigoSunat
        gdescripcion = descripcion
        gserie = serie
        gnumeracion = numeracion
        gnrocaja = nrocaja
        gcodTipoComprobante = codTipoComprobante
    End Sub
End Class
