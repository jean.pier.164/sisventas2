﻿Imports System.Configuration
Imports System.Net.Mail

Public Class vSmtpServer

    Shared USEREMAIL As String = ConfigurationManager.AppSettings.Get("USEREMAIL")
    Shared PASSWORDEMAIL As String = ConfigurationManager.AppSettings.Get("PASSWORDEMAIL")
    Shared PORTEMAIL As String = ConfigurationManager.AppSettings.Get("PORTEMAIL")
    Shared SSLEMAIL As Boolean = ConfigurationManager.AppSettings.Get("SSLEMAIL")
    Shared HOSTEMAIL As String = ConfigurationManager.AppSettings.Get("HOSTEMAIL")

    Public Function obtenerSmtpCliente() As SmtpClient

        Dim SmtpServer As New SmtpClient()
        SmtpServer.Credentials = New Net.NetworkCredential(USEREMAIL, PASSWORDEMAIL)
        SmtpServer.Port = PORTEMAIL
        SmtpServer.Host = HOSTEMAIL
        SmtpServer.EnableSsl = SSLEMAIL

        Return SmtpServer

    End Function

End Class
