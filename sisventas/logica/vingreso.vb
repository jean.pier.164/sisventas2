﻿Public Class vingreso
    Dim idingreso, idusuario, idproveedor As Integer
    Dim fecha As Date
    Dim tipo_comprobante, serie, correlativo, estado As String
    Dim igv As Double


    Public Property gidingreso
        Get
            Return idingreso
        End Get
        Set(ByVal value)
            idingreso = value
        End Set
    End Property

    Public Property gidusuario
        Get
            Return idusuario
        End Get
        Set(ByVal value)
            idusuario = value
        End Set
    End Property
    Public Property gidproveedor
        Get
            Return idproveedor
        End Get
        Set(ByVal value)
            idproveedor = value
        End Set
    End Property

    Public Property gfecha
        Get
            Return fecha
        End Get
        Set(ByVal value)
            fecha = value
        End Set
    End Property


    Public Property gtipo_comprobante
        Get
            Return tipo_comprobante
        End Get
        Set(ByVal value)
            tipo_comprobante = value
        End Set
    End Property


    Public Property gserie
        Get
            Return serie
        End Get
        Set(ByVal value)
            serie = value
        End Set
    End Property

    Public Property gcorrelativo
        Get
            Return correlativo
        End Get
        Set(ByVal value)
            correlativo = value
        End Set
    End Property

    Public Property gestado
        Get
            Return estado
        End Get
        Set(ByVal value)
            estado = value
        End Set
    End Property

    Public Property gigv
        Get
            Return igv
        End Get
        Set(ByVal value)
            igv = value
        End Set
    End Property
    Public Sub New()

    End Sub

    Public Sub New(ByVal idingreso As Integer, ByVal idusuario As Integer, ByVal idproveedor As Integer, ByVal fecha As Date, ByVal tipo_comprobante As String, ByVal serie As String, ByVal correlativo As String, ByVal estado As String, ByVal igv As Double)
        gidingreso = idingreso
        gidusuario = idusuario
        gidproveedor = idproveedor
        gfecha = fecha
        gtipo_comprobante = tipo_comprobante
        gserie = serie
        gcorrelativo = correlativo
        gestado = estado
        gigv = igv

    End Sub
End Class
