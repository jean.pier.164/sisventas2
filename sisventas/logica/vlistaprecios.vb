﻿Public Class vlistaprecios
    Dim idprecios, idproducto As Integer
    Dim undm, sundm As String
    Dim cant, pventa, pcompra As Decimal


    Public Property gidprecios
        Get
            Return idprecios
        End Get
        Set(ByVal value)
            idprecios = value
        End Set
    End Property

    Public Property gundm
        Get
            Return undm
        End Get
        Set(ByVal value)
            undm = value
        End Set
    End Property


    Public Property gcant
        Get
            Return cant
        End Get
        Set(ByVal value)
            cant = value
        End Set
    End Property


    Public Property gpventa
        Get
            Return pventa
        End Get
        Set(ByVal value)
            pventa = value
        End Set
    End Property
    Public Property gidproducto
        Get
            Return idproducto
        End Get
        Set(ByVal value)
            idproducto = value
        End Set
    End Property

    Public Property gsundm
        Get
            Return sundm
        End Get
        Set(ByVal value)
            sundm = value
        End Set
    End Property

    Public Property gpcompra
        Get
            Return pcompra
        End Get
        Set(ByVal value)
            pcompra = value
        End Set
    End Property

    Public Sub New()

    End Sub


    Public Sub New(ByVal idprecios As Integer, ByVal undm As String, ByVal cant As Decimal, ByVal pventa As Decimal, ByVal idproducto As Integer, ByVal sundm As String, ByVal pcompra As Decimal)
        gidprecios = idprecios
        gundm = undm
        gcant = cant
        gpventa = pventa
        gidproducto = idproducto
        gsundm = sundm
        gpcompra = pcompra


    End Sub

End Class
