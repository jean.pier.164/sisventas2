﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmArchivosFE
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.erroricono = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnCDR = New System.Windows.Forms.Button()
        Me.btnXML = New System.Windows.Forms.Button()
        Me.btnBoletasPDF = New System.Windows.Forms.Button()
        Me.btnDocumentos = New System.Windows.Forms.Button()
        Me.btnFacturasPDF = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnCertificadoDigital = New System.Windows.Forms.Button()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'erroricono
        '
        Me.erroricono.ContainerControl = Me
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnCDR)
        Me.GroupBox2.Controls.Add(Me.btnXML)
        Me.GroupBox2.Controls.Add(Me.btnBoletasPDF)
        Me.GroupBox2.Controls.Add(Me.btnDocumentos)
        Me.GroupBox2.Controls.Add(Me.btnFacturasPDF)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(686, 89)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Archivos Generados FE"
        '
        'btnCDR
        '
        Me.btnCDR.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnCDR.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCDR.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnCDR.Location = New System.Drawing.Point(547, 33)
        Me.btnCDR.Name = "btnCDR"
        Me.btnCDR.Size = New System.Drawing.Size(87, 27)
        Me.btnCDR.TabIndex = 19
        Me.btnCDR.Text = "CDR"
        Me.btnCDR.UseVisualStyleBackColor = False
        '
        'btnXML
        '
        Me.btnXML.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnXML.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnXML.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnXML.Location = New System.Drawing.Point(293, 33)
        Me.btnXML.Name = "btnXML"
        Me.btnXML.Size = New System.Drawing.Size(87, 27)
        Me.btnXML.TabIndex = 18
        Me.btnXML.Text = "XML"
        Me.btnXML.UseVisualStyleBackColor = False
        '
        'btnBoletasPDF
        '
        Me.btnBoletasPDF.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnBoletasPDF.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBoletasPDF.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnBoletasPDF.Location = New System.Drawing.Point(168, 33)
        Me.btnBoletasPDF.Name = "btnBoletasPDF"
        Me.btnBoletasPDF.Size = New System.Drawing.Size(87, 27)
        Me.btnBoletasPDF.TabIndex = 17
        Me.btnBoletasPDF.Text = "Boletas PDF"
        Me.btnBoletasPDF.UseVisualStyleBackColor = False
        '
        'btnDocumentos
        '
        Me.btnDocumentos.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnDocumentos.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDocumentos.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnDocumentos.Location = New System.Drawing.Point(421, 33)
        Me.btnDocumentos.Name = "btnDocumentos"
        Me.btnDocumentos.Size = New System.Drawing.Size(87, 27)
        Me.btnDocumentos.TabIndex = 17
        Me.btnDocumentos.Text = "Documentos"
        Me.btnDocumentos.UseVisualStyleBackColor = False
        '
        'btnFacturasPDF
        '
        Me.btnFacturasPDF.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnFacturasPDF.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturasPDF.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnFacturasPDF.Location = New System.Drawing.Point(38, 33)
        Me.btnFacturasPDF.Name = "btnFacturasPDF"
        Me.btnFacturasPDF.Size = New System.Drawing.Size(87, 27)
        Me.btnFacturasPDF.TabIndex = 16
        Me.btnFacturasPDF.Text = "Facturas PDF"
        Me.btnFacturasPDF.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnCertificadoDigital)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 107)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(686, 89)
        Me.GroupBox1.TabIndex = 20
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Otros FE"
        '
        'btnCertificadoDigital
        '
        Me.btnCertificadoDigital.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnCertificadoDigital.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCertificadoDigital.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnCertificadoDigital.Location = New System.Drawing.Point(38, 33)
        Me.btnCertificadoDigital.Name = "btnCertificadoDigital"
        Me.btnCertificadoDigital.Size = New System.Drawing.Size(87, 27)
        Me.btnCertificadoDigital.TabIndex = 16
        Me.btnCertificadoDigital.Text = "Certificado Digital"
        Me.btnCertificadoDigital.UseVisualStyleBackColor = False
        '
        'frmArchivosFE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(718, 267)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmArchivosFE"
        Me.Text = "ACCESO"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents erroricono As System.Windows.Forms.ErrorProvider
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCDR As Button
    Friend WithEvents btnXML As Button
    Friend WithEvents btnBoletasPDF As Button
    Friend WithEvents btnDocumentos As Button
    Friend WithEvents btnFacturasPDF As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btnCertificadoDigital As Button
End Class
