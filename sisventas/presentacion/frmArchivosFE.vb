﻿Imports System.IO

Public Class frmArchivosFE
    Private dt As New DataTable

    Private Sub btnFacturasPDF_Click(sender As Object, e As EventArgs) Handles btnFacturasPDF.Click
        System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FACTURAS_PDF\\"))
    End Sub

    Private Sub btnBoletasPDF_Click(sender As Object, e As EventArgs) Handles btnBoletasPDF.Click
        System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BOLETAS_PDF\\"))
    End Sub

    Private Sub btnXML_Click(sender As Object, e As EventArgs) Handles btnXML.Click
        System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XML\\"))
    End Sub

    Private Sub btnDocumentos_Click(sender As Object, e As EventArgs) Handles btnDocumentos.Click
        System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Documentos\\"))
    End Sub

    Private Sub btnCDR_Click(sender As Object, e As EventArgs) Handles btnCDR.Click
        System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CDR\\"))
    End Sub

    Private Sub btnCertificadoDigital_Click(sender As Object, e As EventArgs) Handles btnCertificadoDigital.Click
        System.Diagnostics.Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Certificado\\"))
    End Sub
End Class