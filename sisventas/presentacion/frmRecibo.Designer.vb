﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRecibo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me.Marketdscomprobante = New BRAVOSPORT.marketdscomprobante()
        Me.MarketdscomprobanteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketDataSet1 = New BRAVOSPORT.marketDataSet1()
        Me.generar_comprobantemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.recibos = New BRAVOSPORT.recibos()
        Me.generar_comprobantemTableAdapter = New BRAVOSPORT.recibosTableAdapters.generar_comprobantemTableAdapter()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        CType(Me.Marketdscomprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketdscomprobanteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidventa
        '
        Me.txtidventa.Location = New System.Drawing.Point(153, 55)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(100, 20)
        Me.txtidventa.TabIndex = 3
        Me.txtidventa.Visible = False
        '
        'Marketdscomprobante
        '
        Me.Marketdscomprobante.DataSetName = "marketdscomprobante"
        Me.Marketdscomprobante.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketdscomprobanteBindingSource
        '
        Me.MarketdscomprobanteBindingSource.DataSource = Me.Marketdscomprobante
        Me.MarketdscomprobanteBindingSource.Position = 0
        '
        'marketDataSet1
        '
        Me.marketDataSet1.DataSetName = "marketDataSet1"
        Me.marketDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'generar_comprobantemBindingSource
        '
        Me.generar_comprobantemBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobantemBindingSource.DataSource = Me.recibos
        '
        'recibos
        '
        Me.recibos.DataSetName = "recibos"
        Me.recibos.EnforceConstraints = False
        Me.recibos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'generar_comprobantemTableAdapter
        '
        Me.generar_comprobantemTableAdapter.ClearBeforeFill = True
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(344, 535)
        Me.ReportViewer1.TabIndex = 4
        '
        'frmRecibo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(344, 535)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Controls.Add(Me.txtidventa)
        Me.Name = "frmRecibo"
        Me.Text = "frmRecibo"
        CType(Me.Marketdscomprobante, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketdscomprobanteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtidventa As TextBox
    Friend WithEvents generar_comprobantemBindingSource As BindingSource
    Friend WithEvents recibos As recibos
    Friend WithEvents generar_comprobantemTableAdapter As recibosTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents MarketdscomprobanteBindingSource As BindingSource
    Friend WithEvents Marketdscomprobante As marketdscomprobante
    Friend WithEvents marketDataSet1 As marketDataSet1
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
End Class
