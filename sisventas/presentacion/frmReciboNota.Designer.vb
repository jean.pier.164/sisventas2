﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmReciboNota
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me.generar_comprobantemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.recibos = New BRAVOSPORT.recibos()
        Me.generar_comprobantemTableAdapter = New BRAVOSPORT.recibosTableAdapters.generar_comprobantemTableAdapter()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.RecibosNota1 = New BRAVOSPORT.recibosNota()
        Me.generar_nota_credito_BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Generar_comprobantemNotaCreditoTableAdapter1 = New BRAVOSPORT.recibosNotaTableAdapters.generar_comprobantemNotaCreditoTableAdapter()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RecibosNota1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.generar_nota_credito_BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtidventa
        '
        Me.txtidventa.Location = New System.Drawing.Point(153, 55)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(100, 20)
        Me.txtidventa.TabIndex = 3
        Me.txtidventa.Visible = False
        '
        'generar_comprobantemBindingSource
        '
        Me.generar_comprobantemBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobantemBindingSource.DataSource = Me.recibos
        '
        'recibos
        '
        Me.recibos.DataSetName = "recibos"
        Me.recibos.EnforceConstraints = False
        Me.recibos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'generar_comprobantemTableAdapter
        '
        Me.generar_comprobantemTableAdapter.ClearBeforeFill = True
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(344, 535)
        Me.ReportViewer1.TabIndex = 4
        '
        'RecibosNota1
        '
        Me.RecibosNota1.DataSetName = "recibosNota"
        Me.RecibosNota1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'generar_nota_credito_BindingSource
        '
        Me.generar_nota_credito_BindingSource.DataMember = "generar_comprobantemNotaCredito"
        Me.generar_nota_credito_BindingSource.DataSource = Me.RecibosNota1
        '
        'Generar_comprobantemNotaCreditoTableAdapter1
        '
        Me.Generar_comprobantemNotaCreditoTableAdapter1.ClearBeforeFill = True
        '
        'frmReciboNota
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(344, 535)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Controls.Add(Me.txtidventa)
        Me.Name = "frmReciboNota"
        Me.Text = "frmRecibo"
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RecibosNota1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.generar_nota_credito_BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtidventa As TextBox
    Friend WithEvents generar_comprobantemBindingSource As BindingSource
    Friend WithEvents recibos As recibos
    Friend WithEvents generar_comprobantemTableAdapter As recibosTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents RecibosNota1 As recibosNota
    Friend WithEvents Generar_comprobantemNotaCreditoTableAdapter As recibosNotaTableAdapters.generar_comprobantemNotaCreditoTableAdapter
    Friend WithEvents TableAdapterManager As recibosNotaTableAdapters.TableAdapterManager
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents generar_nota_credito_BindingSource As BindingSource
    Friend WithEvents Generar_comprobantemNotaCreditoTableAdapter1 As recibosNotaTableAdapters.generar_comprobantemNotaCreditoTableAdapter
End Class
