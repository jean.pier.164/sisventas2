﻿Imports System.Drawing.Imaging
Imports System.IO
Imports FinalXML
Imports Gma.QrCodeNet.Encoding
Imports Gma.QrCodeNet.Encoding.Windows.Render
Imports Microsoft.Reporting.WinForms

Public Class frmReciboNota

    Public _documento As DocumentoElectronico
    Public Shared _rutaPdf As String

    Private Sub frmRecibo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try

            'TODO: esta línea de código carga datos en la tabla 'recibos.generar_comprobantem' Puede moverla o quitarla según sea necesario.
            'Me.generar_comprobantemTableAdapter.Fill(Me.recibos.generar_comprobantem, idventa:=txtidventa.Text)

            Dim dtv As New vventa
            Dim funcVen As New fventa
            Dim dtventa As New DataTable

            dtv.gidventa = txtidventa.Text
            'dtventa = funcVen.verificarVenta(dtv)

            Dim parametro As List(Of ReportParameter) = New List(Of ReportParameter)

            'If dtventa.Rows.Count > 0 Then

            Dim herramientas As Herramientas = New Herramientas()
            Dim recursos = herramientas.GetResourcesPath2()
            Dim nomdocumento = _documento.Emisor.NroDocumento & "-" + DateTime.Parse(_documento.FechaEmision).ToString("yyyy-MM-dd") & "-" +
                    _documento.IdDocumento

            Dim datosAdicionales_CDB As String = ""

            Dim CodigoCertificado As String = ""
            datosAdicionales_CDB = _documento.Emisor.NroDocumento & "|" & _documento.TipoDocumento & "|" & _documento.IdDocumento & "|" &
                    _documento.TotalIgv & "|" & _documento.TotalVenta & "|" & _documento.FechaEmision & "|" &
                    _documento.Receptor.TipoDocumento & "|" & _documento.Receptor.NroDocumento
            CodigoCertificado = datosAdicionales_CDB & "|" & _documento.FirmaDigital

            Dim qrEncoder = New QrEncoder(ErrorCorrectionLevel.H)

            Dim qrCode = qrEncoder.Encode(datosAdicionales_CDB)

            Dim renderer = New GraphicsRenderer(New FixedModuleSize(5, QuietZoneModules.Two), Brushes.Black, Brushes.White)

            Using stream = New FileStream(recursos & "\" + nomdocumento & ".jpeg", FileMode.Create)
                renderer.WriteToStream(qrCode.Matrix, ImageFormat.Jpeg, stream)
            End Using

            parametro.Add(New ReportParameter("pLogo", "file:///" & recursos & "/" + nomdocumento & ".jpeg"))
            parametro.Add(New ReportParameter("pOpGravadas", _documento.Gravadas))
            parametro.Add(New ReportParameter("pOpExoneradas", _documento.Exoneradas))
            parametro.Add(New ReportParameter("pOpInafectas", _documento.Inafectas))
            parametro.Add(New ReportParameter("pOpGratuitas", _documento.Gratuitas))
            parametro.Add(New ReportParameter("pIgv", _documento.TotalIgv))
            parametro.Add(New ReportParameter("pDescuento", _documento.DescuentoGlobal))
            parametro.Add(New ReportParameter("pTotal", _documento.TotalVenta))
            parametro.Add(New ReportParameter("pMontoLetras", _documento.MontoEnLetras))
            parametro.Add(New ReportParameter("pDireccion", _documento.Receptor.Direccion))

            Dim tipNota As String = ""
            If _documento.TipoDocumento.Equals("07") Then
                tipNota = "NC"
                parametro.Add(New ReportParameter("pTipoNota", "NOTA DE CRÉDITO ELECTRÓNICA"))
            End If
            If _documento.TipoDocumento.Equals("08") Then
                tipNota = "ND"
                parametro.Add(New ReportParameter("pTipoNota", "NOTA DE DÉBITO ELECTRÓNICA"))
            End If

            'End If

            Me.ReportViewer1.ProcessingMode = ProcessingMode.Local
            ReportViewer1.LocalReport.EnableExternalImages = True

            ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)
            ReportViewer1.ZoomMode = ZoomMode.Percent
            ReportViewer1.ZoomPercent = 100
            ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.recibosNota.rdlc"
            ReportViewer1.LocalReport.SetParameters(parametro)

            generar_nota_credito_BindingSource.RemoveFilter()
            Me.Generar_comprobantemNotaCreditoTableAdapter1.Fill(Me.RecibosNota1.generar_comprobantemNotaCredito, idnotacredito:=txtidventa.Text, tiponota:=tipNota)


            Me.ReportViewer1.LocalReport.DataSources.Clear()
            Dim ReportDataSource As ReportDataSource = New ReportDataSource()
            ReportDataSource.Value = generar_nota_credito_BindingSource
            ReportDataSource.Name = "recibos"
            ReportViewer1.LocalReport.DataSources.Add(ReportDataSource)

            Me.ReportViewer1.RefreshReport()

            Dim warnings As Warning()
            Dim streamids As String()
            Dim mimeType As String
            Dim encoding As String
            Dim filenameExtension As String
            Dim bytes As Byte() = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, encoding, filenameExtension, streamids, warnings)

            Dim rutaFilePDF As String

            If _documento.TipoDocumento.Equals("01") Then
                Using fs As FileStream = New FileStream("FACTURAS_PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                    fs.Write(bytes, 0, bytes.Length)
                End Using

                rutaFilePDF = "FACTURAS_PDF\\" & nomdocumento & ".pdf"
            Else
                If _documento.TipoDocumento.Equals("03") Then
                    Using fs As FileStream = New FileStream("BOLETAS_PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                        fs.Write(bytes, 0, bytes.Length)
                    End Using

                    rutaFilePDF = "BOLETAS_PDF\\" & nomdocumento & ".pdf"
                Else
                    If _documento.TipoDocumento.Equals("07") Then

                        Using fs As FileStream = New FileStream("NOTA_CREDITO_PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                            fs.Write(bytes, 0, bytes.Length)
                        End Using

                        rutaFilePDF = "NOTA_CREDITO_PDF\\" & nomdocumento & ".pdf"
                    Else
                        If _documento.TipoDocumento.Equals("08") Then

                            Using fs As FileStream = New FileStream("NOTA_DEBITO_PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                                fs.Write(bytes, 0, bytes.Length)
                            End Using

                            rutaFilePDF = "NOTA_DEBITO_PDF\\" & nomdocumento & ".pdf"

                        Else
                            Using fs As FileStream = New FileStream("PDF\\" & nomdocumento & ".pdf", FileMode.Create)
                                fs.Write(bytes, 0, bytes.Length)
                            End Using

                            rutaFilePDF = "PDF\\" & nomdocumento & ".pdf"
                        End If
                    End If
                End If
            End If

            _rutaPdf = rutaFilePDF

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error al mostrar recibo", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.ReportViewer1.RefreshReport()
        End Try

    End Sub

End Class