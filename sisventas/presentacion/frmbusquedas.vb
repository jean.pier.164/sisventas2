﻿Imports System.Configuration
Imports System.Data.SqlClient



Public Class frmbusquedas
    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Private dt As New DataTable
    Dim consulta3 As New SqlCommand
    Dim cadena As String
    Dim datos As New DataSet
    Dim variable As SqlDataReader

    Private Sub frmbusquedas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()
        cboproduct.Focus()
        cboproduct.DroppedDown = True
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select nombre from producto order by nombre")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cboproduct.Items.Add(variable.Item(0))
        End While
        conexion.Close()

    End Sub


    Private Sub mostrarn()
        Try
            Dim func As New fproducto
            dt = func.mostrar

            datalistado1.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado1.DataSource = dt
                txtbuscar.Enabled = True

                datalistado1.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado1.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado1.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        'btnnuevo.Visible = True
        'btneditar.Visible = False

        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado1.DataSource = dv
                'ocultar_columnas()
                txtidproducto.Text = datalistado1.SelectedCells.Item(1).Value
                txtnombre_producto.Text = datalistado1.SelectedCells.Item(3).Value
                txtstock.Text = datalistado1.SelectedCells.Item(5).Value
                txtprecio_unitario.Text = datalistado1.SelectedCells.Item(7).Value

            Else
                inexistente.Visible = True
                datalistado1.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fdetalle_venta
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        'btnnuevo.Visible = True


        buscar()

    End Sub



    Private Sub txtbuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbuscar.TextChanged
        buscar()
    End Sub


    Private Sub cbocampo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbocampo.SelectedIndexChanged

    End Sub

    Private Sub cboproduct_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboproduct.SelectedIndexChanged
        mostrar_nproductos()
        txtnombre_producto.Focus()



    End Sub
    Private Sub mostrar_nproductos()

        Dim func As New fnproductos
        dt = func.mostrarnproducto1

        ''datalistado.Columns.Item("Eliminar").Visible = False

        ''If dt.Rows.Count <> 0 Then
        ''    datalistado.DataSource = dt


        ''    datalistado.ColumnHeadersVisible = True
        ''    inexistente.Visible = False
        ''Else
        ''    datalistado.DataSource = Nothing


        ''    datalistado.ColumnHeadersVisible = False
        ''    inexistente.Visible = True
        ''End If

    End Sub

    Private Sub txtnombre_producto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtnombre_producto.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnguardar.PerformClick()
        End If

    End Sub

    Private Sub txtnombre_producto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtnombre_producto.TextChanged

    End Sub

    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If txtidventa.Text = "" Then
            btnguardarb.PerformClick()
        End If
        If Me.ValidateChildren = True And txtidproducto.Text <> "" And txtcantidad.Text <> "" And txtprecio_unitario.Text <> "" Then
            Try
                Dim dts As New vdetalle_venta
                Dim func As New fdetalle_venta

                dts.gidventa = txtidventa.Text
                dts.gidproducto = txtidproducto.Text
                dts.gcantidad = txtcantidad.Text
                dts.gprecio_unitario = txtprecio_unitario.Text
                If func.insertar(dts) Then
                    If func.disminuir_stock(dts) Then

                    End If
                    'MessageBox.Show("Articulo fue añadido Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()


                Else
                    MessageBox.Show("Articulo  no fue añadido Correctamente, intente de nuevos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()


                End If



            Catch ex As Exception
                'MessageBox.Show("Genera Comprobante para Ingresar productos", "Presiona Guardar", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("ImporteVenta").Value IsNot Nothing) AndAlso
                    (row.Cells("ImporteVenta").Value IsNot DBNull.Value)) Select row.Cells("ImporteVenta").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
            'lbltotal.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub btnguardarb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardarb.Click

    End Sub
End Class