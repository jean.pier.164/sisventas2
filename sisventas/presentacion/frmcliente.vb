﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Xml
Imports Newtonsoft.Json.Linq

Public Class frmcliente

    Private dt As New DataTable


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub frmcliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()
        txtnombre.Text = ""
        txtapellidos.Text = "SN"
        txtdireccion.Text = "SN"
        txttelefono.Text = "SN"
        txtdni.Text = ""
        txtidcliente.Text = "SN"

        Dim comboSource As New Dictionary(Of String, String)()
        comboSource.Add("1", "DNI")
        comboSource.Add("6", "RUC")
        comboSource.Add("4", "CARNET DE EXTRANJERIA")
        comboSource.Add("7", "PASAPORTE")

        cmbTipoDocCliente.DataSource = New BindingSource(comboSource, Nothing)
        cmbTipoDocCliente.DisplayMember = "Value"
        cmbTipoDocCliente.ValueMember = "Key"

    End Sub
    Public Sub limpiar()


        txtnombre.Text = ""
        txtapellidos.Text = ""
        txtdireccion.Text = ""
        txttelefono.Text = ""
        txtdni.Text = ""
        txtidcliente.Text = ""

    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fcliente
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub ocultar_columnas()
        datalistado.Columns(1).Visible = False

    End Sub



    Private Sub txtnombre_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Me.erroricono.SetError(sender, "")
        Else
            Me.erroricono.SetError(sender, "Ingrese el nombre del cliente porfavor, este dato es obligatorio")
        End If
    End Sub

    Private Sub txtapellidos_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtapellidos.TextChanged

    End Sub

    Private Sub txtapellidos_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtapellidos.Validating
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Me.erroricono.SetError(sender, "")
        Else
            Me.erroricono.SetError(sender, "Ingrese los apellidos  del cliente porfavor, este dato es obligatorio")
        End If
    End Sub

    Private Sub txtdireccion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtdireccion.TextChanged

    End Sub

    Private Sub txtdireccion_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtdireccion.Validating
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Me.erroricono.SetError(sender, "")
        Else
            Me.erroricono.SetError(sender, "Ingrese la direccion del cliente porfavor, este dato es obligatorio")
        End If
    End Sub

    Private Sub txtdni_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtdni.TextChanged


    End Sub

    Private Sub txtdni_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtdni.Validating
        If DirectCast(sender, TextBox).Text.Length > 0 Then
            Me.erroricono.SetError(sender, "")
        Else
            Me.erroricono.SetError(sender, "Ingrese el dni  del cliente porfavor, este dato es obligatorio")
        End If
    End Sub

    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnuevo.Click
        limpiar()
        mostrar()


    End Sub

    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click

        Try
            Dim dts As New vcliente
            Dim func As New fcliente
            Dim dtd As New vclienteTipoDocumento

            Dim dtCliente As DataTable

            dts.gnombres = txtnombre.Text.Trim()
            dts.gapellidos = txtapellidos.Text.Trim()
            dts.gdireccion = txtdireccion.Text.Trim()
            dts.gtelefono = txttelefono.Text.Trim()
            dts.gdni = txtdni.Text.Trim()
            dts.gEmail = txtEmail.Text.Trim()

            dtd.gcodigoSunat = cmbTipoDocCliente.SelectedValue
            dts.gtipoDocumento = dtd

            dtCliente = func.verificarClientePorNumDoc(dts)

            If dtCliente.Rows.Count > 0 Then
                MessageBox.Show("Ya existe un cliente con ese numero de documento", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            If txtnombre.Text.Equals("") Then
                MessageBox.Show("Ingrese el nombre del cliente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            If txtdni.Text.Equals("") Then
                MessageBox.Show("Ingrese el número documento identidad", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            If func.insertar(dts) Then

                mostrar()
                limpiar()
            Else
                MessageBox.Show("Cliente no fue registrado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                mostrar()
                limpiar()

            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub datalistado_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidcliente.Text = datalistado.SelectedCells.Item(1).Value
        txtnombre.Text = datalistado.SelectedCells.Item(2).Value
        txtapellidos.Text = datalistado.SelectedCells.Item(3).Value
        txtdireccion.Text = datalistado.SelectedCells.Item(4).Value
        txttelefono.Text = datalistado.SelectedCells.Item(5).Value
        txtdni.Text = datalistado.SelectedCells.Item(6).Value

        If Not IsDBNull(datalistado.SelectedCells.Item(9).Value) Then
            txtEmail.Text = datalistado.SelectedCells.Item(9).Value
        End If

        If Not IsDBNull(datalistado.SelectedCells.Item(8).Value) Then
            cmbTipoDocCliente.SelectedValue = datalistado.SelectedCells.Item(8).Value
        End If

    End Sub

    Private Sub datalistado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellContentClick
        If e.ColumnIndex = Me.datalistado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.datalistado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value

        End If
    End Sub

    Private Sub btneditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneditar.Click

        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos del cliente ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtnombre.Text <> "" And txtapellidos.Text <> "" And txtdireccion.Text <> "" And txttelefono.Text <> "" And txtdni.Text <> "" And txtidcliente.Text <> "" Then
            Try
                Dim dts As New vcliente
                Dim func As New fcliente
                Dim dtd As New vclienteTipoDocumento

                dts.gidcliente = txtidcliente.Text
                dts.gnombres = txtnombre.Text.Trim
                dts.gapellidos = txtapellidos.Text
                dts.gdireccion = txtdireccion.Text.Trim
                dts.gtelefono = txttelefono.Text
                dts.gdni = txtdni.Text.Trim
                dts.gEmail = txtEmail.Text.Trim

                dtd.gcodigoSunat = cmbTipoDocCliente.SelectedValue
                dts.gtipoDocumento = dtd

                If func.editar(dts) Then
                    MessageBox.Show("Cliente Modificado Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else
                    MessageBox.Show("Cliente no fue Modificado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub cbeliminar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbeliminar.CheckedChanged
        If cbeliminar.CheckState = CheckState.Checked Then
            datalistado.Columns.Item("Eliminar").Visible = True
        Else
            datalistado.Columns.Item("Eliminar").Visible = False
        End If
    End Sub

    Private Sub btneliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneliminar.Click
        Dim result As DialogResult
        result = MessageBox.Show("Realmente desea eliminar los clientes seleccionados?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)


        If result = DialogResult.OK Then
            Try
                For Each row As DataGridViewRow In datalistado.Rows
                    Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                    If marcado Then
                        Dim onekey As Integer = Convert.ToInt32(row.Cells("idcliente").Value)
                        Dim vdb As New vcliente
                        Dim func As New fcliente

                        vdb.gidcliente = onekey
                        If func.eliminar(vdb) Then

                        Else
                            MessageBox.Show("Cliente no fue eliminados?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                    End If
                Next

                Call mostrar()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call mostrar()
        End If
        Call limpiar()
    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub datalistado_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        If txtflag.Text = "1" Then
            frmdetalle_venta.txtidcliente.Text = datalistado.SelectedCells.Item(1).Value
            frmdetalle_venta.txtnombre_cliente.Text = datalistado.SelectedCells.Item(2).Value

            If Not IsDBNull(datalistado.SelectedCells.Item(9).Value) Then
                frmdetalle_venta.txtEmail.Text = datalistado.SelectedCells.Item(9).Value
            End If

            Me.Close()

        End If
    End Sub

    Private Sub txtbuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbuscar.TextChanged
        buscar()

    End Sub




    Private Sub txtdni_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtdni.KeyPress
        If Convert.ToInt32(e.KeyChar) = Convert.ToInt32(Keys.Enter) Then
            Dim idDocCliente As String

            idDocCliente = cmbTipoDocCliente.SelectedValue

            If idDocCliente.Equals("6") Then

                Try
                    Dim valida As Boolean = True

                    If txtdni.Text.Length <> 11 Then
                        valida = False
                        MessageBox.Show("El numero de ruc debe contener 11 digitos")
                        Exit Sub
                    End If

                    If valida Then
                        Dim request As WebRequest = WebRequest.Create(String.Format("http://intranet.camaratru.org.pe/SunatPHP-master/ruc.php?ruc={0}", txtdni.Text))
                        request.Method = "POST"
                        Dim response As WebResponse = request.GetResponse()
                        Dim sr As StreamReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("ISO-8859-1"))
                        Dim sunathtml As String = sr.ReadToEnd()
                        sunathtml = sunathtml.Trim()

                        Dim jResults As JObject = JObject.Parse(sunathtml)
                        Dim results As List(Of JToken) = jResults.Children().ToList()

                        For Each item As JProperty In results
                            item.CreateReader()

                            If item.Name.Equals("RazonSocial") Then
                                txtnombre.Text = item.Value()
                            End If

                            If item.Name.Equals("Direccion") Then
                                txtdireccion.Text = item.Value()
                            End If

                        Next

                    Else
                        txtnombre.Clear()
                        txtdireccion.Clear()
                    End If

                Catch ex As Exception
                    txtnombre.Clear()
                    txtdireccion.Clear()
                    MessageBox.Show("Error en la petición: " & ex.Message.ToString())
                End Try

            End If
        End If
    End Sub
End Class