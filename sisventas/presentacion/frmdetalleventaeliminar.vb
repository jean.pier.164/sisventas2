﻿Imports System.Data.SqlClient
Public Class frmdetalleventaeliminar
    Private dt As New DataTable

    Private Sub frmdetalleventaeliminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        mostrar()



        Try
            Dim query As IEnumerable(Of Object) =
                    From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                    Where (
                        (row.Cells("subtotal").Value IsNot Nothing) AndAlso
                        (row.Cells("subtotal").Value IsNot DBNull.Value)) Select row.Cells("subtotal").Value()

            Dim resultado As Decimal =
                    query.Sum(Function(row) Convert.ToDecimal(row))
            lbltotal.Text = String.Format(resultado)



            'frmproducto.btneditar.PerformClick()

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try
    End Sub

    Private Sub mostrar()
        Try
            Dim func As New fdetalle_venta
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

        buscar()

    End Sub

    Private Sub buscar()
        Try
            Dim ds As New DataSet
            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = "idventa=  '" & txtidventa.Text & "'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()
            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ocultar_columnas()
        datalistado.Columns(1).Visible = False
        datalistado.Columns(2).Visible = False
        datalistado.Columns(3).Visible = False


    End Sub

    Private Sub btnquitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnquitar.Click
        Dim result As DialogResult
        result = MessageBox.Show("Realmente desea quitar los articulos de venta?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then
            Try
                For Each row As DataGridViewRow In datalistado.Rows

                    Dim marcado As Boolean = Convert.ToBoolean(row.Cells(0).Value)

                    ''Dim int As Integer = Convert.ToInt32(datalistado.CurrentRow.Cells("iddetalle_venta").Value)

                    If marcado Then
                        'Dim onekey As Integer = Convert.ToInt32(row.Cells("iddetalle_venta").Value)
                        Dim onekey As Integer = Convert.ToInt32(datalistado.CurrentRow.Cells("iddetalle_venta").Value)
                        Dim vdb As New vdetalle_venta
                        Dim func As New fdetalle_venta

                        vdb.giddetalle_venta = onekey

                        vdb.gidventa = datalistado.SelectedCells.Item(2).Value
                        vdb.gidproducto = datalistado.SelectedCells.Item(3).Value
                        vdb.gcantidad = datalistado.SelectedCells.Item(5).Value


                        If func.eliminar(vdb) Then
                            If func.aumentar_stockm(vdb) Then

                            End If
                        Else
                            MessageBox.Show("Articulo fue quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                    End If
                Next

                Call mostrar()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call mostrar()
        End If



        Try
            Dim query As IEnumerable(Of Object) = _
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)() _
                Where ( _
                    (row.Cells("ImporteVenta").Value IsNot Nothing) AndAlso _
                    (row.Cells("ImporteVenta").Value IsNot DBNull.Value)) Select row.Cells("ImporteVenta").Value()

            Dim resultado As Decimal = _
                query.Sum(Function(row) Convert.ToDecimal(row))
            lbltotal.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        txtidproducto.Text = ""
        txtnombre_producto.Text = ""
        txtprecio_unitario.Text = ""


    End Sub

    Private Sub cbeliminar_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If cbeliminar.CheckState = CheckState.Checked Then
        'datalistado.Columns.Item("Eliminar").Visible = True
        'Else
        '    datalistado.Columns.Item("Eliminar").Visible = False
        'End If
    End Sub

    Private Sub datalistado_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidproducto.Text = datalistado.SelectedCells.Item(3).Value
        txtnombre_producto.Text = datalistado.SelectedCells.Item(4).Value
        txtprecio_unitario.Text = datalistado.SelectedCells.Item(6).Value
        txtcantidad.Text = datalistado.SelectedCells.Item(7).Value
        txtstock.Text = datalistado.SelectedCells.Item(10).Value

        Dim chkcell As DataGridViewCheckBoxCell = Me.datalistado.Rows(e.RowIndex).Cells(0)
        chkcell.Value = Not chkcell.Value

    End Sub

    Private Sub btnimprimir_Click(sender As Object, e As EventArgs) Handles btnimprimir.Click
        frmRecibo.txtidventa.Text = txtidventa.Text
        frmRecibo.ShowDialog()
    End Sub
End Class