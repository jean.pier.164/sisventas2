﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frminicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frminicio))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.INToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaPreciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosXFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDiarioBoletasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoResumenesDiariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComunicacionDeBajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoComprobantesSunatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotaCreditoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotaDebitoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CATALOGOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArticulosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CategoriasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PresentacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockMinimoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDePreciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierrasCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosYEgresosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArchivosFEStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lbluser = New System.Windows.Forms.Label()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.lblLicenciaMsje = New System.Windows.Forms.Label()
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.AutoSize = False
        Me.MenuStrip.BackColor = System.Drawing.Color.White
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.INToolStripMenuItem, Me.VentasToolStripMenuItem, Me.ClientesToolStripMenuItem, Me.ComprasToolStripMenuItem, Me.CATALOGOToolStripMenuItem, Me.ConsultasToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.CierrasCajaToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.ArchivosFEStripMenuItem1})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(1370, 67)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'INToolStripMenuItem
        '
        Me.INToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListaPreciosToolStripMenuItem, Me.IngresosXFacturasToolStripMenuItem})
        Me.INToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.INToolStripMenuItem.Image = CType(resources.GetObject("INToolStripMenuItem.Image"), System.Drawing.Image)
        Me.INToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.INToolStripMenuItem.Name = "INToolStripMenuItem"
        Me.INToolStripMenuItem.Size = New System.Drawing.Size(129, 63)
        Me.INToolStripMenuItem.Text = "Clientes"
        '
        'ListaPreciosToolStripMenuItem
        '
        Me.ListaPreciosToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.price_list_1_909330
        Me.ListaPreciosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListaPreciosToolStripMenuItem.Name = "ListaPreciosToolStripMenuItem"
        Me.ListaPreciosToolStripMenuItem.Size = New System.Drawing.Size(249, 56)
        Me.ListaPreciosToolStripMenuItem.Text = "Lista Precios"
        '
        'IngresosXFacturasToolStripMenuItem
        '
        Me.IngresosXFacturasToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources._1043188_200
        Me.IngresosXFacturasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.IngresosXFacturasToolStripMenuItem.Name = "IngresosXFacturasToolStripMenuItem"
        Me.IngresosXFacturasToolStripMenuItem.Size = New System.Drawing.Size(249, 56)
        Me.IngresosXFacturasToolStripMenuItem.Text = "Ingresos x Facturas"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegistroToolStripMenuItem, Me.ModificarVentasToolStripMenuItem, Me.ResumenDiarioBoletasToolStripMenuItem, Me.ListadoResumenesDiariosToolStripMenuItem, Me.ComunicacionDeBajaToolStripMenuItem, Me.ListadoComprobantesSunatToolStripMenuItem, Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem, Me.NotaCreditoMenuItem, Me.NotaDebitoMenuItem})
        Me.VentasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VentasToolStripMenuItem.ForeColor = System.Drawing.Color.DodgerBlue
        Me.VentasToolStripMenuItem.Image = CType(resources.GetObject("VentasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.VentasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(117, 63)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'RegistroToolStripMenuItem
        '
        Me.RegistroToolStripMenuItem.Image = CType(resources.GetObject("RegistroToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RegistroToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.RegistroToolStripMenuItem.Name = "RegistroToolStripMenuItem"
        Me.RegistroToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.RegistroToolStripMenuItem.Text = "Realizar Ventas"
        '
        'ModificarVentasToolStripMenuItem
        '
        Me.ModificarVentasToolStripMenuItem.Image = CType(resources.GetObject("ModificarVentasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ModificarVentasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ModificarVentasToolStripMenuItem.Name = "ModificarVentasToolStripMenuItem"
        Me.ModificarVentasToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ModificarVentasToolStripMenuItem.Text = "Modificar Ventas"
        '
        'ResumenDiarioBoletasToolStripMenuItem
        '
        Me.ResumenDiarioBoletasToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.resumen_resize
        Me.ResumenDiarioBoletasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ResumenDiarioBoletasToolStripMenuItem.Name = "ResumenDiarioBoletasToolStripMenuItem"
        Me.ResumenDiarioBoletasToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ResumenDiarioBoletasToolStripMenuItem.Text = "Resumen Diario Boletas"
        '
        'ListadoResumenesDiariosToolStripMenuItem
        '
        Me.ListadoResumenesDiariosToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.resumen_2
        Me.ListadoResumenesDiariosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListadoResumenesDiariosToolStripMenuItem.Name = "ListadoResumenesDiariosToolStripMenuItem"
        Me.ListadoResumenesDiariosToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListadoResumenesDiariosToolStripMenuItem.Text = "Listado Resumenes Diarios"
        Me.ListadoResumenesDiariosToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ComunicacionDeBajaToolStripMenuItem
        '
        Me.ComunicacionDeBajaToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.baja_
        Me.ComunicacionDeBajaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ComunicacionDeBajaToolStripMenuItem.Name = "ComunicacionDeBajaToolStripMenuItem"
        Me.ComunicacionDeBajaToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ComunicacionDeBajaToolStripMenuItem.Text = "Comunicacion de Baja"
        '
        'ListadoComprobantesSunatToolStripMenuItem
        '
        Me.ListadoComprobantesSunatToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.Bill__1_
        Me.ListadoComprobantesSunatToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListadoComprobantesSunatToolStripMenuItem.Name = "ListadoComprobantesSunatToolStripMenuItem"
        Me.ListadoComprobantesSunatToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListadoComprobantesSunatToolStripMenuItem.Text = "Listado Comprobantes Sunat"
        '
        'ListaComprobantesPendienteEnvíoSunatToolStripMenuItem
        '
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.Updating_Invoices
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Name = "ListaComprobantesPendienteEnvíoSunatToolStripMenuItem"
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Text = "Lista Comprobantes Pendiente Envío Sunat"
        '
        'NotaCreditoMenuItem
        '
        Me.NotaCreditoMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.img_documento
        Me.NotaCreditoMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NotaCreditoMenuItem.Name = "NotaCreditoMenuItem"
        Me.NotaCreditoMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.NotaCreditoMenuItem.Text = "Nota de Crédito"
        '
        'NotaDebitoMenuItem
        '
        Me.NotaDebitoMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.f_icon_c4
        Me.NotaDebitoMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NotaDebitoMenuItem.Name = "NotaDebitoMenuItem"
        Me.NotaDebitoMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.NotaDebitoMenuItem.Text = "Nota de Débito"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ClientesToolStripMenuItem.ForeColor = System.Drawing.Color.SeaGreen
        Me.ClientesToolStripMenuItem.Image = CType(resources.GetObject("ClientesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(146, 63)
        Me.ClientesToolStripMenuItem.Text = "Proveedor"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ComprasToolStripMenuItem.ForeColor = System.Drawing.Color.DarkOrange
        Me.ComprasToolStripMenuItem.Image = CType(resources.GetObject("ComprasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ComprasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(133, 63)
        Me.ComprasToolStripMenuItem.Text = "Compras"
        '
        'CATALOGOToolStripMenuItem
        '
        Me.CATALOGOToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArticulosToolStripMenuItem, Me.CategoriasToolStripMenuItem, Me.PresentacionToolStripMenuItem})
        Me.CATALOGOToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CATALOGOToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.CATALOGOToolStripMenuItem.Image = CType(resources.GetObject("CATALOGOToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CATALOGOToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CATALOGOToolStripMenuItem.Name = "CATALOGOToolStripMenuItem"
        Me.CATALOGOToolStripMenuItem.Size = New System.Drawing.Size(133, 63)
        Me.CATALOGOToolStripMenuItem.Text = "Catalogo"
        '
        'ArticulosToolStripMenuItem
        '
        Me.ArticulosToolStripMenuItem.Image = CType(resources.GetObject("ArticulosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ArticulosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ArticulosToolStripMenuItem.Name = "ArticulosToolStripMenuItem"
        Me.ArticulosToolStripMenuItem.Size = New System.Drawing.Size(204, 56)
        Me.ArticulosToolStripMenuItem.Text = "Articulos"
        '
        'CategoriasToolStripMenuItem
        '
        Me.CategoriasToolStripMenuItem.Image = CType(resources.GetObject("CategoriasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CategoriasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CategoriasToolStripMenuItem.Name = "CategoriasToolStripMenuItem"
        Me.CategoriasToolStripMenuItem.Size = New System.Drawing.Size(204, 56)
        Me.CategoriasToolStripMenuItem.Text = "Categorias"
        '
        'PresentacionToolStripMenuItem
        '
        Me.PresentacionToolStripMenuItem.Image = CType(resources.GetObject("PresentacionToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PresentacionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.PresentacionToolStripMenuItem.Name = "PresentacionToolStripMenuItem"
        Me.PresentacionToolStripMenuItem.Size = New System.Drawing.Size(204, 56)
        Me.PresentacionToolStripMenuItem.Text = "Presentacion"
        '
        'ConsultasToolStripMenuItem
        '
        Me.ConsultasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VentasToolStripMenuItem1, Me.ProductosToolStripMenuItem1, Me.StockMinimoToolStripMenuItem})
        Me.ConsultasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsultasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.ConsultasToolStripMenuItem.Image = CType(resources.GetObject("ConsultasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ConsultasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ConsultasToolStripMenuItem.Name = "ConsultasToolStripMenuItem"
        Me.ConsultasToolStripMenuItem.Size = New System.Drawing.Size(141, 63)
        Me.ConsultasToolStripMenuItem.Text = "Consultas"
        '
        'VentasToolStripMenuItem1
        '
        Me.VentasToolStripMenuItem1.Name = "VentasToolStripMenuItem1"
        Me.VentasToolStripMenuItem1.Size = New System.Drawing.Size(171, 22)
        Me.VentasToolStripMenuItem1.Text = "Ventas"
        '
        'ProductosToolStripMenuItem1
        '
        Me.ProductosToolStripMenuItem1.Name = "ProductosToolStripMenuItem1"
        Me.ProductosToolStripMenuItem1.Size = New System.Drawing.Size(171, 22)
        Me.ProductosToolStripMenuItem1.Text = "Productos"
        '
        'StockMinimoToolStripMenuItem
        '
        Me.StockMinimoToolStripMenuItem.Name = "StockMinimoToolStripMenuItem"
        Me.StockMinimoToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.StockMinimoToolStripMenuItem.Text = "Stock Minimo"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductosToolStripMenuItem2, Me.ListaDePreciosToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReportesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.ReportesToolStripMenuItem.Image = CType(resources.GetObject("ReportesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ReportesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(136, 63)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'ProductosToolStripMenuItem2
        '
        Me.ProductosToolStripMenuItem2.Name = "ProductosToolStripMenuItem2"
        Me.ProductosToolStripMenuItem2.Size = New System.Drawing.Size(190, 22)
        Me.ProductosToolStripMenuItem2.Text = "Productos"
        '
        'ListaDePreciosToolStripMenuItem
        '
        Me.ListaDePreciosToolStripMenuItem.Name = "ListaDePreciosToolStripMenuItem"
        Me.ListaDePreciosToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.ListaDePreciosToolStripMenuItem.Text = "Lista de Precios"
        '
        'CierrasCajaToolStripMenuItem
        '
        Me.CierrasCajaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresosYEgresosToolStripMenuItem})
        Me.CierrasCajaToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CierrasCajaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.CierrasCajaToolStripMenuItem.Image = CType(resources.GetObject("CierrasCajaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CierrasCajaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CierrasCajaToolStripMenuItem.Name = "CierrasCajaToolStripMenuItem"
        Me.CierrasCajaToolStripMenuItem.Size = New System.Drawing.Size(157, 63)
        Me.CierrasCajaToolStripMenuItem.Text = "Cierras Caja"
        '
        'IngresosYEgresosToolStripMenuItem
        '
        Me.IngresosYEgresosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.IngresosYEgresosToolStripMenuItem.Name = "IngresosYEgresosToolStripMenuItem"
        Me.IngresosYEgresosToolStripMenuItem.Size = New System.Drawing.Size(212, 22)
        Me.IngresosYEgresosToolStripMenuItem.Text = "Ingresos y Egresos"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UsuariosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.UsuariosToolStripMenuItem.Image = CType(resources.GetObject("UsuariosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UsuariosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(133, 63)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'ArchivosFEStripMenuItem1
        '
        Me.ArchivosFEStripMenuItem1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ArchivosFEStripMenuItem1.ForeColor = System.Drawing.SystemColors.Desktop
        Me.ArchivosFEStripMenuItem1.Image = Global.BRAVOSPORT.My.Resources.Resources._62319
        Me.ArchivosFEStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ArchivosFEStripMenuItem1.Name = "ArchivosFEStripMenuItem1"
        Me.ArchivosFEStripMenuItem1.Size = New System.Drawing.Size(154, 63)
        Me.ArchivosFEStripMenuItem1.Text = "Archivos Fe"
        '
        'lbluser
        '
        Me.lbluser.AutoSize = True
        Me.lbluser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbluser.Location = New System.Drawing.Point(32, 51)
        Me.lbluser.Name = "lbluser"
        Me.lbluser.Size = New System.Drawing.Size(55, 16)
        Me.lbluser.TabIndex = 9
        Me.lbluser.Text = "Label1"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(53, 19)
        Me.ToolStripStatusLabel.Text = "Estado"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 439)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1370, 24)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'lblLicenciaMsje
        '
        Me.lblLicenciaMsje.AutoSize = True
        Me.lblLicenciaMsje.Location = New System.Drawing.Point(927, 450)
        Me.lblLicenciaMsje.Name = "lblLicenciaMsje"
        Me.lblLicenciaMsje.Size = New System.Drawing.Size(0, 13)
        Me.lblLicenciaMsje.TabIndex = 11
        '
        'frminicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(1370, 463)
        Me.Controls.Add(Me.lblLicenciaMsje)
        Me.Controls.Add(Me.lbluser)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Location = New System.Drawing.Point(500, 50)
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "frminicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ".: Sistema de Ventas :."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents INToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lbluser As System.Windows.Forms.Label
    Friend WithEvents VentasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CierrasCajaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IngresosYEgresosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StockMinimoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ModificarVentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaPreciosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IngresosXFacturasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListaDePreciosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CATALOGOToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArticulosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CategoriasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PresentacionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ResumenDiarioBoletasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoResumenesDiariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComunicacionDeBajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoComprobantesSunatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArchivosFEStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ListaComprobantesPendienteEnvíoSunatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel As ToolStripStatusLabel
    Friend WithEvents StatusStrip As StatusStrip
    Friend WithEvents lblLicenciaMsje As Label
    Friend WithEvents NotaCreditoMenuItem As ToolStripMenuItem
    Friend WithEvents NotaDebitoMenuItem As ToolStripMenuItem
End Class
