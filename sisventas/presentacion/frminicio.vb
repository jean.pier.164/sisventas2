﻿Imports System.Windows.Forms

Public Class frminicio

    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs)
        ' Cree una nueva instancia del formulario secundario.
        Dim ChildForm As New System.Windows.Forms.Form
        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "Ventana " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: agregue código aquí para abrir el archivo.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: agregue código aquí para guardar el contenido actual del formulario en un archivo.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Utilice My.Computer.Clipboard para insertar el texto o las imágenes seleccionadas en el Portapapeles
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Utilice My.Computer.Clipboard para insertar el texto o las imágenes seleccionadas en el Portapapeles
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Utilice My.Computer.Clipboard.GetText() o My.Computer.Clipboard.GetData para recuperar la información del Portapapeles.
    End Sub



    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Cierre todos los formularios secundarios del principal.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub CategoriaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)



    End Sub

    Private Sub ProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)





    End Sub

    Private Sub RegistroToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RegistroToolStripMenuItem.Click


        frmdetalle_venta.MdiParent = Me
        frmdetalle_venta.Show()

    End Sub

    Private Sub ReporteProductosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub ClientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmcliente.MdiParent = Me
        frmcliente.Show()
    End Sub

    Private Sub lbluser_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbluser.VisibleChanged
        Try
            Dim dts As New vusuario
            Dim func As New fusuario

            dts.glogin = frmlogin.txtlogin.Text

            dts.gpassword = frmlogin.txtpassword.Text


            lbluser.Text = "Bienvenido " + frmlogin.txtlogin.Text + " al sistema"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub VentasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VentasToolStripMenuItem1.Click
        If frmlogin.txtacceso.Text = "1" Then

            frmcCantidadProductosporUsuario.MdiParent = Me
            frmcCantidadProductosporUsuario.Show()

        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub



    Private Sub ProductosToolStripMenuItem2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductosToolStripMenuItem2.Click
        ReporteProductos.MdiParent = Me
        ReporteProductos.Show()

    End Sub



    Private Sub ProductosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProductosToolStripMenuItem1.Click



        If frmlogin.txtacceso.Text = "1" Then

            frmcproductosmvendidos.MdiParent = Me
            frmcproductosmvendidos.Show()

        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If

    End Sub

    Private Sub PermanenteValorizadoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmcPermanenteValorizado.MdiParent = Me
        frmcPermanenteValorizado.Show()


    End Sub

    Private Sub ToolsMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub RegistroDeComprasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmcompras.MdiParent = Me
        frmcompras.Show()
    End Sub

    Private Sub frminicio_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmstockminimo.MdiParent = Me
        frmstockminimo.Show()
    End Sub

    Private Sub IngresosYEgresosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IngresosYEgresosToolStripMenuItem.Click
        'frmcierredecaja.MdiParent = Me
        ' frmcierredecaja.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub LaboratorioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'frmlaboratorio.MdiParent = Me
        'frmlaboratorio.Show()



    End Sub

    Private Sub StockMinimoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StockMinimoToolStripMenuItem.Click
        frmstockminimo.MdiParent = Me
        frmstockminimo.Show()
    End Sub

    Private Sub MenuStrip_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MenuStrip.ItemClicked

    End Sub

   
    Private Sub BOLETASFACTURASToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ConsultaDocToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ClientesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ModificarVentasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarVentasToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then

            Dim form As New frmventa
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If

    End Sub

    Private Sub INToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles INToolStripMenuItem.Click
        frmcliente.MdiParent = Me
        frmcliente.Show()
    End Sub

    Private Sub CierrasCajaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CierrasCajaToolStripMenuItem.Click

    End Sub

    Private Sub RegistroDToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub ReportesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReportesToolStripMenuItem.Click

    End Sub

    Private Sub ListaPreciosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaPreciosToolStripMenuItem.Click
        frmlistaprecios.MdiParent = Me
        frmlistaprecios.Show()
    End Sub

    Private Sub IngresosXFacturasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IngresosXFacturasToolStripMenuItem.Click
        FrmIngreso.MdiParent = Me
        FrmIngreso.Show()
    End Sub

    Private Sub ListaDePreciosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDePreciosToolStripMenuItem.Click
        ListaPreciosxCategoria.MdiParent = Me
        ListaPreciosxCategoria.Show()
    End Sub

    Private Sub ClientesToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles ClientesToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then

            frmprovedor.MdiParent = Me
            frmprovedor.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If

    End Sub

    Private Sub CategoriasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CategoriasToolStripMenuItem.Click
        frmcategoria.MdiParent = Me
        frmcategoria.Show()
    End Sub

    Private Sub ArticulosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ArticulosToolStripMenuItem.Click
        frmproducto.MdiParent = Me
        frmproducto.Show()

    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmacceso
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub ResumenDiarioBoletasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ResumenDiarioBoletasToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmcResumenDiario
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub ListadoResumenesDiariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoResumenesDiariosToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmcListaResumenDiario
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub ComunicacionDeBajaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ComunicacionDeBajaToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmcComunicacionBaja
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub ListadoComprobantesSunatToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListadoComprobantesSunatToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmcListadoComprobantesSunat
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub ArchivosFEStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ArchivosFEStripMenuItem1.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmArchivosFE
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub ListaComprobantesPendienteEnvíoSunatToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmcListadoComprobantesPendienteEnvio
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub NotaCreditoMenuItem_Click(sender As Object, e As EventArgs) Handles NotaCreditoMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmnota_credito
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub

    Private Sub NotaDebitoMenuItem_Click(sender As Object, e As EventArgs) Handles NotaDebitoMenuItem.Click
        If frmlogin.txtacceso.Text = "1" Then
            Dim form As New frmnota_debito
            form.MdiParent = Me
            form.Show()
        Else
            MsgBox("No tiene Permiso consulte al Administrador")
        End If
    End Sub
End Class
