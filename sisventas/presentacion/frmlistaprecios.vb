﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class frmlistaprecios


    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Private dt As New DataTable
    Private dt1 As New DataTable
    Dim consulta3 As New SqlCommand
    Dim cadena As String
    Dim datos As New DataSet
    Dim variable As SqlDataReader

    Public Sub limpiar()
        btnguardar.Visible = True
        btneditar.Visible = False
        cboproducto.Text = ""
        txtcantidad.Text = "0"
        txtpventa.Text = "0"

        txtidproducto.Text = ""
        txtidprecios.Text = ""



    End Sub
    Private Sub mostrarm()
        Try
            Dim func As New flistaprecios
            dt = func.mostrarm

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False

                datalistado.Columns(1).Visible = False
                datalistado.Columns(2).Visible = False

            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        btnnuevo.Visible = True
        btneditar.Visible = False



    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                datalistado.Columns(1).Visible = False
                datalistado.Columns(2).Visible = False

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub


    Private Sub btnguardar_Click(sender As Object, e As EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And cboundm.Text <> "" Then
            Try
                Dim dts As New vlistaprecios
                Dim func As New flistaprecios

                dts.gidproducto = txtidproducto.Text
                dts.gundm = cboundm.Text
                dts.gcant = txtcantidad.Text
                dts.gpventa = txtpventa.Text
                dts.gsundm = cbosundm.Text
                dts.gpcompra = txtpcompra.Text


                If func.insertar(dts) Then
                    MessageBox.Show("Registrada Correctamente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrarm()
                    limpiar()
                Else
                    MessageBox.Show(" no fue registrada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrarm()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmlistaprecios_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        mostrarm()


        cboproducto.DroppedDown = True

        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select nombre from producto order by nombre")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader
        While variable.Read = True
            cboproducto.Items.Add(variable.Item(0))



        End While

        conexion.Close()



    End Sub


    Private Sub cboproducto_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboproducto.SelectedIndexChanged
        mostrar_nproductos()
    End Sub


    Private Sub mostrar_nproductos()

        Dim func As New fnproductos
        dt1 = func.mcodigoproducto
    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick
        If e.ColumnIndex = Me.datalistado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.datalistado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value

        End If
    End Sub

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidproducto.Text = datalistado.SelectedCells.Item(1).Value
        txtidprecios.Text = datalistado.SelectedCells.Item(2).Value
        cboproducto.Text = datalistado.SelectedCells.Item(3).Value
        cbosundm.Text = datalistado.SelectedCells.Item(4).Value

        cboundm.Text = datalistado.SelectedCells.Item(5).Value
        txtcantidad.Text = datalistado.SelectedCells.Item(6).Value
        txtpventa.Text = datalistado.SelectedCells.Item(7).Value
        txtpcompra.Text = datalistado.SelectedCells.Item(8).Value

        btneditar.Visible = True
        btnguardar.Visible = False






    End Sub

    Private Sub datalistado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        If txtflag.Text = "1" Then
            frmproducto.txtidcategoria.Text = datalistado.SelectedCells.Item(1).Value
            frmproducto.txtnombre_categoria.Text = datalistado.SelectedCells.Item(2).Value
            Me.Close()
        End If
    End Sub

    Private Sub btneditar_Click(sender As Object, e As EventArgs) Handles btneditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And cboproducto.Text <> "" And cboundm.Text <> "" Then
            Try
                Dim dts As New vlistaprecios
                Dim func As New flistaprecios

                dts.gidprecios = txtidprecios.Text
                dts.gundm = cboundm.Text

                dts.gcant = txtcantidad.Text
                dts.gpventa = txtpventa.Text

                dts.gidproducto = txtidproducto.Text
                dts.gsundm = cbosundm.Text

                dts.gpcompra = txtpcompra.Text


                If func.editar(dts) Then
                    MessageBox.Show("Categoria Modificada Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrarm()

                Else
                    MessageBox.Show("Categoria no fue Modificada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrarm()


                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub



    Private Sub btnnuevo_Click(sender As Object, e As EventArgs) Handles btnnuevo.Click
        limpiar()
    End Sub


    Private Sub txtbuscar_TextChanged(sender As Object, e As EventArgs) Handles txtbuscar.TextChanged
        buscar()
    End Sub

    Private Sub cbeliminar_CheckedChanged(sender As Object, e As EventArgs) Handles cbeliminar.CheckedChanged
        If cbeliminar.CheckState = CheckState.Checked Then
            datalistado.Columns.Item("Eliminar").Visible = True
        Else
            datalistado.Columns.Item("Eliminar").Visible = False
        End If
    End Sub

    Private Sub btneliminar_Click(sender As Object, e As EventArgs) Handles btneliminar.Click
        Dim result As DialogResult
        result = MessageBox.Show("Realmente desea eliminar los Productos seleccionados?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)


        If result = DialogResult.OK Then
            Try
                For Each row As DataGridViewRow In datalistado.Rows
                    Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                    If marcado Then
                        Dim onekey As Integer = Convert.ToInt32(row.Cells("idprecios").Value)
                        Dim vdb As New vlistaprecios
                        Dim func As New flistaprecios

                        vdb.gidprecios = onekey
                        If func.eliminar(vdb) Then

                        Else
                            MessageBox.Show("No fue eliminados?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                    End If
                Next

                Call mostrarm()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call mostrarm()
        End If
        Call limpiar()

    End Sub
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click

        txtre.Text = txtpventa.Text / txtcantidad.Text

    End Sub

    Private Sub txtrc_TextChanged(sender As Object, e As EventArgs)
        txtre.Text = txtpventa.Text / txtcantidad.Text
    End Sub

    Private Sub txtre_TextChanged(sender As Object, e As EventArgs) Handles txtre.TextChanged
        txtre.Text = txtpventa.Text / txtcantidad.Text
    End Sub
End Class