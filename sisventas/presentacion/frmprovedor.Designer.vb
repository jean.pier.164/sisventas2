﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmprovedor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.txtBuscar = New System.Windows.Forms.TextBox()
        Me.Eliminar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dataListado = New System.Windows.Forms.DataGridView()
        Me.chkEliminar = New System.Windows.Forms.CheckBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.label10 = New System.Windows.Forms.Label()
        Me.txtUrl = New System.Windows.Forms.TextBox()
        Me.label9 = New System.Windows.Forms.Label()
        Me.cbBuscar = New System.Windows.Forms.ComboBox()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.label8 = New System.Windows.Forms.Label()
        Me.txtNum_Documento = New System.Windows.Forms.TextBox()
        Me.cbTipo_Documento = New System.Windows.Forms.ComboBox()
        Me.label7 = New System.Windows.Forms.Label()
        Me.cbSector_Comercial = New System.Windows.Forms.ComboBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.tabControl1 = New System.Windows.Forms.TabControl()
        Me.tabPage1 = New System.Windows.Forms.TabPage()
        Me.tabPage2 = New System.Windows.Forms.TabPage()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.label6 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.txtRazon_Social = New System.Windows.Forms.TextBox()
        Me.txtIdproveedor = New System.Windows.Forms.TextBox()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.ttMensaje = New System.Windows.Forms.ToolTip(Me.components)
        Me.errorIcono = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.pictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.dataListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl1.SuspendLayout()
        Me.tabPage1.SuspendLayout()
        Me.tabPage2.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        CType(Me.errorIcono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(617, 31)
        Me.lblTotal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(35, 13)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "label3"
        '
        'txtBuscar
        '
        Me.txtBuscar.Location = New System.Drawing.Point(124, 28)
        Me.txtBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.Size = New System.Drawing.Size(133, 20)
        Me.txtBuscar.TabIndex = 1
        '
        'Eliminar
        '
        Me.Eliminar.HeaderText = "Eliminar"
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        Me.Eliminar.Width = 49
        '
        'dataListado
        '
        Me.dataListado.AllowUserToAddRows = False
        Me.dataListado.AllowUserToDeleteRows = False
        Me.dataListado.AllowUserToOrderColumns = True
        Me.dataListado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dataListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Eliminar})
        Me.dataListado.Location = New System.Drawing.Point(10, 66)
        Me.dataListado.Margin = New System.Windows.Forms.Padding(2)
        Me.dataListado.MultiSelect = False
        Me.dataListado.Name = "dataListado"
        Me.dataListado.ReadOnly = True
        Me.dataListado.RowTemplate.Height = 24
        Me.dataListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataListado.Size = New System.Drawing.Size(1304, 464)
        Me.dataListado.TabIndex = 7
        '
        'chkEliminar
        '
        Me.chkEliminar.AutoSize = True
        Me.chkEliminar.Location = New System.Drawing.Point(10, 49)
        Me.chkEliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.chkEliminar.Name = "chkEliminar"
        Me.chkEliminar.Size = New System.Drawing.Size(80, 17)
        Me.chkEliminar.TabIndex = 5
        Me.chkEliminar.Text = "chkEliminar"
        Me.chkEliminar.UseVisualStyleBackColor = True
        '
        'txtEmail
        '
        Me.txtEmail.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmail.Location = New System.Drawing.Point(394, 197)
        Me.txtEmail.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(122, 20)
        Me.txtEmail.TabIndex = 20
        '
        'label10
        '
        Me.label10.AutoSize = True
        Me.label10.Location = New System.Drawing.Point(329, 197)
        Me.label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(38, 13)
        Me.label10.TabIndex = 19
        Me.label10.Text = "E-mail:"
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtUrl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUrl.Location = New System.Drawing.Point(117, 219)
        Me.txtUrl.Margin = New System.Windows.Forms.Padding(2)
        Me.txtUrl.Name = "txtUrl"
        Me.txtUrl.Size = New System.Drawing.Size(198, 20)
        Me.txtUrl.TabIndex = 18
        '
        'label9
        '
        Me.label9.AutoSize = True
        Me.label9.Location = New System.Drawing.Point(22, 219)
        Me.label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(23, 13)
        Me.label9.TabIndex = 17
        Me.label9.Text = "Url:"
        '
        'cbBuscar
        '
        Me.cbBuscar.FormattingEnabled = True
        Me.cbBuscar.Items.AddRange(New Object() {"Documento", "Razon Social"})
        Me.cbBuscar.Location = New System.Drawing.Point(10, 24)
        Me.cbBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.cbBuscar.Name = "cbBuscar"
        Me.cbBuscar.Size = New System.Drawing.Size(92, 21)
        Me.cbBuscar.TabIndex = 8
        Me.cbBuscar.Text = "Documento"
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.Silver
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(513, 24)
        Me.btnImprimir.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(88, 28)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.Silver
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(416, 24)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(81, 28)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'txtTelefono
        '
        Me.txtTelefono.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelefono.Location = New System.Drawing.Point(117, 197)
        Me.txtTelefono.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(198, 20)
        Me.txtTelefono.TabIndex = 16
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.Location = New System.Drawing.Point(22, 197)
        Me.label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(52, 13)
        Me.label8.TabIndex = 15
        Me.label8.Text = "Teléfono:"
        '
        'txtNum_Documento
        '
        Me.txtNum_Documento.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtNum_Documento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNum_Documento.Location = New System.Drawing.Point(211, 101)
        Me.txtNum_Documento.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNum_Documento.Name = "txtNum_Documento"
        Me.txtNum_Documento.Size = New System.Drawing.Size(198, 20)
        Me.txtNum_Documento.TabIndex = 14
        '
        'cbTipo_Documento
        '
        Me.cbTipo_Documento.FormattingEnabled = True
        Me.cbTipo_Documento.Items.AddRange(New Object() {"DNI", "RUC"})
        Me.cbTipo_Documento.Location = New System.Drawing.Point(116, 100)
        Me.cbTipo_Documento.Margin = New System.Windows.Forms.Padding(2)
        Me.cbTipo_Documento.Name = "cbTipo_Documento"
        Me.cbTipo_Documento.Size = New System.Drawing.Size(92, 21)
        Me.cbTipo_Documento.TabIndex = 13
        Me.cbTipo_Documento.Text = "RUC"
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.Location = New System.Drawing.Point(22, 106)
        Me.label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(89, 13)
        Me.label7.TabIndex = 12
        Me.label7.Text = "Tipo Documento:"
        '
        'cbSector_Comercial
        '
        Me.cbSector_Comercial.FormattingEnabled = True
        Me.cbSector_Comercial.Items.AddRange(New Object() {"Salud", "Alimentos", "Tecnología", "Ropa", "Servicios"})
        Me.cbSector_Comercial.Location = New System.Drawing.Point(424, 67)
        Me.cbSector_Comercial.Margin = New System.Windows.Forms.Padding(2)
        Me.cbSector_Comercial.Name = "cbSector_Comercial"
        Me.cbSector_Comercial.Size = New System.Drawing.Size(92, 21)
        Me.cbSector_Comercial.TabIndex = 11
        Me.cbSector_Comercial.Text = "Alimentos"
        '
        'btnBuscar
        '
        Me.btnBuscar.BackColor = System.Drawing.Color.Silver
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(320, 24)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(79, 28)
        Me.btnBuscar.TabIndex = 2
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'tabControl1
        '
        Me.tabControl1.Controls.Add(Me.tabPage1)
        Me.tabControl1.Controls.Add(Me.tabPage2)
        Me.tabControl1.Location = New System.Drawing.Point(25, 34)
        Me.tabControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(1326, 560)
        Me.tabControl1.TabIndex = 7
        '
        'tabPage1
        '
        Me.tabPage1.Controls.Add(Me.cbBuscar)
        Me.tabPage1.Controls.Add(Me.dataListado)
        Me.tabPage1.Controls.Add(Me.lblTotal)
        Me.tabPage1.Controls.Add(Me.chkEliminar)
        Me.tabPage1.Controls.Add(Me.btnImprimir)
        Me.tabPage1.Controls.Add(Me.btnEliminar)
        Me.tabPage1.Controls.Add(Me.btnBuscar)
        Me.tabPage1.Controls.Add(Me.txtBuscar)
        Me.tabPage1.Location = New System.Drawing.Point(4, 22)
        Me.tabPage1.Margin = New System.Windows.Forms.Padding(2)
        Me.tabPage1.Name = "tabPage1"
        Me.tabPage1.Padding = New System.Windows.Forms.Padding(2)
        Me.tabPage1.Size = New System.Drawing.Size(1318, 534)
        Me.tabPage1.TabIndex = 0
        Me.tabPage1.Text = "Listado"
        Me.tabPage1.UseVisualStyleBackColor = True
        '
        'tabPage2
        '
        Me.tabPage2.Controls.Add(Me.groupBox1)
        Me.tabPage2.Location = New System.Drawing.Point(4, 22)
        Me.tabPage2.Margin = New System.Windows.Forms.Padding(2)
        Me.tabPage2.Name = "tabPage2"
        Me.tabPage2.Padding = New System.Windows.Forms.Padding(2)
        Me.tabPage2.Size = New System.Drawing.Size(623, 346)
        Me.tabPage2.TabIndex = 1
        Me.tabPage2.Text = "Mantenimiento"
        Me.tabPage2.UseVisualStyleBackColor = True
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.txtEmail)
        Me.groupBox1.Controls.Add(Me.label10)
        Me.groupBox1.Controls.Add(Me.txtUrl)
        Me.groupBox1.Controls.Add(Me.label9)
        Me.groupBox1.Controls.Add(Me.txtTelefono)
        Me.groupBox1.Controls.Add(Me.label8)
        Me.groupBox1.Controls.Add(Me.txtNum_Documento)
        Me.groupBox1.Controls.Add(Me.cbTipo_Documento)
        Me.groupBox1.Controls.Add(Me.label7)
        Me.groupBox1.Controls.Add(Me.cbSector_Comercial)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.btnCancelar)
        Me.groupBox1.Controls.Add(Me.btnEditar)
        Me.groupBox1.Controls.Add(Me.btnGuardar)
        Me.groupBox1.Controls.Add(Me.btnNuevo)
        Me.groupBox1.Controls.Add(Me.txtDireccion)
        Me.groupBox1.Controls.Add(Me.txtRazon_Social)
        Me.groupBox1.Controls.Add(Me.txtIdproveedor)
        Me.groupBox1.Controls.Add(Me.label5)
        Me.groupBox1.Controls.Add(Me.label4)
        Me.groupBox1.Controls.Add(Me.label3)
        Me.groupBox1.Location = New System.Drawing.Point(10, 17)
        Me.groupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.groupBox1.Size = New System.Drawing.Size(528, 309)
        Me.groupBox1.TabIndex = 0
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Proveedores"
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(329, 72)
        Me.label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(90, 13)
        Me.label6.TabIndex = 10
        Me.label6.Text = "Sector Comercial:"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(382, 257)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(80, 27)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnEditar
        '
        Me.btnEditar.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.Location = New System.Drawing.Point(280, 257)
        Me.btnEditar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(80, 27)
        Me.btnEditar.TabIndex = 8
        Me.btnEditar.Text = "E&ditar"
        Me.btnEditar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.Location = New System.Drawing.Point(184, 257)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(80, 27)
        Me.btnGuardar.TabIndex = 7
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(87, 257)
        Me.btnNuevo.Margin = New System.Windows.Forms.Padding(2)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(80, 26)
        Me.btnNuevo.TabIndex = 6
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'txtDireccion
        '
        Me.txtDireccion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccion.Location = New System.Drawing.Point(116, 145)
        Me.txtDireccion.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDireccion.Multiline = True
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDireccion.Size = New System.Drawing.Size(226, 43)
        Me.txtDireccion.TabIndex = 5
        '
        'txtRazon_Social
        '
        Me.txtRazon_Social.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtRazon_Social.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRazon_Social.Location = New System.Drawing.Point(116, 71)
        Me.txtRazon_Social.Margin = New System.Windows.Forms.Padding(2)
        Me.txtRazon_Social.Name = "txtRazon_Social"
        Me.txtRazon_Social.Size = New System.Drawing.Size(198, 20)
        Me.txtRazon_Social.TabIndex = 4
        '
        'txtIdproveedor
        '
        Me.txtIdproveedor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtIdproveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdproveedor.Location = New System.Drawing.Point(116, 32)
        Me.txtIdproveedor.Margin = New System.Windows.Forms.Padding(2)
        Me.txtIdproveedor.Name = "txtIdproveedor"
        Me.txtIdproveedor.Size = New System.Drawing.Size(83, 20)
        Me.txtIdproveedor.TabIndex = 3
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(22, 147)
        Me.label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(55, 13)
        Me.label5.TabIndex = 2
        Me.label5.Text = "Dirección:"
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(20, 71)
        Me.label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(73, 13)
        Me.label4.TabIndex = 1
        Me.label4.Text = "Razón Social:"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(18, 37)
        Me.label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(43, 13)
        Me.label3.TabIndex = 0
        Me.label3.Text = "Código:"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.ForeColor = System.Drawing.Color.Maroon
        Me.label1.Location = New System.Drawing.Point(34, 3)
        Me.label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(163, 29)
        Me.label1.TabIndex = 6
        Me.label1.Text = "Proveedores"
        '
        'ttMensaje
        '
        Me.ttMensaje.IsBalloon = True
        '
        'errorIcono
        '
        Me.errorIcono.ContainerControl = Me
        '
        'pictureBox1
        '
        Me.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pictureBox1.Location = New System.Drawing.Point(185, 21)
        Me.pictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(68, 48)
        Me.pictureBox1.TabIndex = 8
        Me.pictureBox1.TabStop = False
        '
        'frmprovedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1362, 627)
        Me.Controls.Add(Me.tabControl1)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.label1)
        Me.Name = "frmprovedor"
        Me.Text = "frmprovedor"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dataListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl1.ResumeLayout(False)
        Me.tabPage1.ResumeLayout(False)
        Me.tabPage1.PerformLayout()
        Me.tabPage2.ResumeLayout(False)
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        CType(Me.errorIcono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents lblTotal As Label
    Private WithEvents txtBuscar As TextBox
    Private WithEvents Eliminar As DataGridViewCheckBoxColumn
    Private WithEvents dataListado As DataGridView
    Private WithEvents chkEliminar As CheckBox
    Private WithEvents txtEmail As TextBox
    Private WithEvents label10 As Label
    Private WithEvents txtUrl As TextBox
    Private WithEvents label9 As Label
    Private WithEvents cbBuscar As ComboBox
    Private WithEvents btnImprimir As Button
    Private WithEvents btnEliminar As Button
    Private WithEvents txtTelefono As TextBox
    Private WithEvents label8 As Label
    Private WithEvents txtNum_Documento As TextBox
    Private WithEvents cbTipo_Documento As ComboBox
    Private WithEvents label7 As Label
    Private WithEvents cbSector_Comercial As ComboBox
    Private WithEvents btnBuscar As Button
    Private WithEvents tabControl1 As TabControl
    Private WithEvents tabPage1 As TabPage
    Private WithEvents tabPage2 As TabPage
    Private WithEvents groupBox1 As GroupBox
    Private WithEvents label6 As Label
    Private WithEvents btnCancelar As Button
    Private WithEvents btnEditar As Button
    Private WithEvents btnGuardar As Button
    Private WithEvents btnNuevo As Button
    Private WithEvents txtDireccion As TextBox
    Private WithEvents txtRazon_Social As TextBox
    Private WithEvents txtIdproveedor As TextBox
    Private WithEvents label5 As Label
    Private WithEvents label4 As Label
    Private WithEvents label3 As Label
    Private WithEvents pictureBox1 As PictureBox
    Private WithEvents label1 As Label
    Private WithEvents ttMensaje As ToolTip
    Private WithEvents errorIcono As ErrorProvider
End Class
