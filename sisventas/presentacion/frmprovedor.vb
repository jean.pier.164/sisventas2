﻿Imports System.Data.SqlClient
Public Class frmprovedor
    Private dt As New DataTable
    Private Sub Limpiar()

        txtRazon_Social.Text = String.Empty
        txtNum_Documento.Text = String.Empty
        txtDireccion.Text = String.Empty
        txtTelefono.Text = String.Empty
        txtUrl.Text = String.Empty
        txtEmail.Text = String.Empty
        txtIdproveedor.Text = String.Empty

    End Sub
    Private Sub OcultarColumnas()

        dataListado.Columns(0).Visible = False
        dataListado.Columns(1).Visible = False
    End Sub
    Private Sub Mostrar()
        Try
            Dim func As New fproveedor
            dt = func.mostrar

            dataListado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                dataListado.DataSource = dt
                txtBuscar.Enabled = True

                dataListado.ColumnHeadersVisible = True

            Else
                dataListado.DataSource = Nothing
                txtBuscar.Enabled = False

                dataListado.ColumnHeadersVisible = False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        btnNuevo.Visible = True
        btnEditar.Visible = False


    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Limpiar()
        Mostrar()
    End Sub

    Private Sub chkEliminar_CheckedChanged(sender As Object, e As EventArgs) Handles chkEliminar.CheckedChanged
        If (chkEliminar.Checked) Then

            dataListado.Columns(0).Visible = True

        Else

            dataListado.Columns(0).Visible = False

        End If
    End Sub

    Private Sub frmprovedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Mostrar()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If Me.ValidateChildren = True And txtRazon_Social.Text <> "" And txtNum_Documento.Text Then
            Try
                Dim dts As New vproveedor
                Dim func As New fproveedor

                dts.grazon_social = txtRazon_Social.Text
                dts.gnum_documento = txtNum_Documento.Text
                dts.gtipo_documento = cbTipo_Documento.Text
                dts.gsector_comercial = cbSector_Comercial.Text

                dts.gdireccion = txtDireccion.Text
                dts.gtelefono = txtTelefono.Text
                dts.gemail = txtEmail.Text
                dts.gurl = txtUrl.Text


                If func.insertar(dts) Then
                    MessageBox.Show(" Registrado Correctamente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Mostrar()

                    'limpiar()
                Else
                    MessageBox.Show("No fue registrado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Mostrar()
                    'limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub dataListado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dataListado.CellContentClick
        If e.ColumnIndex = Me.dataListado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.dataListado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value

        End If
    End Sub

    Private Sub dataListado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dataListado.CellDoubleClick
        txtIdproveedor.Text = Convert.ToString(dataListado.CurrentRow.Cells("idproveedor").Value)
        txtRazon_Social.Text = Convert.ToString(dataListado.CurrentRow.Cells("razon_social").Value)
        cbSector_Comercial.Text = Convert.ToString(dataListado.CurrentRow.Cells("sector_comercial").Value)
        cbTipo_Documento.Text = Convert.ToString(dataListado.CurrentRow.Cells("tipo_documento").Value)
        txtNum_Documento.Text = Convert.ToString(dataListado.CurrentRow.Cells("num_documento").Value)
        txtDireccion.Text = Convert.ToString(dataListado.CurrentRow.Cells("direccion").Value)
        txtTelefono.Text = Convert.ToString(dataListado.CurrentRow.Cells("telefono").Value)
        txtEmail.Text = Convert.ToString(dataListado.CurrentRow.Cells("email").Value)
        txtUrl.Text = Convert.ToString(dataListado.CurrentRow.Cells("url").Value)

        tabControl1.SelectedIndex = 1
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos de la Proveedor ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtRazon_Social.Text <> "" And txtIdproveedor.Text <> "" Then
            Try
                Dim dts As New vproveedor
                Dim func As New fproveedor

                dts.gidproveedor = txtIdproveedor.Text

                dts.grazon_social = txtRazon_Social.Text
                dts.gnum_documento = txtNum_Documento.Text
                dts.gtipo_documento = cbTipo_Documento.Text
                dts.gsector_comercial = cbSector_Comercial.Text

                dts.gdireccion = txtDireccion.Text
                dts.gtelefono = txtTelefono.Text
                dts.gemail = txtEmail.Text
                dts.gurl = txtUrl.Text


                If func.editar(dts) Then
                    MessageBox.Show("Proveedor Modificado Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Mostrar()
                    'limpiar()
                Else
                    MessageBox.Show("Proveedor no fue Modificado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Mostrar()
                    'limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click

    End Sub
End Class