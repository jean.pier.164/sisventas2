﻿Imports System.Data.SqlClient
Public Class frmproveedor


    Private dt As New DataTable
    Private Sub frmproveedor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()
    End Sub
    Public Sub limpiar()
        'btnguardar.Visible = True
        'btneditar.Visible = False

        'txtnombre_proveedor.Text = ""
        'txtcontacto_proveedor.Text = ""
        'txtemail_proveedor.Text = ""
        'txttelefono_proveedor.Text = "0"
        'txtcelular_proveedor.Text = "0"
        'txttipo_proveedor.Text = "0"
        'txtruc_proveedor.Text = ""

 


    End Sub

    Private Sub mostrar()
        Try
            Dim func As New fproveedor
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        btnnuevo.Visible = True
        btneditar.Visible = False

        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                'ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnuevo.Click
        limpiar()
        mostrar()
    End Sub

    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtnombre_proveedor.Text <> "" And txtcontacto_proveedor.Text <> "" And txttelefono_proveedor.Text <> "" And txtcelular_proveedor.Text <> "" And txtemail_proveedor.Text <> "" And txttipo_proveedor.Text <> "" And txtruc_proveedor.Text <> "" Then
            Try
                Dim dts As New vproveedor
                Dim func As New fproveedor






                If func.insertar(dts) Then
                    MessageBox.Show("Producto Registrado Correctamente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    'limpiar()
                Else
                    MessageBox.Show("Producto no fue registrado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    'limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub datalistado_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidproveedor.Text = datalistado.SelectedCells.Item(1).Value
        txtnombre_proveedor.Text = datalistado.SelectedCells.Item(2).Value
        txtcontacto_proveedor.Text = datalistado.SelectedCells.Item(3).Value


        txttelefono_proveedor.Text = datalistado.SelectedCells.Item(4).Value
        txtcelular_proveedor.Text = datalistado.SelectedCells.Item(5).Value
        txtemail_proveedor.Text = datalistado.SelectedCells.Item(6).Value


        txttipo_proveedor.Text = datalistado.SelectedCells.Item(7).Value
        txtruc_proveedor.Text = datalistado.SelectedCells.Item(8).Value



 

        btneditar.Visible = True
        btnguardar.Visible = False

    End Sub

    Private Sub datalistado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellContentClick
        If e.ColumnIndex = Me.datalistado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.datalistado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value

        End If
    End Sub

    Private Sub btneditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos de la categoria ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtnombre_proveedor.Text <> "" And txtidproveedor.Text <> "" Then
            Try
                Dim dts As New vproveedor
                Dim func As New fproveedor

                dts.gidproveedor = txtidproveedor.Text




                If func.editar(dts) Then
                    MessageBox.Show("Categoria Modificada Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    'limpiar()
                Else
                    MessageBox.Show("Categoria no fue Modificada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    'limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub cbeliminar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbeliminar.CheckedChanged
        If cbeliminar.CheckState = CheckState.Checked Then
            datalistado.Columns.Item("Eliminar").Visible = True
        Else
            datalistado.Columns.Item("Eliminar").Visible = False
        End If
    End Sub

    Private Sub btneliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneliminar.Click
        Dim result As DialogResult
        result = MessageBox.Show("Realmente desea eliminar los Productos seleccionados?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)


        If result = DialogResult.OK Then
            Try
                For Each row As DataGridViewRow In datalistado.Rows
                    Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                    If marcado Then
                        Dim onekey As Integer = Convert.ToInt32(row.Cells("idproveedor").Value)
                        Dim vdb As New vproveedor
                        Dim func As New fproveedor

                        vdb.gidproveedor = onekey
                        If func.eliminar(vdb) Then

                        Else
                            MessageBox.Show("Producto no fue eliminados?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                    End If
                Next

                Call mostrar()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call mostrar()
        End If
        Call limpiar()
    End Sub

    Private Sub cbocampo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbocampo.SelectedIndexChanged

    End Sub

    Private Sub txtbuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbuscar.TextChanged
        buscar()
    End Sub
End Class