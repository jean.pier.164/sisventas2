﻿Imports System.Data.SqlClient
Public Class fnproductos
    Inherits conexion
    Dim cmd As New SqlCommand
    Public Function mostrarnproducto() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("nproductos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@nproductos", frmdetalle_venta.cboproduct.Text)

            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                ' no conozco el nombre d elos campos ni de los controles
                frmdetalle_venta.txtidproducto.Text = dt.Rows(0).Item("idproducto").ToString
                frmdetalle_venta.txtnombre_producto.Text = dt.Rows(0).Item("nombre").ToString
                frmdetalle_venta.txtstock.Text = dt.Rows(0).Item("stock").ToString
                frmdetalle_venta.txtprecio_unitario.Text = dt.Rows(0).Item("precio_venta").ToString
                frmdetalle_venta.txtprecio_unitariom.Text = dt.Rows(0).Item("precio_ventam").ToString
                frmdetalle_venta.txtbuscar.Text = dt.Rows(0).Item("codigo").ToString

            End If
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function
    Public Function mcodigoproducto() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("nproductos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@nproductos", frmlistaprecios.cboproducto.Text)

            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                ' no conozco el nombre d elos campos ni de los controles
                frmlistaprecios.txtidproducto.Text = dt.Rows(0).Item("idproducto").ToString


            End If
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

    Public Function mostrarnproducto1() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("nproductos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@nproductos", frmdetalle_venta.cboproduct.Text)

            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                ' no conozco el nombre d elos campos ni de los controles
                frmdetalle_venta.txtidproducto.Text = dt.Rows(0).Item("idproducto").ToString
                frmdetalle_venta.txtnombre_producto.Text = dt.Rows(0).Item("nombre").ToString
                frmdetalle_venta.txtstock.Text = dt.Rows(0).Item("stock").ToString
                frmdetalle_venta.txtprecio_unitario.Text = dt.Rows(0).Item("precio_venta").ToString
                frmdetalle_venta.txtprecio_unitariom.Text = dt.Rows(0).Item("precio_ventam").ToString

            End If
            Return dt
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function



End Class
