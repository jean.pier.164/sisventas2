﻿Imports System.Data.SqlClient
Public Class fegresos
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar_egresosplog() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("egresos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@login", frmcierredecaja.cbologin.Text)
            cmd.Parameters.AddWithValue("@fechae", frmcierredecaja.txtfechai.Text)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function
End Class
