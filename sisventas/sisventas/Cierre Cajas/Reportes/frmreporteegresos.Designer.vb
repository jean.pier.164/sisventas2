﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreporteegresos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource6 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.datasetegresos = New sisventas.datasetegresos()
        Me.egresosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.egresosTableAdapter = New sisventas.datasetegresosTableAdapters.egresosTableAdapter()
        Me.txtfechae = New System.Windows.Forms.TextBox()
        Me.txtlogin = New System.Windows.Forms.TextBox()
        CType(Me.datasetegresos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.egresosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource6.Name = "cdatosegresos"
        ReportDataSource6.Value = Me.egresosBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource6)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "sisventas.rptegresos.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(889, 534)
        Me.ReportViewer1.TabIndex = 0
        '
        'datasetegresos
        '
        Me.datasetegresos.DataSetName = "datasetegresos"
        Me.datasetegresos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'egresosBindingSource
        '
        Me.egresosBindingSource.DataMember = "egresos"
        Me.egresosBindingSource.DataSource = Me.datasetegresos
        '
        'egresosTableAdapter
        '
        Me.egresosTableAdapter.ClearBeforeFill = True
        '
        'txtfechae
        '
        Me.txtfechae.Location = New System.Drawing.Point(394, 98)
        Me.txtfechae.Name = "txtfechae"
        Me.txtfechae.Size = New System.Drawing.Size(100, 20)
        Me.txtfechae.TabIndex = 1
        Me.txtfechae.Visible = False
        '
        'txtlogin
        '
        Me.txtlogin.Font = New System.Drawing.Font("Arial", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtlogin.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtlogin.Location = New System.Drawing.Point(211, 116)
        Me.txtlogin.Name = "txtlogin"
        Me.txtlogin.Size = New System.Drawing.Size(100, 21)
        Me.txtlogin.TabIndex = 2
        '
        'frmreporteegresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(889, 534)
        Me.Controls.Add(Me.txtlogin)
        Me.Controls.Add(Me.txtfechae)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmreporteegresos"
        Me.Text = "frmreporteegresos"
        CType(Me.datasetegresos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.egresosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents egresosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents datasetegresos As sisventas.datasetegresos
    Friend WithEvents egresosTableAdapter As sisventas.datasetegresosTableAdapters.egresosTableAdapter
    Friend WithEvents txtfechae As System.Windows.Forms.TextBox
    Friend WithEvents txtlogin As System.Windows.Forms.TextBox
End Class
