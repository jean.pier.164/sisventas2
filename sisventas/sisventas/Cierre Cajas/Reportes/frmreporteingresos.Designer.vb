﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreporteingresos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource5 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.datasetingresosp = New sisventas.datasetingresosp()
        Me.ingresosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ingresosTableAdapter = New sisventas.datasetingresospTableAdapters.ingresosTableAdapter()
        Me.txtlog = New System.Windows.Forms.TextBox()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        CType(Me.datasetingresosp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ingresosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource5.Name = "datasetingresos"
        ReportDataSource5.Value = Me.ingresosBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource5)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "sisventas.rptingresos.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(920, 518)
        Me.ReportViewer1.TabIndex = 0
        '
        'datasetingresosp
        '
        Me.datasetingresosp.DataSetName = "datasetingresosp"
        Me.datasetingresosp.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ingresosBindingSource
        '
        Me.ingresosBindingSource.DataMember = "ingresos"
        Me.ingresosBindingSource.DataSource = Me.datasetingresosp
        '
        'ingresosTableAdapter
        '
        Me.ingresosTableAdapter.ClearBeforeFill = True
        '
        'txtlog
        '
        Me.txtlog.Location = New System.Drawing.Point(449, 149)
        Me.txtlog.Name = "txtlog"
        Me.txtlog.Size = New System.Drawing.Size(100, 20)
        Me.txtlog.TabIndex = 1
        Me.txtlog.Visible = False
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(227, 127)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(100, 20)
        Me.txtfechai.TabIndex = 2
        Me.txtfechai.Visible = False
        '
        'frmreporteingresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(920, 518)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.txtlog)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmreporteingresos"
        Me.Text = "frmreporteingresos"
        CType(Me.datasetingresosp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ingresosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ingresosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents datasetingresosp As sisventas.datasetingresosp
    Friend WithEvents ingresosTableAdapter As sisventas.datasetingresospTableAdapters.ingresosTableAdapter
    Friend WithEvents txtlog As System.Windows.Forms.TextBox
    Friend WithEvents txtfechai As System.Windows.Forms.TextBox
End Class
