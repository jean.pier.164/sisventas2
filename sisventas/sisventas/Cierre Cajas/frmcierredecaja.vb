﻿Imports System.Data.SqlClient
Public Class frmcierredecaja


    Dim conexion As New SqlConnection("data source=(local);initial catalog=sisventas;integrated security=true")

    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from usuario", conexion)


    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable
    Private Sub frmcierredecaja_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        cbologin.Text = "TODOS"
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select login from usuario")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbologin.Items.Add(variable.Item(0))
        End While

        conexion.Close()
    End Sub




    Private Sub mostrar()
        Try
            Dim func As New fingresos
            dt = func.mostrar_ingresosplog



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True

            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub


    Private Sub mostrare()
        Try
            Dim func As New fegresos
            dt = func.mostrar_egresosplog



            If dt.Rows.Count <> 0 Then
                datalistadoe.DataSource = dt


                datalistadoe.ColumnHeadersVisible = True

            Else
                datalistadoe.DataSource = Nothing


                datalistadoe.ColumnHeadersVisible = False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click
        mostrar()
        mostrare()


        Try
            Dim query As IEnumerable(Of Object) = _
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)() _
                Where ( _
                    (row.Cells("Importe").Value IsNot Nothing) AndAlso _
                    (row.Cells("Importe").Value IsNot DBNull.Value)) Select row.Cells("Importe").Value()

            Dim resultado As Decimal = _
                query.Sum(Function(row) Convert.ToDecimal(row))
            txttotalingresos.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        Try
            Dim query As IEnumerable(Of Object) = _
                From row As DataGridViewRow In datalistadoe.Rows.Cast(Of DataGridViewRow)() _
                Where ( _
                    (row.Cells("Importe").Value IsNot Nothing) AndAlso _
                    (row.Cells("Importe").Value IsNot DBNull.Value)) Select row.Cells("Importe").Value()

            Dim resultado As Decimal = _
                query.Sum(Function(row) Convert.ToDecimal(row))
            txttotalegresos.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

        txtsaldocaja.Text = CDbl(txttotalingresos.Text) - CDbl(txttotalegresos.Text)

    End Sub

    Private Sub datalistadoe_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistadoe.CellContentClick

    End Sub

    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimir.Click
        frmreporteingresos.txtlog.Text = Me.cbologin.Text
        frmreporteingresos.txtfechai.Text = Me.txtfechai.Text
        frmreporteingresos.ShowDialog()


   
    End Sub

    Private Sub btnimprimire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimire.Click
        frmreporteegresos.txtlogin.Text = Me.cbologin.Text
        frmreporteegresos.txtfechae.Text = Me.txtfechai.Text
        frmreporteegresos.ShowDialog()
    End Sub
End Class