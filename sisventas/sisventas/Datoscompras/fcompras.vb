﻿Imports System.Data.SqlClient
Public Class fcompras
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar_compra() As DataTable

        Try
            conectado()
            cmd = New SqlCommand("mostrar_compra")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else
                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()
        End Try



    End Function


    Public Function insertar(ByVal dts As vcompras) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_compras")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@fecha_compra", frmcompras.txtfecha.Text)
            cmd.Parameters.AddWithValue("@tipo_documento", frmcompras.cbtipo_documento.Text)
            cmd.Parameters.AddWithValue("@serie_documento", frmcompras.txtserie_documento.Text)
            cmd.Parameters.AddWithValue("@num_documento", frmcompras.txtnum_documento.Text)


            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try
    End Function


    Public Function editar(ByVal dts As vcompras) As Boolean


        Try
            conectado()
            cmd = New SqlCommand("editar_compras")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idcompras", dts.gidcompras)
            cmd.Parameters.AddWithValue("@fecha_compra", dts.gfecha_compra)
            cmd.Parameters.AddWithValue("@tipo_documento", dts.gtipo_documento)
            cmd.Parameters.AddWithValue("@serie_documento", dts.gserie_documento)
            cmd.Parameters.AddWithValue("@num_documento", dts.gnum_documento)


            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try




    End Function

    Public Function eliminar(ByVal dts As vcompras) As Boolean

        Try
            conectado()
            cmd = New SqlCommand("eliminar_compras")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.Add("@idcompras", SqlDbType.NVarChar, 50).Value = dts.gidcompras

            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function




End Class
