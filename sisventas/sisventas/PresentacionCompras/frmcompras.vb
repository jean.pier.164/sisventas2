﻿Public Class frmcompras
    Private dt As New DataTable

    Private Sub frmcompras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar_compra()


        If frmlogin.txtlogin.Text = "steban" Then
            txtidusuario.Text = "1"
            txtusuario.Text = frmlogin.txtlogin.Text


        End If

        If frmlogin.txtlogin.Text = "YANET" Then
            txtidusuario.Text = "2"
            txtusuario.Text = frmlogin.txtlogin.Text


        End If



        If frmlogin.txtlogin.Text = "DORIS" Then
            txtidusuario.Text = "5"
            txtusuario.Text = frmlogin.txtlogin.Text


        End If


        If frmlogin.txtlogin.Text = "SANDRA" Then
            txtidusuario.Text = "6"
            txtusuario.Text = frmlogin.txtlogin.Text


        End If



        If frmlogin.txtlogin.Text = "rosa" Then
            txtidusuario.Text = "8"
            txtusuario.Text = frmlogin.txtlogin.Text


        End If






        If frmlogin.txtlogin.Text = "joaydy" Then
            txtidusuario.Text = "10"
            txtusuario.Text = frmlogin.txtlogin.Text


        End If

    End Sub


    Public Sub limpiar()
        btnguardar.Visible = True
        btneditar.Visible = False
        txtnum_documento.Text = ""
        txtidcompra.Text = ""

    End Sub

    Private Sub mostrar_compra()
        Try
            Dim func As New fcompras
            dt = func.mostrar_compra()



            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


        buscar()
    End Sub


    Private Sub buscar()
        Try

            Dim ds As New DataSet
            ds.Tables.Add(dt.Copy)
            Dim dv As New DataView(ds.Tables(0))
            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"



            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing
            End If



        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub ocultar_columnas()
        
    End Sub

    Private Sub btnnuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnuevo.Click
        limpiar()
        mostrar_compra()
    End Sub

    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtnum_documento.Text <> "" Then
            Try
                Dim dts As New vcompras
                Dim func As New fcompras

                dts.gfecha_compra = txtfecha.Text
                dts.gtipo_documento = cbtipo_documento.Text
                dts.gserie_documento = txtserie_documento.Text
                dts.gnum_documento = txtnum_documento.Text
                dts.gidusuario = txtidusuario.Text



                If func.insertar(dts) Then


                    MessageBox.Show("Venta Registrada Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar_compra()
                    limpiar()
                    cargar_detalle()
              
                Else
                    MessageBox.Show("Venta no fue registrada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar_compra()


                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub datalistado_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidcompra.Text = datalistado.SelectedCells.Item(1).Value
        txtfecha.Text = datalistado.SelectedCells.Item(2).Value
        cbtipo_documento.Text = datalistado.SelectedCells.Item(3).Value
        txtserie_documento.Text = datalistado.SelectedCells.Item(4).Value
        txtnum_documento.Text = datalistado.SelectedCells.Item(5).Value


        btneditar.Visible = True
        btnguardar.Visible = False
    End Sub

    Private Sub datalistado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellContentClick
        If e.ColumnIndex = Me.datalistado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.datalistado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value

        End If
    End Sub

    Private Sub btneditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos de la Venta ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtnum_documento.Text <> "" And txtidcompra.Text <> "" Then
            Try
                Dim dts As New vcompras
                Dim func As New fcompras

                dts.gidcompras = txtidcompra.Text
                dts.gfecha_compra = txtfecha.Text
                dts.gtipo_documento = cbtipo_documento.Text
                dts.gserie_documento = txtserie_documento.Text
                dts.gnum_documento = txtnum_documento.Text



                If func.editar(dts) Then
                    MessageBox.Show("Venta Modificada Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar_compra()
                    limpiar()
                Else
                    MessageBox.Show("Venta no fue Modificada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar_compra()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub cbeliminar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbeliminar.CheckedChanged
        If cbeliminar.CheckState = CheckState.Checked Then
            datalistado.Columns.Item("Eliminar").Visible = True
        Else
            datalistado.Columns.Item("Eliminar").Visible = False
        End If
    End Sub


    Private Sub cargar_detalle()

        frmdetalle_compra.txtidcompra.Text = datalistado.SelectedCells.Item(1).Value
        frmdetalle_compra.txtfecha.Text = datalistado.SelectedCells.Item(2).Value
        frmdetalle_compra.cbtipo_documento.Text = datalistado.SelectedCells.Item(3).Value
        frmdetalle_compra.txtserie_documento.Text = datalistado.SelectedCells.Item(4).Value
        frmdetalle_compra.txtnum_documento.Text = datalistado.SelectedCells.Item(5).Value


        frmdetalle_compra.ShowDialog()




    End Sub

    Private Sub bntcancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bntcancelar.Click
        Me.Close()
    End Sub

    Private Sub txtbuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbuscar.TextChanged

        buscar()

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub datalistado_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        cargar_detalle()
    End Sub
End Class