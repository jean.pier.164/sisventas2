﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRpermanente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.registro_permanenteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.svDRpermanente = New sisventas.svDRpermanente()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.registro_permanenteTableAdapter = New sisventas.svDRpermanenteTableAdapters.registro_permanenteTableAdapter()
        Me.txtnombre_producto = New System.Windows.Forms.TextBox()
        CType(Me.registro_permanenteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.svDRpermanente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'registro_permanenteBindingSource
        '
        Me.registro_permanenteBindingSource.DataMember = "registro_permanente"
        Me.registro_permanenteBindingSource.DataSource = Me.svDRpermanente
        '
        'svDRpermanente
        '
        Me.svDRpermanente.DataSetName = "svDRpermanente"
        Me.svDRpermanente.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "DsetRpermanente"
        ReportDataSource1.Value = Me.registro_permanenteBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "sisventas.rptRpermanente.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(1063, 476)
        Me.ReportViewer1.TabIndex = 0
        '
        'registro_permanenteTableAdapter
        '
        Me.registro_permanenteTableAdapter.ClearBeforeFill = True
        '
        'txtnombre_producto
        '
        Me.txtnombre_producto.Location = New System.Drawing.Point(151, 101)
        Me.txtnombre_producto.Name = "txtnombre_producto"
        Me.txtnombre_producto.Size = New System.Drawing.Size(100, 20)
        Me.txtnombre_producto.TabIndex = 1
        Me.txtnombre_producto.Visible = False
        '
        'frmRpermanente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1063, 476)
        Me.Controls.Add(Me.txtnombre_producto)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmRpermanente"
        Me.Text = "Reporte Sistema"
        CType(Me.registro_permanenteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.svDRpermanente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents registro_permanenteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents svDRpermanente As sisventas.svDRpermanente
    Friend WithEvents registro_permanenteTableAdapter As sisventas.svDRpermanenteTableAdapters.registro_permanenteTableAdapter
    Friend WithEvents txtnombre_producto As System.Windows.Forms.TextBox
End Class
