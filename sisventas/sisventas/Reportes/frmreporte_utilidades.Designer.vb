﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreporte_utilidades
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.utilidadesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sisventasDataSet1 = New sisventas.sisventasDataSet1()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.utilidadesTableAdapter = New sisventas.sisventasDataSet1TableAdapters.utilidadesTableAdapter()
        Me.txtutilidades = New System.Windows.Forms.TextBox()
        CType(Me.utilidadesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sisventasDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'utilidadesBindingSource
        '
        Me.utilidadesBindingSource.DataMember = "utilidades"
        Me.utilidadesBindingSource.DataSource = Me.sisventasDataSet1
        '
        'sisventasDataSet1
        '
        Me.sisventasDataSet1.DataSetName = "sisventasDataSet1"
        Me.sisventasDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "datasetutilidades"
        ReportDataSource1.Value = Me.utilidadesBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "sisventas.rptutilidades.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(905, 535)
        Me.ReportViewer1.TabIndex = 0
        '
        'utilidadesTableAdapter
        '
        Me.utilidadesTableAdapter.ClearBeforeFill = True
        '
        'txtutilidades
        '
        Me.txtutilidades.Location = New System.Drawing.Point(404, 281)
        Me.txtutilidades.Name = "txtutilidades"
        Me.txtutilidades.Size = New System.Drawing.Size(100, 20)
        Me.txtutilidades.TabIndex = 1
        Me.txtutilidades.Visible = False
        '
        'frmreporte_utilidades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(905, 535)
        Me.Controls.Add(Me.txtutilidades)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmreporte_utilidades"
        Me.Text = "Reporte Utilidades"
        CType(Me.utilidadesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sisventasDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents utilidadesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents sisventasDataSet1 As sisventas.sisventasDataSet1
    Friend WithEvents utilidadesTableAdapter As sisventas.sisventasDataSet1TableAdapters.utilidadesTableAdapter
    Friend WithEvents txtutilidades As System.Windows.Forms.TextBox
End Class
