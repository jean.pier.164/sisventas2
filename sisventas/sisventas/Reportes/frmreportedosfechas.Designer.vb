﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmreportedosfechas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.cdosfechasBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sventascdosfechas = New sisventas.sventascdosfechas()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.cdosfechasTableAdapter = New sisventas.sventascdosfechasTableAdapters.cdosfechasTableAdapter()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.TextBox()
        CType(Me.cdosfechasBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sventascdosfechas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cdosfechasBindingSource
        '
        Me.cdosfechasBindingSource.DataMember = "cdosfechas"
        Me.cdosfechasBindingSource.DataSource = Me.sventascdosfechas
        '
        'sventascdosfechas
        '
        Me.sventascdosfechas.DataSetName = "sventascdosfechas"
        Me.sventascdosfechas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "dsetcdosfechas"
        ReportDataSource1.Value = Me.cdosfechasBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "sisventas.rptcdosfechas.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(964, 609)
        Me.ReportViewer1.TabIndex = 0
        '
        'cdosfechasTableAdapter
        '
        Me.cdosfechasTableAdapter.ClearBeforeFill = True
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(180, 141)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(100, 20)
        Me.txtfechai.TabIndex = 1
        Me.txtfechai.Visible = False
        '
        'txtfechafi
        '
        Me.txtfechafi.Location = New System.Drawing.Point(311, 141)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(100, 20)
        Me.txtfechafi.TabIndex = 2
        Me.txtfechafi.Visible = False
        '
        'frmreportedosfechas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(964, 609)
        Me.Controls.Add(Me.txtfechafi)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmreportedosfechas"
        Me.Text = "Reporte entre Fechas"
        CType(Me.cdosfechasBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sventascdosfechas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents cdosfechasBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents sventascdosfechas As sisventas.sventascdosfechas
    Friend WithEvents cdosfechasTableAdapter As sisventas.sventascdosfechasTableAdapters.cdosfechasTableAdapter
    Friend WithEvents txtfechai As System.Windows.Forms.TextBox
    Friend WithEvents txtfechafi As System.Windows.Forms.TextBox
End Class
