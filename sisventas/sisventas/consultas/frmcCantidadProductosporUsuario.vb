﻿Imports System.Data.SqlClient
Public Class frmcCantidadProductosporUsuario
    Dim conexion As New SqlConnection("data source=(local);initial catalog=sisventas;integrated security=true")

    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from usuario", conexion)


    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()
        cbologin.Text = "TODOS"
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select login from usuario")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbologin.Items.Add(variable.Item(0))
        End While

        conexion.Close()

    End Sub

    Private Sub mostrar()
        Try
            Dim func As New fcventas
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub mostrar_cantprodvendporusuario()
        Try
            Dim func As New fcantprodvendporusuario
            dt = func.mostrar_cantprodvendporusuario()



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub


    Private Sub mostrar_ventasporfechas()
        Try
            Dim func As New fmostrarventasporfechas
            dt = func.mostrar_ventasporfechas()



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub


    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click
        If cbologin.Text = "TODOS" Then
            MessageBox.Show("TODOS")
            mostrar_ventasporfechas()

        Else
            mostrar_cantprodvendporusuario()
        End If


        Try
            Dim query As IEnumerable(Of Object) = _
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)() _
                Where ( _
                    (row.Cells("Cantidades").Value IsNot Nothing) AndAlso _
                    (row.Cells("Cantidades").Value IsNot DBNull.Value)) Select row.Cells("Cantidades").Value()

            Dim resultado As Decimal = _
                query.Sum(Function(row) Convert.ToDecimal(row))
            txtscantidad.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try










        Try
            Dim query As IEnumerable(Of Object) = _
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)() _
                Where ( _
                    (row.Cells("SubTotalCompra").Value IsNot Nothing) AndAlso _
                    (row.Cells("SubTotalCompra").Value IsNot DBNull.Value)) Select row.Cells("SubTotalCompra").Value()

            Dim resultado As Decimal = _
                query.Sum(Function(row) Convert.ToDecimal(row))
            txtsscompra.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try



        Try
            Dim query As IEnumerable(Of Object) = _
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)() _
                Where ( _
                    (row.Cells("SubTotalVenta").Value IsNot Nothing) AndAlso _
                    (row.Cells("SubTotalVenta").Value IsNot DBNull.Value)) Select row.Cells("SubTotalVenta").Value()

            Dim resultado As Decimal = _
                query.Sum(Function(row) Convert.ToDecimal(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try


        txtutilidadpusuario.Text = CDbl(txtssventa.Text) - CDbl(txtsscompra.Text)


    End Sub


    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimir.Click
        frmreportedosfechas.txtfechai.Text = Me.txtfechai.Text
        frmreportedosfechas.txtfechafi.Text = Me.txtfechafi.Text
        frmreportedosfechas.ShowDialog()
    End Sub
End Class