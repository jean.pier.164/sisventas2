﻿Imports System.Data.SqlClient
Public Class frmcPermanenteValorizado

    Dim conexion As New SqlConnection("data source=(local);initial catalog=sisventas;integrated security=true")

    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from producto", conexion)


    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable



    Private Sub frmcProductos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select nombre from producto")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbonombre_producto.Items.Add(variable.Item(0))
        End While

        conexion.Close()

    End Sub



    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimir.Click
        frmRpermanente.txtnombre_producto.Text = Me.cbonombre_producto.Text

        frmRpermanente.ShowDialog()

    End Sub



    Private Sub mostrar_RegistroPermanente()
        Try
            Dim func As New fRegistroPermanente
            dt = func.mostrar_RegistroPermanente()



            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click
        mostrar_RegistroPermanente()
    End Sub
End Class