﻿Imports System.Data.SqlClient
Public Class flaboratorio
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_laboratorio")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function


    Public Function insertar(ByVal dts As vlaboratorio) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_laboratorio")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@nombre_laboratorio", dts.gnombre_laboratorio)



            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function
    Public Function editar(ByVal dts As vlaboratorio) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_laboratorio")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idlaboratorio", dts.gidlaboratorio)
            cmd.Parameters.AddWithValue("@nombre_laboratorio", dts.gnombre_laboratorio)

            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function eliminar(ByVal dts As vlaboratorio) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("eliminar_laboratorio")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.Add("@idlaboratorio", SqlDbType.NVarChar, 50).Value = dts.gidlaboratorio

            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        End Try
    End Function
End Class
