﻿Imports System.Data.SqlClient

Public Class fproductosmvendidos
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar_productosmvendidos() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("pmasvendidos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@nombre", frmcproductosmvendidos.cbonombre_producto.Text)
            cmd.Parameters.AddWithValue("@fechai", frmcproductosmvendidos.txtfechai.Text)
            cmd.Parameters.AddWithValue("@fechafi", frmcproductosmvendidos.txtfechafi.Text)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function

End Class
